<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sku_Model
 *
 * @author mwamb
 */
class Sku_Model extends CI_Model{
    //put your code here
    public $sku="skus";
    
    public function add($data){
        $response=array();
        $counter= $this->checkNew($data);
        if($counter==0){
            $this->db->insert($this->sku,$data);
            $response['message']='Sku set successfully.';
            $response['code'] = 1;
        }
        else{
            $response['message']='Similar sku already exist';
            $response['code'] = 0;
        }
        return json_encode($response);
    }
    public function checkNew($data){
        $this->db->select('sku_id');
        $this->db->where('sku_name',$data['sku_name']);
        $this->db->where('weight',$data['weight']);
        $this->db->from($this->sku);
        $counter= $this->db->count_all_results();
        return $counter;
    }
    
    public function update($data){
        $response=array();
        $counter= $this->checkUpdate($data);
        if($counter==0){
            $sku_id= $data['sku_id'];
            unset($data['sku_id']);
            $this->db->where('sku_id',$sku_id);
            $this->db->update($this->sku,$data);
            $response['message']="Sku updated successfully.";
            $response['code']=1;
        }
        else{
         $response['message']="Failed to update SKU, Similar record exist.";
         $response['code']=0;   
        }
        
        return json_encode($response);
    }
    public function checkUpdate($data){
        $this->db->select('sku_id');
        $this->db->group_start();
        $this->db->where('sku_name',$data['sku_name']);
        $this->db->where('weight',$data['weight']);
        $this->db->group_end();
        $this->db->where('sku_id!=',$data['sku_id']);
        $this->db->from($this->sku);
        $counter=$this->db->count_all_results();
        return $counter;
    }
    public function fetch($sku_data){
        $this->db->select('sku_id,sku_name,sku_type,weight,is_active');
        $this->db->where('sku_id',$sku_data['sku_id']);
        $this->db->from($this->sku);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return json_encode($data);
    }
    public function fetch_all(){
        $this->db->select('sku_id,sku_name,sku_type,weight,is_active');
        $this->db->from($this->sku);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return json_encode($data);
    }
    public function status($data){
        $sku_id=$data['sku_id'];
        unset($data['sku_id']);
        $this->db->where('sku_id',$sku_id);
        $this->db->update($this->sku,$data);
        $response=array();
        $response['message']="Status toggled successfully.";
        $response['code']=1;
        return json_encode($response);
    }
}
