<!-- Main navigation --><!-- Main navigation -->
<script type="text/javascript">
     if(localStorage.getItem('user_type')==5)
    {
        window.location = "genman/dashboard.php";
    }
    else if(localStorage.getItem('user_type')==8)
    {
        window.location = "../dcclerks/dcretailers.php";
    }
      else if(localStorage.getItem('user_type')==4)
    {
       window.location = "../cc/orders.php";
   }
</script>
<?php
/*
$arry_uri=explode('/', $_SERVER['REQUEST_URI']);
$ordersactive=false;
$analyticsactive=false;
$usersactive=false;
$dashboardactive=false;
$inventoryactive =false;
$deliveryactive=false;
if($arry_uri['2']=="orders.php" || $arry_uri['2']=="orders-packed.php" || $arry_uri['2']=="orders-ontransit.php" || $arry_uri['2']=="orders-received.php")
{
$ordersactive=true;
$analyticsactive=false;
$usersactive=false;
$dashboardactive=false;
$inventoryactive =false;
$deliveryactive=false;
}
else if($arry_uri['2']=="dashboard.php" || $arry_uri['2']=="generalanalytics.php" || $arry_uri['2']=="dcanalytics.php" || $arry_uri['2']=="brandanalytics.php"||$arry_uri['2']=="toselling.php"||$arry_uri['2']=="monthlyanalytics.php")
{
$ordersactive=false;
$analyticsactive=true;
$usersactive=false;
$dashboardactive=false;
$inventoryactive =false;
$deliveryactive=false;
}
else if($arry_uri['2']=="users.php" || $arry_uri['2']=="customercare.php" || $arry_uri['2']=="directorslist.php" || $arry_uri['2']=="genmanlist.php"||$arry_uri['2']=="agents.php"||$arry_uri['2']=="clerks.php")
{
$ordersactive=false;
$analyticsactive=false;
$usersactive=true;
$dashboardactive=false;
$inventoryactive =false;
$deliveryactive=false;
}
else if($arry_uri['2']=="dash-today.php" || $arry_uri['2']=="todaymargins.php" || $arry_uri['2']=="dashboardsales.php" )
{
$ordersactive=false;
$analyticsactive=false;
$usersactive=false;
$dashboardactive=true;
$inventoryactive =false;
$deliveryactive=false;
}
else if($arry_uri['2']=="stock.php" || $arry_uri['2']=="distribution_centers_stock.php")
{
$ordersactive=false;
$analyticsactive=false;
$usersactive=false;
$dashboardactive=false;
$inventoryactive =true;
$deliveryactive=false;
}
else if($arry_uri['2']=="delivery-zone.php" || $arry_uri['2']=="delivery-modes.php" || $arry_uri['2']=="delivery-zones.php" || $arry_uri['2']=="delivery-logistics-settings.php" || $arry_uri['2']=="conversions.php" || $arry_uri['2']=="delivery-vehicles.php")
{
$ordersactive=false;
$analyticsactive=false;
$usersactive=false;
$dashboardactive=false;
$inventoryactive =false;
$deliveryactive=true;
}

*/
?>
<?php

$arry_uri=explode('/', $_SERVER['REQUEST_URI']);
$ordersactive=false;
$analyticsactive=false;
$usersactive=false;
$dashboardactive=false;
$inventoryactive =false;
$deliveryactive=false;
if($arry_uri['3']=="orders.php" || $arry_uri['3']=="orders-packed.php" || $arry_uri['3']=="orders-ontransit.php" || $arry_uri['3']=="orders-received.php")
{
$ordersactive=true;
$analyticsactive=false;
$usersactive=false;
$dashboardactive=false;
$inventoryactive =false;
$deliveryactive=false;
}
else if($arry_uri['3']=="analytics.php" || $arry_uri['3']=="analyticsretailers.php" || $arry_uri['3']=="analyticssales.php" || $arry_uri['3']=="brandanalytics.php"||$arry_uri['3']=="toselling.php"||$arry_uri['3']=="monthlyanalytics.php")
{
$ordersactive=false;
$analyticsactive=true;
$usersactive=false;
$dashboardactive=false;
$inventoryactive =false;
$deliveryactive=false;
}
else if($arry_uri['3']=="users.php" || $arry_uri['3']=="customercare.php" || $arry_uri['3']=="directorslist.php" || $arry_uri['3']=="genmanlist.php"||$arry_uri['3']=="agents.php"||$arry_uri['3']=="clerks.php")
{
$ordersactive=false;
$analyticsactive=false;
$usersactive=true;
$dashboardactive=false;
$inventoryactive =false;
$deliveryactive=false;
}
else if($arry_uri['3']=="dash-today.php" || $arry_uri['3']=="todaymargins.php" || $arry_uri['3']=="dashboardsales.php" )
{
$ordersactive=false;
$analyticsactive=false;
$usersactive=false;
$dashboardactive=true;
$inventoryactive =false;
$deliveryactive=false;
}
else if($arry_uri['3']=="stock.php" || $arry_uri['3']=="distribution_centers_stock.php")
{
$ordersactive=false;
$analyticsactive=false;
$usersactive=false;
$dashboardactive=false;
$inventoryactive =true;
$deliveryactive=false;
}
else if($arry_uri['3']=="delivery-zone.php" || $arry_uri['3']=="delivery-modes.php" || $arry_uri['3']=="delivery-zones.php" || $arry_uri['3']=="delivery-logistics-settings.php" || $arry_uri['3']=="conversions.php" || $arry_uri['3']=="delivery-vehicles.php")
{
$ordersactive=false;
$analyticsactive=false;
$usersactive=false;
$dashboardactive=false;
$inventoryactive =false;
$deliveryactive=true;
}

?>
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
            <!-- Main -->
            <li class="navigation-header"><span>Navigation</span> <i class="icon-menu" title="Main pages"></i></li>
            <li class="<?php echo ($dashboardactive ? 'active' : '') ?>">
                <a href="#"><i class="fa fa-home"></i> <span>Dashboard</span></a>
                <ul>
                    <li><a href="dash-today.php"><i class="far fa-circle"></i>Daily sales</a></li>
                    <li><a href="todaymargins.php"><i class="far fa-circle"></i>Daily Margins</a></li>
                    <li><a href="agentsperformance.php"><i class="far fa-circle"></i>Agents Performances</a></li>
                    <li><a href="dashboardsales.php"><i class="far fa-circle"></i>Daily Dashboard</a></li>
                    <!-- <li><a href="activetransactions.php">Filtered Sales</a></li> -->
                </ul>
            </li>
             <li class="<?php echo ($ordersactive ? 'active' : '') ?>">
                <a href="#"><i class="fa fa-shopping-cart"></i> <span>Orders</span></a>
                <ul>
                    <li><a href="orders.php"><i class="far fa-circle"></i>Ordered</a></li>
                    <li><a href="orders-packed.php"><i class="far fa-circle"></i>Packed</a></li>
                    <!--<li><a href="orders-not-assigned-transport.php">Orders not assigned for transport</a></li>-->
                    <li><a href="orders-ontransit.php"><i class="far fa-circle"></i>On Transit</a></li>
                    <li><a href="orders-received.php"><i class="far fa-circle"></i>Received</a></li>
                    <!--<li><a href="orders-backorder.php">Back order</a></li>-->
                </ul>
            </li>
			<li class="<?php echo ($usersactive ? 'active' : '') ?>">
                <a href="#"><i class="fa fa-user-cog"></i> <span>User Manager</span></a>
                <ul>
                  <li><a href="users.php"><i class="far fa-circle"></i>All Users</a></li>
                  <li><a href="clerks.php"><i class="far fa-circle"></i>Clerks</a></li>
                  <li id="supplier"><a href="retailers.php"><i class="far fa-circle"></i>Retailers</a></li>
                </ul>
            </li>
            <li class="<?php echo ($inventoryactive ? 'active' : '') ?>">
                <a href="#"><i class="fas fa-chart-line" aria-hidden="true"></i> <span>Inventory Control</span></a>
                <ul>
                    <li><a href="stock.php"><i class="far fa-circle"></i>General stock levels</a></li>
                    <li><a href="distribution_centers_stock.php"><i class="far fa-circle"></i>Manage Dc stocks</a></li>
                </ul>
            </li>
            <li class="<?php echo ($deliveryactive ? 'active' : '') ?>">
                <a href="#"><i class="fas fa-shipping-fast fa-lg"></i> <span>Delivery</span></a>
                <ul>
                    <li><a href="delivery-zone.php"><i class="far fa-circle"></i>Zones</a></li>
                    <li><a href="delivery-modes.php"><i class="far fa-circle"></i>Delivery modes</a></li>
                    <li><a href="delivery-zones.php"><i class="far fa-circle"></i>Delivery zones</a></li>            
                    <li><a href="delivery-logistics-settings.php"><i class="far fa-circle"></i>Delivery logistic settings</a></li>
                    <li><a href="conversions.php"><i class="far fa-circle"></i>Logistic Conversions</a></li>
                   <li><a href="delivery-vehicles.php"><i class="far fa-circle"></i>Delivery vehicles</a></li>
                </ul>
            </li>
            <!-- <li><a href="promotions.php"><i class="glyphicon glyphicon-tag fa-lg"></i> <span>Promotions</span></a></li> -->
            <li><a href=""><i class="fas fa-cog"></i><span>System Configurations</span></a>
                <ul>
                    <li id="category"><a href="category.php"><i class="far fa-circle"></i>Categories</a></li>
                    <li id="supplier"><a href="suppliers.php"><i class="far fa-circle"></i> <span>Dc Management</span></a></li>
                    <li id="dp_suppliers"><a href="dp-suppliers.php"><i class="far fa-circle"></i>DukaPepe Suppliers</a></li>
                    <li><a href="products.php"><i class="far fa-circle"></i>Products</a></li>
                    <li id="subcategory"><a href="subcategory.php"><i class="far fa-circle"></i>Sub Categories</a></li>
                    <li id="sku"><a href="sku.php"><i class="far fa-circle"></i>SKU</a></li>
                </ul>
            </li>
            
        </ul>
    </div>
</div>
<!-- /main navigation -->
