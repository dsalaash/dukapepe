<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Duka pepe | DC Sales <?php echo date("Y-m-d"); ?></title>
<?php include("../links.php") ?>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet">
    <!-- /theme JS files -->
    <script>
      if(localStorage.getItem('access_token')=="")
          {
           window.location = "http://localhost/dukapepe_portal/dukapepe/";
          }
       
        $(document).ready(function () {
          $(".status-progress").show();
          $('.js-example-basic-multiple').select2();
$( "#datepickerstart" ).datepicker( { dateFormat: 'yy-mm-dd' } );
                      $( "#datepickerend" ).datepicker( { dateFormat: 'yy-mm-dd' } );
                      $( "#datepicker" ).change(function() {});
           $("#formsearchdaysales").on("submit", function(e) 
           {
                document.getElementById('today_total_sales').innerHTML="";
                document.getElementById('userstablebody').innerHTML="";
                var formData = {'access_token': localStorage.getItem('access_token'),'phone':localStorage.getItem('phone'),'retailer':localStorage.getItem('retailer_id'),'startdate':$('#datepickerstart').val(),'enddate':$('#datepickerend').val()};
                // console.log(formData);
                var url = base_url + "analytics/dashboard";
                // console.log(url);
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         // console.log(data);
                        var parse = JSON.parse(data);
                       
                        var user_row="<table class='table table-responsive table-bordered' id='example' style='margin-bottom: 150px; max-width:700px; overflow:scroll;'><thead> <tr><th><b>Date</b></th>";   
                          
                          
                        $.each(parse['dates'], function(k, v) {
                         user_row+="<th >"+v+"</th>";   
                               
                            });  
               
                        user_row+="</tr></thead>";
                        user_row+="<tbody><tr><td><b>DC</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td><b>UMOJA DC</b></td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Sales</b></td>";
                        $.each(parse['dates'], function(k, v) {
                          // Create our number formatter.
                        var formatter = new Intl.NumberFormat('en-US', {
                          style: 'currency',
                          currency: 'Ksh',
                          minimumFractionDigits: 2,
                          // the default value for minimumFractionDigits depends on the currency
                          // and is usually already 2
                        });

                        var umo=formatter.format(parse['umosales'+v]); /* $2,500.00 */

                         user_row+="<td >"+ (Math.round(parseFloat(parse['umosales'+v]))).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";

                        user_row+="<tr><td><b>Retailers</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td>"+parse['umoretailers'+v]+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>New Retailers</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td>"+parse['umoretailersnew'+v]+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Average Per Retailer</b></td>";

                         $.each(parse['dates'], function(k, v) {

                         user_row+="<td>"+(Math.round(parseFloat(parseFloat(parse['umosales'+v])/parseFloat(parse['umoretailers'+v])))).toLocaleString()+"</td>";   
                           });
   //type of orders order
                        user_row+="<tr><td><b>Agent Orders</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td>"+parse['umotoo_agent'+v]+"</td>";
                           });
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Direct Retailer Orders</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td>"+parse['umotoo_direct'+v]+"</td>";
                           });
                        user_row+="</tr>";
                        //end of type of orders 
                        user_row+="</tr>";
                         user_row+="<tr><td><b>Cost Of Sales</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td>"+(Math.round(parseFloat(parse['umocsales'+v]))).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Trading Margin(value)</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td>"+(Math.round((parseFloat(parse['umosales'+v])-parseFloat(parse['umocsales'+v])))).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Trading Margin(%)</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td>"+(((parseFloat(parse['umosales'+v])-parseFloat(parse['umocsales'+v]))/parseFloat(parse['umosales'+v])*100)).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";
                         user_row+="<tr><td><b>Margins(MTD)</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td>"+(Math.round((parseFloat(parse['umomtdsales'+v])-parseFloat(parse['umocmtdsales'+v])))).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Sales(MTD)</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td>"+(Math.round(parseFloat(parse['umomtdsales'+v]))).toLocaleString()+"</td>";   
                           }); 
                         user_row+="<tr><td><b>Opening Stock</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td>"+(parse['umoclosingstock'+v]).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";





                        // fedha DashBoard
                        "<tr></tr>"
                       user_row+="<tr><td><b>DC</b></td>";
                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td><b>FEDHA DC</b></td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Sales</b></td>";
                        $.each(parse['dates'], function(k, v) {
                          // Create our number formatter.
                        var formatter = new Intl.NumberFormat('en-US', {
                          style: 'currency',
                          currency: 'Ksh',
                          minimumFractionDigits: 2,
                          // the default value for minimumFractionDigits depends on the currency
                          // and is usually already 2
                        });
                        var fedha=formatter.format(parse['fedhasales'+v]); /* $2,500.00 */
                         user_row+="<td>"+(Math.round(parseFloat(parse['fedhasales'+v]))).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";

                        user_row+="<tr><td><b>Retailers</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+parse['fedharetailers'+v]+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>New Retailers</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+parse['fedharetailersnew'+v]+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Average Per Retailer</b></td>";

                         $.each(parse['dates'], function(k, v) {

                         user_row+="<td >"+(Math.round(parseFloat(parseFloat(parse['fedhasales'+v])/parseFloat(parse['fedharetailers'+v])))).toLocaleString()+"</td>";   
                           });
   //type of orders order
                        user_row+="<tr><td><b>Agent Orders</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+parse['fedhatoo_agent'+v]+"</td>";
                           });
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Direct Retailer Orders</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+parse['fedhatoo_direct'+v]+"</td>";
                           });
                        user_row+="</tr>";
                        //end of type of orders 
                        user_row+="</tr>";
                         user_row+="<tr><td><b>Cost Of Sales</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+(Math.round(parseFloat(parse['fedhacsales'+v]))).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Trading Margin(value)</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+(Math.round((parseFloat(parse['fedhasales'+v])-parseFloat(parse['fedhacsales'+v])))).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Trading Margin(%)</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+(((parseFloat(parse['fedhasales'+v])-parseFloat(parse['fedhacsales'+v]))/parseFloat(parse['fedhasales'+v])*100)).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";
                         user_row+="<tr><td><b>Margins(MTD)</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+(Math.round((parseFloat(parse['fedhamtdsales'+v])-parseFloat(parse['fedhacmtdsales'+v])))).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Sales(MTD)</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+(Math.round(parseFloat(parse['fedhamtdsales'+v]))).toLocaleString()+"</td>";   
                           }); 
                         user_row+="<tr><td><b>Opening Stock</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+(parse['fedhaclosingstock'+v]).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";

                        // end of fedha dashboard
                        //doni dashboard

                        "<tr></tr>"
                       user_row+="<tr><td><b>DC</b></td>";
                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td><b>DONHOLM DC</b></td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Sales</b></td>";
                        $.each(parse['dates'], function(k, v) {
                          // Create our number formatter.
                        var formatter = new Intl.NumberFormat('en-US', {
                          style: 'currency',
                          currency: 'Ksh',
                          minimumFractionDigits: 2,
                          // the default value for minimumFractionDigits depends on the currency
                          // and is usually already 2
                        });
                        var fedha=formatter.format(parse['donisales'+v]); /* $2,500.00 */
                         user_row+="<td>"+(Math.round(parseFloat(parse['donisales'+v]))).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";

                        user_row+="<tr><td><b>Retailers</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+parse['doniretailers'+v]+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>New Retailers</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+parse['doniretailersnew'+v]+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Average Per Retailer</b></td>";

                         $.each(parse['dates'], function(k, v) {

                         user_row+="<td >"+(Math.round(parseFloat(parseFloat(parse['donisales'+v])/parseFloat(parse['doniretailers'+v])))).toLocaleString()+"</td>";   
                           });
   //type of orders order
                        user_row+="<tr><td><b>Agent Orders</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+parse['donitoo_agent'+v]+"</td>";
                           });
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Direct Retailer Orders</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+parse['donitoo_direct'+v]+"</td>";
                           });
                        user_row+="</tr>";
                        //end of type of orders 
                        user_row+="</tr>";
                         user_row+="<tr><td><b>Cost Of Sales</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+(Math.round(parseFloat(parse['donicsales'+v]))).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Trading Margin(value)</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+(Math.round((parseFloat(parse['donisales'+v])-parseFloat(parse['donicsales'+v])))).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Trading Margin(%)</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+(((parseFloat(parse['donisales'+v])-parseFloat(parse['donicsales'+v]))/parseFloat(parse['donisales'+v])*100)).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";
                         user_row+="<tr><td><b>Margins(MTD)</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+(Math.round((parseFloat(parse['donimtdsales'+v])-parseFloat(parse['donicmtdsales'+v])))).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";
                        user_row+="<tr><td><b>Sales(MTD)</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+(Math.round(parseFloat(parse['donimtdsales'+v]))).toLocaleString()+"</td>";   
                           }); 
                         user_row+="<tr><td><b>Opening Stock</b></td>";

                         $.each(parse['dates'], function(k, v) {
                         user_row+="<td >"+(parse['doniclosingstock'+v]).toLocaleString()+"</td>";   
                           }); 
                        user_row+="</tr>";

                        //end dashboard


                        user_row+="</tbody>";
                        user_row+="</table>"; 
                           
                           // $('#today_total_sales').append(total_sales); 
                            document.getElementById("userstablebody").innerHTML="";
                         
                            $("#userstablebody").append(user_row);
                          
                        $('#example').DataTable( {
                            "ordering": false,
                            dom: 'Bfrtip',
                            buttons: [
                                'copy', 'csv', 'excel', 'pdf', 'print'
                            ]
                        } );
                        
                     $(".status-progress").hide();

                                        
                                },
                       error:function(data){

                                  // console.log(data);
                       }        

        });
           });
 
           
            });
                function linktoprimary(id)
                {
                  
                var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id};
                // console.log(formData);
                var url = base_url + "user/allprimariesfromdc";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var user_row="";
                          $.each(JSON.parse(data), function(k, v) {
                                  user_row+="<option value='"+v.retailer_id+"'>"+v.retailer_name+"("+v.phone+")"+"</option>";
                               
                            });  
                           user_row+="";
                           document.getElementById('primaries').innerHTML="";
                           $("#primaries").append(user_row);
                           $("#agent").val(id);
                           $("#modallinks").modal();

                                        
                                },
                             error:function(data){

                                 new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

                   });
                }
                function viewtransactions(agent)
                {
                   var formData = {'access_token': localStorage.getItem('access_token'),'user_id':agent,'user_type_id':6};
                   var url = base_url + "user/agenttransactions";
                   $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                          var user_row="";
                          var pno=0;
                          $.each(JSON.parse(data), function(k, v) {
                            pno=pno+1;
                                  user_row+="<tr><td>"+pno+"</td><td>"+v.retailer_name+"</td><td>"+v.phone+"</td><td>"+v.dc+"</td></tr>";
                               
                            });  
                           user_row+="";
                           document.getElementById('agenttransactions').innerHTML="";
                           $("#agenttransactions").append(user_row);
                           $("#modaltransactions").modal();
                           $("#example3").DataTable();
                            },
                          error:function(data){
                               new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

        });

                }
                  function transferprimaries(id)
                
                {
                  
                var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id};
                // console.log(formData);
                var url = base_url + "user/allprimarieslinkedtoagent";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var json= JSON.parse(data);

                        
                  // console.log(json['fromname']);
                       var user_row ="";
                          $.each(json['primaries'], function(k, v) {
                                  user_row+="<option value='"+v.retailer_id+"'  selected>"+v.retailer_name+"("+v.phone+")"+"</option>";
                                  
                               
                            });  
                          
                           document.getElementById('primariestotransfer').innerHTML="";
                           $("#primariestotransfer").append(user_row);
                           $("#primariestotransfer").select2();
                          

                           var user_row1 ="";
                          $.each(json['to'], function(k, v) {
                                  user_row1+="<option value='"+v.retailer_id+"'>"+v.retailer_name+"("+v.phone+")"+"</option>";
                                  
                               
                            });  
                          
                           document.getElementById('to').innerHTML="";
                           $("#to").append(user_row1);
                           $("#primariestotransfer").select2();
                           $("#modallinkstransfer").modal();

                           $("#fromname").val(json['fromname']);
                           $("#fromid").val(json['fromid']);

                                        
                                },
                             error:function(data){

                                 new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

                   });
                }
                function unlinkprimary(id)
                {
                    var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':7};
                     var url = base_url + "user/assign_user_type";
                     $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         new PNotify({
                                    text: data,
                                    addclass: 'bg-success'
                                });
                                        
                                },
                       error:function(data){

                                  // console.log(data);
                       }        

                        });
                }
                 function viewagentsales(id)
                {
                  var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':6};
                var url = base_url + "user/assign_user_type";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var msg=   JSON.parse(data);
                         new PNotify({
                                    text: msg['message'],
                                    addclass: 'bg-success'
                                });
                                        
                                },
                       error:function(data){
                               new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

        });
                }
                 function viewagentprimaries(id)
                {
                  var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':6};
                var url = base_url + "user/linkedprimaries";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                          var user_row="";
                          var pno=0;
                          $.each(JSON.parse(data), function(k, v) {
                            pno=pno+1;
                                  user_row+="<tr><td>"+pno+"</td><td>"+v.retailer_name+"</td><td>"+v.phone+"</td><td>"+v.dc+"</td></tr>";
                               
                            });  
                           user_row+="";
                           document.getElementById('primariestable').innerHTML="";
                           $("#primariestable").append(user_row);
                            $("#modalprimaries").modal();
                            $("#example2").DataTable();
                        
                                        
                                },
                       error:function(data){
                               new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

        });
                }
               
               
                
                
               

                        </script>

                    </head>

                    <body>

                    <?php include("../topbar.php") ?>


                    <!-- Page container -->
                    <div class="page-container">
                       <!-- Vertical form modal -->
                        <div id="modalprimaries" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Sales</h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="display nowrap" style="width:100%" id="example2" >
                                    <thead>
                                    <tr>

                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                          <th>Distribution Center</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody id="primariestable">
                                        
                                    </tbody>

                                </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->
                        <!-- Vertical form modal -->
                        <div id="modaltransactions" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title" id="agentnametransaction">Agent Transactions</h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="display nowrap" style="width:100%" id="example3">
                        <thead>
                        <tr>
                            <th>#Order Id</th>
                            <th>Retailer</th>
                            <th>Phone</th>
                            <th>DC</th>
                            <th>Distribution Center</th>
                        </tr>
                        </thead>
                        <tbody id="agentnametransactions">
                            
                        </tbody>

                    </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->
  <!-- Vertical form modal -->
                        <div id="modallinks" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Primary Customers</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formlinkprimaaries" enctype="multipart/form-data">
                                        <input type="hidden" name="agent" id="agent" value="" />
                                        <input type="hidden" name="access_token" id="catToken"/>
                                        <div class="modal-body">
                                            <div class="form-group">

               <select class="js-example-basic-multiple" id="primaries" name="primaries[]" multiple="multiple">
  
               </select>                            

                                            </div>
                                           
                                            

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Submit Links</button>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->
                         <!-- Vertical form modal -->
                        <div id="modallinkstransfer" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Primary Customers' Transfer</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formtransferprimaries" enctype="multipart/form-data">
                                        <input type="hidden" name="agent" id="agent" value="" />
                                        <input type="hidden" name="access_token" id="catToken"/>
                                        <div class="modal-body">
                                          <div class="form-group">
                                            <label>From:</label>
                                                 <input type="text" id="fromname" value="" class="form-control">
                                                 <input type="text" id="fromid" value="" class="form-control">
                                            </div>

                                          <div class="form-group">
                                            <label>To:</label>
                                                 <select class="js-example-basic-multiple" id="to" name="to">
                                                 </select>
                                            </div>
                                            <div class="form-group">
                                              <label>Transfers:</label>
                                                 <select class="js-example-basic-multiple" id="primariestotransfer" name="primariestotransfer[]" multiple="multiple">
                                                 </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Complete Transfer</button>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->

                <!-- /user menu -->

                     <?php include("../sidebargen.php") ?>
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Dukapepe Daily DashBoard For Sale</span>
                        </h4>
                    </div>
                    <div class="heading-elements" id="today_total_sales" style="font-size: 30px; color:green;">
                                       
                                </div>

                </div>


            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">
                <div class="content clearfix">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-body table-responsive">
                               <div class="row">
                                       <form class="heading-form" id="formsearchdaysales" action="#">
                                        <div class="col-lg-3"><label>Start Day</label>
                                         <input type='text' class="form-control" id="datepickerstart" onkeypress="return false;" value="" /></div>

                                         <div class="col-lg-3"><label>End Day</label>
                                         <input type='text' class="form-control" id="datepickerend" onkeypress="return false;" value="<?php  echo date('Y-m-d'); ?>" /></div>
                                         <!--  <div class="col-lg-3"> <label>End Date</label>
                                         <input type='text' class="form-control" id="datepickerend" onkeypress="return false;"/></div> -->
                                          <div class="col-lg-3"><label></label>
                                         <input type='submit' class="btn btn-success form-control" id="loadperiodicgeneral" value="Load Report" /></div>
                                       </form>
                                        </div>
                                <br/>
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            
<div  id="userstablebody" style="min-width: 1200px;">
                </div>

                          
                      
                    
                    </div>
                        </div>
                    </div>
                </div>
                <!-- Footer -->
                <div class="footer text-muted">
                    <?php include("../footer.php") ?>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
    <script type="text/javascript">
      
       $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
    </script>
   

</div>
<!-- /page container -->

</body>
</html>
