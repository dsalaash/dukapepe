<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sub_Category_Model
 *
 * @author mwamb
 */
class Sub_Category_Model extends CI_Model{
    //put your code here
    public $sub_cat_table="sub_categories";
    
    public function add($data){
       $response=array();
       $counter=$this->check_new($data);
       if($counter==0){
            //add a new subcategory
          $this->db->insert($this->sub_cat_table,$data);
          $response['message']="Sub category added successfully"; 
          $response['code']=1; 
       }
       
       else{
           
            $response['message']="Error: Failed to add sub category"; 
            $response['code']=0;    
       }
      
       return json_encode($response);
    }
    
    public function check_new($data){
       $sub_category_name=$data['sub_cat_name'];
       $category_id=$data['cat_id'];
       
       $this->db->select('sub_cat_id');
       $this->db->where('cat_id',$category_id);
       $this->db->where('sub_cat_name',$sub_category_name);
       $this->db->from($this->sub_cat_table);
       $count= $this->db->count_all_results();
       
       return $count;
    }
    
    public function update($data){
        $response=array();

        $sub_category_id=$data['sub_cat_id'];
        $counter= $this->check_edit($data);
        if($counter==0){
        unset($data['sub_cat_id']);

        $this->db->where('sub_cat_id',$sub_category_id);
        $this->db->update($this->sub_cat_table, $data);
        $response['message']="Sub category updated successfully"; 
        $response['code']=1; 
        }
        else{
        $response['message']="Error: Failed to update sub category data, ".$counter." similar records exist"; 
        $response['code']=0;     
        }

        return json_encode($response);
    }
    
    public function check_edit($data){
        $sub_category_id=$data['sub_cat_id'];
        $sub_category_name=$data['sub_cat_name'];
        $category_id=$data['cat_id'];

        $this->db->select('sub_cat_id');
        $this->db->group_start();
        $this->db->where('sub_cat_name',$sub_category_name);
        $this->db->where('cat_id',$category_id);
        $this->db->group_end();
        $this->db->where('sub_cat_id!=',$sub_category_id);
        $this->db->from($this->sub_cat_table);
        $count=$this->db->count_all_results();

        return $count;
    }
    
    
    public function getsubcategorydetails($sub_cat_id){
        // fetch and return json data
       $base_url= base_url()."images/";
       $this->db->select('sub_cat_id AS id,category_name AS category,categories.category_id AS category_id,sub_cat_name AS sub_category,CONCAT("'.$base_url.'",'.$this->sub_cat_table.'.image_url) AS image_url'); 
       $this->db->where($this->sub_cat_table.'.sub_cat_id',$sub_cat_id);
       $this->db->from($this->sub_cat_table);
       $this->db->join('categories', $this->sub_cat_table.".cat_id=categories.category_id");
       $this->db->order_by('sub_cat_name');
       $returned_data= $this->db->get();
       $data=$returned_data->result_array();
       
       return json_encode($data);
    }
    public function fetch($category_id){
        // fetch and return json data
       $base_url= base_url()."images/";
       $this->db->select('sub_cat_id AS id,category_name AS category,categories.category_id AS category_id,sub_cat_name AS sub_category,CONCAT("'.$base_url.'",'.$this->sub_cat_table.'.image_url) AS image_url'); 
       $this->db->where($this->sub_cat_table.'.cat_id',$category_id);
       $this->db->from($this->sub_cat_table);
       $this->db->join('categories', $this->sub_cat_table.".cat_id=categories.category_id");
       $this->db->order_by('sub_cat_name');
       $returned_data= $this->db->get();
       $data=$returned_data->result_array();
       
       return json_encode($data);
    }
    
    public function fetch_all(){
        $base_url= base_url()."images/";
        $this->db->select('sub_cat_id AS id,category_name AS category,categories.category_id AS category_id,sub_cat_name AS sub_category,CONCAT("'.$base_url.'",'.$this->sub_cat_table.'.image_url) AS image_url'); 
        $this->db->from($this->sub_cat_table);
        $this->db->join('categories', $this->sub_cat_table.".cat_id=categories.category_id");
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $this->db->order_by('sub_cat_name');
        $this->db->order_by('category_name');
        
        return json_encode($data);
    }
    
    public function delete($sub_cat_id){
     if($this->check_associated($sub_cat_id)==0){
         $this->db->where('sub_cat_id',$sub_cat_id);
         $this->db->delete('sub_categories');
         $num= $this->db->affected_rows();
}
     else{
      $num=0;   
     }
        $response= $this->Status->response($num);
        
        return json_encode($response);
     }
     
     public function check_associated($sub_cat_id){
     $this->db->select('product_id');
     $this->db->where('sub_cat_id',$sub_cat_id);
     $this->db->from('products');
     $num= $this->db->count_all_results();
     
     return $num;
     }
    
}
