<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Director_Model
 *
 * @author mwamb
 */
class Director_Model extends CI_Model{
    //put your code here
    public $directors="directors";
    
    public function register_director($data){
        $count = $this->newChecker($data);
        if($count==0){
            $this->db->insert($this->directors,$data);
            $response['message'] = "Director added successfully.";
            $response['code'] = 1;
        }
        else{
            $response['message'] = "Error: Director Already exist.";
            $response['code'] = 0;
        }
        return json_encode($response);
    }
    public function newChecker($data){
        $this->db->select('director_id');
        $this->db->where('phone',$data['phone']);
        if($data['email']==""){
        $this->db->or_where('email',$data['email']);
        }
        $this->db->from($this->directors);
        $count= $this->db->count_all_results();
        return $count;
    }

    public  function update_director($data){
        $count= $this->checkUpdate($data);
        if($count==0){
        $supplier_id=$data['director_id'];
        unset($data['director_id']);
        $this->db->where('director_id',$supplier_id);
        $this->db->update($this->directors,$data);
        $response['message'] = "Director details updated successfully.";
        $response['code'] = 1;
        }
        else{
        $response['message'] = "Failed to update director details.";
        $response['code'] = 0;   
        }
        
        return json_encode($response);
    }
    
    
    public function checkUpdate($data){
        $this->db->select('director_id');
        $this->db->where('phone',$data['phone']);
        $this->db->where('director_id !=',$data['director_id']);
        $this->db->from($this->directors);
        $count= $this->db->count_all_results();
        return $count;
    }
    
    
    public function fetch($data){
        $this->db->select('director_id,first_name,last_name,phone,email,is_active');
        $this->db->where('director_id',$data['director_id']);
        $this->db->from($this->directors);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return json_encode($response);
    }
    public function fetch_all(){
        $this->db->select('director_id,first_name,last_name,phone,email,is_active');
        $this->db->from($this->directors);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return json_encode($response);
    }
    
    public function update_profile($data){
        $director_id=$data['director_id'];
        unset($data['director_id']);
        $this->db->where('director_id',$director_id);
        $this->db->update($this->directors,$data);
        $response['message'] = "Director details updated successfuly.";
        $response['code'] = 1;
        
        return json_encode($response);
    }
}
