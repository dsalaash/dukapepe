<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Products
 *
 * @author mwamb
 */
class Dukapepe_Suppliers_Model extends CI_Model {
    //put your code here
    public $dukapepe_suppliers_db='dukapepe_suppliers';
    
    public function add($data){
        $response=array();
        $counter= $this->checkNew($data);
        if($counter==0){
            $this->db->insert($this->dukapepe_suppliers_db,$data);
            $response['message']='Supplier added successfully.';
            $response['code']=1;
        }
        else{
          $response['message']='Supplier with that name already exists';
          $response['code']=0;
        }
        return json_encode($response);
    }

    // public function update_supplier($data){
    //     $supplier_id=$data['supplier_id'];
    //     unset($data['supplier_id']);
    //     $this->db->where('supplier_id',$supplier_id);
    //     $this->db->update($this->dukapepe_suppliers_db,$data);
    //     $response['message'] = "Profile updated successfully.";
    //     $response['code'] = 1;
        
    //     return json_encode($response);
    // }

    public function update_supplier($data){
       $response=array();
       $counter= $this->checkEdit($data);
       if($counter==0){
           $supplier_id=$data['supplier_id'];
           unset($data['supplier_id']);
           $this->db->where('id',$supplier_id);
           $this->db->update($this->dukapepe_suppliers_db,$data);
           $response['message']='Supplier information updated successfully.';
           $response['code']=1;
       }
       else{
        $response['message']='Error, Supplier with the same name exists.';
        $response['code']=0;   
       }
       return json_encode($response);
    }
    public function checkEdit($data){
        $supplier_id=$data['supplier_id'];
        $business_name=$data['business_name'];
        $this->db->select('id');
        $this->db->where('business_name',$business_name);
        $this->db->where('id !=',$supplier_id);
        $this->db->from($this->dukapepe_suppliers_db);
        $count= $this->db->count_all_results();
        return $count;
    }
    
    public function checkNew($data){
        $business_name=$data['business_name'];
        $this->db->select('id');
        $this->db->where('business_name',$business_name);
        $this->db->from($this->dukapepe_suppliers_db);
        $counter=$this->db->count_all_results();
        return $counter;
    }

    public function details($supplier_id){
        $response=array();
        $this->db->select('*');

        $this->db->where('id',$supplier_id);
        $this->db->from($this->dukapepe_suppliers_db);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        
        return json_encode($response);
    }
    public function fetch_all(){
        $this->db->select('*');
        $this->db->from($this->dukapepe_suppliers_db);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return json_encode($data);
    }

}
