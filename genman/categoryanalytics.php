<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka pepe | Category analytics</title>
        <?php include("../links.php") ?>
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });

                if (localStorage.getItem('code') == 1) {
                    // load_category_analytics();



                    $(document).ready(function()
                    {
                        $("#select_timeframe").val('');
                        $("#datepickerstart").val('');
                        $("#datepickerend").val('');
                        // $("#datepickerstart").prop('disabled', true);
                        // $("#datepickerend").prop('disabled', true);
                        // $("#datepickerend").prop('disabled', true);

                        $("#select_timeframe").change(function()
                        {
                            var timeframe = $("#select_timeframe").val();
                            if(timeframe==1)
                            {
                                $("#datepickerstart").val('');
                                $("#datepickerend").val('');
                                $( "#datepickerstart" ).datepicker( { dateFormat: 'yy-mm-dd' } );
                                $( "#datepickerend" ).datepicker( { dateFormat: 'yy-mm-dd' } );
                                $( "#datepicker" ).change(function() {});
                            }
                            else if(timeframe==2)
                            {
                              var now = new Date();
                              var yr = now.getFullYear();
                              var month = now.getMonth()+1;
                              var day = now.getDate();
                              var startdate = yr+"-01-01";
                              var enddate = yr+"-"+month+"-"+day;
                              $("#datepickerstart").val(startdate);
                              $("#datepickerend").val(enddate);
                            }
                            else if(timeframe==3)
                            {
                              var now = new Date();
                              var yr = now.getFullYear();
                              var month = now.getMonth()+1;
                              var day = now.getDate();
                              var startdate = yr+"-"+month+"-01";
                              var enddate = yr+"-"+month+"-"+day;
                              $("#datepickerstart").val(startdate);
                              $("#datepickerend").val(enddate);
                            }
                            else if(timeframe==4)
                            {
                              var now = new Date();
                              var yr = now.getFullYear()-1;
                              var month = now.getMonth();
                              var day = now.getDate();
                              var no_days = new Date(yr, month, 0).getDate(); 
                              var startdate = yr+"-01-01";
                              var enddate = yr+"-12-31";
                              $("#datepickerstart").val(startdate);
                              $("#datepickerend").val(enddate);
                            }
                            else if(timeframe==5)
                            {
                              var now = new Date();
                              var yr = now.getFullYear();
                              var month = now.getMonth();
                              var day = now.getDate();
                              var no_days = new Date(yr, month, 0).getDate(); 
                              var startdate = yr+"-"+month+"-01";
                              var enddate = yr+"-"+month+"-"+no_days;
                              $("#datepickerstart").val(startdate);
                              $("#datepickerend").val(enddate);
                            }
                        });


                        $('#formsearchperiod').submit(function (e)
                        {
                            e.preventDefault();
                            var startdate = $("#datepickerstart").val();
                            var enddate = $("#datepickerend").val();
                            // alert(startdate + enddate);

                            var formData = {'access_token': localStorage.getItem('access_token'), startdate , enddate};
                            var url = base_url+ "analytics/load_category_analytics";
// console.log('json');


                            $.ajax({
                            url: url,
                            type: 'post',
                            data: formData,
                            success: function( data){
                              
                                var parse = JSON.parse(data);
                                // console.log(parse);
                                var rowCount = 1;
                                var table = document.getElementById("agentsbodytable");
                                var rows = table.getElementsByTagName("tr")
                                var directpercentage = 2/100;
                                var agentpercentage = 1/100;

                                var agent_row="<table class='table table-responsive table-bordered' id='example'  max-width:300px; overflow:scroll;'><thead> <tr><th><b>#</b></th><th><b>Agents</b></th><th colspan='3' align='center'><b>Sales (KSH)</b></th><th colspan='3'><b>Commissions (KSH)</b></th></b></tr><tr><td></td><td></td><td><b>Agent</b></td><td><b>Direct</b></td><td><b>Total</b></td><td><b>Agent (1 %)</b></td><td><b>Direct (2 %)</b></td><td><b>Total</b></td></thead>";  

                                $.each(parse['agents'], function(k, v) { 

                                  console.log(v.retailer_name+" agent = "+ parse['total_agent_salesagent'+v.agent_id]);
                                  console.log(v.retailer_name+" direct = "+parse['total_agent_salesdirect'+v.agent_id]);
                                  // var directcommission = ((parseFloat(parse['total_agent_salesdirect'+v.agent_id]))*(parseFloat(directpercentage))).toFixed(2);
                                
                                  agent_row+="<tr><td>"+rowCount+++"</td><td>"+v.retailer_name+"</td><td>"+parseFloat(parse['total_agent_salesagent'+v.agent_id]).toFixed(2)+"</td><td>"+parseFloat(parse['total_agent_salesdirect'+v.agent_id]).toFixed(2)+"</td><td>"+(parseFloat(parse['total_agent_salesagent'+v.agent_id]) + parseFloat(parse['total_agent_salesdirect'+v.agent_id])).toFixed(2)+"</td><td>"+((parseFloat(parse['total_agent_salesagent'+v.agent_id]))*(parseFloat(agentpercentage))).toFixed(2)+"</td><td>"+((parseFloat(parse['total_agent_salesdirect'+v.agent_id]))*(parseFloat(directpercentage))).toFixed(2)+"</td><td>"+(Math.round((parseFloat(parse['total_agent_salesdirect'+v.agent_id]*directpercentage)+(parse['total_agent_salesagent'+v.agent_id]*agentpercentage)))).toLocaleString()+"</td></tr>";   
                                 
                              }); 

                                agent_row+="</tr>";

                                agent_row+="</tbody>";
                                agent_row+="</table>"; 

                                document.getElementById("agentsbodytable").innerHTML="";
                                $("#agentsbodytable").append(agent_row);

                                $('#example').DataTable( {
                                    "ordering": false,
                                    dom: 'Bfrtip',
                                    buttons: [
                                        'copy', 'csv', 'excel', 'pdf', 'print'
                                    ]
                                } );

                            }

                            // error: function( jqXhr, textStatus, errorThrown ){
                            //     console.log( errorThrown );
                            // }
                        });















































                                $('#example').DataTable({
                                "destroy": true,
                                "ajax": {
                                    "url": url,
                                    "data": formData,
                                    "type": "post",
                                    "dataSrc": function (json) {
                                        console.log(json);
                                        return json;

                                    },
                                    "processing": true,
                                    "serverSide": true,
                                    "pagingType": "simple",
                                    language: {
                                        paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                                    }

                                }, 

                                  "columnDefs": [
                                    {
                                        // targets: 2,
                                        // className: 'dt-body-right'
                                    }
                                  ],
                                  "dom": 'Bfrtip',
                                    "buttons": [
                                        'copy', 'csv', 'excel', 'pdf', 'print'
                                    ],
                                "columns": [
                        
                                    {"data": "image_url", orderable: false, searchable: false, render: function (categoriesAnalysis, type, full, meta) {
                                            return '<img src="' + categoriesAnalysis + '" class=" width="50" height="50" alt="">';
                                        }},
                                    {"data": "category_name"},
                                    {"data": "totalsales"},
                                    {"data": "totalpurchase"},
                                    {"data": "margins"},
                                    {"data": "pmargin"},
                                    {"data": "timestamp"},
                                ]
                            });



                        });


                    });






























                } else {
                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });
                }


            });
            // function load_category_analytics() {
            //     var formData = {'access_token': localStorage.getItem('access_token')};
            //     var url = base_url + "order/load_category_analytics";
            //     $('#example').DataTable({
            //         "destroy": true,
            //         "ajax": {
            //             "url": url,
            //             "data": formData,
            //             "type": "post",
            //             "dataSrc": function (json) {
            //                 // console.log(json);
            //                 return json;

            //             },
            //             "processing": true,
            //             "serverSide": true,
            //             "pagingType": "simple",
            //             language: {
            //                 paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
            //             }

            //         }, 

            //           "columnDefs": [
            //             {
            //                 // targets: 2,
            //                 // className: 'dt-body-right'
            //             }
            //           ],
            //           "dom": 'Bfrtip',
            //             "buttons": [
            //                 'copy', 'csv', 'excel', 'pdf', 'print'
            //             ],
            //         "columns": [
            //             // {"data": ""},
            //             // {"data": "totalcost"},
            //             // {"data": "image_url"},
            //             {"data": "image_url", orderable: false, searchable: false, render: function (categoriesAnalysis, type, full, meta) {
            //                     return '<img src="' + categoriesAnalysis + '" class=" width="50" height="50" alt="">';
            //                 }},
            //             {"data": "category_name"},
            //             {"data": "totalsales"},
            //             {"data": "totalpurchase"},
            //             {"data": "margins"},

            //             {"data": "timestamp"},
            //             // {"data": "category"},
            //             // {"data": "subcategory"}
            //         ]
            //     });
            // }
        </script>
    </head>
    <body>
        <?php include("../topbar.php") ?>
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">
                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- Main content -->
                <div class="content-wrapper">
                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Sales analytics by categories</span></h4>
                            </div>
                        </div>
                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                                <li class="active">Sales analytics by categories</li>
                            </ul>

                        </div>
                    </div>
                    <div class="content">
                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading" >
                                    <h6 class="panel-title">
                                    <div >
                                      <div class="row">
                                      </div>
                                      <div class="row">
                                       <form class="heading-form" id="formsearchperiod" action="#">
                                          <div class="col-lg-3"><label>TimeFrame</label>
                                        <!-- <select class="form-control" id="select_timeframe">
                                        <option value="1">Custom<option>
                                          <option value="2">This Year<option>
                                          <option value="3">This Month<option>
                                          <option value="4">Last Year<option>
                                          <option value="5">Last Month<option>
                                          <option value="6">Custom<option>
                                        </select> -->
                                         <select required="required" id="select_timeframe">
                                          <option value="" selected="selected">Kindly select timeframe</option>
                                          <option value="1">Custom</option>
                                          <option value="3">This month</option>
                                          <option value="5">Last month</option>
                                          <option value="2">This year</option>
                                          <option value="4">Last year</option>
                                        </select> 
                                         </div>
                                         <div class="col-lg-3"><label>Start Date</label>
                                         <input type='text' class="form-control" required="required" id="datepickerstart" onkeypress="return false;"/>
                                        </div>
                                        <div class="col-lg-3"> <label>End Date</label>
                                         <input type='text' class="form-control" required="required" id="datepickerend" onkeypress="return false;"/>
                                        </div>
                                        <div class="col-lg-3"><label>.</label>
                                            <input type='submit' class="btn btn-primary form-control" value="Load Report" />
                                        </div>
                                       </form>
                                        </div>
                                    </div>
                                </div>
                            <div class="panel-body">
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            
                            <table class="display nowrap" id="example" style="width:100%">
                                <thead>
                                <tr>
                                    <!-- <th>stock_id</th> -->
                                    <th>Images</th>
                                    <th>Category</th>
                                    <th>Total Sales (Ksh)</th>
                                    <th>Cost of Sales (Ksh)</th>
                                    <th>Margins (Ksh)</th>
                                    <th>Margins %</th>
                                    <th>Category Creation Date</th>
                                </tr>
                                </thead>
                                <tbody id="categoryanalytics">
                                    
                                </tbody>

                            </table>
                            </div>
                        </div>
                        <!-- /basic datatable -->
                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>

                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
    </body>
</html>