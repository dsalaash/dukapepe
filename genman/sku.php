<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka pepe | sku</title>

        <?php include("../links.php") ?>
        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search sku:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });

                if (localStorage.getItem('code') == 1) {
                    loadSku();
                } else {

                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });

                }

                update();
                AddCategory();
                var formData = {
                    'access_token': localStorage.getItem('access_token')
                };
                var url = base_url + "units/fetch_all";
                $.post(url, formData,
                        function (data) {

                            var datas = JSON.parse(data);
                            console.log(datas);
                            var model = $('#selectcat');
                            model.empty();

                            $.each(datas, function (index, element) {
                                model.append("<option value='" + element.unit_id + "'>" + element.unit_name + "</option>");
                            });
                        });


                var formData2 = {
                    'access_token': localStorage.getItem('access_token')
                };
                var url2 = base_url + "units/fetch_all";
                $.post(url2, formData2,
                        function (data) {

                            var datas = JSON.parse(data);
                            console.log(datas);
                            var model = $('#selectcat2');
                            model.empty();

                            $.each(datas, function (index, element) {
                                model.append("<option value='" + element.unit_id + "'>" + element.unit_name + "</option>");
                            });
                        });


                var formDatapQ = {
                    'access_token': localStorage.getItem('access_token')
                };
                var urlpq = base_url + "product_quantity/fetch_all";
                $.post(urlpq, formDatapQ,
                        function (data) {

                            var datas = JSON.parse(data);
                            console.log(datas);
                            var model = $('#selectcatpq');
                            model.empty();

                            $.each(datas, function (index, element) {
                                model.append("<option value='" + element.pq_id + "'>" + element.pq_value + "</option>");
                            });
                        });


                var formDatapQ2 = {
                    'access_token': localStorage.getItem('access_token')
                };
                var urlpq2 = base_url + "product_quantity/fetch_all";
                $.post(urlpq2, formDatapQ2,
                        function (data) {

                            var datas = JSON.parse(data);
                            console.log(datas);
                            var model = $('#selectcatpq2');
                            model.empty();

                            $.each(datas, function (index, element) {
                                model.append("<option value='" + element.pq_id + "'>" + element.pq_value + "</option>");
                            });
                        });

            });
            function loadSku() {
                // console.log(localStorage.getItem('access_token'));
                var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "sku/fetch_all";
                $('#example').DataTable({
                    "destroy": true,
                    "ajax": {
                        "url": url,
                        "data": formData,
                        "type": "post",
                        "dataSrc": function (json) {
                            return json;
                            // console.log(json);
                        },
                        "processing": true,
                        "serverSide": true,
                        "pagingType": "simple",
                        language: {
                            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                        }

                    }, "columns": [
                        {"data": "sku_name"},
                        {"data": "sku_type"},
                        {"data": "weight"},
                        {"data": "is_active", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                if (data == 1) {
                                    return '<a href="javascript: deactivateSku(' + full.sku_id + ')" class="btn btn-xs btn-warning"><i class="icon-lock2"></i>De activate</a>';

                                } else {
                                    return '<a href="javascript: activateSku(' + full.sku_id + ')" class="btn btn-xs btn-success"><i class="icon-unlocked2"></i>Activate</a>';
                                }

                            }},
                        {"data": "sku_id", orderable: false, searchable: false, render: function (data, type, full, meta) {

                                return '<a href="javascript: ajaxmodaledit(' + data + ')" class="btn btn-xs btn-primary"><i class="icon-pencil5"></i>edit</a>';

                            }}
                    ]
                });

            }

            function ajaxmodaledit(id) {
                $(".status-progress").show();
                var url = base_url + "sku/fetch";
                var formData = {
                    'sku_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];
                        $('#catToken').val(localStorage.getItem('access_token'));
                        $('#skuId').val(obj_data['sku_id']);
                        $('#skuName').val(obj_data['sku_name']);
                        $('#skuType').select2().val(obj['sku_type']).trigger("change");
                        $('#weight').val(obj_data['weight']);
                    }
                    $(".status-progress").hide();
                    $('#modalCat').modal('show');
                });

            }


            function ajaxmodaladd() {
                $(".status-progress-add").hide();
                $('#catToken2').val(localStorage.getItem('access_token'));
                $('#modalCatAdd').modal('show');
            }

            function AddCategory() {
                $('#formcategoryAdd').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "/sku/add ",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-add").hide();
                            loadSku();
                            $('#modalCatAdd').modal('hide');
                            var data = JSON.parse(data);
                            new PNotify({
                                title: 'Add SKU Notice',
                                text: data['message'],
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {}
                    });
                });
            }

            function update() {
                $('#formcategory').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress").show();
                    $.ajax({
                        url: base_url + "/sku/update",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $('#modalCat').modal('show');
                            $("#submit").removeClass("disabled");
                            $(".status-progress").hide();

                            location.reload();
                            $('#modalCat').modal('hide');
                            var data = JSON.parse(data);
                            new PNotify({
                                title: 'Update Notice',
                                text: data['message'],
                                addclass: 'bg-success'
                            });

                            // console.log(data);
                        },
                        error: function () {}
                    });
                });
            }

            function activateSku(id) {
                $(".status-progress").show();
                var url = base_url + "/sku/status";
                var formData = {
                    'sku_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 1
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                        title: 'Update Notice',
                        text: 'Sku activated successfully',
                        addclass: 'bg-info'
                    });
                    loadSku();
                    $(".status-progress").hide();
                });
            }

            function deactivateSku(id) {
                $(".status-progress").show();
                var url = base_url + "/sku/status";
                var formData = {
                    'sku_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 0
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                        title: 'Update Notice',
                        text: 'Sku deactivated successfully',
                        addclass: 'bg-warning'
                    });
                    loadSku();
                    $(".status-progress").hide();
                });
            }

        </script>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                       
                        <!-- /user menu -->
                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">SKU</span></h4>
                            </div>

                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                                <li class="active">Basic</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <!-- <div class="panel-heading">
                                <h5 class="panel-title">SKU</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div> -->

                            <div class="panel-body">
                                <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-success pull-right">Add a SKU</a>
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            <table class="table" id="example">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Mass(kg)</th>
                                        <th>Action</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>

                            </table>
                            </div>
                        </div>
                        <!-- /basic datatable -->

                        <!-- Vertical form modal -->
                        <div id="modalCat" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">SKU form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formcategory" enctype="multipart/form-data">
                                        <input type="hidden" name="sku_id" id="skuId"/>
                                        <input type="hidden" name="access_token" id="catToken"/>
                                        
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Name:</label>
                                                <input required type="text" name="sku_name" id="skuName" class="form-control"/>
<!--                                                <select id="selectcat2" name="unit_id" class="form-control">
                                                    <option value="0">Select Unit</option>
                                                </select>-->

                                            </div>
                                            <div class="form-group">

                                                <label>Mass(Kg):</label>
                                                <input required type="number" name="weight" id="weight" class="form-control"/>
<!--                                                <select id="selectcatpq2" name="pq_id" class="form-control">
                                                    <option value="0">Select product quantity</option>
                                                </select>-->

                                            </div>
                                            <div class="form-group">
                                                <label>Type of SKU</label>
                                                <select class="select-search" name="skuType" id="skuType" required="">
                                                    <option value="" disabled="">Select type of sku</option>
                                                    <option value="Large">Large</option>
                                                    <option value="Small">Small</option>
                                                    <option value="Uncategorised">Uncategorised</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <!-- Vertical form modal -->
                        <div id="modalCatAdd" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">SKU form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formcategoryAdd" enctype="multipart/form-data">
                                        <input type="hidden" name="access_token" id="catToken2"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Name:</label>
                                                <input required type="text" name="sku_name" class="form-control"/>
<!--                                                <select id="selectcat" name="unit_id" class="form-control">
                                                    <option value="0">Select Unit</option>
                                                </select>-->

                                            </div>
                                            <div class="form-group">

                                                <label>Mass(kg):</label>
                                                <input required type="number" name="weight" class="form-control"/>
<!--                                                <select id="selectcatpq" name="pq_id" class="form-control">
                                                    <option value="0">Select product quantity</option>
                                                </select>-->

                                            </div>
                                            <div class="form-group">
                                                <label>Type of SKU</label>
                                                <select class="select-search" name="sku_type" required="">
                                                    <option selected="" value="" disabled="">Select type of sku</option>
                                                    <option value="Large">Large</option>
                                                    <option value="Small">Small</option>
                                                    <option value="Uncategorised">Uncategorised</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                            <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>

                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
