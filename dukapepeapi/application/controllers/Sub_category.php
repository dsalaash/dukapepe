<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sub_Category
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Sub_category extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
         $this->load->helper(array('form', 'url'));
         $this->load->library('image');
    }
    public function add(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['cat_id']= $this->input->post('category_id');
        $data['sub_cat_name']= $this->input->post('sub_category_name');
        $data['image_url']= $this->image->upload_image();
        
        $response= $this->Sub_Category_Model->add($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
    public function update(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['cat_id']= $this->input->post('category_id');
        $data['sub_cat_name']= $this->input->post('sub_category_name');
        $data['sub_cat_id']= $this->input->post('sub_category_id');
                //upload image and get file_name
//        if($this->input->post('image_url')!=null){
        $image_url= $this->image->upload_image();
        if(!$image_url==""){
         $data['image_url']=$image_url;   
        }
//        }
        $response= $this->Sub_Category_Model->update($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    
    } 
    
    public function delete(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $sub_category_id= $this->input->post('sub_category_id');
        $response= $this->Sub_Category_Model->delete($sub_category_id);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
    public function getsubcategorydetails(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $sub_cat_id= $this->input->post('sub_category_id');
        $response=$this->Sub_Category_Model->getsubcategorydetails($sub_cat_id);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function fetch(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $category_id= $this->input->post('category_id');
        $response=$this->Sub_Category_Model->fetch($category_id);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function  fetch_all(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $response= $this->Sub_Category_Model->fetch_all();
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
}
