<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author mwamb
 */
class Category_Model extends CI_Model{
    //put your code here
    public $category_table="categories";
   public function index(){
//    fech and return an array of data   
       
       
   }
       public function add($data){
        $response=array();
        //check existence
        $counter= $this->checkCategory($data);
        if($counter==0){
            $this->db->insert($this->category_table,$data);
            $response['message']='Category added successfully.';
            $response['code']=1;
        }
        else{
            $response['message']='Error, Category already exist.';
            $response['code']=0;
        }
        return json_encode($response);
    }
    
    public function checkCategory($data){
        $this->db->select('category_id');
        $this->db->where($this->category_table.'.category_name',$data['category_name']);
        $this->db->from($this->category_table);
        $count= $this->db->count_all_results();
      
        return $count;
    }
    public function fetch($input_data){
        $base_url= base_url()."images/";
        $this->db->select('category_id,category_name,category_description,CONCAT("'.$base_url.'",image_url) AS image_url, is_active');
        if(count($input_data)>0){
        $this->db->where('is_active',$input_data['is_active']);    
        }
        $this->db->from($this->category_table);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return json_encode($data);
    }
    public function getProductDetails($cat_data){
        $base_url= base_url()."images/";
        $category_id=$cat_data['category_id'];
        $this->db->select('category_id,category_name,category_description,CONCAT("'.$base_url.'",image_url) AS image_url, is_active');
        $this->db->where('category_id',$category_id);
        $this->db->from($this->category_table);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return json_encode($data);
    }
    
    public function update($data){
       $response=array();
       $counter= $this->checkEdit($data);
       if($counter==0){
           $category_id=$data['category_id'];
           unset($data['category_id']);
           $this->db->where('category_id',$category_id);
           $this->db->update($this->category_table,$data);
           $response['message']='Category details updated successfully.';
           $response['code']=1;
       }
       else{
        $response['message']='Error, Category details not updated.';
        $response['code']=0;   
       }
       return json_encode($response);
    }
    
    public function checkEdit($data){
        $cat_id=$data['category_id'];
        $cat_name=$data['category_name'];
        
        $this->db->select('category_id');
        $this->db->where('category_name',$cat_name);
        $this->db->where('category_id !=',$cat_id);
        $this->db->from($this->category_table);
        $count= $this->db->count_all_results();
        return $count;
    }
    public function status($data){
       $category_id=$data['category_id'];
       unset($data['category_id']);
       $this->db->where('category_id',$category_id);
       $this->db->update($this->category_table,$data);
       $response=array();
       $response['message']='Category status updated successfully.';
       $response['code']=1;
       return json_encode($response);  
    }
    
    public function delete($category_id){
     if($this->check_associated($category_id)==0){
         $this->db->where('category_id',$category_id);
         $this->db->delete('categories');
         $num= $this->db->affected_rows();
}
     else{
      $num=0;   
     }
        $response= $this->Status->response($num);
        
        return json_encode($response);
     }
     
     public function check_associated($category_id){
     $this->db->select('sub_cat_id');
     $this->db->where('cat_id',$category_id);
     $this->db->from('sub_categories');
     $num= $this->db->count_all_results();
     
     return $num;
     }
    
    
}
