<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka pepe | sub skus</title>
        <?php include("../links.php") ?>
        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search sku:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });

                if (localStorage.getItem('code') == 1) {
                    loadSubSku();
                } else {

                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });

                }

                update();
                AddSubSku();
                var formData = {
                    'access_token': localStorage.getItem('access_token')
                };
                var formData2 = {
                    'access_token': localStorage.getItem('access_token')
                };
                var url2 = base_url + "sku/fetch_all";
                $.post(url2, formData2,
                    function (data) {

                        // var datas = JSON.parse(data);

                        var datas = JSON.parse(data);
                        // console.log(datas);
                        var large_sku = $('#larger_sku');
                        var small_sku = $('#smaller_sku');
                        var edit_large = $('#edit_larger_sku');
                        var edit_small = $('#edit_smaller_sku');
                        large_sku.empty();
                        small_sku.empty();
                         edit_large.empty();
                         edit_small.empty();
                        $.each(datas, function (index, element) {
                            large_sku.append("<option value='" + element.sku_id + "'>" + element.sku_name + "</option>");
                            small_sku.append("<option value='" + element.sku_id + "'>" + element.sku_name + "</option>");
                             edit_large.append("<option value='" + element.sku_id + "'>" + element.sku_name + "</option>");
                             edit_small.append("<option value='" + element.sku_id + "'>" + element.sku_name + "</option>");
                        });

                    });


                // var url = base_url + "Sub_sku/fetch_all";
                // $.post(url, formData,
                //         function (data) {

                //             console.log(data);
                //             console.log('jose');

                //             var datas = JSON.parse(data);
                //             // var edit_large = $('#edit_larger_sku');
                //             edit_large.empty();

                //             $.each(datas, function (index, element) {
                //                 edit_large.append("<option value='" + element.large_sku_id + "'>" + element.larger_sku + "</option>");
                //             });
                //         });

            });
            function loadSubSku() {
                // console.log(localStorage.getItem('access_token'));
                var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "sub_sku/fetch_all";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                        // console.log(data);
                           var user_row="";   
                            
                           var no =0;    
                            $.each(JSON.parse(data), function(k, v) {
                                   no=no+1;
                                  user_row+="<tr><td>"+no+"</td><td>"+v.larger_sku+"</td><td>"+v.smaller_sku+"</td><td>"+v.factor+"</td><td><div class='dropdown'><button class='btn btn-primary btn-sm dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> Actions<span class='caret'></span></button><ul class='dropdown-menu'><li><a href='javascript: ajaxmodaledit("+v.id+")'>Edit</a></li></ul></div></td></tr>";
                                   
                                });  
                                $("#example").append(user_row);
                                // $('#example').DataTable( {
                                //     dom: 'Bfrtip',
                                //     buttons: [
                                //         'copy', 'csv', 'excel', 'pdf', 'print'
                                //     ]
                                // } );             
                        },
                            error:function(data){
                            // console.log(data);
                       }        

        });

            }

            function ajaxmodaledit(id) {
                $(".status-progress").show();
                var url = base_url + "Sub_sku/fetch";
                var formData = {
                    'sku_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    // console.log(json);
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];
                        // console.log(obj_data['sku_id']);
                        $('#EditSubToken').val(localStorage.getItem('access_token'));
                        $('#edit_larger_sku').select2().val(obj_data['larger_sku']).trigger("change");
                        $('#edit_smaller_sku').select2().val(obj_data['smaller_sku']).trigger("change");
                        // $('#edit_smaller_sku').select2().val(obj_data['smaller_sku']).trigger("change");;
                        $('#edit_factor').val(obj_data['factor']);
                    }
                    $(".status-progress").hide();
                    $('#modaleditsubsku').modal('show');
                });

            }


            function ajaxmodaladd() {
                // $(".status-progress-add").hide();
                $('#catToken2').val(localStorage.getItem('access_token'));
                // $('#larger_sku').val('');
                // $('#smaller_sku').val('');
                // $('#factor').val('');
                $('#subskumodal').modal('show');
            }

            function AddSubSku() {
                $('#formSubSkuAdd').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "/Sub_sku/add ",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-add").hide();
                            $('#subskumodal').modal('hide');
                            location.reload();
                            $('#larger_sku').val('');
                            $('#smaller_sku').val('');
                            $('#factor').val('');
                            var data = JSON.parse(data);
                            new PNotify({
                                title: 'Add Sub SKU Notice',
                                text: data['message'],
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {}
                    });
                });
            }

            function update() {
                $('#formeditsubsku').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress").show();
                    $.ajax({
                        url: base_url + "sub_sku/update",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            // $('#modalCat').modal('show');
                            $("#submit").removeClass("disabled");
                            $(".status-progress").hide();

                            location.reload();
                             $('#modaleditsubsku').modal('hide');
                            var data = JSON.parse(data);
                            new PNotify({
                                title: 'Update Notice',
                                text: data['message'],
                                addclass: 'bg-success'
                            });

                            // console.log(data);
                        },
                        error: function () {}
                    });
                });
            }

            function activateSku(id) {
                $(".status-progress").show();
                var url = base_url + "/sku/status";
                var formData = {
                    'sku_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 1
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                        title: 'Update Notice',
                        text: 'Sku activated successfully',
                        addclass: 'bg-info'
                    });
                    loadSubSku();
                    $(".status-progress").hide();
                });
            }

            function deactivateSku(id) {
                $(".status-progress").show();
                var url = base_url + "/sku/status";
                var formData = {
                    'sku_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 0
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                        title: 'Update Notice',
                        text: 'Sku deactivated successfully',
                        addclass: 'bg-warning'
                    });
                    loadSubSku();
                    $(".status-progress").hide();
                });
            }

        </script>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                       
                        <!-- /user menu -->
                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">SKU</span></h4>
                            </div>

                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                                <li class="active">Basic</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <!-- <div class="panel-heading">
                                <h5 class="panel-title">SKU</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div> -->

                            <div class="panel-body">
                                <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-success pull-right"><i class="glyphicon glyphicon-plus"></i> Add Sub SKU</a>
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>

                            <table class="table" id="example">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Large SKU</th>
                                        <th>Smaller SKU</th>
                                        <th>Factor</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                            </table>
                            </div>
                        </div>
                        <!-- /basic datatable -->

                        <!-- Vertical form modal -->
                        <div id="modaleditsubsku" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Edit sku sub-sku form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formeditsubsku" enctype="multipart/form-data">
                                        <input type="hidden" name="sku_id" id="skuId"/>
                                        <input type="hidden" name="access_token" id="EditSubToken"/>
                                        
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Larger SKU:</label>
                                                <select required id="edit_larger_sku" name="edit_larger_sku" class="select-search">
                                                    <option value="0">Select the large SKU</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Smaller SKU:</label>
                                                <select required id="edit_smaller_sku" name="edit_smaller_sku" class="select-search">
                                                    <option value="0">Select the small SKU</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Multiplying Factor:</label>
                                                <input required type="number" name="edit_factor" id="edit_factor" class="form-control"/>
                                            </div>
                                        </div>
                                        <!-- <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Update</button>
                                        </div> -->
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <!-- Vertical form modal -->
                        <div id="subskumodal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Large-Small SKU form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formSubSkuAdd" enctype="multipart/form-data">
                                        <input type="hidden" name="access_token" id="catToken2"/>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <!-- <label>Larger SKU:</label> -->
                                                <select required id="larger_sku" name="larger_sku" class="select-search">
                                                    <option value="">Select the large SKU</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <!-- <label>Smaller SKU:</label> -->
                                                <select required id="smaller_sku" name="smaller_sku" class="select-search">
                                                    <option value="">Select the small SKU</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <!-- <label>Multiplying Factor:</label> -->
                                                <input required type="number" placeholder="Enter the multipying factor" name="factor" id="factor" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                            <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>

                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
