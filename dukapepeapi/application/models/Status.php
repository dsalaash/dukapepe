<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Status_Model
 *
 * @author mwamb
 */
class Status extends CI_Model{
    //put your code here
    
    public function update_status($data){
    $id_value=$data['id_value'];
    $id_column_name=$data['id_column_name'];
    $table_name=$data['table_name'];
    $is_active=$data['is_active'];
    
    $update_data=array();
    $update_data['is_active']=$is_active;
    $this->db->where($id_column_name,$id_value);
    $this->db->update($table_name,$update_data);
    }
    
    public function missing_data_error(){
        $response=array();
        $code=0;
        $message='Error, some data is missing.';
        $response['message']=$message;
        $response['code']=$code;
        
        print_r(json_encode($response));
    }
    
    public function response($num){
        if($num>0){
            $response['message'] = "Operation suceeded";
            $response['code'] = 1;
        }
        else{
         $response['message'] = "Operation failed.";
         $response['code'] = 0;    
        }
        
        return $response;
    }
}
