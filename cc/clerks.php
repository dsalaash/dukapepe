<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Duka pepe | DC Stock</title>

    <?php include("../links.php") ?>

    <!-- /theme JS files -->
    <script>
      if(localStorage.getItem('access_token')=="")
          {
           window.location = "http://localhost/dukapepe_portal/dukapepe/";
          }
       
        $(document).ready(function () {
          $('.js-example-basic-multiple').select2();
            $(".status-progress").hide();
            loadSuppliers();
             function loadSuppliers() {
                console.log(localStorage.getItem('access_token'));
                var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "supplier/fetch_all";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data)
                       {
                         console.log(data);
                        var options="";
                         $.each(JSON.parse(data), function(k, v) {
                          options+="<option value="+v.supplier_id
+">"+v.business_name
+"</option>" ;
                               
                            });  
                         $("#supplier").append(options);

                      
                       },error(data)
                       {

                       }
                     });

            }
              var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "user/get_all_clerks";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                        
                           var user_row="";   
                            
                           var no =0;    
                        $.each(JSON.parse(data), function(k, v) {
                               no=no+1;
                              user_row+="<tr><td>"+no+"</td><td>"+v.retailer_name+"</td><td>"+v.phone+"</td><td><div class='dropdown'><button class='btn btn-flat no-border dropdown-toggle dropleft' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><span class='glyphicon glyphicon-list'></span></button><div class='dropdown-menu dropdown-menu-right' aria-labelledby='dropdownMenuButton'><ul class='list-group '><li class='list-group-item'><a class='dropdown-item text-muted' href='javascript: linktodc("+v.retailer_id+")'>Link to DC</a></li></ul></div></div></td></tr>";
                               
                            });  
                            $("#userstablebody").append(user_row);
                            $("#example").DataTable();

                                        
                                },
                       error:function(data){

                                  console.log(data);
                       }        

        });
                linktodc = function(id)
                {
                 $("#modalinkclerkdc").modal();
                 $("#clerk").val(id);
                }
           ////////////////////
           $("#formlinkprimaaries").on("submit", function(e) 
           {
             e.preventDefault();
             var resend_activation = $("#reset_code_phone").val();
             var formData = {'access_token': localStorage.getItem('access_token'),'primaries':$("#primaries").val(),'agent':$("#agent").val()};
                console.log($("#primaries").val());
              $.ajax({
                       type:'POST',
                       url:  base_url + "user/linkprimaries",
                       data: formData,
                       success:function(data){
                         new PNotify({
                                    text: "Operation Succssful",
                                    addclass: 'bg-success'
                                });
                             window.location=window.location.href;      
                                },
                       error:function(data){

                                  console.log(data);
                       }        

                   });
         
  
           }); 
           ////////////
           $("#formlinkclerktodc").on("submit", function(e) 
           {
             e.preventDefault();
             var resend_activation = $("#reset_code_phone").val();
             var formData = {'access_token': localStorage.getItem('access_token'),'clerk':$("#clerk").val(),'supplier':$("#supplier").val()};
                console.log(formData);
              $.ajax({
                       type:'POST',
                       url:  base_url + "user/linkclerk_to_dc",
                       data: formData,
                       success:function(data)
                       {
                         new PNotify({
                                    text: "Operation Succssful",
                                    addclass: 'bg-success'
                                });
                               
                      },
                       error:function(data)
                       {

                                  console.log(data);
                       }        

                   });
         
  
           }); 
           //
            $("#formtransferprimaries").on("submit", function(e) 
           {
             e.preventDefault();
            
             var formData = {'access_token': localStorage.getItem('access_token'),'primaries':$("#primariestotransfer").val(),'agent':$("#to").val(),'from':$("#fromid").val()};
                console.log(formData);
              $.ajax({
                       type:'POST',
                       url:  base_url + "user/transferlinkedprimaries",
                       data: formData,
                       success:function(data){
                        console.log(data);
                         // new PNotify({
                         //            text: "Operation Succssful",
                         //            addclass: 'bg-success'
                         //        });
                             // window.location=window.location.href;      
                                },
                       error:function(data){

                                  console.log(data);
                       }        

                   });
         
  
           });   
             
            //////////////////    
           
            });
                function linktoprimary(id)
                {
                  
                var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id};
                console.log(formData);
                var url = base_url + "user/allprimariesfromdc";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var user_row="";
                          $.each(JSON.parse(data), function(k, v) {
                                  user_row+="<option value='"+v.retailer_id+"'>"+v.retailer_name+"("+v.phone+")"+"</option>";
                               
                            });  
                           user_row+="";
                           document.getElementById('primaries').innerHTML="";
                           $("#primaries").append(user_row);
                           $("#agent").val(id);
                           $("#modallinks").modal();

                                        
                                },
                             error:function(data){

                                 new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

                   });
                }
                function viewtransactions(agent)
                {
                   var formData = {'access_token': localStorage.getItem('access_token'),'user_id':agent,'user_type_id':6};
                   var url = base_url + "user/agenttransactions";
                   $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                          var user_row="";
                          var pno=0;
                          $.each(JSON.parse(data), function(k, v) {
                            pno=pno+1;
                                  user_row+="<tr><td>"+pno+"</td><td>"+v.retailer_name+"</td><td>"+v.phone+"</td><td>"+v.dc+"</td></tr>";
                               
                            });  
                           user_row+="";
                           document.getElementById('agenttransactions').innerHTML="";
                           $("#agenttransactions").append(user_row);
                           $("#modaltransactions").modal();
                           $("#example3").DataTable();
                            },
                          error:function(data){
                               new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

        });

                }
                  function transferprimaries(id)
                
                {
                  
                var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id};
                console.log(formData);
                var url = base_url + "user/allprimarieslinkedtoagent";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var json= JSON.parse(data);

                        
                  console.log(json['fromname']);
                       var user_row ="";
                          $.each(json['primaries'], function(k, v) {
                                  user_row+="<option value='"+v.retailer_id+"'  selected>"+v.retailer_name+"("+v.phone+")"+"</option>";
                                  
                               
                            });  
                          
                           document.getElementById('primariestotransfer').innerHTML="";
                           $("#primariestotransfer").append(user_row);
                           $("#primariestotransfer").select2();
                          

                           var user_row1 ="";
                          $.each(json['to'], function(k, v) {
                                  user_row1+="<option value='"+v.retailer_id+"'>"+v.retailer_name+"("+v.phone+")"+"</option>";
                                  
                               
                            });  
                          
                           document.getElementById('to').innerHTML="";
                           $("#to").append(user_row1);
                           $("#primariestotransfer").select2();
                           $("#modallinkstransfer").modal();

                           $("#fromname").val(json['fromname']);
                           $("#fromid").val(json['fromid']);

                                        
                                },
                             error:function(data){

                                 new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

                   });
                }
                function unlinkprimary(id)
                {
                    var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':7};
                     var url = base_url + "user/assign_user_type";
                     $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         new PNotify({
                                    text: data,
                                    addclass: 'bg-success'
                                });
                                        
                                },
                       error:function(data){

                                  console.log(data);
                       }        

                        });
                }
                 function viewagentsales(id)
                {
                  var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':6};
                var url = base_url + "user/assign_user_type";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var msg=   JSON.parse(data);
                         new PNotify({
                                    text: msg['message'],
                                    addclass: 'bg-success'
                                });
                                        
                                },
                       error:function(data){
                               new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

        });
                }
                 function viewagentprimaries(id)
                {
                  var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':6};
                var url = base_url + "user/linkedprimaries";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                          var user_row="";
                          var pno=0;
                          $.each(JSON.parse(data), function(k, v) {
                            pno=pno+1;
                                  user_row+="<tr><td>"+pno+"</td><td>"+v.retailer_name+"</td><td>"+v.phone+"</td><td>"+v.dc+"</td></tr>";
                               
                            });  
                           user_row+="";
                           document.getElementById('primariestable').innerHTML="";
                           $("#primariestable").append(user_row);
                            $("#modalprimaries").modal();
                            $("#example2").DataTable();
                        
                                        
                                },
                       error:function(data){
                               new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

        });
                }
               
               
                
                
               

    </script>

</head>

<body>

<?php include("../topbar.php") ?>


<!-- Page container -->
<div class="page-container">
   <!-- Vertical form modal -->
                        <div id="modalprimaries" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">lInk Primaries</h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table datatable-basic" id="example2">
                        <thead>
                        <tr>

                            <th>#</th>
                            <th>Name</th>
                            <th>Phone</th>
                              <th>Distribution Center</th>
                            
                        </tr>
                        </thead>
                        <tbody id="primariestable">
                            
                        </tbody>

                    </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->
                        <!-- Vertical form modal -->
                        <div id="modaltransactions" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title" id="agentnametransaction">Agent Transactions</h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table datatable-basic" id="example3">
                        <thead>
                        <tr>
                            <th>#Order Id</th>
                            <th>Retailer</th>
                            <th>Phone</th>
                            <th>Distribution Center</th>
                        </tr>
                        </thead>
                        <tbody id="agentnametransactions">
                            
                        </tbody>

                    </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->
  <!-- Vertical form modal -->
                        <div id="modalinkclerkdc" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Link The Clerk to DC</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formlinkclerktodc" enctype="multipart/form-data">
                                        <input type="hidden" name="clerk" id="clerk" value="" />
                                        <input type="hidden" name="access_token" id="catToken"/>
                                        <div class="modal-body">
                                            <div class="form-group">
<label>Select Supplier</label>
               <select class="js-example-basic-multiple" id="supplier" name="">
  
               </select>                            

                                            </div>
                                           
                                            

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="formbtlinkclerktodc" value="add">Submit Links</button>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->
                         <!-- Vertical form modal -->
                        <div id="modallinkstransfer" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Primary Customers' Transfer</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formtransferprimaries" enctype="multipart/form-data">
                                        <input type="hidden" name="agent" id="agent" value="" />
                                        <input type="hidden" name="access_token" id="catToken"/>
                                        <div class="modal-body">
                                          <div class="form-group">
                                            <label>From:</label>
                                                 <input type="text" id="fromname" value="" class="form-control">
                                                 <input type="text" id="fromid" value="" class="form-control">
                                            </div>

                                          <div class="form-group">
                                            <label>To:</label>
                                                 <select class="js-example-basic-multiple" id="to" name="to">
                                                 </select>
                                            </div>
                                            <div class="form-group">
                                              <label>Transfers:</label>
                                                 <select class="js-example-basic-multiple" id="primariestotransfer" name="primariestotransfer[]" multiple="multiple">
                                                 </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Complete Transfer</button>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->

                <!-- /user menu -->

                <?php include("../sidebar.php") ?>
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Dukapepe User Manager</span>
                        </h4>
                    </div>

                </div>


            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">
                <div class="content clearfix">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">All Users In the system</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body table-responsive">
                                <!--<a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Orders</a>-->
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            


                          
                      <table class="table datatable-basic" id="example" style="margin-bottom: 150px;">
                        <thead>
                        <tr>

                            <th>#</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="userstablebody">
                            
                        </tbody>

                    </table>
                    </div>
                        </div>
                    </div>
                </div>
                    

                <!-- Basic datatable -->
                <!-- <div class="panel ">
                   


                    <table class="table datatable-basic" id="example">
                        <thead>
                        <tr>

                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Approved</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="userstablebody">
                            
                        </tbody>

                    </table>
                </div> -->
                <!-- /basic datatable -->

              


               


               
               


               


                <!-- Footer -->
                <div class="footer text-muted">
                    <?php include("../footer.php") ?>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
    <script type="text/javascript">
      
       $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
    </script>
   

</div>
<!-- /page container -->

</body>
</html>
