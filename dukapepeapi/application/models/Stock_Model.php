<?php
date_default_timezone_set("UTC");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Stock_Model
 *
 * @author mwamb
 */
class Stock_Model extends CI_Model{
    //put your code here
    public $stock='stock';
    public $stock_history='stock_history';
    public $price_history='price_history';
     public function updatestockDetails($data,$stock)
  {
          // $this->db->set('product_id', 'points+'.$points, FALSE);
          $this->db->where('stock_id', $stock);
          if($this->db->update('stock',$data))
          {
           $response['message']="Stock updated Successfully";
           $response['code']=1;
           $response['code']=$data;
           return json_encode($response);
          }
          else
          {
           $response['message']="Stock has not been updated.Please Try again.";
           $response['code']=0;
           return json_encode($response); 
          }
  }
    //0 stock changes
    public function closingstock()
    {
              $this->db->select('*');
              $this->db->from('suppliers');
              $query1=$this->db->get();
             
              foreach ($query1->result_array() as $row)
              { 
                 $this->db->select('SUM(unit_bp*quantity) as closingstock');
                 $this->db->from('stock');
                 $this->db->where('stock.supplier_id',$row['supplier_id']);
                 $queryordervat=$this->db->get();
                 $datathis=$queryordervat->result_array();
                 $total_stock=$datathis[0]['closingstock'];
                 $closingstock['supplier_id']=$row['supplier_id'];
                 $closingstock['amount']=$total_stock;
                 $this->db->insert('closingstocks',$closingstock);

              }
    }
    
    //0 stock changes
    public function fetchitem($passed_data)
    {
          $this->db->select('stock_id,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,stock.unit_bp as unit_bp,unit_cost,vat,quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
        $this->db->where('stock_id',$passed_data['stock_id']);
        $this->db->from($this->stock);
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        $returned_data= $this->db->get();
        $data['itemdetails']=$returned_data->result_array();

        $this->db->select('*');
        $this->db->from('products');
        $returned_dataproducts= $this->db->get();
        $data['products']=$returned_dataproducts->result_array();

        $this->db->select('*');
        $this->db->from('skus');
        $returned_dataskus= $this->db->get();
        $data['skus']=$returned_dataskus->result_array();


        return json_encode($data);
    }
    public function new_purchase($data)
    {
       $i=0;
       $supplier = $this->getclerksupplier($data['retaier']);
       $from =$data[''];
        while($i<sizeof($data['stock_id']))
        {
          $store['stock_id']=$data['stock_id'][$i];
          $store['quantity']=$data['quantity'][$i];
          $store['unit_bp']=$data['bp'][$i];
          $store['unit_cost']=$data['sp'][$i];
          $store['purchase_id']=date('Y-m-d G:i:s').$data['stock_id'].$data['retaier'];
          $store['supplier_id']=$supplier;
          $store['from']= $from;
          $store['clerk_id']=$data['retaier'];
          $this->db->insert('purchase_history',$store);
          $log['purchase_id'] =  $store['purchase_id'];
          $log['action'] = "Add";
          $log['action_by'] =$store['clerk_id'];
          $this->db->insert('purchase_logs',$log);
          $this->convertpurchasetostock($store['stock_id'],$store['quantity'],$store['unit_bp'],$store['unit_cost']);
          
         
          $i++;
        }
        $response['code']=1;
        $response['message']="Purchases recorded successfully";
        return json_encode($response);    }
    public function getclerksupplier($clerk)
    {   
      
         $this->db->select('supplier_id');
         $this->db->from('clerk_dc');
         $this->db->where('clerk_id',$clerk);
         $getresult= $this->db->get();
         $retailer=$getresult->result_array();
         return $retailer[0]['supplier_id'];
    }
    public function convertpurchasetostock($stock,$quantity,$bp,$sp)
    {   
          $this->db->set('quantity', 'quantity+'.$quantity, FALSE);
          $this->db->set('unit_bp', $bp, FALSE);
          $this->db->set('unit_cost', $sp, FALSE);
          $this->db->where('stock_id', $stock);
          $this->db->update('stock');
        
    }
    public function seetransactionsitems($data)
    {
           $this->db->select('current_transactions_items.stock_id,products.product_name,current_transactions_items.quantity,skus.sku_name,current_transactions_items.timestamp');
           $this->db->from('current_transactions_items');
           $this->db->join('stock','stock.stock_id=current_transactions_items.stock_id');
           $this->db->join('products','products.product_id=stock.product_id');
            $this->db->join('skus','skus.sku_id=stock.sku_id');
           $this->db->where('retailer_id',$data['retailer']);
           $getresult= $this->db->get();
           $response['transactionsitems']=$getresult->result_array();
            $response['retailer']=$data['retailer'];
           return $response;
    }
    public function get_all_activetransactions($data)
    {
            $this->db->select('supplier_id');
            $this->db->where('retailer_id',$data['retailer']);
            $this->db->from('retailer_suppliers');
            $returned_data= $this->db->get();
            $dataa=$returned_data->result_array();
            $supplier_id=$dataa[0]['supplier_id'];
            $response['supplier']=$supplier_id;
           $this->db->select('order_transactions.id,retailers.retailer_name,retailers.retailer_id,retailers.phone,order_transactions.timestamp');
           $this->db->from('order_transactions');
           $this->db->join('retailer_suppliers','retailer_suppliers.retailer_id=order_transactions.retailer_id');
           $this->db->join('retailers','retailers.retailer_id=order_transactions.retailer_id');
           $this->db->where('retailer_suppliers.supplier_id',$response['supplier']);
           $getresult= $this->db->get();
           $response['transactions']=$getresult->result_array();
           return $response;

    }
     public function dateDiff($time1, $time2, $precision = 6) {
   
    // If not numeric then convert texts to unix timestamps
    if (!is_int($time1)) {
      $time1 = strtotime($time1);
    }
    if (!is_int($time2)) {
      $time2 = strtotime($time2);
    }

    // If time1 is bigger than time2
    // Then swap time1 and time2
    if ($time1 > $time2) {
      $ttime = $time1;
      $time1 = $time2;
      $time2 = $ttime;
    }

    // Set up intervals and diffs arrays
    $intervals = array('year','month','day','hour','minute','second');
    $diffs = array();

    // Loop thru all intervals
    foreach ($intervals as $interval) {
      // Create temp time from time1 and interval
      $ttime = strtotime('+1 ' . $interval, $time1);
      // Set initial values
      $add = 1;
      $looped = 0;
      // Loop until temp time is smaller than time2
      while ($time2 >= $ttime) {
        // Create new temp time from time1 and interval
        $add++;
        $ttime = strtotime("+" . $add . " " . $interval, $time1);
        $looped++;
      }
 
      $time1 = strtotime("+" . $looped . " " . $interval, $time1);
      $diffs[$interval] = $looped;
    }
    
    $count = 0;
    $times = array();
    // Loop thru all diffs
    foreach ($diffs as $interval => $value) {
      // Break if we have needed precission
      if ($count >= $precision) {
        break;
      }
      // Add value and interval 
      // if value is bigger than 0
      if ($value > 0) {
        // Add s if value is not 1
        if ($value != 1) {
          $interval .= "s";
        }
        // Add value and interval to times array
        $times[] = $value . " " . $interval;
        $count++;
      }
    }

    // Return string with times
    return implode(", ", $times);
  }
    public function return_all_on_closure($order_data)
    {   
        $stock_data=$order_data['order_details'];
        $i=0;
        while($i<count($stock_data))
        {
              
           $this->db->where('retailer_id', $data["retailer_id"]);
           $this->db->where('stock_id', $data["stock_id"]);
           $this->db->delete('current_transactions_items');
               $this->db->select('*');
               $this->db->where('retailer_id', $data["retailer_id"]);
               $this->db->from('current_transactions_items');
               if($this->db->count_all_results()==0)
                  {
                   $this->db->where('retailer_id', $data["retailer_id"]);
                   $this->db->delete('order_transactions');
                  }
                                          
                  
        }
    
    }
    public function get_qty_stock_by_id($data)
    {
       //find if the item is avaailable.First count from order transactions where stock id matches.first fetch quantity in 
       //the transactions items table.secondly fetch quantity in the stock table. If transaction = stock return 0 .if transaction<stock return proceed.
      $this->db->select_sum('quantity');
      $this->db->where('stock_id',$data['stock_id']);
      $this->db->from('current_transactions_items');
      $query=$this->db->get();
      $qtytransaction=(int)$query->row()->quantity;

      $this->db->select('quantity');
      $this->db->where('stock_id',$data['stock_id']);
      $this->db->from('stock');
      $query=$this->db->get();
      $qtystock=(int)$query->row()->quantity;
      //if stock and transaction qty are equal
      if($qtytransaction==$qtystock)
      {
         return "zero";
      }
      //if stockqty < transactionqty
      else if($qtytransaction<$qtystock)
      {
             //check if it is the first item from that customer
               $this->db->select('*');
               $this->db->where('retailer_id',$data['retailer_id']);
               $this->db->from('order_transactions');
               $first_item=$this->db->count_all_results();
               //if it is not the first item in the cart for that tetailer
                if($first_item>0)
                 {            $this->db->select('*');
                               $this->db->where('retailer_id',$data['retailer_id']);
                               $this->db->from('order_transactions');
                                $querytrans=$this->db->get();
                               $returnedtrans =  $querytrans->result_array(); 
                         //get quantity if item available for the retailer available 
                           $this->db->select('quantity');
                           $this->db->where('stock_id',$data['stock_id']);
                           $this->db->where('retailer_id', $data["retailer_id"]);
                           $this->db->from('current_transactions_items');
                           $itemavailable=$this->db->count_all_results();
                           //if it is  available
                            if($itemavailable==1)
                             {  
                               
                                     // $returned_data_cart_item= $this->db->get();
                                     $this->db->select('quantity');
                                     $this->db->where('stock_id',$data['stock_id']);
                                     $this->db->where('retailer_id', $data["retailer_id"]);
                                     $this->db->from('current_transactions_items');
                                     $quanty_returned=$this->db->get();
                                     $qty_returned=$quanty_returned->result_array();
                                    
                                     $qty_amt=0;
                                     $qty_amt=(int)$qty_returned[0]['quantity'];
                                     $qty_amt=$qty_amt+1;
                                       $dataup = array(
                                             'quantity' => $qty_amt
                                             );
                                      $this->db->where('retailer_id', $data["retailer_id"]);
                                      $this->db->where('stock_id', $data["stock_id"]);
                                     $this->db->update('current_transactions_items',  $dataup);

                                      $dataup1 = array(
                                             'timestamp' => date('Y-m-d G:i:s')
                                             );
                                      $this->db->where('retailer_id', $data["retailer_id"]);
                                      
                                     $this->db->update('order_transactions',  $dataup1);
                                     // return $response[0]['quantity'];
                                      return "proceed"; 
                              }
                           ///if it is  available
                              //if item not available in the cart
                               else if($itemavailable==0)
                                 {
                                      $data = array(
                                      'retailer_id' => $data["retailer_id"],
                                      'stock_id' => $data["stock_id"],
                                      'quantity' => 1,
                                      'trans_id' =>$returnedtrans[0]['id']
                                       );
                                      $this->db->insert('current_transactions_items', $data);
                                      $dataup1 = array(
                                               'timestamp' => date('Y-m-d G:i:s')
                                               );
                                      $this->db->where('retailer_id', $data["retailer_id"]);
                                      $this->db->update('order_transactions',  $dataup1);
                                      return "proceed"; 
                                 }
                              ///if item not available in the cart
                 }
                else if($first_item==0)
                 {
                       $dataa = array(
                          'retailer_id' => $data["retailer_id"]
                          );
                         $this->db->insert('order_transactions', $dataa); 
                               $this->db->select('*');
                               $this->db->where('retailer_id',$data['retailer_id']);
                               $this->db->from('order_transactions');
                               $querytrans=$this->db->get();
                               $returnedtrans =  $querytrans->result_array(); 
                         $dataitem = array(
                          'retailer_id' => $data["retailer_id"],
                          'stock_id' => $data["stock_id"],
                          'quantity' => 1,
                          'trans_id' =>$returnedtrans[0]['id']
                           );
                         $this->db->insert('current_transactions_items', $dataitem); 
                          return "proceed";

                 }
               ///if it is the first item in the cart for that tetailer
             ///check if it is the first item from that customer
      }
      //if available check if the customer is doing first item placement and if that is true create atransaction

      //add item to transactions items table
        
    }
    public function return_qty_to_stock($data)
    {


          $this->db->set('quantity', 'quantity-1', FALSE);
          $this->db->where('stock_id', $data['stock_id']);
          $this->db->where('retailer_id', $data['retailer_id']);
          $this->db->update('current_transactions_items');

            $this->db->select('quantity');
            $this->db->where('stock_id',$data['stock_id']);
            $this->db->where('retailer_id', $data["retailer_id"]);
            $this->db->from('current_transactions_items');
            $quanty_returned=$this->db->get();
            $qty_returned=$quanty_returned->result_array();
            $qty_amt=0;
            $qty_amt=(int)$qty_returned[0]['quantity'];
             

             $this->db->select_sum('quantity');
              $this->db->where('stock_id',$data['stock_id']);
              $this->db->from('current_transactions_items');
              $query=$this->db->get();
              $qtytransaction=(int)$query->row()->quantity;

              $this->db->select('quantity');
              $this->db->where('stock_id',$data['stock_id']);
              $this->db->from('stock');
              $query=$this->db->get();
              $qtystock=(int)$query->row()->quantity;
              $remaining= $qtystock-$qtytransaction;
                if($remaining<0)
                {
                  $remaining=0;
                }
                         if($qty_amt==0)
                               {
                                      $this->db->where('retailer_id', $data["retailer_id"]);
                                      $this->db->where('stock_id', $data["stock_id"]);
                                      $this->db->delete('current_transactions_items');
                                      
                                      $this->db->select('*');
                                      $this->db->where('retailer_id', $data["retailer_id"]);
                                      $this->db->from('current_transactions_items');
                                      if($this->db->count_all_results()==0)
                                      {
                                           $this->db->where('retailer_id', $data["retailer_id"]);
                                           $this->db->delete('order_transactions');
                                      }
                                      return $remaining;
                                      
                                     
                               }
                               else if($qty_amt>0)
                               {
                                     $dataup = array(
                                     'quantity' => $qty_amt
                                     );
                                    $this->db->where('retailer_id', $data["retailer_id"]);
                                    $this->db->where('stock_id', $data["stock_id"]);
                                    $this->db->update('current_transactions_items',  $dataup);
                                    return $remaining;
                               }


             //

     
    }
    public function clearcache()
    {
    $this->db->select('*');
    $this->db->from('order_transactions');
    $query1=$this->db->get();
    $response=0;
    foreach ($query1->result_array() as $row)
    { 
     $now = date('Y-m-d G:i:s');
     $diff = $row['timestamp'];
     $timeFirst  = strtotime($row['timestamp']);
     $timeSecond = strtotime(date('Y-m-d G:i:s'));
     $response = $timeSecond - $timeFirst;
     if($response>5)
     {
        $this->db->where('trans_id', $row['id']);
        $this->db->delete('current_transactions_items');
        $this->db->where('id', $row['id']);
        $this->db->delete('order_transactions');

     }
    
    }
    return json_encode($response);
    }
     public function return_all_to_store($data)
   
    {
           $this->db->where('retailer_id', $data["retailer_id"]);
           $this->db->where('stock_id', $data["stock_id"]);
               $this->db->delete('current_transactions_items');
               $this->db->select('*');
               $this->db->where('retailer_id', $data["retailer_id"]);
               $this->db->from('current_transactions_items');
               if($this->db->count_all_results()==0)
                  {
                   $this->db->where('retailer_id', $data["retailer_id"]);
                   $this->db->delete('order_transactions');
                  }  
              $this->db->select_sum('quantity');
              $this->db->where('stock_id',$data['stock_id']);
              $this->db->from('current_transactions_items');
              $query=$this->db->get();
              $qtytransaction=(int)$query->row()->quantity;

              $this->db->select('quantity');
              $this->db->where('stock_id',$data['stock_id']);
              $this->db->from('stock');
              $query=$this->db->get();
              $qtystock=(int)$query->row()->quantity;
              $remaining= $qtystock-$qtytransaction;
                if($remaining<0)
                {
                  $remaining=0;
                }
                return $remaining;
                      
    }
    
    ///0 stock changes
    
    
    
    public function new_product($data){
        $response=array();
        $count= $this->checkNew($data);
        if($count==0){
            $this->db->insert($this->stock,$data);
            // add to item history
            $stock_id= $this->getStockId($data);
            $history_data=array();
            $history_data['stock_id']=$stock_id;
            $history_data['quantity']=$data['quantity'];
            $this->add_stock($history_data);
            //price history
            $price_data=array();
            $price_data['stock_id']=$stock_id;
            $price_data['unit_bp']=$data['unit_bp'];
            $price_data['unit_cost']=$data['unit_cost'];
            $price_data['vat']=$data['vat'];
            $this->add_price($price_data);
            
            $response['message']="Product added to stock successfully.";
            $response['code']=1;
        }
        else{
         $response['message']="Similar product exist under same supplier and skus, Check ant try again.";
         $response['code']=0;   
        }
        return json_encode($response);
    }
    
    public function getStockId($data){
        $this->db->select('stock_id');
        $this->db->where('product_id',$data['product_id']);
        $this->db->where('sku_id',$data['sku_id']);
        $this->db->where('supplier_id',$data['supplier_id']);
        $this->db->from($this->stock);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return $response[0]['stock_id'];
    }

    public function checkNew($data){
        $this->db->select('stock_id');
        $this->db->where('product_id',$data['product_id']);
        $this->db->where('sku_id',$data['sku_id']);
        $this->db->where('supplier_id',$data['supplier_id']);
        $this->db->from($this->stock);
        $count=$this->db->count_all_results();
     return $count;
    }
    
    public function add_stock($data){
    $this->db->insert($this->stock_history,$data);
    }
    public function add_price($data){
    $this->db->insert($this->price_history,$data);
    }
    public function currentStock($data){
        $this->db->select('quantity');
        $this->db->where('stock_id',$data['stock_id']);
        $this->db->from($this->stock);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return $response[0]['quantity'];
    }
    
    public function new_stock($data){
       $current_stock= $this->currentStock($data);
       $total_stock=$current_stock+$data['quantity'];
       
       //add to history
       $this->add_stock($data);
       //update stock
       $stock=array();
       $stock['quantity']=$total_stock;
       $this->db->where('stock_id',$data['stock_id']);
       $this->db->update($this->stock,$stock);
       $response=array();
       $response['message']="Stock updated successfully.";
       $response['code']=1;
       
       return json_encode($response);
    }
    public function update_bp($data){
        // $this->db->insert($this->price_history,$data);
        // update price 
        $stock_id=$data['stock_id'];
        unset($data['stock_id']);
       $this->db->where('stock_id',$stock_id);
       $this->db->update($this->stock,$data);
       $rows= $this->db->affected_rows();
       $response=array();
       if($rows>0){
       $response['message']="Buying Price updated successfully.";
       $response['code']=$rows;
       }
       else{
       $response['message']="Error while updating price, unknown stock item.";
       $response['code']=$rows;    
       }
       
       return json_encode($response);
    }
    public function getSkuFactor($larger_sku)
    {   
      
         $this->db->select('factor');
         $this->db->from('skus_relationships');
         $this->db->where('larger_sku',$larger_sku);
         $getresult= $this->db->get();
         $retailer=$getresult->result_array();
         return $retailer[0]['factor'];
    }
     public function divide_large_sku($data){

        $this->db->select('smaller_sku,factor,stock.product_id,stock.supplier_id,stock.quantity,stock.unit_bp');
        $this->db->where('larger_sku',$data['sku_id']);
        $this->db->where('stock.product_id',$data['product_id']);
        $this->db->where('stock.supplier_id',$data['supplier_id']);
        $this->db->from('skus_relationships');
        $this->db->join('stock','stock.sku_id=skus_relationships.larger_sku');
        $returned_data= $this->db->get();

        $rowC = $returned_data->row();
        if(isset($rowC))
        {
          $response=$returned_data->result_array();

          $factor = $response[0]['factor'];
          $smaller_sku = $response[0]['smaller_sku'];

          $product_id = $data['product_id'];
          $supplier_id = $data['supplier_id'];
          $larger_sku = $data['sku_id'];

          $available_quantity = $response[0]['quantity'];
          $quantity_passed = $data['divideQty'];
          $selling_price = $data['selling_price'];
          $buying_price = $response[0]['unit_bp'];
          $buying_price = ($buying_price / $factor);


          $finalQuantity = ($quantity_passed * $factor);

          $this->db->set('quantity', 'quantity+'. (int) $finalQuantity, FALSE);
          $this->db->set('unit_cost',$selling_price);
          $this->db->set('unit_bp',round($buying_price));
          $where = array('sku_id' => $smaller_sku, 'product_id' => $product_id, 'supplier_id' => $supplier_id);
          $this->db->where($where);
          $this->db->update('stock');

          $this->db->set('quantity', 'quantity-'. (int) $quantity_passed, FALSE);
          $where = array('sku_id' => $larger_sku, 'product_id' => $product_id, 'supplier_id' => $supplier_id);
          $this->db->where($where);
          $this->db->update('stock');

          $rows= $this->db->affected_rows();
          $updateresponse=array();
          if($rows>0){
          $updateresponse['message']="Quantity divided to smaller SKU successfully.";
          $updateresponse['code']=1;
          }
          else{
          $updateresponse['message']="Error while dividing to smaller SKU.";
          $updateresponse['code']=0;    
          }
        }
        else{
          $updateresponse['message']="This SKU does not have a small SKU relationship, set the relationship first to continue.";
          $updateresponse['code']=0;
        }

        return json_encode($updateresponse); 
    }

    public function combine_small_sku($data)
    {
        $this->db->select('larger_sku,factor,stock.product_id,stock.supplier_id,stock.quantity');
        $this->db->where('smaller_sku',$data['sku_id']);
        $this->db->where('stock.product_id',$data['product_id']);
        $this->db->where('stock.supplier_id',$data['supplier_id']);
        $this->db->from('skus_relationships');
        $this->db->join('stock','stock.sku_id=skus_relationships.smaller_sku');
        $returned_data= $this->db->get();

        //check whether this sku has a large sku relationship
        $rowC = $returned_data->row();
        if(isset($rowC))
        {
          $response=$returned_data->result_array();
          $factor = $response[0]['factor'];
          $larger_sku = $response[0]['larger_sku'];

          $product_id = $data['product_id'];
          $supplier_id = $data['supplier_id'];
          $smaller_sku = $data['sku_id'];
          $available_quantity = $response[0]['quantity'];
          $quantity_passed = $data['quantity_to_combine'];

          $finalQuantity = ($quantity_passed / $factor);
          //checking if combining the given skus gives a whole number for large sku
          $check_mod = $quantity_passed % $factor;
          if($check_mod != 0)
          {
            $updateresponse['message']="Kindly use multiples of ".$factor." to get a whole number for larger SKU";
            $updateresponse['code']=0;
          }
          else
          {
            $this->db->set('quantity', 'quantity+'. (int) $finalQuantity, FALSE);
            $where = array('sku_id' => $larger_sku, 'product_id' => $product_id, 'supplier_id' => $supplier_id);
            $this->db->where($where);
            $this->db->update('stock');

            $this->db->set('quantity', 'quantity-'. (int) $quantity_passed, FALSE);
            $where = array('sku_id' => $smaller_sku, 'product_id' => $product_id, 'supplier_id' => $supplier_id);
            $this->db->where($where);
            $this->db->update('stock');
            $rows= $this->db->affected_rows();
            $updateresponse=array();

            if($rows>0){
            $updateresponse['message']="Quantity combined to large SKU successfully.";
            $updateresponse['code']=1;
            }
            else{
            $updateresponse['message']="Error while combining to large SKU.";
            $updateresponse['code']=0;    
            }
          }
        }
        else{
          $updateresponse['message']="This SKU does not have a large SKU relationship, set the relationship first to continue.";
          $updateresponse['code']=0;
        }

        return json_encode($updateresponse);
    }

    public function update_price($data){
        $this->db->insert($this->price_history,$data);
        // update price 
        $stock_id=$data['stock_id'];
        unset($data['stock_id']);
       $this->db->where('stock_id',$stock_id);
       $this->db->update($this->stock,$data);
       $rows= $this->db->affected_rows();
       $response=array();
       if($rows>0){
       $response['message']="Price updated successfully.";
       $response['code']=1;
       }
       else{
       $response['message']="Error while updating price, unknown stock item.";
       $response['code']=0;    
       }
       
       return json_encode($response);
    }
    public function supplier_stockdc($passed_data)
    {

           $this->db->select('supplier_id');
           $this->db->where('clerk_id',$passed_data['retailer']);
           $this->db->from('clerk_dc');
           $returned_data= $this->db->get();
           $dataa=$returned_data->result_array();
         
          $supplier_id=$dataa[0]['supplier_id'];
          $response['supplier']=$supplier_id;
           
        $this->db->select('stock_id,stock.unit_bp AS unit_bp,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,unit_cost,vat,quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
        $this->db->where($this->stock.'.supplier_id',$response['supplier']);
        $this->db->from($this->stock);
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        
        //get reviews
        $reviews = $this->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        
        $i++;
        }
        return json_encode($data); 
    }
    public function updatepurchaseload($data)
    {
         $this->db->select('*');
         $this->db->where('purchase_history.id',$data['purchase']);
         $this->db->from('purchase_history');
         $returned_data= $this->db->get();
         $data=$returned_data->result_array();
         return json_encode($data[0]); 
    }
    public function updatepurchase($data,$purchase,$old,$stock)
    {  $response=array();
          $this->db->where('id',$purchase);
          if($this->db->update('purchase_history',$data))
          {  

                                     $difference= $data['quantity']-$old;
                                     if($difference>0)
                                     {
                                                
                                                $this->db->set('quantity', 'quantity+'.$difference, FALSE);
                                                $this->db->set('unit_bp', $data['unit_bp'], FALSE);
                                                $this->db->set('unit_cost',$data['unit_cost'], FALSE);
                                                $this->db->where('stock_id', $stock);
                                                if($this->db->update('stock'))
                                                {
                                                   $response['code']=1;
                                                   $response['message']="Purchase record edited successfully";
                                                }
                                  
                                    }
                                     else
                                     {
                                               
                                                $this->db->set('quantity', 'quantity+'.$difference, FALSE);
                                                 $this->db->set('unit_bp', $data['unit_bp'], FALSE);
                                                $this->db->set('unit_cost',$data['unit_cost'], FALSE);
                                                $this->db->where('stock_id', $stock);
                                                if($this->db->update('stock'))
                                                {
                                                    $response['code']=1;
                                                   $response['message']="Purchase record edited successfully";
                                                }
                                     }
            
          
          }
              else
              {
               $response['code']=0;
               $response['message']="Purchase record not edited successfully.Please try again"; 
              }
              return json_encode($response);
            }
    public function supplier_purchasesdc($passed_data)
    {
           
        $this->db->select('purchase_history.id,purchase_history.stock_id,purchase_history.unit_bp AS unit_bp,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,purchase_history.unit_cost,vat,purchase_history.quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
        $this->db->where('purchase_history.clerk_id',$passed_data['retailer']);
        $this->db->from('purchase_history');
        $this->db->join('stock','stock.stock_id=purchase_history.stock_id');
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        
        //get reviews
        $reviews = $this->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        
        $i++;
        }
        return json_encode($data); 
    }
    public function supplier_stock($passed_data){
        $this->db->select('stock_id,stock.unit_bp AS unit_bp,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,unit_cost,vat,quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
        $this->db->where($this->stock.'.supplier_id',$passed_data['supplier_id']);
        $this->db->from($this->stock);
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        
        //get reviews
        $reviews = $this->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        
        $i++;
        }
        return json_encode($data);
    }
    public function get_stock($passed_data){
        $this->db->select('stock_id,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,unit_cost,stock.vat AS vat,quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,sub_categories.sub_cat_id AS sub_category_id,sub_categories.sub_cat_name AS sub_category_name,'
                . 'supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
        $this->db->where('products.sub_cat_id',$passed_data['sub_category_id']);
        if(isset($passed_data['retailer_id'])){
           $this->db->where('retailer_suppliers.retailer_id',$passed_data['retailer_id']);   
        }
$this->db->where('stock.quantity >',0);
$this->db->where('stock.quantity >',0);
        $this->db->from($this->stock);
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('sub_categories','sub_categories.sub_cat_id=products.sub_cat_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        if(isset($passed_data['retailer_id'])){
        $this->db->join('retailer_suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id');
        }
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        
        //get reviews
        $reviews = $this->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        
        $i++;
        }
        return json_encode($data);
    }
    public function fetch($passed_data){
        $this->db->select('stock_id,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,stock.unit_bp as unit_bp,unit_cost,vat,quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
        $this->db->where('stock_id',$passed_data['stock_id']);
        if(isset($passed_data['retailer_id'])){
           $this->db->where('retailer_suppliers.retailer_id',$passed_data['retailer_id']);   
       }
        $this->db->from($this->stock);
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        
        if(isset($passed_data['retailer_id'])){
        $this->db->join('retailer_suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id');
        }
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
            // product images
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        //get reviews
        $reviews = $this->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        
        $i++;
        }
        return json_encode($data);
    }
    public function fetch_all($input){
       $this->db->select('stock_id,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,stock.unit_bp as unit_bp,unit_cost,vat,quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
       if(isset($input['retailer_id'])){
           $this->db->where('retailer_suppliers.retailer_id',$input['retailer_id']);   
       }
        $this->db->from($this->stock);
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        if(isset($input['retailer_id'])){
        $this->db->join('retailer_suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id');
        }
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
            // images
        $images = $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
            //get reviews
        $reviews = $this->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        $i++;
        }
        return json_encode($data); 
    }
    public function fetch_stock_history($data){
        $this->db->select('quantity,timestamp');
        $this->db->where('stock_id',$data['stock_id']);
        $this->db->from($this->stock_history);
        $this->db->order_by('timestamp DESC');
        $returned_data=$this->db->get();
        $response=$returned_data->result_array();
        return json_encode($response);
    }
    public function delete_stock_history($data){
      $response=array();
      $array_data= $this->stock_history_quantity($data);
      $quantity=$array_data[0]['quantity'];
      $data['stock_id']=$array_data[0]['stock_id'];
      $stock_quantity= $this->currentStock($data);
      if($quantity>$stock_quantity){
          $response['message']='Error, Products already issued.';
          $response['code']=0;
      }
      else{
          //update stock
          $q_array=array();
          $q_array['quantity']=$stock_quantity-$quantity;
          $this->db->where('stock_id',$data['stock_id']);
          $this->db->update($this->stock,$q_array);
          //stock history data
          $this->db->where('id',$data['id']);
          $this->db->delete($this->stock_history);
          $response['message']='Product quantities deleted and stock updated accordingly.';
          $response['code']=1;
      }
      return json_encode($response);
    }
    
    public function stock_history_quantity($data){
        $this->db->select('stock_id,quantity');
        $this->db->where('id',$data['id']);
        $this->db-from($this->stock_history);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return $response;
    }
    
    public function direct_update($data){
      $stock_id=$data['stock_id'];
      unset($data['stock_id']);
      
      $this->db->where('stock_id',$stock_id);
      $this->db->update($this->stock,$data);
      $rows = $this->db->affected_rows();
      $response=array();
      if($rows>0){
      $response['message']="Stock updated successfully.";
      $response['code']=1;
      }
      else{
      $response['message']="Failed to update stock quantity.";
      $response['code']=0;    
      }
      return json_encode($response);
    }
    
    
       public function recommended($check_array){
           // get common retailer shopped items    
       $this->db->select('stock.stock_id AS stock_id,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,stock.unit_cost AS unit_cost,ordered_products.vat AS vat,SUM(ordered_products.quantity) AS quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
        $this->db->where('orders.retailer_id',$check_array['retailer_id']);
        
        if(isset($check_array['retailer_id'])){
           $this->db->where('retailer_suppliers.retailer_id',$check_array['retailer_id']);   
        }
        
        $this->db->from($this->stock);
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('ordered_products','ordered_products.stock_id=stock.stock_id');
        $this->db->join('orders','ordered_products.order_id=orders.order_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
       
        if(isset($check_array['retailer_id'])){
        $this->db->join('retailer_suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id');
        } 
        
        $this->db->group_by('stock_id');
        $this->db->order_by('quantity','DESC');
        $this->db->limit(10);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
            // product_images
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        //get reviews
        $reviews = $this->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        
        $i++;
        }
        return json_encode($data);  
       }
       
    public function review($data){
        $review_data=$this->check_review($data);
        if(count($review_data)==0){ // insert as new entry
            $this->db->insert('reviews',$data); 
            $response['message'] = "Reviewed successfully.";
            $response['code'] = 1;
        }
        else{ // update entry
            $this->db->where('review_id',$review_data[0]['review_id']);
            $this->db->update('reviews',$data);
            $response['message'] = "Review updated successfully.";
            $response['code'] = 1;
        }
        return json_encode($response);
    }
    
    public function check_review($data){
        $this->db->select('review_id');
        $this->db->where('retailer_id',$data['retailer_id']);
        $this->db->where('stock_id',$data['stock_id']);
        $this->db->from('reviews');
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return $response;
    }
    
    
    public function get_reviews($stock_id){
        $this->db->select();
        $this->db->where('stock_id',$stock_id);
        $this->db->from('reviews');
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        
        return $response;
    }
    
    public function delete($stock_id){
     if($this->check_associated($stock_id)==0 && $this->check_associated2($stock_id)==0 && $this->check_associated3($stock_id)==0 ){
         $this->db->where('stock_id',$stock_id);
         $this->db->delete('stock');
         $num= $this->db->affected_rows();
     }
     else{
      $num=0;   
     }
        $response= $this->Status->response($num);
        
        return json_encode($response);
     }
     
     public function check_associated($stock_id){
     $this->db->select('order_id');
     $this->db->where('stock_id',$stock_id);
     $this->db->from('ordered_products');
     $num= $this->db->count_all_results();
     return $num;
     }
     public function check_associated2($stock_id){
     $this->db->select('promotion_id');
     $this->db->where('stock_id',$stock_id);
     $this->db->from('promotions');
     $num= $this->db->count_all_results();
     return $num;
     }
     
     public function check_associated3($stock_id){
     $this->db->select('review');
     $this->db->where('stock_id',$stock_id);
     $this->db->from('reviews');
     $num= $this->db->count_all_results();
     return $num;
     }
     
     public function update_vat($data){
       $stock_id = $data['stock_id'];
       
       unset($data['stock_id']);
       
       $this->db->where('stock_id',$stock_id);
       
       $this->db->update($this->stock,$data);
       $num = $this->db->affected_rows();
       
       if($num>0){
           $response['message'] = $num." Stock items updated.";
           $response['code'] = 1;
       }
       else{
          $response['message'] = "No record updated.";
          $response['code'] = 0; 
       }
       
       return json_encode($response);
     }
     
     
     public function dc_to_dc_transfer($data){
         $product_info = $this->dc_from($data);
         $rem = ($product_info['quantity'])-$data['no_transfer'];
         if($rem >= 0){
           // success operation
          $res1 = $this->update_from($data, $product_info);
           
          $res2 = $this->update_to($data, $product_info);
          
          if($res1['code']==1 && $res2['code']==1){
           $response['message'] =  "Succcess, transfer made successfully.";
            $response['code']   = 1;    
           }
           else{
            $response['message'] =  "Failed, error while transfering. Close all applications and restart.";
            $response['code']   = 0;   
           }
         }
         else{
             //fail operation
            $response['message'] =  "Failed, the stock is less than the number to be transferred.";
            $response['code']   = 0;
         }
         
         
         return json_encode($response);
     }
     
     
     public function dc_from($data){
         $res=0;
         $this->db->select('quantity,product_id,sku_id');
         $this->db->where('stock_id',$data['stock_id']);
//         $this->db->where('supplier_id',$data['from']);
         $this->db->from('stock');
         $returned_data= $this->db->get();
         $response=$returned_data->result_array();
         if(count($response)>0){
         $res=$response[0];
         }
         
         return $res;
     }
     
     
     public function update_from($passed_data,$product_info){
        $stock_id  = $passed_data['stock_id'];
        $rem=$product_info['quantity']-$passed_data['no_transfer'];
        
        $from['quantity'] = $rem;
        
        $this->db->where('stock_id',$stock_id);
        $this->db->update('stock',$from);
        $num = $this->db->affected_rows(); 
        if($num>0){
           $response['message'] = " Stock items updated.";
           $response['code'] = 1;
       }
       else{
            $response['message'] = "No changes detected";
          $response['code'] = 0; 
       
       }
       
       return $response;
     }
     
     
     public function update_to($passed_data, $product_info){
         $product_id  = $product_info['product_id'];
         $sku_id  = $product_info['sku_id'];
         $supplier_id  = $passed_data['to'];
         $rem=$passed_data['no_transfer'];
          $this->db->query('UPDATE stock SET quantity=quantity+'.$rem.' WHERE '
        . 'product_id='.$product_id.' && sku_id='.$sku_id.' && supplier_id='.$supplier_id );
        $num = $this->db->affected_rows(); 
        
        if($num>0){
           $response['message'] = " Stock items updated.";
           $response['code'] = 1;
       }
       else{
           // new insertion
           $new['product_id']= $product_id;
           $new['supplier_id']= $supplier_id;
           $new['sku_id']= $sku_id;
           $new['unit_cost']= 0;
           $new['vat']= 0;
           $new['quantity']= $rem;
           $new['is_active']= 1;
           
           $this->db->insert('stock',$new);
           
          $response['message'] = "Product inserted successfully.";
          $response['code'] = 0; 
       }
       
       return $response;
     }
}
