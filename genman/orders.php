<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Duka pepe | Orders</title>

    <?php include("../links.php") ?>

    <!-- /theme JS files -->
    <script>
        $(document).ready(function () {

            $(".status-progress").hide();
            $.extend($.fn.dataTable.defaults, {
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search orders:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                },
                drawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                },
                preDrawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                }
            });
            if (localStorage.getItem('code') == 1) {
                loadOrders();
            } else {

                new PNotify({
                    title: 'Primary notice',
                    text: 'Error: You are not authorised to view this page.',
                    addclass: 'bg-warning'
                });
            }


            Ordersadd();
            unitPriceUpdate();
            stockNew();
        });

        function loadOrders() {
            $(window).resize(function () {
                console.log($(window).height());
                $('.dataTables_scrollBody').css('height', ($(window).height() - 290));
            });
            console.log(localStorage.getItem('access_token'));
            var formData = {'status_id': '1', 'access_token': localStorage.getItem('access_token')};
            var url = base_url + "order/get_order_by_status";
            $('#example').DataTable({
                "destroy": true,
                "scrollX": true,
                "bJQueryUI": true,
                "fnDrawCallback": function () {
                    $('#example').closest(".dataTables_scrollBody").height(($(window).height() - 290));
                },


                "ajax": {
                    "url": url,
                    "data": formData,
                    "type": "post",
                    "dataSrc": function (json) {
                        return json;
                    },
                    "processing": true,
                    "serverSide": true,
                    "pagingType": "simple",
                    language: {
                        paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                    }


                }, "columns": [
                    {"data": "order_id"},
                    {"data": "retailer_name"},
                    {"data": "distribution_center"},
                    {"data": "phone"},
                    {"data": "email"},
                    {"data": "payment_method"},
                    {"data": "vat"},
                    {"data": "delivery_vat"},
                    {"data": "product_cost"},
                    {"data": "delivery_cost"},
                    {"data": "total_cost"},
                    {"data": "timestamp"},
                    {
                        "data": "payment_status_id",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, full, meta) {
                            if (data == 2) {

                                if (full['payment_method'].trim() == 'Cash') {
                                    return '<span class="label label-warning">Pending' + '</br>' + 'Cash Payment' + '</br>' + 'Confirmation</span>';
                                } else {
                                    return '<span class="label label-warning">Pending' + '</br>' + 'Till' + '</br>' +
                                        'Number' + '</br>' + 'Payment' + '</br>' + 'Confirmation</span>';
                                }


                            } else {
                                return '<span class="label label-primary">Paid</span>';
                            }

                        }
                    },
                    {
                        orderable: false, searchable: false, render: function (data, type, full, meta) {
                            var paid = null;
                            if (full['payment_status_id'] == 2) {
                                paid = '<li><a href="javascript: paymentChange(\'' + full['order_id'] + '\')">Confirm payment</a></li>';
                            } else {
                                paid = '<li></li>';
                            }
                            var delete_order = null;
                            if (full['payment_status_id'] == 2) {
                                delete_order = '<li><a href="javascript: deleteOrder(\'' + full['order_id'] + '\')">Delete</a></li>';
                            } else {
                                delete_order = '<li></li>';
                            }


                            var links = '<ul class="icons-list"><li class="dropdown kenn"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>\n\
                                        <ul class="bae dropdown-menu dropdown-menu-right">\n\
<li><a href="javascript: updateStatus(\'' + full['order_id'] + '\',2)">Packed</a></li>\n\
<li><a href="javascript: orderDetails(\'' + full['order_id'] + '\' , \'' + full['payment_method'] + '\')">Details</a></li>\n\
<li><a href="javascript: paymentDetails(\'' + full['order_id'] + '\', \'' + full['payment_method'] + '\')">Payment Details</a></li>\n\
' + paid + ' \n\
' + delete_order + '\n\
</ul></li></ul>'
                            return links;
                        }
                    }

                ]
            });
        }

        function paymentChange(order_id) {
            $(".status-progress").show();
            var url = base_url + "order/approve_payment";
            var formData = {
                'order_id': order_id,
                'access_token': localStorage.getItem('access_token')
            };
            $.post(url, formData, function (json) {
                //obj = JSON.parse(json);
                var data = JSON.parse(json);
                loadOrders();
                if (data['code'] == 1) {
                    new PNotify({
                        text: data['message'],
                        addclass: 'bg-success'
                    });
                } else {
                    new PNotify({
                        text: data['message'],
                        addclass: 'bg-success'
                    });
                }
                $(".status-progress").hide();
                //$('#modalPayments').modal('show');
            });
        }

        function paymentDetails(order_id, paymentType) {
            document.getElementById("typa").innerHTML = paymentType;
            $(".status-progress").show();
            var url = base_url + "order/fetch_mpesa_payment";
            var formData = {
                'order_id': order_id,
                'access_token': localStorage.getItem('access_token')
            };

            console.log(paymentType);
            $.post(url, formData, function (json) {
                obj = JSON.parse(json);
                var count = obj.length;
                for (i = 0; i < count; i++) {
                    var obj_data = obj[i];

                    console.log(obj_data);

                    document.getElementById("phone").innerHTML = obj_data['msisdn'];
                    document.getElementById("amount").innerHTML = obj_data['amount'];
                    document.getElementById("mpesa_trx_date").innerHTML = obj_data['mpesa_trx_date'];

                }
                $(".status-progress").hide();
                $('#modalPayments').modal('show');
            });
        }

        function deleteOrder(order_id) {
            $(".status-progress").show();
            var url = base_url + "order/delete_order";
            var formData = {
                'order_id': order_id,
                'access_token': localStorage.getItem('access_token')
            };
            $.post(url, formData, function (json) {
                obj = JSON.parse(json);
                $(".status-progress").hide();
                new PNotify({
                    text: obj['message'],
                    addclass: 'bg-success'
                });
                loadOrders();
            });
        }


        function orderDetails(order_id, paymentType) {

            $(".status-progress").show();
            var url = base_url + "/order/get_supplier_orders";
            var formData = {
                'order_id': order_id,
                'access_token': localStorage.getItem('access_token')
            };

            $.post(url, formData, function (json) {
                obj = JSON.parse(json);
                var count = obj.length;
                $('#tablemy tbody').html('');
                $("#tBody").empty();

                document.getElementById("type").invatnerHTML = paymentType;
                for (i = 0; i < count; i++) {
                    var obj_data = obj[i];
                    obj2 = obj_data['product_details'];
                    //alert(obj_data['vat']);
                    document.getElementById("vat").invatnerHTML = obj_data['vat'];
                    var count2 = obj_data['product_details'].length;
                    var trHTML = '';

                    total = 0;

                    // $.each(obj_data['product_details'], function (i, userData) {

                    for (j = 0; j < count2; j++) {
                        var obj_data2 = obj2[j];
                        total = total + parseInt(obj_data2['ordered_unit_cost']);
                        var image_url = "";
                        console.log(obj_data2['product_name']);
                        if ((obj_data2['image_url'].length) > 0) {
                            image_url = obj_data2['image_url'][0]['image_url'];

                        }


                        trHTML +=
                            '<tr><td>'
                            + '<img src="' + image_url + '" alt="No Image" style="width: 50px; height: 58px;">'
                            + '</td><td>'
                            + obj_data2['product_name'] + ' [' + obj_data2['sku_name'] + '] '
                            + '</td><td>'
                            + obj_data2['product_description']
                            + '</td><td>'
                            + obj_data2['supplier_code']
                            + '</td><td>'
                            + obj_data2['ordered_quantity']
                            + '</td><td>'
                            + obj_data2['ordered_unit_cost']
                            + '</td></tr>';

                    }

                    document.getElementById("total").innerHTML = total;
                    //});

                    $('#tBody').append(trHTML);
                }
                $(".status-progress").hide();
                $('#modalorderDetails').modal('show');
            });
        }


        function addOrders(id) {
            $(".status-progress").show();
            var url = base_url + "stock/fetch";
            var formData = {
                'stock_id': id,
                'access_token': localStorage.getItem('access_token')
            };
            $.post(url, formData, function (json) {
                obj = JSON.parse(json);
                var count = obj.length;
                for (i = 0; i < count; i++) {
                    var obj_data = obj[i];
                    $('#stockToken').val(localStorage.getItem('access_token'));
                    $('#stockId').val(obj_data['stock_id']);
                    $('#quantity').val(obj_data['quantity']);
                }
                $(".status-progress").hide();
                $('#modalAddstock').modal('show');
            });
        }


        function updateStatus(id, status_id) {
            $(".status-progress").show();
            var url = base_url + "/order/update_order_status";
            var formData = {
                'order_id': id,
                'status_id': status_id,
                'access_token': localStorage.getItem('access_token')
            };
            $.post(url, formData, function (json) {
                obj = JSON.parse(json);
                var count = obj.length;
                for (i = 0; i < count; i++) {

                    new PNotify({
                        title: 'Notice',
                        text: obj_data['message'],
                        addclass: 'bg-warning'
                    });
                }

                $(".status-progress").hide();
                window.location.reload(true);
            });
        }


        function ajaxmodaladd() {
            $(".status-progress-add").hide();
            $('#supplier_id').val(localStorage.getItem('supplier_id'));
            $('#modalOrdersNew').modal('show');
        }

        function Ordersadd() {
            $('#formstockadd').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-add").show();
                $.ajax({
                    url: base_url + "/stock/new_stock ",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress-add").hide();
                        loadOrders();
                        $('#modalAddstock').modal('hide');
                        new PNotify({
                            title: 'Add Orders Notice',
                            text: 'Quantity updated successfully',
                            addclass: 'bg-success'
                        });
                    },
                    error: function () {
                    }
                });
            });
        }

        function unitPriceUpdate() {
            $('#formunitprice').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-add").show();
                $.ajax({
                    url: base_url + "/stock/update_price ",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress-add").hide();
                        loadOrders();
                        $('#modalUnitstock').modal('hide');
                        new PNotify({
                            title: 'Notice',
                            text: 'Unit price updated successfully',
                            addclass: 'bg-success'
                        });
                    },
                    error: function () {
                    }
                });
            });
        }

        function stockNew() {
            $('#formstockNew').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-add").show();
                $.ajax({
                    url: base_url + "/stock/new_product ",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress-add").hide();
                        loadOrders();
                        $('#modalOrdersNew').modal('hide');
                        new PNotify({
                            title: 'Notice',
                            text: 'New Orders added successfully',
                            addclass: 'bg-success'
                        });
                    },
                    error: function () {
                    }
                });
            });
        }


        function activateCategory(id) {
            $(".status-progress").show();
            var url = base_url + "/category/status";
            var formData = {
                'category_id': id,
                'access_token': localStorage.getItem('access_token'),
                'is_active': 1
            };
            $.post(url, formData, function (json) {
                var data = JSON.parse(json);
                new PNotify({
                    title: 'Update Notice',
                    text: data['message'],
                    addclass: 'bg-info'
                });
                loadCategoryUpdate();
                $(".status-progress").hide();
            });
        }

        function deactivateCategory(id) {
            $(".status-progress").show();
            var url = base_url + "/category/status";
            var formData = {
                'category_id': id,
                'access_token': localStorage.getItem('access_token'),
                'is_active': 0
            };
            $.post(url, formData, function (json) {
                var data = JSON.parse(json);
                new PNotify({
                    title: 'Update Notice',
                    text: data['message'],
                    addclass: 'bg-warning'
                });
                loadCategoryUpdate();
                $(".status-progress").hide();
            });
        }

    </script>

</head>

<body>


<?php include("../topbar.php") ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->

                <!-- /user menu -->

                <?php include("../sidebargen.php") ?>
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Orders</span>
                        </h4>
                    </div>

                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                        <li class="active">Orders</li>
                    </ul>

                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Basic datatable -->
                <div class="panel panel-flat">
                   <!--  <div class="panel-heading">
                        <h5 class="panel-title">Orders</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div> -->

                    <div class="panel-body">
                        <!--<a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Orders</a>-->
                        <img class="text-center center-block status-progress" src="../assets/loader/loader.gif"/>
                     <style type="text/css">
                              #example .dropdown-menu{
                                  position: relative;
                              }
                          </style>

                    <table class="table datatable-basic" id="example" width="100%">
                        <thead>
                        <tr>
                            <th>Order id</th>
                            <th>Retailer Name</th>
                            <th>Distribution Centre</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Payment type</th>
                            <th>Product VAT</th>
                            <th>Delivery VAT</th>
                            <th>Product Cost</th>
                            <th>Delivery Cost</th>
                            <th>Total Cost</th>
                            <th>Timestamp</th>
                            <th>Payment status</th>
                            <th>Action</th>

                        </tr>
                        </thead>

                    </table>
                </div>
                </div>
                <!-- /basic datatable -->

                <!-- Vertical form modal -->
                <div id="modalAddstock" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Add stock form</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formstockadd"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="stock_id" id="stockId"/>
                                <input type="hidden" name="access_token" id="stockToken"/>
                                <div class="modal-body">
                                    <div class="form-group">

                                        <label>Quantity</label>
                                        <input name="quantity" id="quantity" class="form-control"
                                               placeholder="Quantity"/>

                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Update
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->


                <!-- Vertical form modal -->
                <div id="modalUnitstock" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Update Unit Price</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formunitprice"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="stock_id" id="stockId"/>
                                <input type="hidden" name="access_token" id="stockToken"/>
                                <div class="modal-body">
                                    <div class="form-group">

                                        <label>Unit Price</label>
                                        <input name="unit_cost" id="unit_cost" class="form-control"
                                               placeholder="Unit Price"/>

                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Update
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->


                <!-- Vertical form modal -->
                <div id="modalOrdersNew" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">New Orders Product form</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formstockNew"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="supplier_id" id="supplier_id"/>
                                <input type="hidden" name="access_token" id="supToken"/>
                                <div class="modal-body">
                                    <div class="form-group">

                                        <label>Sku:</label>
                                        <select id="selectsku" name="sku_id" class="select-search">
                                            <option value="0">Select Sku</option>
                                        </select>

                                    </div>
                                    <div class="form-group">

                                        <label>Product:</label>
                                        <select id="selectpro" name="product_id" class="select-search">
                                            <option value="0">Select Product</option>
                                        </select>

                                    </div>
                                    <div class="form-group">

                                        <label>Unit Price:</label>
                                        <input name="unit_cost" id="unit_cost" class="form-control"
                                               placeholder="Unit Price"/>

                                    </div>
                                    <div class="form-group">

                                        <label>Quantity:</label>
                                        <input name="quantity" id="quantity" class="form-control"
                                               placeholder="Quantity"/>

                                    </div>

                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                    <img class="text-center center-block status-progress-add"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->

                <!-- Vertical form modal -->
                <div id="modalorderDetails" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Order details</h5>
                                <b>Payment Type<p id="type"></p></b>
                                <b><p id="vat"></p></b>
                            </div>

                            <div class="modal-body">

                                <table id="tablemy" class="table">

                                    <thead>
                                    <tr>
                                        <th>Product Image</th>
                                        <th>Product name</th>
                                        <th>Product description</th>
                                        <th>Sold By</th>
                                        <th>Item Quantity</th>
                                        <th>Unit Price</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tBody"></tbody>
                                    <tfoot>
                                    <tr style="font-size:20px;">
                                        <th>Total: <p id="total"></p></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->

                <!-- Vertical form modal -->
                <div id="modalPayments" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Payment details</h5>
                            </div>

                            <div class="modal-body">
                                <table id="tablemy" class="table">

                                    <tbody>
                                    <tr>
                                        <th>Payment Method</th>
                                        <td><p id="typa"></p></td>
                                    </tr>
                                    <tr>
                                        <th>Phone</th>
                                        <td><p id="phone"></p></td>
                                    </tr>
                                    <tr>
                                        <th>Amount</th>
                                        <td><p id="amount"></p></td>
                                    </tr>
                                    <tr>
                                        <th>Transaction date</th>
                                        <td><p id="mpesa_trx_date"></p></td>
                                    </tr>

                                    </tbody>


                                </table>
                            </div>


                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->

                <!-- Footer -->
                <div class="footer text-muted">
                    <?php include("../footer.php") ?>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
