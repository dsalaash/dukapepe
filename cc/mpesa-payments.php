<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Duka pepe | Mpesa Payments</title>

        <?php include("../links.php") ?>

        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search mpesa payments:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });
                if (localStorage.getItem('code') == 1) {
                    loadMpesaPayments();
                } else {
                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });
                }

                deliveryZoneUpdate();

            });
            function loadMpesaPayments() {
                console.log(localStorage.getItem('access_token'));
                var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "order/fetch_mpesa_payment";
                $('#example').DataTable({
                    "destroy": true,
                    "ajax": {
                        "url": url,
                        "data": formData,
                        "type": "post",
                        "dataSrc": function (json) {
                            return json;
                        },
                        "processing": true,
                        "serverSide": true,
                        "pagingType": "simple",
                        language: {
                            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                        }

                    }, "columns": [
                        {"data": "order_id"},
                        {"data": "msisdn"},
                        {"data": "amount"},
                        {"data": "description"},
                        {"data": "trx_status", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                if (data == 'Failed') {
                                    return '<span class="label label-warning">Failed</span>';
                                } else {
                                    return '<span class="label label-success">Success</span>';             }

                            }},
                    ]
                });
            }



            function updateDeliveryModal(id) {
                $(".status-progress").show();
                var url = base_url + "delivery/fetch_delivery_zones";
                var formData = {
                    'id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];
                        $('#accesstoken').val(localStorage.getItem('access_token'));
                        $('#settingsID').val(obj_data['id']);
                        $('#min_distance').val(obj_data['min_distance']);
                        $('#max_distance').val(obj_data['max_distance']);
                        $('#rate').val(obj_data['rate']);
                    }
                    $(".status-progress").hide();
                    $('#modalAddstock').modal('show');
                });

            }


            function deliveryZoneUpdate() {
                $('#formvehicleassign').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "delivery/update_delivery_zones",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-add").hide();
                            loadDeliveryZones();
                            $('#modalAddstock').modal('hide');
                            if (data['code'] == 1) {
                                new PNotify({
                                    text: data['message'],
                                    addclass: 'bg-success'
                                });
                            } else {
                                new PNotify({
                                    text: data['message'],
                                    addclass: 'bg-warning'
                                });
                            }
                        },
                        error: function () {}
                    });
                });
            }



        </script>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->

                        <!-- /user menu -->

                        <?php include("../sidebar.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Mpesa Payments</span></h4>
                            </div>

                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                                <li class="active">Mpesa Payments</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">Mpesa Payments</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>

                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <!--<a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Stock</a>-->
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            </div>

                            <table class="table" id="example">
                                <thead>
                                    <tr>
                                        <th>Order id</th>
                                        <th>Phone number</th>
                                        <th>Amount</th>
                                        <th>Description</th>
           
                                        <th>Status</th>



                                    </tr>
                                </thead>

                            </table>
                        </div>
                        <!-- /basic datatable -->

                        <!-- Vertical form modal -->
                        <div id="modalAddstock" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Update delivery zones</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formvehicleassign" enctype="multipart/form-data">
                                        <input type="hidden" name="id" id="settingsID"/>
                                        <input type="hidden" name="access_token" id="accesstoken"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Minimum Distance</label>
                                                <input name="min_distance" id="min_distance" class="form-control" placeholder="Minimum distance" />

                                            </div>
                                            <div class="form-group">

                                                <label>Maximum Distance</label>
                                                <input name="max_distance" id="max_distance" class="form-control" placeholder="Maximum distance" />

                                            </div>
                                            <div class="form-group">

                                                <label>Rate</label>
                                                <input name="rate" id="rate" class="form-control" placeholder="Rate" />

                                            </div>


                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->

                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
