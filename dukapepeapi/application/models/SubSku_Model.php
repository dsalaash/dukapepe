<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SubSku_Model
 *
 * @author mwamb
 */
class SubSku_Model extends CI_Model{
    //put your code here
    public $sku_relationships="skus_relationships";
    public $skus="skus";
    
    public function add($data){
        $response=array();
        $counter= $this->checkNew($data);
        if($counter==0){
            $this->db->insert($this->sku_relationships,$data);
            $response['message']='Sku-Sub sku set successfully.';
            $response['code'] = 1;
        }
        else{
            $response['message']='Similar sub sku already exist';
            $response['code'] = 0;
        }
        return json_encode($response);
    }
    public function checkNew($data){
        $this->db->select('id');
        $this->db->where('larger_sku',$data['larger_sku']);
        $this->db->where('smaller_sku',$data['smaller_sku']);
        $this->db->from($this->sku_relationships);
        $counter= $this->db->count_all_results();
        return $counter;
    }
    
    public function update($data){
        $response=array();
        $counter= $this->checkUpdate($data);
        if($counter==0){
            $sku_id= $data['id'];
            unset($data['sku_id']);
            $this->db->where('sku_id',$sku_id);
            $this->db->update($this->sku_relationships,$data);
            $response['message']="Relationship updated successfully updated successfully.";
            $response['code']=1;
        }
        else{
         $response['message']="Failed to update, Similar record exist.";
         $response['code']=0;   
        }
        
        return json_encode($response);
    }
    public function checkUpdate($data){
        $this->db->select('id');
        $this->db->where('larger_sku',$data['larger_sku']);
        $this->db->where('smaller_sku',$data['smaller_sku']);
        $this->db->from($this->sku_relationships);
        $counter=$this->db->count_all_results();
        return $counter;
    }
    public function fetch($sku_data){
        $this->db->select('larger_sku,smaller_sku,factor,skus.sku_name');
        $this->db->where('id',$sku_data['sku_id']);
        $this->db->from($this->sku_relationships);
        $this->db->join('skus', $this->sku_relationships.".id=skus.sku_id");
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return json_encode($data);

        // foreach ($this->fetch_all_skus() as $sk) {
        //     $getSkuName[$sk['sku_id']] = $sk['sku_name'];
        // }

        // $this->db->select('id,larger_sku,smaller_sku,factor');
        // $this->db->where('id',$sku_data['sku_id']);
        // $this->db->from($this->sku_relationships);
        // $returned_data= $this->db->get();
        // $data=$returned_data->result_array();
        
        // $a=0;
        // foreach ($data as $relation) {
        //   $relation['larger_sku'] = $getSkuName[$relation['larger_sku']];
        //   $relation['smaller_sku'] = $getSkuName[$relation['smaller_sku']];

        //   $finalRelationArray[$a] = $relation;
        //   $a++;
        // }

        // return json_encode($finalRelationArray);


    }
    public function fetch_all_skus(){
        $this->db->select('sku_id,sku_name,weight,is_active');
        $this->db->from('skus');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return $data;
        // return json_encode($data);
    }

    public function fetch_all(){

        foreach ($this->fetch_all_skus() as $sk) {
            $getSkuName[$sk['sku_id']] = $sk['sku_name'];
        }

        $this->db->select('id,larger_sku,smaller_sku,factor');
        $this->db->from($this->sku_relationships);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        
        $a=0;
        foreach ($data as $relation) {
            $relation['large_sku_id'] = $relation['larger_sku'];
            $relation['small_sku_id'] = $relation['smaller_sku'];
          $relation['larger_sku'] = $getSkuName[$relation['larger_sku']];
          $relation['smaller_sku'] = $getSkuName[$relation['smaller_sku']];


          $finalRelationArray[$a] = $relation;
          $a++;
        }

        return json_encode($finalRelationArray);
        // return json_encode($data);
    }

    public function status($data){
        $sku_id=$data['sku_id'];
        unset($data['sku_id']);
        $this->db->where('sku_id',$sku_id);
        $this->db->update($this->sku,$data);
        $response=array();
        $response['message']="Status toggled successfully.";
        $response['code']=1;
        return json_encode($response);
    }
}
