
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("../links.php") ?>
    <!-- /theme JS files -->

</head>

<body>
   <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->

                        <!-- /user menu -->

                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->
                <script type="text/javascript">
                   if(localStorage.getItem('access_token')=="")
          {
           window.location = "http://localhost/dukapepe_portal/dukapepe/";
          }
                      $(document).ready(function ()
                       {
                        //fech initial analytics year
                           var formData = {'access_token': localStorage.getItem('access_token')};
                           var url = "http://192.168.191.2/dukapepeapi/index.php/Analytics/genman_dashboard_index";
                           $.ajax({
                           type:'POST',
                           url: url,
                           data: formData,
                           success:function(data)
                           {

                           },
                           error:function(data)
                           {
                            console.log(data);
                           }        

                            });
                        ///fech intial analytics year

                         

// $( "#datepicker" ).datepicker({
//   altFormat: "yy-mm-dd"
// });
// // Getter
// var altFormat = $( ".selector" ).datepicker( "option", "altFormat" );
// Setter
$( "#datepicker" ).datepicker( { dateFormat: 'yy-mm' } );
$( "#datepicker" ).change(function() {
 //fech initial analytics year
 var month =$("#datepicker").val();
                           var formData = {'access_token': localStorage.getItem('access_token'),'month': month};
                           var url = "http://192.168.191.2/dukapepeapi/index.php/Analytics/genman_dashboard_analytics_by_month";
                           $.ajax({
                           type:'POST',
                           url: url,
                           data: formData,
                           success:function(data)
                           {
                           
                            console.log(data);
                            var parse=JSON.parse(data);
                            console.log(parse);
                            $("#").innerHTML ="";
                            document.getElementById("selected_month_sales").innerHTML = "";
                             document.getElementById("selected_month_orders").innerHTML = "";
                              document.getElementById("selected_month_retailers").innerHTML = "";
                            
                            
                             $("#selected_month_sales").append(parse['totalsalesamount']);
                            $("#selected_month_retailers").append(parse['totalretailerssamount']);
                            $("#selected_month_orders").append(parse['totalorderssamount']);
                            // console.log(parse);
                            // $("#new_customers_this_month").append(parse['total_retailers_this_month']);
                            // $("#orders_this_month").append(parse['total_orders_this_month']);
                            // $("#total_sales_this_month").append(parse['total_sales_this_month']);
                            //general graphp
                                         $(function () {
                                            Highcharts.chart('dukapepe_general_analytics', {
                                                            chart: {
                                                                type: 'spline'
                                                            },
                                                            title: {
                                                                text: 'Daily Sales In the selected Month sales'
                                                            },
                                                            subtitle: {
                                                                text: 'Source: duka-pepe.com'
                                                            },
                                                            xAxis: {
                                                                categories: parse['days']
                                                            },
                                                            yAxis: {
                                                                title: {
                                                                    text: 'Sales (KSH)'
                                                                }
                                                            },
                                                            plotOptions: {
                                                                line: {
                                                                    dataLabels: {
                                                                        enabled: true
                                                                    },
                                                                    enableMouseTracking: false
                                                                }
                                                            },
                                                            series: [{
                                                                name: 'General Sales',
                                                                data: parse['sales']
                                                            }]
                                                        });

                                            });
                           ///general graph
                            //dcsales comparisons
                                         $(function () {Highcharts.chart('dukapepe_dc_sales_comparisons', {

                                                            chart: {
                                                                type: 'spline'
                                                            },
                                                            title: {
                                                                text: 'Daily Sales for The Selected Month'
                                                            },
                                                            subtitle: {
                                                                text: 'Source: duka-pepe.com'
                                                            },
                                                            xAxis: {
                                                                categories: parse['days']
                                                            },
                                                            yAxis: {
                                                                title: {
                                                                    text: 'sales(Ksh.)'
                                                                },
                                                                labels: {
                                                                    formatter: function () {
                                                                        return this.value;
                                                                    }
                                                                }
                                                            },
                                                            tooltip: {
                                                                crosshairs: true,
                                                                shared: true
                                                            },
                                                            plotOptions: {
                                                                spline: {
                                                                    marker: {
                                                                        radius: 4,
                                                                        lineColor: '#666666',
                                                                        lineWidth: 1
                                                                    }
                                                                }
                                                            },
                                                            series: [{
                                                                name: 'Fedha',
                                                                marker: {
                                                                    symbol: 'square'
                                                                },
                                                                data: parse['salesfedha']

                                                            },
                                                            {
                                                                name: 'Kitengela',
                                                                marker: {
                                                                    symbol: 'square'
                                                                },
                                                                data: parse['saleskitengela']

                                                            }, {
                                                                name: 'Ngong',
                                                                marker: {
                                                                    symbol: 'diamond'
                                                                },
                                                                data: parse['salesngong']
                                                            }]

                                                        });
                                            });
                           ///dcsales comparisons
                            //dcsales comparisons
                                         $(function () {Highcharts.chart('dukapepe_dc_retailer_comparisons',{

                                                            chart: {
                                                                type: 'spline'
                                                            },
                                                            title: {
                                                                text: 'Daily Retailer registration for the selected month'
                                                            },
                                                            subtitle: {
                                                                text: 'Source: duka-pepe.com'
                                                            },
                                                            xAxis: {
                                                                categories: parse['days']
                                                            },
                                                            yAxis: {
                                                                title: {
                                                                    text: 'No of Retailers',
                                                                },
                                                                labels: {
                                                                    formatter: function () {
                                                                        return this.value;
                                                                    }
                                                                }
                                                            },
                                                            tooltip: {
                                                                crosshairs: true,
                                                                shared: true
                                                            },
                                                            plotOptions: {
                                                                spline: {
                                                                    marker: {
                                                                        radius: 4,
                                                                        lineColor: '#666666',
                                                                        lineWidth: 1
                                                                    }
                                                                }
                                                            },
                                                            series: [{
                                                                name: 'Fedha',
                                                                marker: {
                                                                    symbol: 'square'
                                                                },
                                                                data: parse['retailersfedha']

                                                            },
                                                            {
                                                                name: 'Kitengela',
                                                                marker: {
                                                                    symbol: 'square'
                                                                },
                                                                data: parse['retailerskitengela']

                                                            }, {
                                                                name: 'Ngong',
                                                                marker: {
                                                                    symbol: 'diamond'
                                                                },
                                                                data: parse['retailersngong']
                                                            }]

                                                        });
                                            });
                           ///dcsales comparisons

                           },
                           error:function(data)
                           {
                            console.log(data);
                           }        

                       });
                        ///fech intial analytics year
});

  });  

 </script>


<!-- Modal  retailer distribution-->
<div class="modal fade" id="dcretailersdistribution" tabindex="-1" role="dialog" aria-labelledby="DC Retailer " aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DC -Retailer Distribution</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="containerdcretailersdistribution" style="width:100%; height:400px;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal  retailer distribution-->
<!-- Modal  order distribution-->
<div class="modal fade" id="dcordersdistribution" tabindex="-1" role="dialog" aria-labelledby="DC order Distribution" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DC - Order Distribution</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

         <div id="containerdcordersdistribution" style="width:100%; height:400px;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal  order distribution-->
<!-- Modal  sales distribution-->
<div class="modal fade" id="dcsalesdistribution" tabindex="-1" role="dialog" aria-labelledby="DC order Distribution" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DC -Sales Dstribution</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
         <div id="containerdcsalesdistribution" style="width:100%; height:400px;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal  sales distribution-->

            <!-- Main content -->
            <div class="content-wrapper">
                <!-- //customerdistribution modal-->

               <!--  ///customerdistribution modal -->

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
                        </div>

                        <div class="heading-elements">
                            <div class="heading-btn-group">
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                            </div>
                        </div>
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-gear position-left"></i>
                                    More Analytics
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#"><i class="icon-user-lock"></i>Monthly</a></li>
                                    <li><a href="#"><i class="icon-statistics"></i>Daily</a></li>
                                    <li><a href="#"><i class="icon-statistics"></i>DC Monthly Comparisons</a></li>
                                    <li><a href="#"><i class="icon-statistics"></i>5 best Selling Items Pe DC</a></li>
                                    <!-- <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="icon-gear"></i> All settings</a></li> -->
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">

                   <!-- Support tickets -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">Analytics For the Selected Month 
                                    <div class="heading-elements">
                                       <form class="heading-form" action="#">
                                         <input type='text' class="form-control" id="datepicker" onkeypress="return false;"/>
                                       </form>
                                    </div>
                                    
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-xlg text-nowrap">
                                        <tbody>
                                            <tr>
                                                <td class="col-md-4">
                                                    <div class="media-left media-middle">
                                                        <div id="tickets-status"></div>
                                                    </div>

                                                    <div class="media-left">
                                                        <h5 class="text-semibold no-margin" id="selected_month_sales">
                                                        
                                                    </div>
                                                    <span >sales(ksh)</span>
                                                </td>

                                                <td class="col-md-3">
                                                    <div class="media-left media-middle">
                                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-alarm-add"></i></a>
                                                    </div>

                                                    <div class="media-left">
                                                        <h5 class="text-semibold no-margin" id="selected_month_orders">
                                                            
                                                        </h5>
                                                        <small class="display-block no-margin">total orders</small>
                                                    </div>
                                                </td>

                                                <td class="col-md-3">
                                                    <div class="media-left media-middle">
                                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-spinner11"></i></a>
                                                    </div>

                                                    <div class="media-left">
                                                        <h5 class="text-semibold no-margin" id="selected_month_retailers">
                                                            
                                                        </h5>
                                                        <small class="display-block no-margin">New Customer Registration</small>
                                                    </div>
                                                </td>
                                                 <td class="col-md-3">
                                                    <div class="media-left media-middle">
                                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-spinner11"></i></a>
                                                    </div>

                                                    <div class="media-left">
                                                        <h5 class="text-semibold no-margin" id="selected_month_retailers">
                                                            
                                                        </h5>
                                                        <small class="display-block no-margin">DCs</small>
                                                    </div>
                                                </td>
                                                 <td class="col-md-3">
                                                    <div class="media-left media-middle">
                                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-spinner11"></i></a>
                                                    </div>

                                                    <div class="media-left">
                                                        <h5 class="text-semibold no-margin" id="selected_month_retailers">
                                                            
                                                        </h5>
                                                        <small class="display-block no-margin">Brands</small>
                                                    </div>
                                                </td>

                                                <td class="text-right col-md-2">
                                                    <a href="#" class="btn bg-teal-400"><i class="icon-statistics position-left"></i> Report</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>

                                <div class="table-responsive">
                                    <!-- Rounded colored tabs -->
                    <div class="row">
                        

                        <div class="col-md-12">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title"></h6>
                                    <div class="heading-elements">
                                        <ul class="icons-list">
                                            <li><a data-action="collapse"></a></li>
                                            <li><a data-action="reload"></a></li>
                                            <li><a data-action="close"></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs bg-slate nav-tabs-component nav-justified">
                                            <li class="active"><a href="#colored-rounded-justified-tab1" data-toggle="tab">Dukapepe</a></li>
                                            <li><a href="#colored-rounded-justified-tab2" data-toggle="tab">DC Performance Sales Comparison</a></li>
                                            <li><a href="#colored-rounded-justified-tab3" data-toggle="tab">DC Performance Retailer Registration Comparison</a></li>
                                            <li><a href="#colored-rounded-justified-tab4" data-toggle="tab">Brands' Performances</a></li>
                                            
                                        </ul>

                                        <div class="tab-content">
                                            <div class="tab-pane active" id="colored-rounded-justified-tab1">
                                              <button class="btn btn-success pull-right">Tabular Data</button>
                                                <div id="dukapepe_general_analytics" style="width:100%; height:500px;"></div>
                                            </div>
                                             <div class="tab-pane" id="colored-rounded-justified-tab2">
                                              <div class="row">
                                                 <div class="col-lg-3 col-md-3 col-sm-3"></div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                              <form>
                                                <select class="form-control">
                                                  <option>i</option>
                                                  <option>i</option>
                                                  <option>i</option>
                                                </select>
                                              </div>
                                               <div class="col-lg-3 col-md-3 col-sm-3">
                                              <input class="btn btn-success " type="submit" value="View DC Details">
                                            </div>
                                            </form>
                                          </div>
                                               <div id="dukapepe_dc_sales_comparisons" style="width:100%; height:500px;"></div>
                                            </div>

                                            <div class="tab-pane" id="colored-rounded-justified-tab3">
                                              <div class="row">
                                                 <div class="col-lg-3 col-md-3 col-sm-3"></div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                              <form>
                                                <select class="form-control">
                                                  <option>i</option>
                                                  <option>i</option>
                                                  <option>i</option>
                                                </select>
                                              </div>
                                               <div class="col-lg-3 col-md-3 col-sm-3">
                                              <input class="btn btn-success " type="submit" value="View DC Details">
                                            </div>
                                            </form>
                                          </div>
                                                <div id="dukapepe_dc_retailer_comparisons" style="width:100%; height:500px;"></div>
                                            </div>

                                            <div class="tab-pane" id="colored-rounded-justified-tab4">
                                              <div class="row">
                                                 <div class="col-lg-3 col-md-3 col-sm-3"></div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                              <form>
                                                <select class="form-control">
                                                  <option>i</option>
                                                  <option>i</option>
                                                  <option>i</option>
                                                </select>
                                              </div>
                                               <div class="col-lg-3 col-md-3 col-sm-3">
                                              <input class="btn btn-success " type="submit" value="View DC Details">
                                            </div>
                                            </form>
                                          </div>
                                              <!--  <div id="dukapepe_dc_retailer_comparisons" style="width:100%; height:700px;"></div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /rounded colored tabs -->



                                </div>
                            </div>
                            <!-- /support tickets -->




                    <!-- Dashboard content -->
                   
                    <!-- /dashboard content -->


                    <!-- Footer -->
               <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
