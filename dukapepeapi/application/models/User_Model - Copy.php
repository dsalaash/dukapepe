<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_Model
 *
 * @author mwamb
 */
class User_Model extends CI_Model{
    //put your code here
    public $users_table='users';
    public $activation_table='account_validation';
    public $login_details="login_details";
    public function __construct() {
        parent::__construct();
        
        $this->load->library('encryption');
        $this->load->library('encrypt');
    }
    //mult user
     public function get_all_system_users()
    {
        $this->db->select('retailer_id,retailer_name,retailers.phone AS phone,retailers.email AS email,retailers.is_active AS is_active,is_approved,retailers.image_url AS image_url');
        $this->db->from('retailers');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return json_encode($data);
    }
    public function agenttransactions($id)
    {
       $this->db->select('agent_retailer_orders.total_cost');
       $this->db->where('agent_retailer_orders.agent_id',$id);
       $this->db->from('agent_retailer_orders');
       $returned_data= $this->db->get();
       $data=$returned_data->result_array();
       return json_encode($data);
    }
    public function getretailerdc($id)
    {
        $this->db->select('*');
        $this->db->from('retailer_suppliers');
        $this->db->where('retailer_id',$id);
        $dc= $this->db->get()->row()->supplier_id;
        return $dc;

    }
     public function linkprimaries($data)
     {
      // $sizeoagent_retailer_ordersfarray = sizeOf($data);
      if($data['primaries']=="")
      {
         
      }
      else
      {
        for($i=0;$i<sizeOf($data['primaries']);$i++)
        {
            $dataa = array('retailer_id' =>$data['primaries'][$i],'agent_id'=>$data['agent']);
             $this->db->insert('agent_primary_retailer', $dataa);

              
        }
        return json_encode($data['primaries']);
        }
    
      
     }
     public function transferlinkedprimaries($datasent)
     {
      
       
          //check if the agent record exists
      $agent=$datasent['agent'];
      $from =$datasent['from'];
      $primaries = $datasent['primaries'];
      $receiver = 38;
          $this->db->select('*');
          $this->db->where('agent_id',$datasent['agent']);
          $this->db->from('agent_primary_retailer');
           $no_rec=(int)$this->db->count_all_results();
           if($no_rec<1)
           {
            ///
                        $this->db->select('phone,retailer_id,retailer_name,image_url');
                        $this->db->where('retailer_id',$agent);
                        $this->db->from('retailers');
                        $returned_data= $this->db->get();
                        $user_data=$returned_data->result_array();
                        $phone=$user_data[0]['phone'];
                        $retailer_id=$user_data[0]['retailer_id'];
                        $retailer_name=$user_data[0]['retailer_name'];
                        $image_url=$user_data[0]['image_url'];
                        $data = array(
                        'first_name' => 'Change',
                        'last_name' => 'Change',
                        'email' => 'Change',
                        'phone' => $phone,
                        'gender' => 'choose',
                        'id_number' => '00000',
                        'dob' => '00-00-0000',
                        'agent_id' =>$retailer_id,
                        'retailer_name' =>$retailer_name,
                        'is_approved' => 1,
                        'image_url' => $image_url,
                        'mpesa_number' => $phone
                         );
                        $this->db->insert('agents', $data);
                               for($i=0;$i<sizeOf($datasent['primaries']);$i++)
                                {
                                  $updatestatus=$this->db->query('UPDATE agent_primary_retailer SET  agent_id= '.$agent.' WHERE retailer_id = '.$primaries[$i]); 
                       
                                      
                                }
                          
                          
                          

           }
           else
           {
                               for($i=0;$i<sizeOf($datasent['primaries']);$i++)
                                {
                                  $updatestatus=$this->db->query('UPDATE agent_primary_retailer SET  agent_id= '.$agent.' WHERE retailer_id = '.$primaries[$i]); 
                       
                                      
                                }
                   
           }

   return json_encode($primaries);
      
    
     }
     public function getagentretailers($data)
     {
        $this->db->select('retailers.phone,agent_primary_retailer.agent_id AS retailer_id,retailers.retailer_name');
        $this->db->join('retailers','retailers.retailer_id=agent_primary_retailer.retailer_id'); 
        $this->db->join('retailer_suppliers','retailers.retailer_id=retailer_suppliers.retailer_id'); 
        $this->db->join('suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id'); 
       $this->db->where('agent_primary_retailer.agent_id',$data['user_id']);
        $this->db->from('agent_primary_retailer');
        $returned_data= $this->db->get();
        $dataa=$returned_data->result_array();
        // return $dataa;
        return json_encode($dataa);
     }
      public function linkedprimaries($data)
     {
      
        $this->db->select('retailers.phone,agent_primary_retailer.retailer_id AS retailer_id,retailers.retailer_name,suppliers.business_name AS dc');
        $this->db->join('retailers','retailers.retailer_id=agent_primary_retailer.retailer_id'); 
        $this->db->join('retailer_suppliers','retailers.retailer_id=retailer_suppliers.retailer_id'); 
        $this->db->join('suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id'); 

        $this->db->where('agent_primary_retailer.agent_id',$data['user_id']);
        $this->db->from('agent_primary_retailer');
        $returned_data= $this->db->get();
        $dataa=$returned_data->result_array();
        // return $dataa;
        return json_encode($dataa);
     }
     public function allprimariesfromdc($data)
     {  $dc= $this->getretailerdc($data['user_id']);
        $this->db->select('retailers.phone,retailers.retailer_id,retailers.retailer_name,retailer_suppliers.supplier_id');
        $this->db->join('retailers','retailers.retailer_id=retailer_suppliers.retailer_id');
        $this->db->where('retailer_suppliers.supplier_id',$dc);
        $this->db->where('retailers.retailer_id NOT IN (select retailer_id from agent_primary_retailer)',NULL,FALSE);
        
        $this->db->from('retailer_suppliers');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
       
        return json_encode($data);
    }
    public function allprimarieslinkedtoagent($data)
    {
    
        
         $sql = "SELECT retailer_id FROM agent_primary_retailer WHERE agent_primary_retailer.agent_id = ?";
         $query=$this->db->query($sql, $data['user_id']);
         $response['primaries']= array();
          foreach ($query->result_array() as $row)
          {

             $this->db->select('retailers.phone,retailers.retailer_id,retailers.retailer_name');
             $this->db->where('retailers.retailer_id',$row['retailer_id']);
             $this->db->from('retailers');
             $returned_data= $this->db->get();
             $dataa =  $returned_data->result_array();
             array_push($response['primaries'],array('retailer_id'=>$dataa[0]['retailer_id'],'retailer_name'=>$dataa[0]['retailer_name'],'phone'=>$dataa[0]['phone']));
          }
             $this->db->select('retailers.phone,retailers.retailer_id,retailers.retailer_name,retailer_suppliers.supplier_id');
             $this->db->join('retailer_suppliers','retailer_suppliers.retailer_id=retailers.retailer_id');
             $this->db->where('retailers.retailer_id',$data['user_id']);
             $this->db->from('retailers');
             $returned_data= $this->db->get(); 
             $from= $returned_data->result_array();
             $response['fromname']= $from[0]['retailer_name'];
              $response['fromid']= $from[0]['retailer_id'];

             
              $this->db->select('retailers.phone,retailers.retailer_id,retailers.retailer_name,retailer_suppliers.supplier_id');
              $this->db->join('retailers','retailers.retailer_id=retailer_suppliers.retailer_id');
              $this->db->where('retailer_suppliers.supplier_id',$from[0]['supplier_id']);
              $this->db->where('retailers.retailer_id NOT IN (select retailer_id from agent_primary_retailer)',NULL,FALSE);
              $this->db->from('retailer_suppliers');
              $returned_data= $this->db->get();
              $response['to']=$returned_data->result_array();

       
        return json_encode($response); 
    }
       public function get_all_agents()
    {
        $this->db->select('agent_id,retailer_name,phone,email email,is_active,is_approved,image_url');
        $this->db->from('agents');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return json_encode($data);
    }
    public function get_all_clerks()
    {
      $this->db->select('retailer_id,retailer_name,phone,email email,is_active,is_approved,image_url');
        $this->db->from('dc_clerks');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return json_encode($data); 
    }
    public function linkclerk_to_dc($data)
    {
      $dataa = array('clerk_id' =>$data['clerk'],'supplier_id'=>$data['supplier']);
      $saved=$this->db->insert('clerk_dc', $dataa); 
      if($saved)
      {
        $response['code']=1;
        $response['message']="Clerk linked to dc successfully";
        return json_encode($response);
      }
    }
    
    
    public function assign_user_type($data)
    {
        //get the user details from retailers
        $user_type_id = $data['user_type_id'];
        $user_id =  $data['user_id'];
        $this->db->select('phone,retailer_id,retailer_name,image_url');
        $this->db->where('retailer_id',$user_id);
        $this->db->from('retailers');
        $returned_data= $this->db->get();
        $user_data=$returned_data->result_array();
        $phone=$user_data[0]['phone'];
        $retailer_id=$user_data[0]['retailer_id'];
        $retailer_name=$user_data[0]['retailer_name'];
        $image_url=$user_data[0]['image_url'];
        $result_count=1;
        if($result_count>0)
        {
           
          $updatestatus=$this->db->query('UPDATE login_details SET   user_type= '.$user_type_id.' WHERE phone ='.$phone);
          $logoutdata['phone']=$phone;
          $this->logoutafterassign($logoutdata);
           if($updatestatus==1)
            { 
               
                   if($user_type_id==2)
                    {
                        $data = array(
                        'first_name' => 'Change',
                        'last_name' => 'Change',
                        'email' => 'Change',
                        'phone' => $phone,
                        'gender' => 'choose',
                        'id_number' => '00000',
                        'dob' => '00-00-0000',
                        'retailer_id' =>$retailer_id,
                        'retailer_name' =>$retailer_name,
                        'is_approved' => 1,
                        'image_url' => $image_url,
                        'mpesa_number' => $phone
                         );
                        $this->db->select('*');
                        
                        $this->db->from('customer_care');
                        $this->db->where('retailer_id',$retailer_id);
                        $this->db->get();
                        $no_rec=(int)$this->db->count_all_results();
                        if($no_rec==1)
                        {
                                  
              
                                   if($this->db->insert('customer_care', $data)) 
                                   {
                                    
                                      return "Customer Care Record Successfully";
                                   }
                                   else
                                   {
                                      return "Customer care record Not created Successfully";
                                   }
                        }
                        else
                        {
                                   
                                      return "Customer Care Account For This account already Exist".$data['user_type_id'];
                                 
                                  
                        }
                       
                    }
                    else if($user_type_id==7)
                    {
                        $data = array(
                        'first_name' => 'Change',
                        'last_name' => 'Change',
                        'email' => 'Change',
                        'phone' => $phone,
                        'gender' => 'choose',
                        'id_number' => '00000',
                        'dob' => '00-00-0000',
                        'agent_id' =>$retailer_id,
                        'retailer_name' =>$retailer_name,
                        'is_approved' => 1,
                        'image_url' => $image_url,
                        'mpesa_number' => $phone
                         );
                        $this->db->select('*');
                        
                        $this->db->from('agents');
                        $this->db->where('agent_id',$retailer_id);
                        $this->db->get();
                        $no_rec=$this->db->count_all_results();
                        if($no_rec==1)
                        {
                                  
              
                                   if($this->db->insert('agents', $data)) 
                                   {
                                   
                                      return "Agent Record Successfully";
                                   }
                                   else
                                   {
                                      return "Agent record Not created Successfully";
                                   }
                        }
                        else
                        {
                                   
                                      return "Agent Account For This account already Exist".$data['user_type_id'];
                                 
                                  
                        }
                       
                    }
                    else if($user_type_id==8)
                    {
                         $data = array(
                        'first_name' => 'Change',
                        'last_name' => 'Change',
                        'email' => 'Change',
                        'phone' => $phone,
                        'gender' => 'choose',
                        'id_number' => '00000',
                        'dob' => '00-00-0000',
                        'retailer_id' =>$retailer_id,
                        'retailer_name' =>$retailer_name,
                        'is_approved' => 1,
                         'is_active' => 1,
                          'is_notification' => 0,
                        'image_url' => $image_url,
                        'mpesa_number' => $phone
                         );
                        $this->db->select('*');
                        $this->db->where('retailer_id',$retailer_id);
                        $this->db->from('dc_clerks');
                        $this->db->get();
                        $no_rec=(int)$this->db->count_all_results();
                        if($insert_status=$this->db->insert('dc_clerks', $data))
                        {
                                  
              
                                   if($insert_status==1) 
                                   {
                                    $access_token="";
                                  
                                      return "Dc Clerks Record Successfully";
                                   }
                                   else
                                   {
                                      return "Dc Clerks record Not created Successfully";
                                   }
                        }
                      }
                    else if($user_type_id==4)
                    {
                        $data = array(
                        'first_name' => 'Change',
                        'last_name' => 'Change',
                        'email' => 'Change it',
                        'phone' => $phone,
                        'retailer_id' =>$retailer_id,
                        'retailer_name' =>$retailer_name,
                        'is_approved' => 1,
                        'image_url' => $image_url,
                        'mpesa_number' => $phone
                         );
                        
              
                                   if($this->db->insert('directors', $data)) 
                                   {
                                    
                                      return "Director Account Created Successfully";
                                   }
                                   else
                                   {
                                      return "Director account for this User Already Exist";
                                   }
                        
                           
                    }
                    else if($user_type_id==6)
                    {
                       

                        
              
                        
                        $this->db->select('*');
                      
                        $this->db->from('genman');
                          $this->db->where('retailer_id',$retailer_id);
                        $this->db->get();
                        $no_rec=(int)$this->db->count_all_results();
                        
                        $data = array(
                        'first_name' => 'Change',
                        'last_name' => 'Change',
                        'email' => 'Change ',
                        'phone' => $phone,
                        'id_number' => '00000',
                        'retailer_id' =>$retailer_id,
                        'retailer_name' =>$retailer_name,
                        'is_approved' => 1,
                        'image_url' => $image_url,
                        'mpesa_number' => $phone
                         );
                        $insert_status="0";
                        
                     
                        
                                  if($this->db->insert('genman', $data)) 
                                   {
                                      return "General Manager Record created Successfully";
                                   }
                                   else
                                   {
                                      return "General Manager record Not created Successfully";
                                   }
                     
                    }
               
            }  
        }
        
       
    }
    ///multi user

    public function has_validated($phone){
        $this->db->select('id');
        $this->db->where('phone',$phone);
        $this->db->where('is_active',0);
        $this->db->from($this->activation_table);
        $count= $this->db->count_all_results();
        return $count==1;
    }
    public function deactivateaccount($data)
    {
        $updatestatus=$this->db->query('UPDATE login_details SET  is_active=0 WHERE phone ='.$data['user_id']); 
        if($updatestatus)
        {
           $access_data=array();
     $access_data['access_token']=NULL;
     $access_data['is_logged']=0; 

     $this->db->where('phone',$data['phone']);
     $this->db->where('access_token',$data['access_token']);
     $this->db->update($this->login_details,$access_data);
          $data['code']=1;
          $data['message']="Account deactivated";
          return json_encode($data);
        }
    }
    public function login($data){
        $response=array();
        $this->db->select('password,user_type,is_logged,is_active');
        $this->db->where('phone',$data['phone']);
        $this->db->where('is_active',1);
        $this->db->from($this->login_details);
        $returned_data= $this->db->get();
        $login_data=$returned_data->result_array();
        if(count($login_data)==0){
          $response['message']="Either your account is inactive or You used the wrong details";
          $response['code']=0;
        }
        
        else{
            // compare passwords
            
            if($data['password']==$this->encryption->decrypt($login_data[0]['password'])){
                $access_token=$this->set_token($data['phone']);
                $response['message']="Login success";
                $response['code']=1;
                $response['phone']=$data['phone'];
                $response['user_type']=$login_data[0]['user_type'];
                $response['access_token']=$access_token;
                $response['is_logged']=$login_data[0]['is_logged'];
//                $response['is_active']=$login_data[0]['is_active'];
                $response['is_validated']= $this->has_validated($data['phone']);
                
                $user_info= $this->get_user_info($response);
                if($response['user_type']==1 && $user_info['is_approved']==0){
                $response['message']="Your account has not been approved by our administrator.";
                $response['code']=0;
                $response['access_token']="";
                $response['is_logged']=0;
                }
                
               $response = array_merge($response,$user_info);
             }
            else{
                $response['message']="Wrong username and password combination.";
                $response['code']=0;   
                
            }
        }
        
        return json_encode($response);
    }
    public function get_user_info($data){
        $base_url= base_url()."images/";
        $table="";
        if($data['user_type']==1){ //RETAILERS
            $this->db->select('retailer_id,retailer_name,mpesa_number,email,CONCAT("'.$base_url.'",image_url) AS image_url, is_active,is_approved');
            $table="retailers";
         }
         
        else if($data['user_type']==2){ //customer
          $this->db->select('retailer_id,retailer_name,mpesa_number,email,CONCAT("'.$base_url.'",image_url) AS image_url, is_active,is_approved');
            $table="customer_care"; 
        }
        else if($data['user_type']==3){//SUPPLIERS
        $this->db->select('supplier_id,first_name,last_name,business_name,email,location_name,latitude,longitude,availability,is_active');
            $table="suppliers";    
        }
        else if($data['user_type']==4){ //director
         

             $this->db->select('retailer_id,retailer_name,mpesa_number,email,CONCAT("'.$base_url.'",image_url) AS image_url, is_active,is_approved');
            $table="directors";   
        }
         else if($data['user_type']==6){ //genman
         

             $this->db->select('retailer_id,retailer_name,mpesa_number,email,CONCAT("'.$base_url.'",image_url) AS image_url, is_active,is_approved');
            $table="genman";   
        }
         else if($data['user_type']==7){ //genman
         

             $this->db->select('agent_id AS retailer_id,retailer_name,mpesa_number,email,CONCAT("'.$base_url.'",image_url) AS image_url, is_active,is_approved');
            $table="agents";   
        }
         else if($data['user_type']==8){ //genman
         

             $this->db->select('retailer_id,retailer_name,mpesa_number,email,CONCAT("'.$base_url.'",image_url) AS image_url, is_active,is_approved');
            $table="dc_clerks";   
        }

        $this->db->where('phone',$data['phone']);
        $this->db->from($table);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
//        print_r ($response);
        return $response[0];
    }
    
    public function unset_data($login_data,$elements){
        for($i=0;$i<count($elements);$i++){
        unset($login_data[0][$elements[$i]]);
//        echo ','.$elements[$i];
        }
        
        return $login_data[0];
    }
    
    public function change_password($data){
        $phone=$data['phone'];
        unset($data['phone']);
        $this->db->where('phone',$phone);
        $this->db->update($this->login_details,$data);
        
        $num = $this->db->affected_rows();
        if($num>0){
        $response['message'] = "New password set sucessfully.";
        $response['code'] = 1;
        }
        else{
        $response['message'] = "No change detected.";
        $response['code'] = 0;    
        }
      
        return json_encode($response);
    }
     public  function logoutafterassign($data){
     $access_data=array();
     $access_data['access_token']=NULL;
     $access_data['is_logged']=0; 

     $this->db->where('phone',$data['phone']);
   
     $this->db->update($this->login_details,$access_data);

     
    }
   
    public  function logout($data){
     $access_data=array();
     $access_data['access_token']=NULL;
     $access_data['is_logged']=0; 

     $this->db->where('phone',$data['phone']);
     $this->db->where('access_token',$data['access_token']);
     $this->db->update($this->login_details,$access_data);

     $response=array();
     $response['message']="logged out successfully.";
     $response['code']=1;

     return json_encode($response);
    }
    public function set_token($phone){
         $token=$this->generate_token();
        $access_data=array();
        $access_data['access_token']= $token;
        $access_data['is_logged']=1;
        $this->db->where('phone',$phone);
        $this->db->update($this->login_details,$access_data);

        return $token;
    }

    public function generate_token(){
     $length=60;
     $randstr="";
     srand((double) microtime(TRUE) * 1000000);
     //our array add all letters and numbers if you wish
     $chars = array(
         'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'p',
         'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z','0', '1', '2', '3', '4', '5',
         '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 
         'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

     for ($rand = 0; $rand <= $length; $rand++) {
         $random = rand(0, count($chars) - 1);
         $randstr .= $chars[$random];
     }
     return $randstr;
    }
    
    public function save_logins($data){
        $this->db->insert($this->login_details,$data);   
    }
    public function save_activation($data){
        $this->db->insert($this->activation_table,$data);   
    }
   
    public function getoldpasswords(){
        $this->db->select('id,password');
        $this->db->from('login_details');
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        
        for($i=0;$i<count($response);$i++){
            $id=$response[$i]['id'];
            $password=$this->encrypt->decode($response[$i]['password']);
            // update with plain password
            if($password!=""){
                $data['password'] = $password;
                $this->db->where('id',$id);
                $this->db->update('login_details',$data);
            }
        }
        
        $this->updatepasswords();
        
        return "process complete";
    }
    
    public function updatepasswords(){
     $this->db->select('id,password');
        $this->db->from('login_details');
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        
        for($i=0;$i<count($response);$i++){
            $id=$response[$i]['id'];
            $password=$this->encryption->encrypt($response[$i]['password']);
            // update with newly encrypted password
            if($password!=""){
                $data['password'] = $password;
                $this->db->where('id',$id);
                $this->db->update('login_details',$data);
            }
        }   
    }
    
    public function reset_key($length){
     $randstr="";
     srand((double) microtime(TRUE) * 1000000);
     //our array add all letters and numbers if you wish
     $chars = array(
         '0', '1', '2', '3', '4', '5','6', '7', '8', '9',
         'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K','L', 'M', 
         'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

     for ($rand = 0; $rand <= $length; $rand++) {
         $random = rand(0, count($chars) - 1);
         $randstr .= $chars[$random];
     }
     return $randstr;
    }
    
    public function  reset_password($data){
        $access_code = $this->reset_key(7);
        $access_data['access_token'] = $access_code;
        
        $this->db->where('phone',$data['phone']);
        $this->db->update($this->login_details,$access_data);
        
       $num = $this->db->affected_rows();
        if($num>0){
        $response['message'] = "Passcode generated successfully. Check your phone and enter the received code.";
        $response['code'] = 1;
        
        //send sms to user
        $message = "Your Passcode is : ".$access_code;
        
        $this->SendSMS->action_send($data['phone'],$message);
        }
        else{
        $response['message'] = "No such user.";
        $response['code'] = 0;    
        }
      
        return json_encode($response); 
    }
    
    public function reset_pass2($data){
        $access_token = $data['access_token'];
        unset($data['access_token']);
        
        $data['access_token'] = null;
        
        $this->db->where('access_token',$access_token);
        $this->db->update($this->login_details,$data);
        
        $num = $this->db->affected_rows();
        if($num>0){
        $response['message'] = "Password reset sucessfully.";
        $response['code'] = 1;
        }
        else{
        $response['message'] = "No change detected.";
        $response['code'] = 0;    
        }
      
        return json_encode($response);
    }
}
