<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Promotions
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Promotions extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    public function add(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $data=array();
      
     $stock_id= $this->input->post('stock_id');
     $start_date= $this->input->post('start_date');
     $end_date= $this->input->post('end_date');
     $data['stock_id']=$stock_id;
     $data['start_date']=$start_date;
     $data['end_date']=$end_date;
     
     $response= $this->Promotions_Model->add($data);
     }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
    public function update(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $data=array();
      
     $promotion_id= $this->input->post('promotion_id');
     $stock_id= $this->input->post('stock_id');
     $start_date= $this->input->post('start_date');
     $end_date= $this->input->post('end_date');
     $data['promotion_id']=$promotion_id;
     $data['stock_id']=$stock_id;
     $data['start_date']=$start_date;
     $data['end_date']=$end_date;
     
     $response= $this->Promotions_Model->add($data);
     }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
    public function fetch(){
       $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){ 
     $response= $this->Promotions_Model->fetch();
     }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
    public function fetch_all(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){  
     $response= $this->Promotions_Model->fetch_all();
     }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
    public function supplier_promotions(){
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
    if($access_response['code']==1){
        $supplier_id= $this->input->post('supplier_id');  
        $response= $this->Promotions_Model->supplier_promotions($supplier_id);
    }
    else{
       $response= json_encode($access_response);   
    }
        echo $response;  
    }
    public function per_category(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
            $category_id= $this->input->post('category_id');  
            $response= $this->Promotions_Model->per_category($category_id);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;  
    }
    
//    v2 of promotions
    public function new_promotion(){
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
    if($access_response['code']==1){
     $data['message']  = $this->input->post('message');
     $data['is_active'] = $this->input->post('is_active');
     
     $response = $this->Promotions_Model->new_promotion($data);
     
     }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;  
    }
   
    public function update_promotion(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
         
        $data['id'] = $this->input->post('id');
        $data['message'] = $this->input->post('message');
        $data['is_active'] = $this->input->post('is_active');
        
        $response= $this->Promotions_Model->update_promotion($data);
        
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;  
    }
    
    
    public function view_promotions(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
         
        $data=array();
        if($this->input->post('id')!=null){
             $data['id'] = $this->input->post('id');
        }
        if($this->input->post('is_active')!=null){
            $data['is_active'] = $this->input->post('is_active');
        }
        $response= $this->Promotions_Model->view_promotions($data);
        
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;  
    }
}
