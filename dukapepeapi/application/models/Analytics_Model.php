<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Analytics_Model extends CI_Model{
public $retailers = "retailers";    
public $retailer_suppliers = "retailer_suppliers";
public $suppliers = "suppliers";
public $orders  = "orders";
public $supplier_orders = "suplier_orders";    
public $ordered_products = "ordered_products";    
public $stock = "stock";    
public $skus = "skus";    
public $products = "products"; 
public $manufaturers = "manufacturers";  

public $categories="categories";
public $sub_categories="sub_categories";
public $orders_payment="orders_payment";
public $agents="agents";
public $login_details="login_details";


 
 public function sales_details_this_year($data)
{
  $dc =$data['dc'];
  $start_date = $data['start_date'];
  $end_date = $data['end_date'];
 // $query = $this->db->query("SELECT 
 //  order_id FROM supplier_orders WHERE supplier_orders.timestamp >= ".$start_date" AND supplier_orders.timestamp <= ".$end_date." AND supplier_orders.supplier_id = 3");
 $sql = "SELECT order_id FROM supplier_orders WHERE supplier_orders.timestamp >= ? AND supplier_orders.timestamp <= ? AND supplier_orders.supplier_id = ?";
  $query=$this->db->query($sql, array($start_date, $end_date,10));
  
  $response['dcs'] = array();
  $response['dcs_list_of_sales'] = array();
  foreach ($query->result_array() as $row)
  {
    $this->db->select('ordered_products.*,products.product_name');
    $this->db->from($this->ordered_products);
    $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id'); 
     $this->db->join('skus','skus.sku_id=stock.sku_id');
    $this->db->join('products','products.product_id=stock.product_id');  
    $this->db->where('ordered_products.timestamp >=', $start_date);
    $this->db->where('ordered_products.timestamp <=',  $end_date);
    $this->db->where('ordered_products.order_id =', $row['order_id']);
    // $this->db->group_by('ordered_products.order_id');
    $returned_data= $this->db->get();
    array_push($response['dcs_list_of_sales'], $returned_data->result_array());
  }
  return json_encode($response);
}
// public function dashboard($data)
//  {
//   $date_from = date("Y-m-01");   
// $date_from = strtotime($date_from); // Convert date to a UNIX timestamp  
  
// // Specify the end date. This date can be any English textual format  
// $date_to = $data['date']; 
// $date_to = strtotime($date_to); // Convert date to a UNIX timestamp  
// //$resarr['dates']=array();

  
// // Loop from the start date to end date and output all dates inbetween  
// $resarr=array();
// $resarr['dates']=array();
// for ($i=$date_from; $i<=$date_to; $i+=86400) 
//  { 
//           // type of orders
//           $resarr['fedhatoo_agent'.date("Y-m-d", $i)]=array();
//           $resarr['fedhatoo_direct'.date("Y-m-d", $i)]=array();
//           $resarr['umotoo_agent'.date("Y-m-d", $i)]=array();
//           $resarr['umotoo_direct'.date("Y-m-d", $i)]=array();

//           // umoja direct purchases
//           $this->db->select('orders.order_id');
          
//           $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',7);
//           $this->db->where('orders.order_type',1);

//           $this->db->from('orders');
//           $this->db->join('supplier_orders','supplier_orders.order_id=orders.order_id');
//           $query = $this->db->get(); // get query result
//           $count = (int)$query->num_rows(); //get current query record.

//           if($count>0)
//           {
//             array_push($resarr['umotoo_direct'.date("Y-m-d", $i)], $count);
//           }
//           else
//           {
//             array_push($resarr['umotoo_direct'.date("Y-m-d", $i)], 0);
//           }

//           // umoja purchases through agent_id
//           $this->db->select('orders.order_id');
          
//           $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',7);
//           $this->db->where('orders.order_type',0);

//           $this->db->from('orders');
//            $this->db->join('supplier_orders','supplier_orders.order_id=orders.order_id');
//           $query = $this->db->get(); // get query result
//           $count = (int)$query->num_rows(); //get current query record.
//           if($count>0)
//           {
//             array_push($resarr['umotoo_agent'.date("Y-m-d", $i)], $count);
//           }
//           else
//           {
//             array_push($resarr['umotoo_agent'.date("Y-m-d", $i)], 0);
//           }

//           // end of umoja
//           // fedha 

//           // fedha purchases through agent_id
//           $this->db->select('orders.order_id');
          
//           $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',11);
//           $this->db->where('orders.order_type',0);

//           $this->db->from('orders');
//            $this->db->join('supplier_orders','supplier_orders.order_id=orders.order_id');
//           $query = $this->db->get(); // get query result
//           $count = (int)$query->num_rows(); //get current query record.
//           if($count>0)
//           {
//             array_push($resarr['fedhatoo_agent'.date("Y-m-d", $i)], $count);
//           }
//           else
//           {
//             array_push($resarr['fedhatoo_agent'.date("Y-m-d", $i)], 0);
//           }

//           // fedha direct purchases
//           $this->db->select('orders.order_id');
          
//           $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',11);
//           $this->db->where('orders.order_type',1);
//           $this->db->from('orders');
//           $this->db->join('supplier_orders','supplier_orders.order_id=orders.order_id');
//           $query = $this->db->get(); // get query result
//           $count = (int)$query->num_rows(); //get current query record.

//           if($count>0)
//           {
//             array_push($resarr['fedhatoo_direct'.date("Y-m-d", $i)], $count);
//           }
//           else
//           {
//             array_push($resarr['fedhatoo_direct'.date("Y-m-d", $i)], 0);
//           }

//           // end fedha
//           // end of type of orders

//           $resarr['fedharetailersnew'.date("Y-m-d", $i)]=array();
//           $resarr['umoretailersnew'.date("Y-m-d", $i)]=array();
//           $this->db->select('retailers.retailer_id');
          
//           $this->db->where('retailers.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('retailers.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('retailer_suppliers.supplier_id ',7);
//           $this->db->from('retailers');
//            $this->db->join('retailer_suppliers','retailer_suppliers.retailer_id=retailers.retailer_id');
//           $query = $this->db->get(); // get query result
//         $count = (int)$query->num_rows(); //get current query record.
//           if($count>0)
//           {
            
//             array_push($resarr['umoretailersnew'.date("Y-m-d", $i)], $count);
//           }
//           else
//           {
//             array_push($resarr['umoretailersnew'.date("Y-m-d", $i)], 0);
//           }

//           $this->db->select('retailers.retailer_id');
//           $this->db->where('retailers.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('retailers.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('retailer_suppliers.supplier_id ',11);
//           $this->db->from('retailers');
//            $this->db->join('retailer_suppliers','retailer_suppliers.retailer_id=retailers.retailer_id');
//            $query1 = $this->db->get(); // get query result
//         $count1 = (int)$query1->num_rows(); //get current query record.
//           if($count1>0)
//           {
            
//             array_push($resarr['fedharetailersnew'.date("Y-m-d", $i)], $count1);
//           }
//           else
//           {
//             array_push($resarr['fedharetailersnew'.date("Y-m-d", $i)], 0);
//           }
  


//    $resarr['fedhaclosingstock'.date("Y-m-d", $i)]=array();
//           $resarr['umoclosingstock'.date("Y-m-d", $i)]=array();
//           $this->db->select('SUM(amount) as amount');
//           $this->db->from('closingstocks');
//                $this->db->where('closingstocks.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('closingstocks.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('closingstocks.supplier_id ',7);
//           $queryumostock=$this->db->get();
         
//           if($this->db->count_all_results()==1){
//              $datathisumostock=$queryumostock->result_array();
//             array_push($resarr['umoclosingstock'.date("Y-m-d", $i)], $datathisumostock[0]['amount']);
//           }
//           else
//           {
//             array_push($resarr['umoclosingstock'.date("Y-m-d", $i)], 0);
//           }
          


//           $this->db->select('SUM(amount) as amount');
//           $this->db->from('closingstocks');
//                 $this->db->where('closingstocks.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('closingstocks.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('closingstocks.supplier_id ',11);
//           $queryfedhastock=$this->db->get();
//           if($this->db->count_all_results()==1){
//              $datathisfedhastock=$queryfedhastock->result_array();
//           array_push($resarr['fedhaclosingstock'.date("Y-m-d", $i)], $datathisfedhastock[0]['amount']);
//         }
//         else
//         {
//           array_push($resarr['fedhaclosingstock'.date("Y-m-d", $i)], 0);
//         }

//           array_push($resarr['dates'], date("Y-m-d", $i));
//           $resarr['umovat'.date("Y-m-d", $i)]=array();
//           $resarr['fedhavat'.date("Y-m-d", $i)]=array();
//           $this->db->select_sum('supplier_orders.vat');
//           $this->db->from('supplier_orders');
//           $this->db->where('supplier_orders.timestamp >=',date("Y-m-01").' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',7);
//           $queryvatumo=$this->db->get();
//           $totalordersmtdumo1vat=(int)$queryvatumo->row()->vat;
//           array_push($resarr['umovat'.date("Y-m-d", $i)], $totalordersmtdumo1vat);

//           $this->db->select_sum('supplier_orders.vat');
//           $this->db->from('supplier_orders');
//           $this->db->where('supplier_orders.timestamp >=',date("Y-m-01").' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',11);
//           $queryvatfedha=$this->db->get();
//           $totalordersmtdfedhacsalesvat=(int)$queryvatfedha->row()->vat;
//           array_push($resarr['fedhavat'.date("Y-m-d", $i)],$totalordersmtdfedhacsalesvat);
//           ///fetch vats total

//           ///fetch vats total
//           //dc sales

//           //dc sales
//           $totalvatumo =0;
//           $totalvatfedha =0;

//           $resarr['umovat1'.date("Y-m-d", $i)]=array();
//           $this->db->select('*');
//           $this->db->from('supplier_orders');
//           $this->db->where('supplier_orders.timestamp >=',date("Y-m-01").' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',7);
//           $queryumoorders=$this->db->get();
//           foreach ($queryumoorders->result_array() as $row)
//              {
//                     $this->db->select('SUM(ordered_products.quantity*ordered_products.unit_cost) AS totalcost,SUM(ordered_products.quantity*ordered_products.vat) As total_vat');
//                      $this->db->where('ordered_products.timestamp >=',date("Y-m-01").' 00:00:00');
//                      $this->db->where('ordered_products.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//                      $this->db->where('ordered_products.order_id ',$row['order_id']);
//                      $this->db->from('ordered_products');
//                      $queryordervat=$this->db->get();
//                      $datathis=$queryordervat->result_array();
//                      // $totalcostthis = $totalcostthis+$datathis[0]['totalcost'];
//                     $totalvatumo = $totalvatumo+$datathis[0]['total_vat'];
//              }
//           array_push($resarr['umovat1'.date("Y-m-d", $i)],$totalvatumo);
//           $resarr['fedhavat1'.date("Y-m-d", $i)]=array();
//           $this->db->select('*');
//           $this->db->from('supplier_orders');
//           $this->db->where('supplier_orders.timestamp >=',date("Y-m-01").' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',11);
//           $queryfedhaorder=$this->db->get();
//           foreach ( $queryfedhaorder->result_array() as $row)
//              {
//                     $this->db->select('SUM(ordered_products.quantity*ordered_products.unit_cost) AS totalcost,SUM(ordered_products.quantity*ordered_products.vat) As total_vat');
//                      $this->db->where('ordered_products.timestamp >=',date("Y-m-01", $i).' 00:00:00');
//                      $this->db->where('ordered_products.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//                      $this->db->where('ordered_products.order_id ',$row['order_id']);
//                      $this->db->from('ordered_products');
//                      $queryordervat=$this->db->get();
//                      $datathis=$queryordervat->result_array();
//                      // $totalcostthis = $totalcostthis+$datathis[0]['totalcost'];
//                      $totalvatfedha = $totalvatfedha+$datathis[0]['total_vat'];
//              }
//           array_push($resarr['fedhavat1'.date("Y-m-d", $i)],$totalvatfedha);
//           ///fetch vats totals
          
//           ///fetch vats totals


//           $resarr['umocmtdsales'.date("Y-m-d", $i)]=array();
//           $resarr['fedhacmtdsales'.date("Y-m-d", $i)]=array();
//           $this->db->select_sum('supplier_orders.cost_of_sales');
//           $this->db->from('supplier_orders');
//           $this->db->where('supplier_orders.timestamp >=',date("Y-m-01", $i).' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',7);
//           $query=$this->db->get();
//           $totalordersmtdumo1=(int)$query->row()->cost_of_sales;
//           array_push($resarr['umocmtdsales'.date("Y-m-d", $i)], $totalordersmtdumo1);

//           $this->db->select_sum('supplier_orders.cost_of_sales');
//           $this->db->from('supplier_orders');
//           $this->db->where('supplier_orders.timestamp >=',date("Y-m-01", $i).' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',11);
//           $query=$this->db->get();
//           $totalordersmtdfedhacsales=(int)$query->row()->cost_of_sales;
//           array_push($resarr['fedhacmtdsales'.date("Y-m-d", $i)],$totalordersmtdfedhacsales);


//           $resarr['umomtdsales'.date("Y-m-d", $i)]=array();
//           $resarr['fedhamtdsales'.date("Y-m-d", $i)]=array();
//           $this->db->select('SUM(supplier_orders.total_cost) as total_cost, SUM(supplier_orders.vat) as vat');
//           $this->db->from('supplier_orders');
//           $this->db->where('supplier_orders.timestamp >=',date("Y-m-01", $i).' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',7);
//           $queryumomtd=$this->db->get();
//           $datathisumomtd=$queryumomtd->result_array();
//           array_push($resarr['umomtdsales'.date("Y-m-d", $i)], $datathisumomtd[0]['vat']+$datathisumomtd[0]['total_cost']);

//           $this->db->select('SUM(supplier_orders.total_cost) as total_cost, SUM(supplier_orders.vat) as vat');
//           $this->db->from('supplier_orders');
//          $this->db->where('supplier_orders.timestamp >=',date("Y-m-01", $i).' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',11);
//           // $this->db->where('retailer_id', $row['retailer_id']);
//           $queryfedhamtd=$this->db->get();
//           $datathisfedhamtd=$queryfedhamtd->result_array();
//           array_push($resarr['fedhamtdsales'.date("Y-m-d", $i)], $datathisfedhamtd[0]['vat']+$datathisfedhamtd[0]['total_cost']);
//   //
//           $resarr['umosales'.date("Y-m-d", $i)]=array();
//           $resarr['fedhasales'.date("Y-m-d", $i)]=array();
//           $resarr['umovat'.date("Y-m-d", $i)]=array();
//           $resarr['fedhavat'.date("Y-m-d", $i)]=array();
//           $this->db->select('SUM(supplier_orders.total_cost) as total_cost, SUM(supplier_orders.vat) as vat');
//           $this->db->from('supplier_orders');
//           $this->db->where('supplier_orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',7);
//           $queryumo=$this->db->get();
//           $datathisumo=$queryumo->result_array();
//           array_push($resarr['umosales'.date("Y-m-d", $i)], $datathisumo[0]['vat']+$datathisumo[0]['total_cost']);



//           $this->db->select('SUM(supplier_orders.total_cost) as total_cost, SUM(supplier_orders.vat) as vat');
//           $this->db->from('supplier_orders');
//           $this->db->where('supplier_orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',11);
//           // $this->db->where('retailer_id', $row['retailer_id']);
//           $queryfedha=$this->db->get();
//           $datathisfedha=$queryfedha->result_array();
//           array_push($resarr['fedhasales'.date("Y-m-d", $i)], $datathisfedha[0]['vat']+$datathisfedha[0]['total_cost']);

//           $resarr['umocsales'.date("Y-m-d", $i)]=array();
//           $resarr['fedhacsales'.date("Y-m-d", $i)]=array();
//           $this->db->select_sum('supplier_orders.cost_of_sales');
//           $this->db->from('supplier_orders');
//           $this->db->where('supplier_orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',7);
//           $query=$this->db->get();
//           $totalorderscumo=(int)$query->row()->cost_of_sales;
//           array_push($resarr['umocsales'.date("Y-m-d", $i)],  $totalorderscumo);

//           $this->db->select_sum('supplier_orders.cost_of_sales');
//           $this->db->from('supplier_orders');
//           $this->db->where('supplier_orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',11);
//           // $this->db->where('retailer_id', $row['retailer_id']);
//           $query=$this->db->get();
//           $totalorderscfedha=(int)$query->row()->cost_of_sales;
//           array_push($resarr['fedhacsales'.date("Y-m-d", $i)], $totalorderscfedha);


//           $resarr['umoretailers'.date("Y-m-d", $i)]=array();
//           $resarr['fedharetailers'.date("Y-m-d", $i)]=array();
//           $this->db->select('distinct(orders.retailer_id)');
         
//            $this->db->join('supplier_orders','supplier_orders.order_id = orders.order_id');
//             $this->db->from('orders');
//           $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',7);
//           // $query2=$this->db->get();
//           $totalordersumo= (int)$this->db->count_all_results();
//           array_push($resarr['umoretailers'.date("Y-m-d", $i)], $totalordersumo);

//           $this->db->select('distinct(orders.retailer_id)');
          
//           $this->db->join('supplier_orders','supplier_orders.order_id = orders.order_id');
//           $this->db->from('orders');
//           $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
//           $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
//           $this->db->where('supplier_orders.supplier_id ',11);
//           // $query2=$this->db->get();
//           $totalordersfedha= $this->db->count_all_results();
//           array_push($resarr['fedharetailers'.date("Y-m-d", $i)], $totalordersfedha);

          
       

    

  
// }



public function dashboard($data)
 {
  //$date_from = date("2018-12-01");
$date_from = $data['startdate'];  
$date_from = strtotime($date_from); // Convert date to a UNIX timestamp  
  
// Specify the end date. This date can be any English textual format  
$date_to = $data['enddate']; 
$date_to = strtotime($date_to); // Convert date to a UNIX timestamp  
//$resarr['dates']=array();

  
// Loop from the start date to end date and output all dates inbetween  
$resarr=array();
$resarr['dates']=array();
for ($i=$date_from; $i<=$date_to; $i+=86400) 
 { 
          // type of orders
          $resarr['fedhatoo_agent'.date("Y-m-d", $i)]=array();
          $resarr['fedhatoo_direct'.date("Y-m-d", $i)]=array();
          $resarr['umotoo_agent'.date("Y-m-d", $i)]=array();
          $resarr['umotoo_direct'.date("Y-m-d", $i)]=array();
          $resarr['donitoo_agent'.date("Y-m-d", $i)]=array();
          $resarr['donitoo_direct'.date("Y-m-d", $i)]=array();

          // umoja direct purchases
          $this->db->select('orders.order_id');
          $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',7);
          $this->db->where('orders.order_type',1);
          $this->db->from('orders');
          $this->db->join('supplier_orders','supplier_orders.order_id=orders.order_id');
          $query = $this->db->get(); // get query result
          $count = (int)$query->num_rows(); //get current query record.
          if($count>0)
          {
            array_push($resarr['umotoo_direct'.date("Y-m-d", $i)], $count);
          }
          else
          {
            array_push($resarr['umotoo_direct'.date("Y-m-d", $i)], 0);
          }

          // umoja purchases through agent_id
          $this->db->select('orders.order_id');
          $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',7);
          $this->db->where('orders.order_type',0);
          $this->db->from('orders');
          $this->db->join('supplier_orders','supplier_orders.order_id=orders.order_id');
          $query = $this->db->get(); // get query result
          $count = (int)$query->num_rows(); //get current query record.
          if($count>0)
          {
            array_push($resarr['umotoo_agent'.date("Y-m-d", $i)], $count);
          }
          else
          {
            array_push($resarr['umotoo_agent'.date("Y-m-d", $i)], 0);
          }
          // end of umoja
          // fedha 
          // fedha purchases through agent_id
          $this->db->select('orders.order_id');
          $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',11);
          $this->db->where('orders.order_type',0);
          $this->db->from('orders');
          $this->db->join('supplier_orders','supplier_orders.order_id=orders.order_id');
          $query = $this->db->get(); // get query result
          $count = (int)$query->num_rows(); //get current query record.
          if($count>0)
          {
            array_push($resarr['fedhatoo_agent'.date("Y-m-d", $i)], $count);
          }
          else
          {
            array_push($resarr['fedhatoo_agent'.date("Y-m-d", $i)], 0);
          }
          // fedha direct purchases
          $this->db->select('orders.order_id');
          $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',11);
          $this->db->where('orders.order_type',1);
          $this->db->from('orders');
          $this->db->join('supplier_orders','supplier_orders.order_id=orders.order_id');
          $query = $this->db->get(); // get query result
          $count = (int)$query->num_rows(); //get current query record.
          if($count>0)
          {
            array_push($resarr['fedhatoo_direct'.date("Y-m-d", $i)], $count);
          }
          else
          {
            array_push($resarr['fedhatoo_direct'.date("Y-m-d", $i)], 0);
          }
          // end fedha
           // doni
          // fedha purchases through agent_id
          $this->db->select('orders.order_id');
          $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',13);
          $this->db->where('orders.order_type',0);
          $this->db->from('orders');
          $this->db->join('supplier_orders','supplier_orders.order_id=orders.order_id');
          $query = $this->db->get(); // get query result
          $count = (int)$query->num_rows(); //get current query record.
          if($count>0)
          {
            array_push($resarr['donitoo_agent'.date("Y-m-d", $i)], $count);
          }
          else
          {
            array_push($resarr['donitoo_agent'.date("Y-m-d", $i)], 0);
          }
          // fedha direct purchases
          $this->db->select('orders.order_id');
          $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',13);
          $this->db->where('orders.order_type',1);
          $this->db->from('orders');
          $this->db->join('supplier_orders','supplier_orders.order_id=orders.order_id');
          $query = $this->db->get(); // get query result
          $count = (int)$query->num_rows(); //get current query record.
          if($count>0)
          {
            array_push($resarr['donitoo_direct'.date("Y-m-d", $i)], $count);
          }
          else
          {
            array_push($resarr['donitoo_direct'.date("Y-m-d", $i)], 0);
          }
          // end doni
          // end of type of orders
          //new retailers
          $resarr['fedharetailersnew'.date("Y-m-d", $i)]=array();
          $resarr['umoretailersnew'.date("Y-m-d", $i)]=array();
          $resarr['doniretailersnew'.date("Y-m-d", $i)]=array();
          $this->db->select('retailers.retailer_id');
          $this->db->where('retailers.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('retailers.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('retailer_suppliers.supplier_id ',7);
          $this->db->from('retailers');
          $this->db->join('retailer_suppliers','retailer_suppliers.retailer_id=retailers.retailer_id');
          $query = $this->db->get(); // get query result
          $count = (int)$query->num_rows(); //get current query record.
          if($count>0)
          {
            array_push($resarr['umoretailersnew'.date("Y-m-d", $i)], $count);
          }
          else
          {
            array_push($resarr['umoretailersnew'.date("Y-m-d", $i)], 0);
          }
          $this->db->select('retailers.retailer_id');
          $this->db->where('retailers.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('retailers.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('retailer_suppliers.supplier_id ',11);
          $this->db->from('retailers');
          $this->db->join('retailer_suppliers','retailer_suppliers.retailer_id=retailers.retailer_id');
          $query1 = $this->db->get(); // get query result
          $count1 = (int)$query1->num_rows(); //get current query record.
          if($count1>0)
          {
           array_push($resarr['fedharetailersnew'.date("Y-m-d", $i)], $count1);
          }
          else
          {
            array_push($resarr['fedharetailersnew'.date("Y-m-d", $i)], 0);
          }

          $this->db->select('retailers.retailer_id');
          $this->db->where('retailers.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('retailers.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('retailer_suppliers.supplier_id ',13);
          $this->db->from('retailers');
          $this->db->join('retailer_suppliers','retailer_suppliers.retailer_id=retailers.retailer_id');
          $query1 = $this->db->get(); // get query result
          $count1 = (int)$query1->num_rows(); //get current query record.
          if($count1>0)
          {
           array_push($resarr['doniretailersnew'.date("Y-m-d", $i)], $count1);
          }
          else
          {
            array_push($resarr['doniretailersnew'.date("Y-m-d", $i)], 0);
          }
          ///end list of new retailers
          //fetch closing stocks
          $resarr['fedhaclosingstock'.date("Y-m-d", $i)]=array();
          $resarr['umoclosingstock'.date("Y-m-d", $i)]=array();
          $resarr['doniclosingstock'.date("Y-m-d", $i)]=array();
          $this->db->select('SUM(amount) as amount');
          $this->db->from('closingstocks');
          $this->db->where('closingstocks.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('closingstocks.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('closingstocks.supplier_id ',7);
          $queryumostock=$this->db->get();
          if($this->db->count_all_results()==1)
          {
            $datathisumostock=$queryumostock->result_array();
            array_push($resarr['umoclosingstock'.date("Y-m-d", $i)], $datathisumostock[0]['amount']);
          }
          else
          {
            array_push($resarr['umoclosingstock'.date("Y-m-d", $i)], 0);
          }
          $this->db->select('SUM(amount) as amount');
          $this->db->from('closingstocks');
          $this->db->where('closingstocks.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('closingstocks.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('closingstocks.supplier_id ',11);
          $queryfedhastock=$this->db->get();
          if($this->db->count_all_results()==1)
          {
           $datathisfedhastock=$queryfedhastock->result_array();
           array_push($resarr['fedhaclosingstock'.date("Y-m-d", $i)], $datathisfedhastock[0]['amount']);
          }
          else
          {
          array_push($resarr['fedhaclosingstock'.date("Y-m-d", $i)], 0);
          }

          $this->db->select('SUM(amount) as amount');
          $this->db->from('closingstocks');
          $this->db->where('closingstocks.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('closingstocks.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('closingstocks.supplier_id ',13);
          $queryfedhastock=$this->db->get();
          if($this->db->count_all_results()==1)
          {
           $datathisfedhastock=$queryfedhastock->result_array();
           array_push($resarr['doniclosingstock'.date("Y-m-d", $i)], $datathisfedhastock[0]['amount']);
          }
          else
          {
          array_push($resarr['doniclosingstock'.date("Y-m-d", $i)], 0);
          }
          ///end of fetching closing stocks
          array_push($resarr['dates'], date("Y-m-d", $i));
          //load cost of cost of mtd sales
          $resarr['umocmtdsales'.date("Y-m-d", $i)]=array();
          $resarr['fedhacmtdsales'.date("Y-m-d", $i)]=array();
          $resarr['donicmtdsales'.date("Y-m-d", $i)]=array();
          $this->db->select_sum('supplier_orders.cost_of_sales');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',date("Y-m-01", $i).' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',7);
          $query=$this->db->get();
          $totalordersmtdumo1=(int)$query->row()->cost_of_sales;
          array_push($resarr['umocmtdsales'.date("Y-m-d", $i)], $totalordersmtdumo1);

          $this->db->select_sum('supplier_orders.cost_of_sales');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',date("Y-m-01", $i).' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',11);
          $query=$this->db->get();
          $totalordersmtdfedhacsales=(int)$query->row()->cost_of_sales;
          array_push($resarr['fedhacmtdsales'.date("Y-m-d", $i)],$totalordersmtdfedhacsales);

          $this->db->select_sum('supplier_orders.cost_of_sales');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',date("Y-m-01", $i).' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',13);
          $query=$this->db->get();
          $totalordersmtdfedhacsales=(int)$query->row()->cost_of_sales;
          array_push($resarr['donicmtdsales'.date("Y-m-d", $i)],$totalordersmtdfedhacsales);
          ///close load of cost of mtd sales
          //load mtd sales
          $resarr['umomtdsales'.date("Y-m-d", $i)]=array();
          $resarr['fedhamtdsales'.date("Y-m-d", $i)]=array();
          $resarr['donimtdsales'.date("Y-m-d", $i)]=array();
          $this->db->select('SUM(supplier_orders.total_cost) as total_cost, SUM(supplier_orders.vat) as vat');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',date("Y-m-01", $i).' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',7);
          $queryumomtd=$this->db->get();
          $datathisumomtd=$queryumomtd->result_array();
          array_push($resarr['umomtdsales'.date("Y-m-d", $i)], $datathisumomtd[0]['vat']+$datathisumomtd[0]['total_cost']);

          $this->db->select('SUM(supplier_orders.total_cost) as total_cost, SUM(supplier_orders.vat) as vat');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',date("Y-m-01", $i).' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',11);
          $queryfedhamtd=$this->db->get();
          $datathisfedhamtd=$queryfedhamtd->result_array();
          array_push($resarr['fedhamtdsales'.date("Y-m-d", $i)], $datathisfedhamtd[0]['vat']+$datathisfedhamtd[0]['total_cost']);

          $this->db->select('SUM(supplier_orders.total_cost) as total_cost, SUM(supplier_orders.vat) as vat');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',date("Y-m-01", $i).' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',13);
          $queryfedhamtd=$this->db->get();
          $datathisfedhamtd=$queryfedhamtd->result_array();
          array_push($resarr['donimtdsales'.date("Y-m-d", $i)], $datathisfedhamtd[0]['vat']+$datathisfedhamtd[0]['total_cost']);
          ///close load mtd sales
          //load sales
          $resarr['umosales'.date("Y-m-d", $i)]=array();
          $resarr['fedhasales'.date("Y-m-d", $i)]=array();
          $resarr['donisales'.date("Y-m-d", $i)]=array();
          $this->db->select('SUM(supplier_orders.total_cost) as total_cost, SUM(supplier_orders.vat) as vat');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',7);
          $queryumo=$this->db->get();
          $datathisumo=$queryumo->result_array();
          array_push($resarr['umosales'.date("Y-m-d", $i)], $datathisumo[0]['vat']+$datathisumo[0]['total_cost']);

          $this->db->select('SUM(supplier_orders.total_cost) as total_cost, SUM(supplier_orders.vat) as vat');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',11);
          $queryfedha=$this->db->get();
          $datathisfedha=$queryfedha->result_array();
          array_push($resarr['fedhasales'.date("Y-m-d", $i)], $datathisfedha[0]['vat']+$datathisfedha[0]['total_cost']);

          $this->db->select('SUM(supplier_orders.total_cost) as total_cost, SUM(supplier_orders.vat) as vat');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',13);
          $queryfedha=$this->db->get();
          $datathisfedha=$queryfedha->result_array();
          array_push($resarr['donisales'.date("Y-m-d", $i)], $datathisfedha[0]['vat']+$datathisfedha[0]['total_cost']);
          ///close load of sales

          //cost of sales
          $resarr['umocsales'.date("Y-m-d", $i)]=array();
          $resarr['fedhacsales'.date("Y-m-d", $i)]=array();
          $resarr['donicsales'.date("Y-m-d", $i)]=array();
          $this->db->select_sum('supplier_orders.cost_of_sales');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',7);
          $query=$this->db->get();
          $totalorderscumo=(int)$query->row()->cost_of_sales;
          array_push($resarr['umocsales'.date("Y-m-d", $i)],  $totalorderscumo);

          $this->db->select_sum('supplier_orders.cost_of_sales');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',11);
          $query=$this->db->get();
          $totalorderscfedha=(int)$query->row()->cost_of_sales;
          array_push($resarr['fedhacsales'.date("Y-m-d", $i)], $totalorderscfedha);

          $this->db->select_sum('supplier_orders.cost_of_sales');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',13);
          $query=$this->db->get();
          $totalorderscfedha=(int)$query->row()->cost_of_sales;
          array_push($resarr['donicsales'.date("Y-m-d", $i)], $totalorderscfedha);
           ///cost of sales
          //total retailer oreders
          $resarr['umoretailers'.date("Y-m-d", $i)]=array();
          $resarr['fedharetailers'.date("Y-m-d", $i)]=array();
          $resarr['doniretailers'.date("Y-m-d", $i)]=array();
          $this->db->select('distinct(orders.retailer_id)');
          $this->db->join('supplier_orders','supplier_orders.order_id = orders.order_id');
          $this->db->from('orders');
          $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',7);
          $totalordersumo= (int)$this->db->count_all_results();
          array_push($resarr['umoretailers'.date("Y-m-d", $i)], $totalordersumo);

          $this->db->select('distinct(orders.retailer_id)');
          $this->db->join('supplier_orders','supplier_orders.order_id = orders.order_id');
          $this->db->from('orders');
          $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',11);
          $totalordersfedha= $this->db->count_all_results();
          array_push($resarr['fedharetailers'.date("Y-m-d", $i)], $totalordersfedha);

          $this->db->select('distinct(orders.retailer_id)');
          $this->db->join('supplier_orders','supplier_orders.order_id = orders.order_id');
          $this->db->from('orders');
          $this->db->where('orders.timestamp >=',date("Y-m-d", $i).' 00:00:00');
          $this->db->where('orders.timestamp <=',date("Y-m-d", $i).' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',13);
          $totalordersfedha= $this->db->count_all_results();
          array_push($resarr['doniretailers'.date("Y-m-d", $i)], $totalordersfedha);
          ///total retailer oreders

          
       

    

  
}


return json_encode($resarr);
 }
public function customer_details_this_year()
{
  // $query = $this->db->query("SELECT retailer_id FROM retailers WHERE retailers.timestamp >= '2017-01-01' AND retailers.timestamp <= '2018-12-31'");
  $sql = "SELECT order_id FROM supplier_orders WHERE retailer_suppliers.timestamp >= ? AND retailer_suppliers.timestamp <= ? AND retailer_suppliers.supplier_id = ?";
  $query=$this->db->query($sql, array($start_date, $end_date,10));
  $response['customers_list'] = array();
          foreach ($query->result_array() as $row)
          {
            $this->db->select('retailers.retailer_id,retailers.retailer_name,retailers.phone,COUNT(orders.order_id) AS no_orders, SUM(ordered_products.quantity)*ordered_products.unit_cost AS amount');
            $this->db->from('retailers');
            $this->db->join('orders','orders.retailer_id = retailers.retailer_id'); 
            $this->db->join('ordered_products','ordered_products.order_id = orders.order_id');  
            $this->db->where('orders.timestamp >=', '2017-01-01');
            $this->db->where('orders.timestamp <=', '2018-12-31');
            $this->db->where('orders.retailer_id =', $row['retailer_id']);
            $returned_data= $this->db->get();
            array_push($response['customers_list'], $returned_data->result_array());
          }
  
  return json_encode($response);
}
public function order_details_this_year()
{
         // $query = $this->db->query("SELECT order_id FROM orders WHERE orders.timestamp >= '2017-01-01' AND orders.timestamp <= '2018-12-31'");
  $sql = "SELECT order_id FROM supplier_orders WHERE supplier_orders.timestamp >= ? AND supplier_orders.timestamp <= ? AND supplier_orders.supplier_id = ?";
          $response['orders_list'] = array();
          foreach ($query->result_array() as $row)
          {
            $this->db->select('ordered_products.order_id,COUNT(ordered_products.order_id) AS no_items, SUM(ordered_products.quantity)*ordered_products.unit_cost AS amount');
            $this->db->from('ordered_products');
            $this->db->where('ordered_products.timestamp >=', '2017-01-01');
            $this->db->where('ordered_products.timestamp <=', '2018-12-31');
            $this->db->where('ordered_products.order_id =', $row['order_id']);
            $returned_data= $this->db->get();
            array_push($response['orders_list'], $returned_data->result_array());
          }
  
  return json_encode($response); 
}
public function periodic_brand_analysis($dataa)
{
 
    $this->db->select('SUM(ordered_products.quantity) AS no_items,SUM(ordered_products.quantity)*ordered_products.unit_cost AS total_cost,ordered_products.unit_cost AS unit_cost,ordered_products.timestamp AS time,name,sku_name,business_name,product_name,product_description');
    $this->db->from($this->ordered_products);
    $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id');
    $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
    $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
    $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
    $this->db->join($this->manufaturers,$this->products.'.manufacturer_id='.$this->manufaturers.'.manufacturer_id');
    $this->db->where('manufaturers.manufacturer_id',$dataa['manufacturer_id']);
    $this->db->where('ordered_products.timestamp >=',$dataa['start_date'].' 00:00:00');
    $this->db->where('ordered_products.timestamp <=',$dataa['end_date'].' 24:59:59');
    $this->db->group_by('products.product_id,sku_name,ordered_products.unit_cost');
    $this->db->order_by('SUM(ordered_products.quantity)', 'desc');
    $this->db->limit(5);
    $returned_data= $this->db->get();
    $data['brand_list']=$returned_data->result_array();
    return json_encode($data);
   
}

public function daily_general($data)
 {
    $response=array();
    $this->db->select('*');
    $this->db->where('DATE(timestamp) ',$data['start_date']);
    // $this->db->where('ordered_products.timestamp <=',$data['end_date']);
    $this->db->from('ordered_products');
    $query1=$this->db->get();
    // $rows=$query1->result_array();
    $response['total_sales']=0;
    foreach ($query1->result_array() as $row)
    { 
      $item_total_cost=0;
      $tiem_cost_novat=$row['unit_cost']*$row['quantity'];
      $item_vat=$row['vat']*$row['quantity'];
      $item_total_cost =$tiem_cost_novat+$item_vat;
      $response['total_sales']=$response['total_sales']+$item_total_cost ;
    }

   

    $this->db->select('*');
     $this->db->where('DATE(timestamp) ',$data['start_date']);
    // $this->db->where('ordered_products.timestamp <=',$data['end_date']);
    $this->db->from('orders');
    $query=$this->db->get();
    $response['sales_list']= $query->result_array();


    $this->db->select('*');
    $this->db->from('retailer_suppliers');
     $this->db->where('DATE(timestamp) ',$data['start_date']);
    // $this->db->where('ordered_products.timestamp <=',$data['end_date']);
    $response['total_newcustomers']=(int)$this->db->count_all_results();

    $this->db->select('*');
    $this->db->where('DATE(timestamp) ',$data['start_date']);
    // $this->db->where('ordered_products.timestamp <=',$data['end_date']);
    $this->db->from('orders');
    $response['total_orders']=(int)$this->db->count_all_results();

    $this->db->select('*');
    $this->db->from('retailer_suppliers');
    // $this->db->where('retailer_suppliers.timestamp <',$data['start_date'].' 00:00:00');
     $this->db->where('DATE(retailer_suppliers.timestamp) ',$data['start_date']);
    $response['o_customers']=(int)$this->db->count_all_results();

    $this->db->select('*');
    $this->db->from('retailer_suppliers');
    //$this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
    $this->db->where('DATE(retailer_suppliers.timestamp) ',$data['start_date']);
    
    $response['c_customers']=(int)$this->db->count_all_results();

    $this->db->select('*');
    $this->db->from('retailer_suppliers');
    $this->db->where('DATE(timestamp) ',$data['start_date']);
    // $this->db->where('ordered_products.timestamp <=',$data['end_date']);
    $response['new_customers']=(int)$this->db->count_all_results();

    $this->db->select('*');
     $this->db->where('DATE(timestamp) ',$data['start_date']);
    // $this->db->where('ordered_products.timestamp <=',$data['end_date']);
    $this->db->from('orders');
    $query=$this->db->get();
    $response['sales_list']= $query->result_array();

    $this->db->select('retailers.retailer_name,retailers. phone,retailers. phone,retailers. mpesa_number,suppliers.business_name ');
    $this->db->join('retailers','retailers.retailer_id=retailer_suppliers.retailer_id');
    $this->db->join('suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id');
    $this->db->from('retailer_suppliers');
     $this->db->where('DATE(retailers.timestamp) ',$data['start_date']);
    // $this->db->where('ordered_products.timestamp <=',$data['end_date']);
    $query=$this->db->get();
    $response['new_customers']=$query->result_array();


    $this->db->select('SUM(ordered_products.quantity) AS no_items,sku_name,business_name,product_name');
    $this->db->from($this->ordered_products);
    $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id');
    $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
    $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
    $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
    // $this->db->where('ordered_products.timestamp >=',$data['start_date'].' 00:00:00');
    // $this->db->where('ordered_products.timestamp <=',$data['end_date'].' 24:59:59');
     $this->db->where('DATE(ordered_products.timestamp) ',$data['start_date']);
    // $this->db->where('ordered_products.timestamp <=',$data['end_date']);
    $this->db->group_by('products.product_id,sku_name,business_name');
    $this->db->order_by('SUM(ordered_products.quantity)', 'desc');
    $returned_data= $this->db->get();
    $response['topselling']=$returned_data->result_array();


      $query = $this->db->query("SELECT supplier_id,business_name FROM suppliers");
      $response['list_of_dcs'] = $query->result_array(); 
      
     $response['dcs'] = array();
     $response['dcs_list_of_sales'] = array();
      $response['dcs_no_cutomers'] = array();
      foreach ($query->result_array() as $row)
      {
          array_push($response['dcs'], $row['business_name']);


          $this->db->select_sum('total_cost');
          $this->db->from('supplier_orders');
          // $this->db->where('supplier_orders.timestamp >=',$data['start_date'].' 00:00:00');
          // $this->db->where('supplier_orders.timestamp <=',$data['end_date'].' 24:59:59');
          $this->db->where('DATE(supplier_orders.timestamp) ',$data['start_date']);
          $this->db->where('supplier_id', $row['supplier_id']);
          $query=$this->db->get();
          $totalsalesdc=(int)$query->row()->total_cost;
          $response['totalsales'.$row['supplier_id']]=$totalsalesdc;
          array_push($response['dcs_list_of_sales'], $totalsalesdc);

         $this->db->select('*');
         $this->db->from(' retailer_suppliers');
         $this->db->where('supplier_id', $row['supplier_id']);
         // $this->db->where('retailer_suppliers.timestamp >=',$data['start_date'].' 00:00:00');
         // $this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
           $this->db->where('DATE(retailer_suppliers.timestamp) ',$data['start_date']);
         array_push($response['dcs_no_cutomers'], $this->db->count_all_results());

          $this->db->select('SUM(ordered_products.quantity) AS no_items,name,sku_name,business_name,product_name,product_description');
          $this->db->from($this->ordered_products);
          $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id');
          $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
          $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
          $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
          $this->db->join($this->manufaturers,$this->products.'.manufacturer_id='.$this->manufaturers.'.manufacturer_id');
          $this->db->where('suppliers.supplier_id',$row['supplier_id']);
          $this->db->group_by('products.product_id,sku_name');
          $this->db->order_by('SUM(ordered_products.quantity)', 'desc');
          $this->db->limit(10);
          $returned_data= $this->db->get();
          $response['topselling'.$row['supplier_id']]=$returned_data->result_array();

          $this->db->select('SUM(supplier_orders.total_cost) AS totalcost,suppliers.business_name');
          $this->db->from('supplier_orders');
          $this->db->join('suppliers','suppliers.supplier_id=supplier_orders.supplier_id');
          // $this->db->where('supplier_orders.timestamp >=',$data['start_date'].' 00:00:00');
          // $this->db->where('supplier_orders.timestamp <=',$data['end_date'].' 24:59:59');
          $this->db->where('DATE(supplier_orders.timestamp) ',$data['start_date']);
          $this->db->where('suppliers.supplier_id',$row['supplier_id']);
          $this->db->group_by('suppliers.business_name');
          $returned_dc_sales= $this->db->get();
          $response['dcsales'.$row['supplier_id']]=$returned_dc_sales->result_array();
         

          

      }
      return json_encode($response);
} 
 public function periodic_general($data)
 {
    $response=array();
    $this->db->select('*');
    $this->db->where('ordered_products.timestamp >=',$data['start_date'].' 00:00:00');
    $this->db->where('ordered_products.timestamp <=',$data['end_date'].' 24:59:59');
    $this->db->from('ordered_products');
    $query1=$this->db->get();
    // $rows=$query1->result_array();
    $response['total_sales']=0;
    foreach ($query1->result_array() as $row)
    { 
      $item_total_cost=0;
      $tiem_cost_novat=$row['unit_cost']*$row['quantity'];
      $item_vat=$row['vat']*$row['quantity'];
      $item_total_cost =$tiem_cost_novat+$item_vat;
      $response['total_sales']=$response['total_sales']+$item_total_cost ;
    }

   

    $this->db->select('*');
    $this->db->where('orders.timestamp >=',$data['start_date'].' 00:00:00');
    $this->db->where('orders.timestamp <=',$data['end_date'].' 24:59:59');
    $this->db->from('orders');
    $query=$this->db->get();
    $response['sales_list']= $query->result_array();


    $this->db->select('*');
    $this->db->from('retailer_suppliers');
     $this->db->where('retailer_suppliers.timestamp >=',$data['start_date'].' 00:00:00');
    $this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
    $response['total_newcustomers']=(int)$this->db->count_all_results();

    $this->db->select('*');
    $this->db->where('orders.timestamp >=',$data['start_date'].' 00:00:00');
    $this->db->where('orders.timestamp <=',$data['end_date'].' 24:59:59');
    $this->db->from('orders');
    $response['total_orders']=(int)$this->db->count_all_results();

    $this->db->select('*');
    $this->db->from('retailer_suppliers');
    $this->db->where('retailer_suppliers.timestamp <',$data['start_date'].' 00:00:00');
    $response['o_customers']=(int)$this->db->count_all_results();

    $this->db->select('*');
    $this->db->from('retailer_suppliers');
    $this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
    $response['c_customers']=(int)$this->db->count_all_results();

    $this->db->select('*');
    $this->db->from('retailer_suppliers');
     $this->db->where('retailer_suppliers.timestamp >=',$data['start_date'].' 00:00:00');
    $this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
    $response['new_customers']=(int)$this->db->count_all_results();

    $this->db->select('*');
    $this->db->where('orders.timestamp >=',$data['start_date'].' 00:00:00');
    $this->db->where('orders.timestamp <=',$data['end_date'].' 24:59:59');
    $this->db->from('orders');
    $query=$this->db->get();
    $response['sales_list']= $query->result_array();

    $this->db->select('retailers.retailer_name,retailers. phone,retailers. phone,retailers. mpesa_number,suppliers.business_name ');
    $this->db->join('retailers','retailers.retailer_id=retailer_suppliers.retailer_id');
    $this->db->join('suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id');
    $this->db->from('retailer_suppliers');
    $this->db->where('retailer_suppliers.timestamp >=',$data['start_date'].' 00:00:00');
    $this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
    $query=$this->db->get();
    $response['new_customers']=$query->result_array();


    $this->db->select('SUM(ordered_products.quantity) AS no_items,sku_name,business_name,product_name');
    $this->db->from($this->ordered_products);
    $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id');
    $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
    $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
    $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
    $this->db->where('ordered_products.timestamp >=',$data['start_date'].' 00:00:00');
    $this->db->where('ordered_products.timestamp <=',$data['end_date'].' 24:59:59');
    $this->db->group_by('products.product_id,sku_name,business_name');
    $this->db->order_by('SUM(ordered_products.quantity)', 'desc');
    $returned_data= $this->db->get();
    $response['topselling']=$returned_data->result_array();


      $query = $this->db->query("SELECT supplier_id,business_name FROM suppliers");
      $response['list_of_dcs'] = $query->result_array(); 
      
     $response['dcs'] = array();
     $response['dcs_list_of_sales'] = array();
      $response['dcs_no_cutomers'] = array();
      foreach ($query->result_array() as $row)
      {
          array_push($response['dcs'], $row['business_name']);


          $this->db->select_sum('total_cost');
          $this->db->from('supplier_orders');
          $this->db->where('supplier_orders.timestamp >=',$data['start_date'].' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',$data['end_date'].' 24:59:59');
          $this->db->where('supplier_id', $row['supplier_id']);
          $query=$this->db->get();
          $totalsalesdc=(int)$query->row()->total_cost;
          $response['totalsales'.$row['supplier_id']]=$totalsalesdc;
          array_push($response['dcs_list_of_sales'], $totalsalesdc);

         $this->db->select('*');
         $this->db->from(' retailer_suppliers');
         $this->db->where('supplier_id', $row['supplier_id']);
         $this->db->where('retailer_suppliers.timestamp >=',$data['start_date'].' 00:00:00');
         $this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
         array_push($response['dcs_no_cutomers'], $this->db->count_all_results());

          $this->db->select('SUM(ordered_products.quantity) AS no_items,name,sku_name,business_name,product_name,product_description');
          $this->db->from($this->ordered_products);
          $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id');
          $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
          $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
          $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
          $this->db->join($this->manufaturers,$this->products.'.manufacturer_id='.$this->manufaturers.'.manufacturer_id');
          $this->db->where('suppliers.supplier_id',$row['supplier_id']);
          $this->db->group_by('products.product_id,sku_name');
          $this->db->order_by('SUM(ordered_products.quantity)', 'desc');
          $this->db->limit(10);
          $returned_data= $this->db->get();
          $response['topselling'.$row['supplier_id']]=$returned_data->result_array();

          $this->db->select('SUM(supplier_orders.total_cost) AS totalcost,suppliers.business_name');
          $this->db->from('supplier_orders');
          $this->db->join('suppliers','suppliers.supplier_id=supplier_orders.supplier_id');
          $this->db->where('supplier_orders.timestamp >=',$data['start_date'].' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',$data['end_date'].' 24:59:59');
          $this->db->where('suppliers.supplier_id',$row['supplier_id']);
          $this->db->group_by('suppliers.business_name');
            $returned_dc_sales= $this->db->get();
           $response['dcsales'.$row['supplier_id']]=$returned_dc_sales->result_array();
         

          

      }
      return json_encode($response);
} 
 public function periodic_customer($data)
 {
    // $response=array();
    // $this->db->select('*');
    // $this->db->where('ordered_products.timestamp >=',$data['start_date'].' 00:00:00');
    // $this->db->where('ordered_products.timestamp <=',$data['end_date'].' 24:59:59');
    // $this->db->from('ordered_products');
    // $query1=$this->db->get();
    // // $rows=$query1->result_array();
    // $response['total_sales']=0;
    // foreach ($query1->result_array() as $row)
    // { 
    //   $item_total_cost=0;
    //   $tiem_cost_novat=$row['unit_cost']*$row['quantity'];
    //   $item_vat=$row['vat']*$row['quantity'];
    //   $item_total_cost =$tiem_cost_novat+$item_vat;
    //   $response['total_sales']=$response['total_sales']+$item_total_cost ;
    // }

   

    // $this->db->select('*');
    // $this->db->where('orders.timestamp >=',$data['start_date'].' 00:00:00');
    // $this->db->where('orders.timestamp <=',$data['end_date'].' 24:59:59');
    // $this->db->from('orders');
    // $query=$this->db->get();
    // $response['sales_list']= $query->result_array();


    // $this->db->select('*');
    // $this->db->from('retailer_suppliers');
    //  $this->db->where('retailer_suppliers.timestamp >=',$data['start_date'].' 00:00:00');
    // $this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
    // $response['total_newcustomers']=(int)$this->db->count_all_results();

    // $this->db->select('*');
    // $this->db->where('orders.timestamp >=',$data['start_date'].' 00:00:00');
    // $this->db->where('orders.timestamp <=',$data['end_date'].' 24:59:59');
    // $this->db->from('orders');
    // $response['total_orders']=(int)$this->db->count_all_results();

    // $this->db->select('*');
    // $this->db->from('retailer_suppliers');
    // $this->db->where('retailer_suppliers.timestamp <',$data['start_date'].' 00:00:00');
    // $response['o_customers']=(int)$this->db->count_all_results();

    // $this->db->select('*');
    // $this->db->from('retailer_suppliers');
    // $this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
    // $response['c_customers']=(int)$this->db->count_all_results();

    // $this->db->select('*');
    // $this->db->from('retailer_suppliers');
    //  $this->db->where('retailer_suppliers.timestamp >=',$data['start_date'].' 00:00:00');
    // $this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
    // $response['new_customers']=(int)$this->db->count_all_results();

    // $this->db->select('*');
    // $this->db->where('orders.timestamp >=',$data['start_date'].' 00:00:00');
    // $this->db->where('orders.timestamp <=',$data['end_date'].' 24:59:59');
    // $this->db->from('orders');
    // $query=$this->db->get();
    // $response['sales_list']= $query->result_array();

    $this->db->select('retailers.retailer_id,retailers.retailer_name,retailers. phone,retailers. phone,retailers. mpesa_number,suppliers.business_name ');
    $this->db->join('retailers','retailers.retailer_id=retailer_suppliers.retailer_id');
    $this->db->join('login_details','login_details.phone=retailers.phone');
    $this->db->join('suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id');
    $this->db->from('retailer_suppliers');
    $this->db->where('retailer_suppliers.timestamp >=',$data['start_date'].' 00:00:00');
     $this->db->where('login_details.user_type =',1);
    $this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
    $query=$this->db->get();
    $response['new_customers']=$query->result_array();
   
    
    $this->db->select('retailers.retailer_id,retailers.retailer_name,retailers. phone,retailers. phone,retailers. mpesa_number,suppliers.business_name ');
    $this->db->join('retailers','retailers.retailer_id=retailer_suppliers.retailer_id');
     $this->db->join('login_details','login_details.phone=retailers.phone');
    $this->db->join('suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id');
    $this->db->from('retailer_suppliers');
      $this->db->where('login_details.user_type =',1);
    // $this->db->where('retailer_suppliers.timestamp >=',$data['start_date'].' 00:00:00');
    // $this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
    $query1=$this->db->get();
    $response['all_customers']=$query1->result_array();
    foreach ($query1->result_array() as $row)
    {
          $this->db->select_sum('total_cost');
          $this->db->from('orders');
          $this->db->where('orders.timestamp >=',$data['start_date'].' 00:00:00');
          $this->db->where('orders.timestamp <=',$data['end_date'].' 24:59:59');
          $this->db->where('retailer_id', $row['retailer_id']);
          $query=$this->db->get();
          $totalordersretailer=(int)$query->row()->total_cost;
          $response['retailer_totals'.$row['retailer_id']]=$totalordersretailer;
    }
    
    // $this->db->select('SUM(ordered_products.quantity) AS no_items,sku_name,business_name,product_name');
    // $this->db->from($this->ordered_products);
    // $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id');
    // $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
    // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
    // $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
    // $this->db->where('ordered_products.timestamp >=',$data['start_date'].' 00:00:00');
    // $this->db->where('ordered_products.timestamp <=',$data['end_date'].' 24:59:59');
    // $this->db->group_by('products.product_id,sku_name,business_name');
    // $this->db->order_by('SUM(ordered_products.quantity)', 'desc');
    // $returned_data= $this->db->get();
    // $response['topselling']=$returned_data->result_array();


//       $query = $this->db->query("SELECT supplier_id,business_name FROM suppliers");
//       $response['list_of_dcs'] = $query->result_array(); 
      
//      $response['dcs'] = array();
//      $response['dcs_list_of_sales'] = array();
//       $response['dcs_no_cutomers'] = array();
//       foreach ($query->result_array() as $row)
//       {
//           array_push($response['dcs'], $row['business_name']);


//           $this->db->select_sum('total_cost');
//           $this->db->from('supplier_orders');
//           $this->db->where('supplier_orders.timestamp >=',$data['start_date'].' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',$data['end_date'].' 24:59:59');
//           $this->db->where('supplier_id', $row['supplier_id']);
//           $query=$this->db->get();
//           $totalsalesdc=(int)$query->row()->total_cost;
//           $response['totalsales'.$row['supplier_id']]=$totalsalesdc;
//           array_push($response['dcs_list_of_sales'], $totalsalesdc);

//          $this->db->select('*');
//          $this->db->from(' retailer_suppliers');
//          $this->db->where('supplier_id', $row['supplier_id']);
//          $this->db->where('retailer_suppliers.timestamp >=',$data['start_date'].' 00:00:00');
//          $this->db->where('retailer_suppliers.timestamp <=',$data['end_date'].' 24:59:59');
//          array_push($response['dcs_no_cutomers'], $this->db->count_all_results());

//           $this->db->select('SUM(ordered_products.quantity) AS no_items,name,sku_name,business_name,product_name,product_description');
//           $this->db->from($this->ordered_products);
//           $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id');
//           $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
//           $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
//           $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
//           $this->db->join($this->manufaturers,$this->products.'.manufacturer_id='.$this->manufaturers.'.manufacturer_id');
//           $this->db->where('suppliers.supplier_id',$row['supplier_id']);
//           $this->db->group_by('products.product_id,sku_name');
//           $this->db->order_by('SUM(ordered_products.quantity)', 'desc');
//           $this->db->limit(10);
//           $returned_data= $this->db->get();
//           $response['topselling'.$row['supplier_id']]=$returned_data->result_array();

//           $this->db->select('SUM(supplier_orders.total_cost) AS totalcost,suppliers.business_name');
//           $this->db->from('supplier_orders');
//           $this->db->join('suppliers','suppliers.supplier_id=supplier_orders.supplier_id');
//           $this->db->where('supplier_orders.timestamp >=',$data['start_date'].' 00:00:00');
//           $this->db->where('supplier_orders.timestamp <=',$data['end_date'].' 24:59:59');
//           $this->db->where('suppliers.supplier_id',$row['supplier_id']);
//           $this->db->group_by('suppliers.business_name');
//             $returned_dc_sales= $this->db->get();
//            $response['dcsales'.$row['supplier_id']]=$returned_dc_sales->result_array();
         

          

     
      return json_encode($response);
} 
  public function todaysales($data)
 {
           $this->db->select('supplier_id');
           $this->db->where('retailer_id',$data['retailer']);
           $this->db->from('retailer_suppliers');
           $returned_data= $this->db->get();
           $dataa=$returned_data->result_array();
         
          $supplier_id=$dataa[0]['supplier_id'];
          $response['supplier']=$supplier_id;
          $date=$data['date'];
          $this->db->select('orders.order_id,orders.agent_id,retailers.retailer_name AS retailer_name,retailers.retailer_id,retailers.phone,supplier_orders.total_cost,supplier_orders.vat AS supp_vat , orders.order_type as order_type');
          $this->db->where('supplier_orders.timestamp >=',$date.' 00:00:00');
          $this->db->where('supplier_orders.timestamp <=',$date.' 24:59:59');
          $this->db->where('supplier_orders.supplier_id ',$supplier_id);

          // $this->db->join($this->stock,$this->stock.'.stock_id=ordered_products.stock_id');
          $this->db->join($this->orders,$this->orders.'.order_id=supplier_orders.order_id');
          // $this->db->join($this->orders_payment,$this->orders_payment.'.order_id=orders.order_id','left');
           // $this->db->join("supplier_orders",'supplier_orders.order_id='.$this->orders.'.order_id');
           // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
          $this->db->join($this->retailers,$this->retailers.'.retailer_id='.$this->orders.'.retailer_id');
          $this->db->from('supplier_orders');
          $this->db->order_by('retailer_name');
          
          $query=$this->db->get();
          $response['sales_list']= $query->result_array();
          $response['total_sales']=0;
          foreach ($query->result_array() as $row)
          {
            $this->db->select('ordered_products.order_id AS id_order,products.product_id AS product_id,ordered_products.stock_id AS stock_id,ordered_products.unit_cost,ordered_products.quantity,products.product_name,skus.sku_name,ordered_products.unit_cost As total_unit_cost,(ordered_products.cost_of_sales*ordered_products.quantity) As total_unit_bp,ordered_products.vat AS total_unit_vat,ordered_products.timestamp');
            $this->db->where('ordered_products.timestamp >=',$date.' 00:00:00');
            $this->db->where('ordered_products.timestamp <=',$date.' 24:59:59');
            $this->db->where('orders.order_id ',$row['order_id']);
            $this->db->from('ordered_products');
          // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
            $this->db->join($this->orders,$this->orders.'.order_id=ordered_products.order_id');
            $this->db->join($this->stock,$this->stock.'.stock_id=ordered_products.stock_id');
            $this->db->join('products','products.product_id=stock.product_id');
            $this->db->join('skus','skus.sku_id=stock.sku_id');
          $this->db->join($this->retailers,$this->retailers.'.retailer_id='.$this->orders.'.retailer_id');
          // $this->db->order_by('ordered_products.timestamp');
          $query2=$this->db->get();
          $agentname="";
          $response['retailer_products'.$row['order_id']] =$query2->result_array(); 

         // $response['agent_name'.$row['order_id']]="Direct Purchase";

    //load paymentdetails
        $this->db->select('orders_payment.paid_by,orders_payment.amount_paid,orders_payment.id As payer_id,orders_payment.payment_date');
         $this->db->where('orders_payment.order_id ',$row['order_id']);
          $this->db->from($this->orders_payment);
          $querypayments=$this->db->get();
          $querypayments->result_array();

        $response['order_payments'.$row['order_id']] =$querypayments->result_array();
        //load payment details

     if($row['order_type']==0)
     {
       $this->db->select('retailer_name');
      $this->db->from('retailers');
      $this->db->where('retailer_id',$row['agent_id']);
      
    // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
   // $this->db->order_by('ordered_products.timestamp');
      $query4=$this->db->get();
      $returnedbyquery4=$query4->result_array();
      $agentname= $returnedbyquery4[0]['retailer_name'];
      $response['agent_name'.$row['order_id']]=$agentname;
     }
     else
     {
        $response['agent_name'.$row['order_id']]="Direct Purchase";
     }
     
      $this->db->select('supplier_orders.total_cost as totalcost,supplier_orders.vat as totalvat');
      $this->db->where('supplier_orders.timestamp >=',$date.' 00:00:00');
      $this->db->where('supplier_orders.timestamp <=',$date.' 24:59:59');
      $this->db->where('supplier_orders.order_id ',$row['order_id']);
      $this->db->from('supplier_orders');
    
    
    
    // $this->db->order_by('ordered_products.timestamp');
    $query3=$this->db->get();
    $datatotal=$query3->result_array();;
    $response['order_total'.$row['order_id']] =$datatotal[0]['totalcost']+$datatotal[0]['totalvat'];
    $response['total_sales']=$response['total_sales']+round($datatotal[0]['totalcost']+$datatotal[0]['totalvat']);
   // $response['order_bp'.$row['order_id']]= $datatotal[0]['totalbp'];
   // $response['order_margin'.$row['order_id']] = $response['order_total'.$row['order_id']]-$datatotal[0]['totalbp'];
        
    }

    return json_encode($response);
 } 
 public function todaysalesSuper($data)
 {
    $date=$data['date'];
    $this->db->select('orders.order_id,orders.agent_id,retailers.retailer_name AS retailer_name,retailers.retailer_id,retailers.phone,supplier_orders.total_cost, supplier_orders.vat AS supp_vat , orders.order_type as order_type');
    $this->db->where('supplier_orders.timestamp >=',$date.' 00:00:00');
    $this->db->where('supplier_orders.timestamp <=',$date.' 24:59:59');
    // $this->db->where('supplier_orders.supplier_id ',$supplier_id);
    // $this->db->join($this->stock,$this->stock.'.stock_id=ordered_products.stock_id');
    $this->db->join($this->orders,$this->orders.'.order_id=supplier_orders.order_id');
    // $this->db->join($this->orders_payment,$this->orders_payment.'.order_id=orders.order_id','left');
     // $this->db->join("supplier_orders",'supplier_orders.order_id='.$this->orders.'.order_id');
     // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
    $this->db->join($this->retailers,$this->retailers.'.retailer_id='.$this->orders.'.retailer_id');
    $this->db->from('supplier_orders');
    $this->db->order_by('retailer_name');
    
    $query=$this->db->get();
    $response['sales_list']= $query->result_array();
    $response['total_sales'] =0;
    foreach ($query->result_array() as $row)
    {
      $this->db->select('ordered_products.order_id AS id_order,products.product_id AS product_id,ordered_products.stock_id AS stock_id,ordered_products.unit_cost,ordered_products.quantity,products.product_name,skus.sku_name,ordered_products.unit_cost As total_unit_cost,(ordered_products.cost_of_sales*ordered_products.quantity) As total_unit_bp,ordered_products.vat AS total_unit_vat,ordered_products.timestamp');
      $this->db->where('ordered_products.timestamp >=',$date.' 00:00:00');
      $this->db->where('ordered_products.timestamp <=',$date.' 24:59:59');
      $this->db->where('orders.order_id ',$row['order_id']);
      $this->db->from('ordered_products');
    // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
      $this->db->join($this->orders,$this->orders.'.order_id=ordered_products.order_id');
      $this->db->join($this->stock,$this->stock.'.stock_id=ordered_products.stock_id');
      $this->db->join('products','products.product_id=stock.product_id');
      $this->db->join('skus','skus.sku_id=stock.sku_id');
    $this->db->join($this->retailers,$this->retailers.'.retailer_id='.$this->orders.'.retailer_id');
    
    
    // $this->db->order_by('ordered_products.timestamp');
    $query2=$this->db->get();
        $response['retailer_products'.$row['order_id']] =$query2->result_array(); 
         

      //load paymentdetails
        $this->db->select('orders_payment.paid_by,orders_payment.amount_paid,orders_payment.id As payer_id,orders_payment.payment_date');
         $this->db->where('orders_payment.order_id ',$row['order_id']);
          $this->db->from($this->orders_payment);
          $querypayments=$this->db->get();
          $querypayments->result_array();

        $response['order_payments'.$row['order_id']] =$querypayments->result_array();
        //load payment details
     if($row['order_type']==0)
     {
       $this->db->select('retailer_name');
      $this->db->from('retailers');
      $this->db->where('retailer_id',$row['agent_id']);
      
    // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
   // $this->db->order_by('ordered_products.timestamp');
      $query4=$this->db->get();
      $returnedbyquery4=$query4->result_array();
      $agentname= $returnedbyquery4[0]['retailer_name'];
      $response['agent_name'.$row['order_id']]=$agentname;
     }
     else
     {
        $response['agent_name'.$row['order_id']]="Direct Purchase";
     }

     

      $this->db->select('supplier_orders.total_cost as totalcost,supplier_orders.vat as totalvat');
      $this->db->where('supplier_orders.timestamp >=',$date.' 00:00:00');
      $this->db->where('supplier_orders.timestamp <=',$date.' 24:59:59');
      $this->db->where('supplier_orders.order_id ',$row['order_id']);
      $this->db->from('supplier_orders');
    
    
    
    // $this->db->order_by('ordered_products.timestamp');
    $query3=$this->db->get();
    $datatotal=$query3->result_array();
    $response['order_total'.$row['order_id']] =$datatotal[0]['totalcost']+$datatotal[0]['totalvat'];
    $response['total_sales']=$response['total_sales']+round($datatotal[0]['totalcost']+$datatotal[0]['totalvat']);
   // $response['order_bp'.$row['order_id']]= $datatotal[0]['totalbp'];
   // $response['order_margin'.$row['order_id']] = $response['order_total'.$row['order_id']]-$datatotal[0]['totalbp'];
        
    }

    return json_encode($response);


 
    
    

 }
 public function todaymargins($data)
 {
  
           $this->db->select('supplier_id');
           $this->db->where('retailer_id',$data['retailer']);
           $this->db->from('retailer_suppliers');
           $returned_data= $this->db->get();
           $dataa=$returned_data->result_array();
         
          $supplier_id=$dataa[0]['supplier_id'];
           $response['supplier']=$supplier_id;
    $date=$data['date'];
    $this->db->select('orders.order_id,retailers.retailer_name AS retailer_name,retailers.retailer_id,retailers.phone,supplier_orders.total_cost,supplier_orders.cost_of_sales as total_cost_of_sales');
    $this->db->where('supplier_orders.timestamp >=',$date.' 00:00:00');
    $this->db->where('supplier_orders.timestamp <=',$date.' 24:59:59');
    $this->db->where('supplier_orders.supplier_id ',$supplier_id);
    // $this->db->join($this->stock,$this->stock.'.stock_id=ordered_products.stock_id');
    $this->db->join($this->orders,$this->orders.'.order_id=supplier_orders.order_id');
     // $this->db->join("supplier_orders",'supplier_orders.order_id='.$this->orders.'.order_id');
     // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
    $this->db->join($this->retailers,$this->retailers.'.retailer_id='.$this->orders.'.retailer_id');
    $this->db->from('supplier_orders');
    $this->db->order_by('retailer_name');
    
    $query=$this->db->get();
    $response['sales_list']= $query->result_array();
    $response['total_margins']=0;
    foreach ($query->result_array() as $row)
    {
      $this->db->select('ordered_products.unit_cost,ordered_products.quantity,products.product_name,skus.sku_name,(ordered_products.unit_cost*ordered_products.quantity) As sp,(ordered_products.cost_of_sales*ordered_products.quantity) As bp,(ordered_products.vat*ordered_products.quantity) As vat,ordered_products.cost_of_sales as bp,ordered_products.timestamp');
      $this->db->where('ordered_products.timestamp >=',$date.' 00:00:00');
      $this->db->where('ordered_products.timestamp <=',$date.' 24:59:59');
      $this->db->where('orders.order_id ',$row['order_id']);
      $this->db->from('ordered_products');
    // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
      $this->db->join($this->orders,$this->orders.'.order_id=ordered_products.order_id');
      $this->db->join($this->stock,$this->stock.'.stock_id=ordered_products.stock_id');
      $this->db->join('products','products.product_id=stock.product_id');
      $this->db->join('skus','skus.sku_id=stock.sku_id');
    $this->db->join($this->retailers,$this->retailers.'.retailer_id='.$this->orders.'.retailer_id');
    
    
    // $this->db->order_by('ordered_products.timestamp');
    $query2=$this->db->get();
        $response['retailer_products'.$row['order_id']] =$query2->result_array(); 

   //      $this->db->select('agent_primary_retailer.agent_id');
   //    $this->db->from('agent_primary_retailer');
   //    $this->db->where('agent_primary_retailer.retailer_id',$row['retailer_id']);
   //    $query4=$this->db->get();
   //    $dataretaileragentid=$query4->result_array();
   //    $agent_id =  $dataretaileragentid[0]['agent_id'];


   //    $this->db->select('retailer_name');
   //    $this->db->from('retailers');
   //    $this->db->where('retailer_id',$agent_id);
      
   //  // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
   // // $this->db->order_by('ordered_products.timestamp');
   //    $query4=$this->db->get();
   //    $returnedbyquery4=$query4->result_array();
   //    $agentname= $returnedbyquery4[0]['retailer_name'];
      $response['agent_name']="Not necessary";

      $this->db->select('supplier_orders.total_cost as totalcost,supplier_orders.vat as totalvat,supplier_orders.cost_of_sales as cost_of_sales');
      $this->db->where('supplier_orders.timestamp >=',$date.' 00:00:00');
      $this->db->where('supplier_orders.timestamp <=',$date.' 24:59:59');
      $this->db->where('supplier_orders.order_id ',$row['order_id']);
      $this->db->from('supplier_orders');
    
    
    
    // $this->db->order_by('ordered_products.timestamp');
    $query3=$this->db->get();
    $datatotal=$query3->result_array();;
    $response['order_total'.$row['order_id']] =round($datatotal[0]['totalcost']+$datatotal[0]['totalvat']);
   $response['order_bp'.$row['order_id']]= $datatotal[0]['cost_of_sales'];
   $response['order_margin'.$row['order_id']] = $response['order_total'.$row['order_id']]-$datatotal[0]['cost_of_sales'];
   $response['total_margins']=$response['total_margins']+$response['order_margin'.$row['order_id']];
        
    }

    return json_encode($response);


 
    
    

 }
 public function todaymarginsSuper($data)
 {
  
          //  $this->db->select('supplier_id');
          //  $this->db->where('retailer_id',$data['retailer']);
          //  $this->db->from('retailer_suppliers');
          //  $returned_data= $this->db->get();
          //  $dataa=$returned_data->result_array();
         
          // $supplier_id=$dataa[0]['supplier_id'];
          //  $response['supplier']=$supplier_id;
    $date=$data['date'];
    $this->db->select('orders.order_id,retailers.retailer_name AS retailer_name,retailers.retailer_id,retailers.phone,supplier_orders.total_cost,supplier_orders.cost_of_sales as total_cost_of_sales');
    $this->db->where('supplier_orders.timestamp >=',$date.' 00:00:00');
    $this->db->where('supplier_orders.timestamp <=',$date.' 24:59:59');
    // $this->db->where('supplier_orders.supplier_id ',$supplier_id);
    // $this->db->join($this->stock,$this->stock.'.stock_id=ordered_products.stock_id');
    $this->db->join($this->orders,$this->orders.'.order_id=supplier_orders.order_id');
     // $this->db->join("supplier_orders",'supplier_orders.order_id='.$this->orders.'.order_id');
     // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
    $this->db->join($this->retailers,$this->retailers.'.retailer_id='.$this->orders.'.retailer_id');
    $this->db->from('supplier_orders');
    $this->db->order_by('retailer_name');
    
    $query=$this->db->get();
    $response['sales_list']= $query->result_array();
    $response['total_margins']=0;
    foreach ($query->result_array() as $row)
    {
       $this->db->select('ordered_products.unit_cost,ordered_products.quantity,products.product_name,skus.sku_name,(ordered_products.unit_cost*ordered_products.quantity) As sp,(ordered_products.cost_of_sales*ordered_products.quantity) As bp,(ordered_products.vat*ordered_products.quantity) As vat,ordered_products.timestamp');
      $this->db->where('ordered_products.timestamp >=',$date.' 00:00:00');
      $this->db->where('ordered_products.timestamp <=',$date.' 24:59:59');
      $this->db->where('orders.order_id ',$row['order_id']);
      $this->db->from('ordered_products');
    // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
      $this->db->join($this->orders,$this->orders.'.order_id=ordered_products.order_id');
      $this->db->join($this->stock,$this->stock.'.stock_id=ordered_products.stock_id');
      $this->db->join('products','products.product_id=stock.product_id');
      $this->db->join('skus','skus.sku_id=stock.sku_id');
      $this->db->join($this->retailers,$this->retailers.'.retailer_id='.$this->orders.'.retailer_id');
    
    
    // $this->db->order_by('ordered_products.timestamp');
    $query2=$this->db->get();
        $response['retailer_products'.$row['order_id']] =$query2->result_array(); 

   //      $this->db->select('agent_primary_retailer.agent_id');
   //    $this->db->from('agent_primary_retailer');
   //    $this->db->where('agent_primary_retailer.retailer_id',$row['retailer_id']);
   //    $query4=$this->db->get();
   //    $dataretaileragentid=$query4->result_array();
   //    $agent_id =  $dataretaileragentid[0]['agent_id'];


   //    $this->db->select('retailer_name');
   //    $this->db->from('retailers');
   //    $this->db->where('retailer_id',$agent_id);
      
   //  // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
   // // $this->db->order_by('ordered_products.timestamp');
   //    $query4=$this->db->get();
   //    $returnedbyquery4=$query4->result_array();
   //    $agentname= $returnedbyquery4[0]['retailer_name'];
      $response['agent_name']="Not necessary";

      $this->db->select('supplier_orders.total_cost as totalcost,supplier_orders.vat as totalvat,supplier_orders.cost_of_sales as cost_of_sales');
      $this->db->where('supplier_orders.timestamp >=',$date.' 00:00:00');
      $this->db->where('supplier_orders.timestamp <=',$date.' 24:59:59');
      $this->db->where('supplier_orders.order_id ',$row['order_id']);
      $this->db->from('supplier_orders');
    
    
    
    // $this->db->order_by('ordered_products.timestamp');
    $query3=$this->db->get();
    $datatotal=$query3->result_array();;
    $response['order_total'.$row['order_id']] =round($datatotal[0]['totalcost']+$datatotal[0]['totalvat']);
   $response['order_bp'.$row['order_id']]= $datatotal[0]['cost_of_sales'];
   $response['order_margin'.$row['order_id']] = $response['order_total'.$row['order_id']]-$datatotal[0]['cost_of_sales'];
   $response['total_margins']=$response['total_margins']+$response['order_margin'.$row['order_id']];
        
    }

    return json_encode($response);


 
    
    

 }

 public function todaystocks($data)
 {
    $this->db->select('products.product_name,skus.sku_name,stock.quantity,stock.unit_cost,suppliers.business_name');
    // $this->db->where('ordered_products.timestamp >=','2018-06-04'.' 00:00:00');
    // $this->db->where('ordered_products.timestamp <=','2018-06-04'.' 24:59:59');
    // $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->supplier_orders.'.supplier_id');
    $this->db->join($this->products,$this->products.'.product_id=stock.product_id');
    $this->db->join($this->skus,$this->skus.'.sku_id=stock.sku_id');
    $this->db->join($this->suppliers,$this->suppliers.'.supplier_id=stock.supplier_id');
    // $this->db->join($this->retailers,$this->retailers.'.retailer_id='.$this->orders.'.retailer_id');
    $this->db->from('stock');
    $this->db->order_by('products.product_name');
    // $this->db->group_by('retailers.retailer_name');
    $query=$this->db->get();
    $response['stock_list']= $query->result_array();
    
    return json_encode($response);


 
    
    

 } 
 // public function retailer_products($retailer)
 // {
    
 //    $response['retailer_products']= $query->result_array();
 //    return json_encode($response);
 // }
 public function brand_dc_periodic()
 {
   

 } 

 public  function top_items()
      {
    $this->db->select('SUM(ordered_products.quantity) AS no_items,name,sku_name,business_name,product_name,product_description');
    $this->db->from($this->ordered_products);
    $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id');
    $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
    $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
    $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
    $this->db->join($this->manufaturers,$this->products.'.manufacturer_id='.$this->manufaturers.'.manufacturer_id');
    $this->db->where('suppliers.supplier_id',3);
    $this->db->group_by('products.product_id,sku_name');
    $this->db->order_by('SUM(ordered_products.quantity)', 'desc');
    $this->db->limit(5);
    $returned_data= $this->db->get();
    $data['kitengela_top_selling']=$returned_data->result_array();


    $this->db->select('SUM(ordered_products.quantity) AS no_items,name,sku_name,business_name,product_name,product_description');
    $this->db->from($this->ordered_products);
    $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id');
    $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
    $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
    $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
    $this->db->join($this->manufaturers,$this->products.'.manufacturer_id='.$this->manufaturers.'.manufacturer_id');
    $this->db->where('suppliers.supplier_id',10);
    $this->db->group_by('product_name,sku_name');
    $this->db->order_by('SUM(ordered_products.quantity)', 'desc');
    $this->db->limit(5);
    $returned_data= $this->db->get();
    $data['ngong_top_selling']=$returned_data->result_array();

     $this->db->select('SUM(ordered_products.quantity) AS no_items,name,sku_name,business_name,product_name,product_description');
    $this->db->from($this->ordered_products);
    $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id');
    $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
    $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
    $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
    $this->db->join($this->manufaturers,$this->products.'.manufacturer_id='.$this->manufaturers.'.manufacturer_id');
    $this->db->where('suppliers.supplier_id',11);
    $this->db->group_by('product_name,sku_name');
    $this->db->order_by('SUM(ordered_products.quantity)', 'desc');
    $this->db->limit(5);
    $returned_data= $this->db->get();
    $data['fedha_top_selling']=$returned_data->result_array();

    $this->db->select('SUM(ordered_products.quantity) AS no_items,name,sku_name,business_name,product_name,product_description');
    $this->db->from($this->ordered_products);
    $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id');
    $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
    $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
    $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
    $this->db->join($this->manufaturers,$this->products.'.manufacturer_id='.$this->manufaturers.'.manufacturer_id');
    $this->db->where('suppliers.supplier_id',12);
   $this->db->group_by('product_name,sku_name');
    $this->db->order_by('SUM(ordered_products.quantity)', 'desc');
    $this->db->limit(5);
    $returned_data= $this->db->get();
    $data['bomet_top_selling']=$returned_data->result_array();
    return json_encode($data);
       
        // $this->db->select('stock_id,product_id,sku_id');
        // $this->db->from('stock');
        // $returned_data= $this->db->get();
        // $allstock=$returned_data->result_array();  
        // $total_product=sizeof($allstock);
        // $arrayperproduct = array();
        // $arrayproductdetails = array();
        // $arrayskus = array();
        // for($i=0;$i<$total_product;$i++)
        //     {
        //      $stock_id = $allstock[$i]['stock_id'];
        //      $product_id = $allstock[$i]['product_id'];
        //      $sku_id = $allstock[$i]['sku_id'];
        //      $this->db->select_sum('quantity');
        //      $this->db->from('ordered_products');
        //      $this->db->where('stock_id', $stock_id);
        //      $query=$this->db->get();
        //      $totalquantity=(int)$query->row()->quantity;
        //      array_push($arrayperproduct, ''.$stock_id.'=>'.$totalquantity.''); 
        //      //get the product_name
        //      $this->db->select('product_name AS product_name');
        //      $this->db->where('product_id', $product_id);
        //      $this->db->from('products');
        //      $returned_data= $this->db->get();
        //      $product_name=$returned_data->row_array(); 
        //      array_push($arrayproductdetails, '['.$stock_id.'=>'.$product_name['product_name'].']'); 
        //      //get the sku_name
        //      $this->db->select('sku_name AS sku_name');
        //      $this->db->where('sku_id', $product_id);
        //      $this->db->from('skus');
        //      $returned_data= $this->db->get();
        //      $sku_name=$returned_data->row_array(); 
        //      array_push($arrayskus, '['.$stock_id.'=>'.$sku_name['sku_name'].']'); 

        //     }
        // $data['stock_counts'] = $arrayperproduct;
        // $data['product_names'] = $arrayproductdetails;
        // $data['sku_names'] = $arrayskus;
        // $data['total_products'] = $total_product;
        // return json_encode($data);
      }

public function genman_dashboard_analytics_by_month($data)
   {
    
    //get number of days in a month
    $str = $data['month'];
    $exploded=(explode("-",$str));
    $year= $exploded[0];
    $month= $exploded[1];

     $data['total_days_this_month'] = cal_days_in_month(CAL_GREGORIAN, $month, $year); // 31
     //generate an array holding number of days
     $array = array();
     $array1 = array();
     $arraydckitengela = array();
     $arraydcngong = array();
     $arraydcfedha = array();
     $arraydckitengelar = array();
     $arraydcngongr = array();
     $arraydcfedhar = array();
     for($i=1;$i<=($data['total_days_this_month']);$i++)
     {
       array_push($array, $i);
       $this->db->select_sum('total_cost');
       $this->db->from('supplier_orders');
       $this->db->where('Date(timestamp)', $data['month'].'-'.$i);
       $query=$this->db->get();
       $totalsales=(int)$query->row()->total_cost;
       //totalsales kitengela
       $this->db->select_sum('total_cost');
       $this->db->from('supplier_orders');
       $this->db->where('Date(timestamp)', $data['month'].'-'.$i);
       $this->db->where('supplier_id', 3);
       $query=$this->db->get();
       $totalsaleskitengela=(int)$query->row()->total_cost;
       //total sales ngong
       $this->db->select_sum('total_cost');
       $this->db->from('supplier_orders');
       $this->db->where('Date(timestamp)', $data['month'].'-'.$i);
       $this->db->where('supplier_id', 10);
       $query=$this->db->get();
       $totalsalesngong=(int)$query->row()->total_cost;
       //total sales fedha
       $this->db->select_sum('total_cost');
       $this->db->from('supplier_orders');
       $this->db->where('Date(timestamp)', $data['month'].'-'.$i);
       $this->db->where('supplier_id', 11);
       $query=$this->db->get();
       $totalsalesfedha=(int)$query->row()->total_cost;

       //totalretailers kitengela
       $this->db->select('*');
       $this->db->where('Date(timestamp)', $data['month'].'-'.$i);
       $this->db->where('supplier_id', 3);
       $this->db->from(' retailer_suppliers');
       $totalretailerskitengela=round($this->db->count_all_results());
       //totalretailers fedha
       $this->db->select('*');
       $this->db->where('Date(timestamp)', $data['month'].'-'.$i);
       $this->db->where('supplier_id', 11);
       $this->db->from(' retailer_suppliers');
       $totalretailersfedha=round($this->db->count_all_results());
       //totalretailers ngong
       $this->db->select('*');
       $this->db->where('Date(timestamp)', $data['month'].'-'.$i);
       $this->db->where('supplier_id', 10);
       $this->db->from(' retailer_suppliers');
       $totalretailersngong=round($this->db->count_all_results());

        
       
       // $query=$this->db->get();
      
       if($totalsales==null)
       {
        $totalsales=0;
       }
       if($totalsaleskitengela==null)
       {
        $totalsaleskitengela=0;
       }
       if($totalsalesngong==null)
       {
        $totalsalesngong=0;
       }
       if($totalsalesfedha==null)
       {
        $totalsalesfedha=0;
       }

       //retailers set values
        if($totalretailersngong==null)
       {
        $totalretailersngong=0;
       }
       if($totalretailerskitengela==null)
       {
        $totalretailerskitengela=0;
       }
       if($totalretailersfedha==null)
       {
        $totalretailersfedha=0;
       }

      array_push($array1, $totalsales);
      array_push($arraydckitengela, $totalsaleskitengela);
      array_push($arraydcfedha, $totalsalesfedha);
      array_push($arraydcngong, $totalsalesngong);


       array_push($arraydckitengelar, $totalretailerskitengela);
      array_push($arraydcfedhar, $totalretailersfedha);
      array_push($arraydcngongr, $totalretailersngong);
     }
     $data['days']=$array;
     $data['sales']=$array1;
     $data['saleskitengela']=$arraydckitengela;
     $data['salesngong']=$arraydcngong;
     $data['salesfedha']=$arraydcfedha;
     $data['retailerskitengela']=$arraydckitengelar;
     $data['retailersngong']=$arraydcngongr;
     $data['retailersfedha']=$arraydcfedhar;

       array_push($array, $i);
       $this->db->select_sum('total_cost');
       $this->db->from('supplier_orders');
       $this->db->where('Month(timestamp)',$month);
       $this->db->where('Year(timestamp)',$year);
       $query=$this->db->get();
       $data['totalsalesamount']=(int)$query->row()->total_cost;

       $this->db->select('*');
       $this->db->where('Month(timestamp)',$month);
       $this->db->where('Year(timestamp)',$year);
       $this->db->from(' supplier_orders');
       $data['totalorderssamount']=round($this->db->count_all_results());

       $this->db->select('*');
       $this->db->where('Month(timestamp)',$month);
       $this->db->where('Year(timestamp)',$year);
       $this->db->from(' supplier_orders');
       $data['totalretailerssamount']=round($this->db->count_all_results());
     return json_encode($data);

   }

public function genman_dashboard_index()
   {
      $currentyear = date("Y");
      $previousyear = $currentyear -1;
      $wherecurrent = "DATE(timestamp) BETWEEN '".$currentyear."-01-01' AND '".$currentyear."-12-31'";
      $whereprevious = "DATE(timestamp) BETWEEN '".$previousyear."-01-01' AND '".$previousyear."-12-31'";
      $data['dcs'] = array();
      $this->db->select_sum('total_cost');
      $this->db->from('supplier_orders');
      $this->db->where($wherecurrent);
      $query=$this->db->get();
      $dukapepe_sales = (int)$query->row()->total_cost;
      $data['sales_amount'] =$dukapepe_sales;


      $this->db->select('*');
      $this->db->where($wherecurrent);
      $this->db->from('retailer_suppliers');
      $dukapepe_retailers = (int)$this->db->count_all_results();
      $data['retailers_no'] = $dukapepe_retailers;

      $this->db->select('*');
      $this->db->where($wherecurrent);
      $this->db->from('retailer_suppliers');
      $dukapepe_retailers_prev = (int)$this->db->count_all_results();
      $data['orders_no'] = $dukapepe_retailers_prev;


      $this->db->select_sum('total_cost');
      $this->db->from('supplier_orders');
      $this->db->where($whereprevious);
      $query=$this->db->get();
      $dukapepe_sales_prev = (int)$query->row()->total_cost;
      $data['sales_amount_prev'] = $dukapepe_sales_prev;

      $this->db->select('*');
      $this->db->where($whereprevious);
      $this->db->from('retailer_suppliers');
      $dukapepe_retailers_no = (int)$this->db->count_all_results();
      $data['retailers_no_prev'] = $dukapepe_retailers_no;

      $this->db->select('*');
      $this->db->where($whereprevious);
      $this->db->from('supplier_orders');
      $dukapepe_orders_prev = (int)$this->db->count_all_results();
      $data['orders_no_prev'] =   $dukapepe_orders_prev ;

     
      $data['sales_amount_arr'] = array();
      $data['retailers_no_arr'] = array();
      $data['orders_no_arr'] = array();

      $data['sales_amount_prev_arr'] = array();
      $data['retailers_no_prev_arr'] = array();
      $data['orders_no_prev_arr'] = array();
     
      $this->db->select('*');
      $this->db->where($whereprevious);
      $this->db->from('supplier_orders');
      $dukapepe_orders_prev = (int)$this->db->count_all_results();
       
      $query = $this->db->query("SELECT supplier_id,business_name FROM suppliers");
      foreach ($query->result_array() as $row)
      {
       array_push($data['dcs'], $row['business_name']);
       
      $this->db->select_sum('total_cost');
      $this->db->from('supplier_orders');
      $this->db->where($wherecurrent);
       $this->db->where('supplier_orders.supplier_id',$row['supplier_id']);
       $query=$this->db->get();
       $sales=(int)$query->row()->total_cost;
       // $sales_percentage = ($sales/$dukapepe_sales)*100;
       array_push($data['sales_amount_arr'], $sales);

       $this->db->select_sum('total_cost');
       $this->db->from('supplier_orders');
       $this->db->where($whereprevious);
       $this->db->where('supplier_orders.supplier_id',$row['supplier_id']);
       $query_prev=$this->db->get();
       $sales_prev=(int)$query_prev->row()->total_cost;
       // $sales_percentage_prev = ($sales_prev/$dukapepe_sales_prev)*100;
       array_push($data['sales_amount_prev_arr'], $sales_prev);



       $this->db->select('*');
       $this->db->where($wherecurrent);
       $this->db->where('retailer_suppliers.supplier_id',$row['supplier_id']);
       $this->db->from('retailer_suppliers');
       $retailers=(int)$this->db->count_all_results();
       // $retailers_percentage = ($retailers/$dukapepe_retailers)*100;
       array_push($data['retailers_no_arr'], $retailers);

       $this->db->select('*');
       $this->db->where($whereprevious);
       $this->db->where('retailer_suppliers.supplier_id',$row['supplier_id']);
       $this->db->from('retailer_suppliers');
       $retailers_prev=(int)$this->db->count_all_results();
       // $retailers_percentage_prev = ($retailers_prev/$dukapepe_retailers_prev)*100;
       array_push($data['retailers_no_prev_arr'], $retailers_prev);




       $this->db->select('*');
       $this->db->where($wherecurrent);
       $this->db->where('supplier_id', $row['supplier_id']);
       $this->db->from('supplier_orders');
       $orders=(int)$this->db->count_all_results();
       // $orders_percentage = ($orders/$dukapepe_orders)*100;
       array_push($data['orders_no_arr'], $orders);

       $this->db->select('*');
       $this->db->where($whereprevious);
       $this->db->where('supplier_id', $row['supplier_id']);
       $this->db->from('supplier_orders');
       $orders_prev=(int)$this->db->count_all_results();
       // $orders_percentage_prev = ($orders_prev/$dukapepe_orders_prev)*100;
       array_push($data['orders_no_prev_arr'], $orders_prev);

       
      }
     return json_encode($data);
   }
   public function genman_dashboard_index_this_month()
   {
//this month total customers
       $data['total_days_this_month'] = cal_days_in_month(CAL_GREGORIAN, 11, 2017); // 31
       $this->db->select('*');
       $where = "DATE(timestamp) BETWEEN '2017-05-01' AND '2017-05-".$data['total_days_this_month']."'";
       $this->db->where($where);
       $this->db->where('supplier_id', 3);
       $this->db->from(' retailer_suppliers');
       $data['total_retailers_this_month']=round($this->db->count_all_results());

///this month total customers
//this month total sales
       $this->db->select_sum('total_cost');
       $where = "DATE(timestamp) BETWEEN '2017-05-01' AND '2017-05-".$data['total_days_this_month']."'";
       $this->db->where($where);
       $this->db->from('supplier_orders');
       $query=$this->db->get();
       $data['total_sales_this_month']=round($query->row()->total_cost);
///this month total sales
//this month total orders
       $this->db->select('*');
       $where = "DATE(timestamp) BETWEEN '2017-05-01' AND '2017-05-".$data['total_days_this_month']."'";
       $this->db->where($where);
       $this->db->from('supplier_orders');
       $data['total_orders_this_month']=round(round($this->db->count_all_results()));
///this month total orders


       $this->db->select_sum('total_cost');
       $where = "DATE(timestamp) BETWEEN '2017-05-01' AND '2017-05-".$data['total_days_this_month']."'";
       $this->db->where($where);
     
       $this->db->from('supplier_orders');
       $query=$this->db->get();
       $data['total_sales_this_month']=round($query->row()->total_cost);

       $this->db->select_sum('total_cost');
       $where = "DATE(timestamp) BETWEEN '2017-05-01' AND '2017-05-".$data['total_days_this_month']."'";
       $this->db->where($where);
       $this->db->where('supplier_id',10);
       $this->db->from('supplier_orders');
       $query=$this->db->get();
       $data['total_sales_ngong_this_month']=round($query->row()->total_cost);

       $this->db->select_sum('total_cost');
       $where = "DATE(timestamp) BETWEEN '2017-05-01' AND '2017-05-".$data['total_days_this_month']."'";
       $this->db->where($where);
       $this->db->where('supplier_id',14);
       $this->db->from('supplier_orders');
       $query=$this->db->get();
       $data['total_sales_bomet_this_month']=round($query->row()->total_cost);


        $this->db->select_sum('total_cost');
       $where = "DATE(timestamp) BETWEEN '2017-05-01' AND '2017-05-".$data['total_days_this_month']."'";
       $this->db->where($where);
       $this->db->where('supplier_id',11);
       $this->db->from('supplier_orders');
       $query=$this->db->get();
       $data['total_sales_fedha_this_month']=round($query->row()->total_cost);

        $this->db->select_sum('total_cost');
       $where = "DATE(timestamp) BETWEEN '2017-05-01' AND '2017-05-".$data['total_days_this_month']."'";
       $this->db->where($where);
       $this->db->where('supplier_id',3);
       $this->db->from('supplier_orders');
       $query=$this->db->get();
       $data['total_sales_kitengela_this_month']=round($query->row()->total_cost);
       //
       $query = $this->db->query("SELECT supplier_id,business_name FROM suppliers");
      $response['list_of_dcs'] = $query->result_array(); 
      
      $data['dcs'] = array();
      $data['dcs_list_of_sales'] = array();
      $data['dcs_no_cutomers'] = array();
      foreach ($query->result_array() as $row)
      {
         array_push($data['dcs'], $row['business_name']);

      }
       //
       return json_encode($data);
   }


public function registration(){
    $json_cc=$this->Customer_Care_Model->fetch_all();
    $cc_info= json_decode($json_cc,true);
    $i=0;
    while($i<count($cc_info)){
     $cc_id=$cc_info[$i]['cc_id'];   
     $cc_info[$i]['number_registered'] = $this->count_myretailers($cc_id);
     $cc_info[$i]['retailers'] = $this->registered_retailers($cc_id);
    $i++;
    }
    
    return json_encode($cc_info);
}


public function count_myretailers($cc_id){
    $this->db->select('retailer_id'); 
    $this->db->where('referred_by',$cc_id);
    $this->db->from($this->retailers);
    $count= $this->db->count_all_results();
    
    return $count;
}


public function registered_retailers($cc_id){
        $data['referred_by']  = $cc_id;
        $data['is_summary']  = 1;
        $retailers_info = json_decode($this->Retailer_Model->fetch_all($data),true);
        return $retailers_info;
}

public function retailers_registered($dates){
    $dates['is_summary']  = 1;
    $retailers_info = $this->Retailer_Model->fetch_all($dates);
    
    return $retailers_info;
}

public function sales_volume($dates){
    $date_range = $this->date_range($dates);
    $this->db->select('SUM(ordered_products.quantity) AS no_items,sku_name,business_name,product_name,product_description,"'.$date_range.'" AS date_range');
    $this->db->where('ordered_products.timestamp >=',$dates['start_date'].' 00:00:00');
    $this->db->where('ordered_products.timestamp <=',$dates['end_date'].' 24:59:59');
    if(isset($dates['product_id'])){
    $this->db->where('stock.supplier_id',$dates['product_id']);
    }
    $this->db->from($this->ordered_products);
    $this->db->join($this->stock,$this->stock.'.stock_id='.$this->ordered_products.'.stock_id');
    $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
    $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
    $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
    $this->db->group_by('business_name,sku_name');
    
    $returned_data= $this->db->get();
    $data=$returned_data->result_array();
    return json_encode($data);
}

public function date_range($dates){
 $start_date= date_create($dates['start_date']);
 $end_date= date_create($dates['end_date']);
 
 return $start_date->format("d MY")." to ".$end_date->format("d MY").' : '.(date_diff($start_date,$end_date)->format("%a")+1).' day(s)';
}
public function load_category_analytics($data){
          $base_url= base_url()."images/";
          $getSumArray = array();
            $subCatSql = "SELECT * FROM sub_categories";
            $subCatQuery=$this->db->query($subCatSql);
            $subCatResponse = $subCatQuery->result_array();
            foreach ($subCatResponse as $subCatRes)
            {
               $getCatIdFromSubCat[$subCatRes['sub_cat_id']] = $subCatRes['cat_id'];

            }
            // return json_encode($getCatIdFromSubCat);  
            $productsql = "SELECT * FROM products";
            $productsquery=$this->db->query($productsql);
            $productsresponse = $productsquery->result_array();
            foreach ($productsresponse as $productsres)
            {
               $getSubCatIdFromProdId[$productsres['product_id']] = $productsres['sub_cat_id'];
            }
          
            $stockssql = "SELECT * FROM stock";
            $stocksquery=$this->db->query($stockssql);
            $stocksresponse = $stocksquery->result_array();
            foreach ($stocksresponse as $stocksres)
            {
               $getProductIdFromStockID[$stocksres['stock_id']] = $stocksres['product_id'];
            }
            $start_date = $data['start_date'];
            $end_date = $data['end_date'];
            $orderedProductssql = "SELECT SUM((unit_cost*quantity)+(vat*quantity)) AS totalcost,SUM(cost_of_sales*quantity) AS totalpurchases,stock_id FROM ordered_products  WHERE cost_of_sales > 0 AND timestamp >=? AND timestamp <?  GROUP BY stock_id";
            $orderedProductsquery=$this->db->query($orderedProductssql,array($start_date,$end_date));
            $orderedProducresponse = $orderedProductsquery->result_array();
            $total="";
            $z=0;

            foreach ($orderedProducresponse as $orderedProducts)
            {
               $orderedProducts['stock_id'] = $getProductIdFromStockID[$orderedProducts['stock_id']];
                           
               $getSumArray[$getCatIdFromSubCat[$getSubCatIdFromProdId[$orderedProducts['stock_id']]]] += $orderedProducts['totalcost'];
                $getSumArray["ALL"] += $orderedProducts['totalcost'];
               $getSumArrayPurchases[$getCatIdFromSubCat[$getSubCatIdFromProdId[$orderedProducts['stock_id']]]] += $orderedProducts['totalpurchases'];
               $getSumArrayPurchases["ALL"] += $orderedProducts['totalpurchases'];
                // $getSumArraymargins[$getCatIdFromSubCat[$getSubCatIdFromProdId[$orderedProducts['stock_id']]]] += $orderedProducts['margin'];
            }
             // return json_encode($getSumArray);
            $base_url= base_url()."images/";
            $this->db->select('category_name,category_id AS cost_of_sales,category_id AS margin,category_id AS totalsales,timestamp,CONCAT("'.$base_url.'",'.$this->categories.'.image_url) AS image_url');
            $this->db->from($this->categories);
            $returned_data= $this->db->get();
            $response_data=$returned_data->result_array();
            $y=0;
            $tmargins=0;
            foreach ($response_data as $category_response) 
            {
              $rowmargins = $getSumArray[$category_response['totalsales']] -$getSumArrayPurchases[$category_response['cost_of_sales']];
              $category_response['margins'] = number_format($rowmargins,"2",".",",");
              $tmargins = $getSumArray["ALL"] - $getSumArrayPurchases["ALL"];

              $pmargin = ($rowmargins / $tmargins) * 100;

              $category_response['pmargin'] = number_format($pmargin,"2",".",",")." %";
              $category_response['totalsales'] = number_format($getSumArray[$category_response['totalsales']],"2",".",",");
              $category_response['totalpurchase'] = number_format($getSumArrayPurchases[$category_response['cost_of_sales']],"2",".",",");
              

              $category_response['timestamp'] = date_format(date_create($category_response['timestamp']),'jS \ F Y'); 
              // $category_response['totalcost'] = '444';
          
              $categoriesAnalysis[$y++] = $category_response;
            }

            return json_encode($categoriesAnalysis);
       } 
       public function load_product_analytics($data){

          $this->db->select('SUM((ordered_products.unit_cost + ordered_products.vat)*ordered_products.quantity) AS totalsales,SUM(ordered_products.cost_of_sales*ordered_products.quantity) AS totalpurchases,products.product_name,skus.sku_name,suppliers.business_name');
          $this->db->where('ordered_products.timestamp >=',$data['start_date'].' 00:00:00');
          $this->db->where('ordered_products.timestamp <=',$data['end_date'].' 24:59:59');

          $this->db->from($this->ordered_products);
          $this->db->join($this->stock,'stock.stock_id= ordered_products.stock_id');
          $this->db->join($this->skus,$this->skus.'.sku_id='.$this->stock.'.sku_id');
          $this->db->join($this->products,$this->products.'.product_id='.$this->stock.'.product_id');
          $this->db->join($this->suppliers,$this->suppliers.'.supplier_id='.$this->stock.'.supplier_id');
          $this->db->group_by('products.product_id,stock.stock_id');
          
          $productsquery= $this->db->get();
          $response = $productsquery->result_array();

          return json_encode($response);
       }
       public function load_agents_report($data)
       {
            $active_status = 1;
            $typeofuser = 7;

            $start_date = $data['start_date'];
            $end_date = $data['end_date'];

	$this->db->distinct('agents.mpesa_number');
            $this->db->select('agents.retailer_name,agents.phone,agents.agent_id,login_details.phone,login_details.user_type,login_details.is_active');
            $this->db->from('agents');
            $this->db->join('login_details','login_details.phone=agents.phone'); 
            $this->db->where('login_details.user_type', $typeofuser);
            $this->db->where('login_details.is_active',  $active_status);
            $this->db->order_by('agents.retailer_name');
            $agentsquery=$this->db->get();
            $response['agents'] = $agentsquery->result_array();
            $data['total_agent_sales']=array();
 
            // $response['agent_total_sales']=0;
            foreach ($response['agents'] as $row)
            {
             
              $this->db->select('orders.agent_id,SUM(orders.total_cost) AS total_cost,SUM(orders.vat) AS total_vat');
              $this->db->where('orders.agent_id',$row['agent_id']);
              $this->db->where('orders.timestamp >=',$start_date);
              $this->db->where('orders.timestamp <',$end_date);
              $this->db->from('orders');
              $this->db->where('orders.order_type',0);
              $orderquery=$this->db->get();
              $returnedarry=$orderquery->result_array();

              // $response['total_agent_sales'.$row['agent_id']] = ;


             $response['total_agent_salesagent'.$row['agent_id']] = $returnedarry[0]['total_cost'] + $returnedarry[0]['total_vat'];

                $this->db->select('orders.agent_id,SUM(orders.total_cost) AS total_cost,SUM(orders.vat) AS total_vat');
              $this->db->where('orders.agent_id',$row['agent_id']);
              $this->db->where('orders.timestamp >=',$start_date);
              $this->db->where('orders.timestamp <',$end_date);
              $this->db->from('orders');
              $this->db->where('orders.order_type',1);
              $orderquerydirect=$this->db->get();
              $returnedarrydirect=$orderquerydirect->result_array();

              
              $response['total_agent_salesdirect'.$row['agent_id']] = $returnedarrydirect[0]['total_cost'] + $returnedarrydirect[0]['total_vat'];

              


              // array_push($data['total_agent_sales'],['agenttotaldirect'.$row['agent_id']=>$returnedarrydirect[0]['total_cost']]);


           }
           $response['total_agent_sales']=$data['total_agent_sales'];
            return json_encode($response);

       }

       
}

