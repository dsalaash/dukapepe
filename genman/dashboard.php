
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("../links.php") ?>
    <!-- /theme JS files -->
</head>
<body>
   <?php include("../topbar.php") ?>
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">
                        <!-- User menu -->
                        <!-- /user menu -->
                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->
                <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
                <script type="text/javascript">                    
                     if(localStorage.getItem('access_token')=="")
                          {
                           window.location = "http://test.duka-pepe/dukapepe/";
                          }
                      $(document).ready(function ()
                       {
                        //fech initial analytics year
                        function sendRequestForSalesDetails(year,dc)
                         {
                           //fech initial analytics year
                            var light_2 = $('body');
                            $(light_2).block({
                                message: '<i class="icon-spinner2 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'none'
                                }
                            });
                        var parse
                        var now = new Date();
                        var yr = now.getFullYear();
                        var start_date = yr+"-01-01";
                        var end_date = yr+"-12-31";
                        var formData = {'access_token': localStorage.getItem('access_token'), 'dc':dc ,'start_date':start_date, 'end_date':end_date};
                           console.log(formData);
                           var url = base_url + "Analytics/sales_details_this_year";
                           $.ajax({
                           type:'POST',
                           url: url,
                           data: formData,
                           success:function(data)
                           {
                            parse= JSON.parse(data);
                            $(light_2).unblock();
                                var retailers_list="<table class='table' id='example2'><thead><tr><th>Order Id</th><th>Product Name</th><th>Sku Name</th><th>No Items</th><th>Unit Cost</th><th>Sub Total</th></tr></thead><tbody>";
                                     $.each(parse['dcs_list_of_sales'], function (k, v) {
                                      // console.log(parse['dcs_list_of_sales'][k]["product_name"]);
                                      $.each(parse['dcs_list_of_sales'][k], function (index, value) {
                                      retailers_list+="<tr><td>"+ value.order_id
                                      // +"<td>"+ value.product_name
                                          // +"</td><td>"+ value.sku_name
                                          +"</td><td>"+ value.no_items
                                          // +"</td><td>"+ value.unit_cost
                                          +"</td><td>"+ value.product
                                          +"</td></tr>";
                                      });
                                    });
                               retailers_list+="</tbody></table>";
                               document.getElementById("details_dig").innerHTML = "";
                               $("#details_dig").append(retailers_list);
                               $("#example2").DataTable( {"processing": true} );
                            $("#modal_large").modal();
                            return console.log(parse);
                            },
                           error:function(data)
                           {
                            console.log(data);
                           }        
                          });
                        ///fech intial analytics year
                         }
              function sendRequestForSalesDetailslast(year,dc)
                         {
                           //fech initial analytics year
                            var light_2 = $('body');
                            $(light_2).block({
                                message: '<i class="icon-spinner2 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'none'
                                }
                            });
                        var parse
                        var now = new Date();
                        var yr = now.getFullYear()-1;
                        var start_date = yr+"-01-01";
                        var end_date = yr+"-12-31";
                        var formData = {'access_token': localStorage.getItem('access_token'), 'dc':dc ,'start_date':start_date, 'end_date':end_date};
                           console.log(formData);
                           var url = base_url + "Analytics/sales_details_this_year";
                           $.ajax({
                           type:'POST',
                           url: url,
                           data: formData,
                           success:function(data)
                           {
                            parse= JSON.parse(data);
                            $(light_2).unblock();
                                var retailers_list="<table class='table' id='example2'><thead><tr><th>Order Id</th><th>No Items</th><th>Sub Total</th></tr></thead><tbody>";
                                           $.each(parse['dcs_list_of_sales'], function (k, v) {
                                            // console.log(parse['dcs_list_of_sales'][k]["product_name"]);
                                            var no_items =v.length;
                                            var subtotal;
                                            var order_id;
                                            if(no_items==1)
                                                { 
                                                console.log(v['unit_cost']);
                                                // retailers_list+="<tr><td>"+ v.order_id;
                                                // +"</td><td>"+v.length
                                                // +"</td><td>"+subtotal
                                                // +"</td></tr>";
                                                }
                                                else if(no_items>1)
                                                {
                                                      $.each(parse['dcs_list_of_sales'][k], function (index, value) 
                                                      {
                                                        var no_items=v.length;
                                                        var subtotal =0;
                                                        var order_id =0;
                                                        if(index==0) 
                                                        {
                                                         order_id=value['order_id']; 
                                                        }      
                                                       });
                                                      retailers_list+="<tr><td>"+order_id
                                                                +"</td><td>"+v.length
                                                                +"</td><td>"+subtotal
                                                                +"</td></tr>";                                                 
                                                }
                                    });
                               retailers_list+="</tbody></table>";
                               document.getElementById("details_dig").innerHTML = "";
                               $("#details_dig").append(retailers_list);
                               $("#example2").DataTable( {"processing": true} );
                            $("#modal_large").modal();
                            return console.log(parse);
                            },
                           error:function(data)
                           {
                            console.log(data);
                           }        
                          });
                        ///fech intial analytics year
                         }
                         //
                           var formData = {'access_token': localStorage.getItem('access_token')};
                           var url = base_url + "Analytics/genman_dashboard_index";
                           $.ajax({
                           type:'POST',
                           url: url,
                           data: formData,
                           success:function(data)
                           {
                            var parse = JSON.parse(data);
                            console.log(parse);
                            //
                            var jan = parseFloat(parse['jan_total']);
                            $("#total_orders_this_year").append(parse['orders_no']);
                            $("#total_sales_this_year").append(parse['sales_amount']);
                            $("#total_customers_this_year").append(parse['retailers_no']);
                            $("#total_orders_last_year").append(parse['orders_no_prev']);
                            $("#total_sales_last_year").append(parse['sales_amount_prev']);
                            $("#total_customers_last_year").append(parse['retailers_no_prev']);

                            //this year visual
                            $(function () { 
                                                Highcharts.chart('justified-badges-tab1', {
                                                    
                                                   chart: {
        type: 'bar'
        
    },
    title: {
        text: 'Sales'
    },
    xAxis: {
        categories: parse['dcs']
    },

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                         var dc = this.category;
                         var now = new Date();
                         var yr = now.getFullYear();
                         sendRequestForSalesDetails(yr,dc);
                    }
                }
            }
        }
    },

    series: [{
          name:'Sales Amount (Ksh)',
        data: parse['sales_amount_arr']
    }]
                        });
                    });
    ///this year visual
      //this year visual
    $(function () { 
                        Highcharts.chart('justified-badges-tab2', {
                            
                                                                          chart: {
        type: 'bar'
    },
      title: {
        text: 'No Of Orders'
    },
    xAxis: {
        categories: parse['dcs']
    },

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        alert('Category: ' + this.category + ', value: ' + this.y);
                    }
                }
            }
        }
    },

    series: [{
        name:'No Of Orders',
        data: parse['orders_no_arr']
    }]
                                                });
                                            });
                            ///this year visual
                              //this year visual
                                        $(function () { 
                                                Highcharts.chart('justified-badges-tab3', {chart: {
        type: 'bar'
    },
    title: {
        text: 'No Of Retailers'
    },
    xAxis: {
        categories: parse['dcs']
    },

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        alert('Category: ' + this.category + ', value: ' + this.y);
                    }
                }
            }
        }
    },

    series: [{
         name:'No Of Customers',
        data: parse['retailers_no_arr']
    }]
                                                });
                                            });
                            ///this year visual

                               //this year visual
                            $(function () { 
                                                Highcharts.chart('justified-badges-tab5', {
                                                    
                                                   chart: {
        type: 'bar'
    },
    title: {
        text: 'Sales'
    },
    xAxis: {
        categories: parse['dcs']
    },

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                         var dc = this.category;
                         var now = new Date();
                         var yr = now.getFullYear()-1;
                       sendRequestForSalesDetailslast(yr,dc);
                    }
                }
            }
        }
    },

series: [{
name:'Sales(Ksh)',
data: parse['sales_amount_prev_arr']
}]
});
});
///this year visual
//this year visual
$(function () { 
Highcharts.chart('justified-badges-tab6', {
    
                                                  chart: {
type: 'bar'
},
title: {
text: 'No Of Orders'
},
xAxis: {
categories: parse['dcs']
},

plotOptions: {
series: {
cursor: 'pointer',
point: {
events: {
click: function () {
alert('Category: ' + this.category + ', value: ' + this.y);
}
}
}
}
},

series: [{
name:'No Of Orders',
data: parse['orders_no_prev_arr']
}]
});
});
///this year visual
//this year visual
$(function () { 
Highcharts.chart('justified-badges-tab7', {chart: {
type: 'bar'
},
title: {
text: 'No Of Retailers'
},
xAxis: {
categories: parse['dcs']
},

    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        alert('Category: ' + this.category + ', value: ' + this.y);
                    }
                }
            }
        }
    },

    series: [{
          name:'No Of Customers',
        data: parse['retailers_no_prev_arr']
    }]
                                                });
                                            });
                            ///this year visual
                             
                            },
                           error:function(data)
                           {
                            console.log(data);
                           }        

                           });
                        ///fech intial analytics year

                         //fech initial analytics year
                          
                           var formData = {'access_token': localStorage.getItem('access_token')};
                           var url = base_url + "Analytics/genman_dashboard_index_this_month";
                           $.ajax({
                           type:'POST',
                           url: url,
                           data: formData,
                           success:function(data)
                           {
                            

                            var parse= JSON.parse(data);
                            // $("#new_customers_this_month").append(parse['total_retailers_this_month']);
                            // $("#orders_this_month").append(parse['total_orders_this_month']);
                            // // $("#total_sales_this_month").append(parse['total_sales_this_month']);
                            //  $("#total_sales_this_month").append(parse['total_sales_this_month']);
                          

                            },
                           error:function(data)
                           {
                            console.log(data);
                           }        

                          });
                        ///fech intial analytics year

                        $( "#datepicker" ).datepicker({
                          altFormat: "yy-mm-dd"
                        });
                        // Getter
                        var altFormat = $( ".selector" ).datepicker( "option", "altFormat" );
                        // Setter
                        $( "#datepicker" ).datepicker( "option", "altFormat", "yy-mm-dd" );
                          });          
 </script>


<!-- Full width modal -->
                <div id="modal_large" class="modal fade">
                    <div class="modal-dialog modal-full">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Details</h5>
                            </div>

                            <div class="modal-body" id="details_dig">
                               
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /full width modal -->

            <!-- Main content -->
            <div class="content-wrapper">
                <!-- //customerdistribution modal-->

               <!--  ///customerdistribution modal -->

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
                        </div>

                        <div class="heading-elements">
                            <div class="heading-btn-group">
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                            </div>
                        </div>
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-gear position-left"></i>
                                    More Analytics
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="montlyanalytics.php"><i class="icon-user-lock"></i>Monthly</a></li>
                                    <li><a href="annual.php"><i class="icon-statistics"></i>Annual</a></li>
                                    <li><a href="topitems.php"><i class="icon-statistics"></i>topitems</a></li>
                                    <!-- <li><a href="#"><i class="icon-statistics"></i>DC Monthly Comparisons</a></li>
                                    <li><a href="#"><i class="icon-statistics"></i>5 best Selling Items Pe DC</a></li> -->
                                    <!-- <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="icon-gear"></i> All settings</a></li> -->
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->
                    <div class="row">
                        <div class="col-lg-6">
                             <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">This Year</h6>
                                    <div class="heading-elements">
                                    </div>
                                </div>
                                <div class="content-group-sm" id="app_sales"></div>
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
                                             <li ><a href="#justified-badges-tab1" data-toggle="tab">Total Sales <span class="badge badge-info position-right" id="total_sales_this_year"></span></a></li>
                                               <li><a href="#justified-badges-tab2" data-toggle="tab">Total orders <span class="badge bg-slate position-right" id="total_orders_this_year"></span></a></li>
                                               <li ><a href="#justified-badges-tab3" data-toggle="tab"><span class="badge badge-success position-left" id="total_customers_this_year"></span> Customers</a></li>


                           
                                        </ul>
                                        <div class="tab-content">
                                            <div  class=" tab-pane active" id="justified-badges-tab1">
                                                
                                                
                                            </div>
                                            <div class="tab-pane" id="justified-badges-tab2" >
                                               
                                                
                                            </div>
                                            <div class="tab-pane" id="justified-badges-tab3">
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div class="col-lg-6">
                             <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">Last Year</h6>
                                    <div class="heading-elements">
                                    </div>
                                </div>
                                <div class="panel-body">
                                <div class="content-group-sm" id="app_sales"></div>
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
                                               <li ><a href="#justified-badges-tab5" data-toggle="tab"> <span class="badge badge-info position-right" id="total_sales_last_year"></span>Total Sales</a></li>
                                               <li ><a href="#justified-badges-tab6" data-toggle="tab"> <span class="badge bg-slate position-right" id="total_orders_last_year"></span>Total Orders</a></li>
                                                <li ><a href="#justified-badges-tab7" data-toggle="tab"><span class="badge badge-success position-left" id="total_customers_last_year"></span> Customers</a></li>
                                            
                                         
                                        </ul>
                                        <div  class="tab-content">
                                            <div  class=" tab-pane active" id="justified-badges-tab5" style="width:100; height:auto;">
                                                 
                                                
                                            </div>
                                            <div class="tab-pane" id="justified-badges-tab6" style="width:100%; height:auto;">
                                             
                                                
                                            </div>
                                            <div class="tab-pane" id="justified-badges-tab7" style="width:100%; height:auto;">
                                            
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            <!-- /sales stats -->

                        </div>
                    </div>
                    <!-- /main charts -->
                    <!-- Dashboard content -->
                    <!-- /dashboard content -->
                    <!-- Footer -->
               <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
