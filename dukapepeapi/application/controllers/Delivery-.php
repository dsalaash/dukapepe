<?php



/*

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */



/**

 * Description of Delivery_Model

 *

 * @author mwamb

 */

header('Access-Control-Allow-Origin: *');

class Delivery extends CI_Controller{

    //put your code here

    public function __construct() {

        parent::__construct();

        $this->load->library('curl');

    }



    public function get_all_vehicles(){

    $url = "https://app.detrack.com/api/v1/vehicles/view/all.json"; 

    $this->curl->create($url);

    $this->curl->ssl(FALSE);

    $post_data = array ("key" => DETRACK_KEY);

    $json_output = $this->curl->simple_post($url, $post_data);

    $vehicle_info= json_decode($json_output,true); 

    $all_vehicles=$vehicle_info['vehicles'];

    

    echo json_encode($all_vehicles);

}



public function get_deliveries(){

    $data=array();

    if($this->input->post('vehicle_no')!=null){

     $data['vehicle_no'] = $this->input->post('vehicle_no');   

    }

    if($this->input->post('not_assigned')!=null){

     $data['not_assigned'] = $this->input->post('not_assigned');   

    }

    if($this->input->post('order_id')!=null){

     $data['order_id'] = $this->input->post('order_id');   

    }

    

    $response= $this->Delivery_Model->get_deliveries($data);

    

    echo $response;

}



       

       public function assign_vehicle(){

//        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    

//            if($access_response['code']==1){

                $order_id = $this->input->post('order_id');

                $vehicle_no = $this->input->post('vehicle_no');

                $data['order_id'] = $order_id;

                $data['vehicle_no'] = $vehicle_no;

                // get order infor

               $order_info = $this->Delivery_Model->get_order_details($order_id); 

               $order_info['assign_to'] =$vehicle_no; 

                

                // get all items ordered

                $items = $this->Delivery_Model->get_items($order_id);

                $order_info['items'] =$items;

                // send to detrack

                $all_info=array();

                array_push($all_info, $order_info);

                $data_string = json_encode($all_info);   

                

                $url = "https://app.detrack.com/api/v1/deliveries/create.json"; 

                $this->curl->create($url);

                $this->curl->ssl(FALSE);

                $post_data = array ("key" =>DETRACK_KEY,"json"=>$data_string);

                $json_output = $this->curl->simple_post($url, $post_data);

//                echo $json_output;

                $delivery_data= json_decode($json_output,true); 

                // check if delivery was placed successfully.

//              update database

                $status=$delivery_data['info']['status'];

                if($status==0){

                $response= $this->Delivery_Model->assign_vehicle($data);

                if($response['code']==1){

                  $status_data['order_id'] = $order_id;

                  $status_data['status_id'] = 3;

                $response= $this->Order_Model->update_order_status($status_data);  

                }

                }

                else{

                    $response['message'] = "Error, order not assigned to ".$vehicle_no;

                    $response['code']=0;

                }

//            }

//            else{

//                $response= json_encode($access_response);   

//            }

        echo json_encode($response);    

       }

    

       public function vehicle_info(){

           $vehicle_no= $this->input->post('vehicle_no');

           $data=array($vehicle_no);

           $url = "https://app.detrack.com/api/v1/vehicles/view.json"; 

           $this->curl->create($url);

           $this->curl->ssl(FALSE);

           $post_data = array ("key" => DETRACK_KEY,"json"=> json_encode($data));

           $json_output = $this->curl->simple_post($url, $post_data);

            $array_output= json_decode($json_output,true); 

        //    $all_vehicles=$vehicle_info['vehicles'];

            $results=$array_output['results'][0];



            echo json_encode($results);

       }



       public function new_logistics_settings(){

           $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    

            if($access_response['code']==1){

                $data['supplier_id'] = $this->input->post('supplier_id');

                $data['delivery_mode_id'] = $this->input->post('delivery_mode_id');

                $data['minimum_cost'] = $this->input->post('minimum_cost');

                $data['max_load'] = $this->input->post('max_load');





                $response = $this->Delivery_Model->new_logistics_settings($data);

            }

            else{

               $response= json_encode($access_response);   

            }

           echo $response;

       }

       

       public function new_delivery_zone(){

           $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    

            if($access_response['code']==1){

                $data['supplier_id'] = $this->input->post('supplier_id');

                $data['delivery_mode_id'] = $this->input->post('delivery_mode_id');

                $data['zone_id'] = $this->input->post('zone_id');

                $data['min_distance'] = $this->input->post('min_distance');

                $data['max_distance'] = $this->input->post('max_distance');

                $data['rate'] = $this->input->post('rate');



                $response = $this->Delivery_Model->new_delivery_zone($data);

           }

            else{

               $response= json_encode($access_response);   

            }

           echo $response;

       }

       public function update_logistic_settings(){

//           $delivery_mode_id= $this->input->post('delivery_mode_id');

           $minimum_cost= $this->input->post('minimum_cost');

           $max_load= $this->input->post('max_load');

           $id= $this->input->post('id');

           

//           $data['delivery_mode_id'] = $delivery_mode_id;

           $data['minimum_cost'] = $minimum_cost;

           $data['max_load'] = $max_load;

           $data['id'] = $id;

           $response = $this->Delivery_Model->update_logistic_settings($data);

           

           echo $response;

       }

       

       public function fetch_logistic_settings(){

         $data=array();

         if($this->input->post('id')!=null){

             $data['id']=$this->input->post('id');

         }

         if($this->input->post('supplier_id')!=null){

             $data['supplier_id']=$this->input->post('supplier_id');

         }

        $response = $this->Delivery_Model->fetch_all_logistic_settings($data);

           

           echo $response;   

       }

       

       public function update_delivery_zones(){

           $data['id'] = $this->input->post('id');

           $data['min_distance'] = $this->input->post('min_distance');

           $data['max_distance']  = $this->input->post('max_distance');

           $data['rate'] = $this->input->post('rate');

           $response = $this->Delivery_Model->update_delivery_zones($data);

           echo $response;

           

       }

       public function fetch_delivery_zones(){

           $data=array();

         if($this->input->post('id')!=null){

             $data['id']=$this->input->post('id');

         }

         if($this->input->post('supplier_id')!=null){

             $data['supplier_id']=$this->input->post('supplier_id');

         }

           $response = $this->Delivery_Model->fetch_delivery_zones($data);

           

           echo $response;  

       }

       

       public function view_zones(){

        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    

            if($access_response['code']==1){

               

                $response = $this->Delivery_Model->view_zones();

           }

            else{

               $response= json_encode($access_response);   

            }

           echo $response;   

       }

       

       public function view_delivery_modes(){

        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    

            if($access_response['code']==1){

               

                $response = $this->Delivery_Model->view_delivery_modes();

           }

            else{

               $response= json_encode($access_response);   

            }

           echo $response;    

       }

       

       public function notify(){

       $json_data = $this->input->post('json'); 

       $array_data= json_decode($json_data,true);

       

       $this->Delivery_Model->all_data($json_data);

       $order_id=$array_data['do'];

       $this->Delivery_Model->delivered($order_id);

       

       }

       

       public function getcc_phone(){

       $response= $this->Delivery_Model->getcc_phone();

       echo json_encode($response);

       }

       

       public function delivered_items(){

           $order_id= $this->input->post('order_id');
           $reponsedata=$this->Delivery_Model->delivered_items($order_id);
           $this->curl->create($url);

           $this->curl->ssl(FALSE);

           $post_data = array("retailer_id" => "263", "order_id" => $order_id, "items" => $reponsedata);
           $url="http://test.duka-pepe.com/inshop/inshopapi/index.php/stock/syncstocksallretailers";
           $json_output = $this->curl->simple_post($url, $post_data);
           echo $json_output;
           // echo json_encode($post_data);


       }

       public function simulate_delivery_email(){

           $order_id= $this->input->post('order_id');

           $response = $this->Delivery_Model->delivered_items($order_id);

           echo $response;

       }

       

       

       public function deliver_order(){

           $order_id = $this->input->post('order_id');

           $special_notes = $this->input->post('special_notes');

           $expected_delivery_time = $this->input->post('expected_delivery_time');

           $expected_pickup_time = $this->input->post('expected_pickup_time');

          

           $data['order_id'] = $order_id;

           $data['special_notes'] = $special_notes;

           $data['expected_delivery_time'] = $expected_delivery_time;

           $data['expected_pickup_time'] = $expected_pickup_time;

          

           

           $order_details = $this->Delivery_Model->deliver_order($data);

           if(json_decode($order_details,true)['order_details']==0){

            $response['message'] =  "No such order";

            $response['code'] = 0;

            $json_output= json_encode($response);

           }

           else{   

//           echo $order_details;

//           send response and order id to ryda

           $url= $this->Delivery_Model->get_delivery_url();

           $this->curl->create($url);

           $this->curl->ssl(FALSE);

           $post_data = array("key" => "ytyr", "order_id" => $order_id, "order_details" => $order_details);

           $json_output = $this->curl->simple_post($url, $post_data);

           }

           echo $json_output;

       }

}
