<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Duka pepe | Delivery zones</title>

    <?php include("../links.php") ?>

    <!-- /theme JS files -->
    <script>
        $(document).ready(function () {
            $(".status-progress").hide();
            $.extend($.fn.dataTable.defaults, {
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search delivery zones:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                },
                drawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                },
                preDrawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                }
            });
            if (localStorage.getItem('code') == 1) {
                loadDeliveryZones();
            } else {

                new PNotify({
                    title: 'Primary notice',
                    text: 'Error: You are not authorised to view this page.',
                    addclass: 'bg-warning'
                });
            }

            deliveryZoneUpdate();
            getSuppliers();
            getDeliveryModes();
            getDeliveryZones();
            deliveryZoneAdd();

        });

        function getSuppliers() {
            var formData = {
                'access_token': localStorage.getItem('access_token')
            };
            var url = base_url + "supplier/fetch_all";

            $.post(url, formData,
                function (data) {

                    var datas = JSON.parse(data);

                    var model = $('#selectsupplier_id');
                    model.empty();

                    $.each(datas, function (index, element) {
                        model.append("<option value='" + element.supplier_id + "'>" + element.business_name + "</option>");
                    });
                });
        }

        function getDeliveryModes() {
            var formData = {
                'access_token': localStorage.getItem('access_token')
            };
            var url = base_url + "delivery/view_delivery_modes";

            $.post(url, formData,
                function (data) {

                    var datas = JSON.parse(data);

                    var model = $('#selectdeliverymode');
                    model.empty();

                    $.each(datas, function (index, element) {
                        model.append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                });
        }

        function getDeliveryZones() {
            var formData = {
                'access_token': localStorage.getItem('access_token')
            };
            var url = base_url + "delivery/view_zones";

            $.post(url, formData,
                function (data) {

                    var datas = JSON.parse(data);

                    var model = $('#selectdeliveryzone');
                    model.empty();

                    $.each(datas, function (index, element) {
                        model.append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                });
        }


        function loadDeliveryZones() {
            console.log(localStorage.getItem('access_token'));
            var formData = {'access_token': localStorage.getItem('access_token')};
            var url = base_url + "delivery/fetch_delivery_zones ";
            $('#example').DataTable({
                "destroy": true,
                "ajax": {
                    "url": url,
                    "data": formData,
                    "type": "post",
                    "dataSrc": function (json) {
                        return json;
                    },
                    "processing": true,
                    "serverSide": true,
                    "pagingType": "simple",
                    language: {
                        paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                    }

                }, "columns": [
                    {"data": "business_name"},
                    {"data": "zone_name"},
                    {"data": "delivery_mode"},
                    {"data": "min_distance"},
                    {"data": "max_distance"},
                    {"data": "rate"},
                    {
                        orderable: false, searchable: false, render: function (data, type, full, meta) {
                        var links = '<ul class="icons-list"><li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>\n\
                                        <ul class="dropdown-menu dropdown-menu-right">\n\
<li><a href="javascript: updateDeliveryModal(' + full['id'] + ')">Edit</a></li>\n\
\n\
</ul></li></ul>';
                        return links;
                    }
                    }
                ]
            });
        }


        function ajaxmodaladd() {
            $(".status-progress-add").hide();
            $('#catTokenSup').val(localStorage.getItem('access_token'));
            $('#modalAddDelivery').modal('show');
        }


        function updateDeliveryModal(id) {
            $(".status-progress").show();
            var url = base_url + "delivery/fetch_delivery_zones";
            var formData = {
                'id': id,
                'access_token': localStorage.getItem('access_token')
            };
            $.post(url, formData, function (json) {
                obj = JSON.parse(json);
                var count = obj.length;
                for (i = 0; i < count; i++) {
                    var obj_data = obj[i];
                    $('#accesstokenupdate').val(localStorage.getItem('access_token'));
                    $('#settingsIDupdate').val(obj_data['id']);
                    $('#min_distanceupdate').val(obj_data['min_distance']);
                    $('#max_distanceupdate').val(obj_data['max_distance']);
                    $('#rateupdate').val(obj_data['rate']);
                }
                $(".status-progress").hide();
                $(".status-progress-add").hide();
                $('#modalAddstock').modal('show');
            });

        }


        function deliveryZoneUpdate() {
            $('#formvehicleassign').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-add").show();
                $.ajax({
                    url: base_url + "delivery/update_delivery_zones",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress-add").hide();
                        loadDeliveryZones();
                        $('#modalAddstock').modal('hide');

                        var data = JSON.parse(data);
                        if (data['code'] == 1) {
                            new PNotify({

                                text: data['message'],
                                addclass: 'bg-success'
                            });
                        } else {
                            new PNotify({

                                text: data['message'],
                                addclass: 'bg-warning'
                            });
                        }
                    },
                    error: function () {
                    }
                });
            });
        }


        function deliveryZoneAdd() {
            $('#formdelivery').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-add").show();
                $.ajax({
                    url: base_url + "delivery/new_delivery_zone",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress-add").hide();
                        loadDeliveryZones();
                        $('#modalAddDelivery').modal('hide');
                        var data = JSON.parse(data);
                        if (data['code'] == 1) {
                            new PNotify({

                                text: data['message'],
                                addclass: 'bg-success'
                            });
                        } else {
                            new PNotify({

                                text: data['message'],
                                addclass: 'bg-warning'
                            });
                        }
                    },
                    error: function () {
                    }
                });
            });
        }





    </script>

</head>

<body>

<?php include("../topbar.php") ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->

                <!-- /user menu -->

                <?php include("../sidebar.php") ?>
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span
                                    class="text-semibold">Delivery zones</span></h4>
                    </div>

                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                        <li class="active">Delivery zones</li>
                    </ul>

                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Basic datatable -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Delivery zones</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Delivery Zone</a>
                        <img class="text-center center-block status-progress" src="../assets/loader/loader.gif"/>
                    </div>

                    <table class="table" id="example">
                        <thead>
                        <tr>
                            <th>Business name</th>
                            <th>Zone name</th>
                            <th>Delivery mode</th>
                            <th>Min_distance</th>
                            <th>Max_distance</th>
                            <th>Rate</th>
                            <th>Edit</th>
                        </tr>
                        </thead>

                    </table>
                </div>
                <!-- /basic datatable -->


                <!-- Vertical form modal -->
                <div id="modalAddDelivery" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">New Delivery Zone</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formdelivery"
                                  enctype="multipart/form-data">
<!--                                <input type="hidden" name="id" id="settingsID"/>-->
                                <input type="hidden" name="access_token" id="catTokenSup"/>
                                <div class="modal-body">
                                    <div class="form-group">

                                        <label>Select Supplier:</label>
                                        <select required id="selectsupplier_id" name="supplier_id" class="form-control">
                                            <option value="0">Select Supplier</option>
                                        </select>

                                    </div>

                                    <div class="form-group">

                                        <label>Select Delivery Mode:</label>
                                        <select required id="selectdeliverymode" name="delivery_mode_id" class="form-control">
                                            <option value="0">Select delivery mode</option>
                                        </select>

                                    </div>
                                    <div class="form-group">

                                        <label>Select Delivery Zone:</label>
                                        <select required id="selectdeliveryzone" name="zone_id" class="form-control">
                                            <option value="0">Select delivery zone</option>
                                        </select>

                                    </div>

                                    <div class="form-group">

                                        <label>Minimum Distance (Kms)</label>
                                        <input name="min_distance" id="min_distance" class="form-control"
                                               placeholder="Minimum distance"/>

                                    </div>
                                    <div class="form-group">

                                        <label>Maximum Distance (Kms)</label>
                                        <input name="max_distance" id="max_distance" class="form-control"
                                               placeholder="Maximum distance"/>

                                    </div>
                                    <div class="form-group">

                                        <label>Rate</label>
                                        <input name="rate" id="rate" class="form-control" placeholder="Rate"/>

                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                    <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->

                <!-- Vertical form modal -->
                <div id="modalAddstock" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Update delivery zones</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formvehicleassign"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="id" id="settingsIDupdate"/>
                                <input type="hidden" name="access_token" id="accesstokenupdate"/>
                                <div class="modal-body">
                                    <div class="form-group">

                                        <label>Minimum Distance</label>
                                        <input name="min_distance" id="min_distanceupdate" class="form-control"
                                               placeholder="Minimum distance"/>

                                    </div>
                                    <div class="form-group">

                                        <label>Maximum Distance</label>
                                        <input name="max_distance" id="max_distanceupdate" class="form-control"
                                               placeholder="Maximum distance"/>

                                    </div>
                                    <div class="form-group">

                                        <label>Rate</label>
                                        <input name="rate" id="rateupdate" class="form-control" placeholder="Rate"/>

                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                    <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->

                <!-- Footer -->
                <div class="footer text-muted">
                    <?php include("../footer.php") ?>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
