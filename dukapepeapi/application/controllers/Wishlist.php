<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Wishlist
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Wishlist extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    public function add(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $retailer_id= $this->input->post('retailer_id');
        $stock_id= $this->input->post('stock_id');

        $data=array();
        $data['retailer_id']=$retailer_id;
        $data['stock_id']=$stock_id;
        $response= $this->Wishlist_Model->add($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
    public function status(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $wishlist_id= $this->input->post('wishlist_id');
        $stock_id= $this->input->post('stock_id');
        $data['wishlist_id'] = $wishlist_id;
        $data['stock_id'] = $stock_id;
        $response= $this->Wishlist_Model->status($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;  
    }
    public function mywishlist(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $retailer_id= $this->input->post('retailer_id');
        $data=array();
        $data['retailer_id'] = $retailer_id;
        $response= $this->Wishlist_Model->mywishlist($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function fetch_all(){
        
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
            $response= $this->Wishlist_Model->fetch_all();
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response; 
    }
   
}
