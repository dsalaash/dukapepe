<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Shopping_Model
 *
 * @author mwamb
 */
class Shopping_Model extends CI_Model{
    //put your code here
   public $categories='categories'; 
   public $orders='orders';
   public $products='products';
   public $promotions='promotions';
  
    public  function recent_orders($data){
        $retailer_id=$data['retailer_id'];
        $base_url= base_url()."images/";
        //get recent ordered items
        $this->db->select('ordered_products.unit_cost AS unit_cost,'.$this->orders.'.timestamp AS timestamp,'.$this->products.'.product_name AS product_name,'.$this->products.'.product_description AS description,product_images.image_url AS image');
        $this->db->where($this->orders.'.retailer_id',$retailer_id);
        $this->db->where($this->orders.'.is_active',1);
        $this->db->from($this->orders);
        $this->db->join('supplier_orders', $this->orders.'.order_id=supplier_orders.order_id');
        $this->db->join('ordered_products','supplier_orders.order_id=ordered_products.order_id');
        $this->db->join('stock', 'stock.stock_id=ordered_products.stock_id');
        $this->db->join($this->products,'stock.product_id='.$this->products.'.product_id');
        $this->db->join('product_images',$this->products.'.product_id=product_images.product_id');
        $this->db->group_by('product_name');
        $this->db->limit(5);
        $returned_data= $this->db->get();
       $new_data=$returned_data->result_array();
       
       return json_encode($new_data);
    }
    
    public function categories(){
        $base_url= base_url()."images/";
        $this->db->select('category_id,category_name,category_description,image_url');
        $this->db->where('is_active',1);
        $this->db->from($this->categories);
        $this->db->order_by('category_name ASC');
        $this->db->limit(5);
        $returned_data= $this->db->get();
       $new_data=$returned_data->result_array();
       
       return json_encode($new_data);  
    }
    public function promotions(){
        $base_url= base_url()."images/";
        $this->db->select('promotion_id,unit_cost,quantity,pq_value,unit_initial,unit_name,product_name,product_description,image_url');
        $this->db->where($this->promotions.'.is_active',1);
        $this->db->from($this->promotions);
        $this->db->join('stock', $this->promotions.'.stock_id=stock.stock_id');
        $this->db->join('sku','stock.sku_id=sku.sku_id');
        $this->db->join('product_quantities','sku.pq_id=product_quantities.pq_id');
        $this->db->join('units', 'sku.unit_id=units.unit_id');
        $this->db->join($this->products, $this->products.'.product_id=stock.product_id');
        $this->db->join('product_images', $this->products.'.product_id=product_images.image_url');
        $this->db->limit(2);
        $returned_data= $this->db->get();
        $new_data=$returned_data->result_array();
       
        return json_encode($new_data);
    }
    public function recommended(){
    
        $response= $this->Shopping_Model->recommended();
        echo $response;
    }
    public function last_orders($data){
       $retailer_id=$data['retailer_id'];
       
       $this->db->select('order_id,total_cost,orders.status_id AS status,status_name');
       $this->db->where('orders.is_active',1);
       $this->db->where('orders.retailer_id',$retailer_id);
       $this->db->from('orders');
       $this->db->join('order_status','orders.status_id=order_status.status_id');
       $this->db->limit(5);
       $returned_data= $this->db->get();
       $response=$returned_data->result_array();
       
       return json_encode($response);
    }
}
