<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Stock
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Stock extends CI_Controller{
    //nstock changes 
     public function return_all_on_closure()
    {
        $received_json= $this->input->post('order');
        $data= json_decode($received_json,true);
        $response_data= $this->Stock_Model->return_all_on_closure($data);

    }
     public function get_qty_stock_by_id()
    {
     $stock_item_id=$this->input->post('stock_id');
     $data['stock_id']=$stock_item_id;
     $response_data= $this->Stock_Model->get_qty_stock_by_id($data);
      
     echo $response_data;     
    }
    public function return_qty_to_stock()
    {
     $stock_item_id=$this->input->post('stock_id');
     $data['stock_id']=$stock_item_id;
     $response_data= $this->Stock_Model->return_qty_to_stock($data);
     if($response_data>0)
     {
          echo "reduced";
     }   
     else{
        echo "failed";
     }  
    }
     public function return_all_to_store()
    {
     $stock_item_id=$this->input->post('stock_id');
     $stock_item_qty=$this->input->post('stock_qty');
     $data['stock_id']=$stock_item_id;
     $data['stock_qty']=$stock_item_qty;
     $response_data= $this->Stock_Model->return_all_to_store($data);
     if($response_data>0)
     {
          echo "reduced";
     }   
     else{
        echo "failed";
     }     
    }

    ///new stock changes
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    public function new_product(){
     $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
     if($access_response['code']==1){
        $data=array();
     $data['product_id']= $this->input->post('product_id');
     $data['supplier_id']= $this->input->post('supplier_id');
     $data['sku_id']= $this->input->post('sku_id');
     $data['unit_cost']= $this->input->post('unit_cost');
     $data['quantity']= $this->input->post('quantity');
     $data['vat']= $this->input->post('vat');
     
     $response_data= $this->Stock_Model->new_product($data);
     }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function new_stock(){
     $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
     $stock_items_id=$this->input->post('stock_id');
     $quantity= $this->input->post('quantity');
     
     $data['stock_id']=$stock_items_id;
     $data['quantity']=$quantity;
     
     $response_data= $this->Stock_Model->new_stock($data);
     }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;   
    }
    
    public function update_price(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['stock_id']= $this->input->post('stock_id');
        $data['unit_cost']= $this->input->post('unit_cost');
        $response= $this->Stock_Model->update_price($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
    public function supplier_stock(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['supplier_id']= $this->input->post('supplier_id');
        $response= $this->Stock_Model->supplier_stock($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function get_stock(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['sub_category_id']= $this->input->post('sub_category_id');
        if($this->input->post('retailer_id')!=null){
            $data['retailer_id'] = $this->input->post('retailer_id');   
        } 
        $response= $this->Stock_Model->get_stock($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
    public function fetch(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['stock_id']= $this->input->post('stock_id');
        
        if($this->input->post('retailer_id')!=null){
            $data['retailer_id'] = $this->input->post('retailer_id');   
        }
        $response= $this->Stock_Model->fetch($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function fetch_all(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        if($this->input->post('retailer_id')!=null){
            $data['retailer_id'] = $this->input->post('retailer_id');   
        }
        $response= $this->Stock_Model->fetch_all($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function fetch_stock_history(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['stock_id']= $this->input->post('stock_id');
        $response= $this->Stock_Model->fetch_stock_history($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function delete_stock_history(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['id']= $this->input->post('id');
        $response= $this->Stock_Model->delete_stock_history($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function direct_update(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['stock_id']= $this->input->post('stock_id');
        $data['quantity']= $this->input->post('quantity');
        $response= $this->Stock_Model->direct_update($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function recommended(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $retailer_id= $this->input->post('retailer_id');
        $data['retailer_id']=$retailer_id;
        
        $response= $this->Stock_Model->recommended($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
    public function review(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $stock_id= $this->input->post('stock_id');
        $retailer_id= $this->input->post('retailer_id');
        $rate= $this->input->post('rate');
        $title= $this->input->post('title');
        $message= $this->input->post('message');
        
        $data['stock_id'] = $stock_id;
        $data['retailer_id'] = $retailer_id;
        $data['rate'] = $rate;
        $data['title'] = $title;
        $data['message'] = $message;
        
        $response= $this->Stock_Model->review($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
        public function delete(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $stock_id= $this->input->post('stock_id');
        $response= $this->Stock_Model->delete($stock_id);
        }
        
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
    public function update_vat(){
        $data['stock_id']= $this->input->post('stock_id');
        $data['vat']= $this->input->post('vat');
        
        $response = $this->Stock_Model->update_vat($data);
        
        echo $response;
    }
    
    public function dc_to_dc_transfer(){
         $data['to'] = $this->input->post('recipient_supplier');
         $data['stock_id'] = $this->input->post('stock_id');
         $data['no_transfer'] = $this->input->post('quantity');
         
         $response = $this->Stock_Model->dc_to_dc_transfer($data);
         
         echo $response;
         
     }
}
