<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Delivery_Point_Model
 *
 * @author mwamb
 */
class Delivery_Point_Model extends CI_Model {
    //put your code here
    
    public $delivery_point="delivery_points";
    
    public function add($data){
        $response=array();
      if($this->checker($data)==0){
       $this->db->insert($this->delivery_point,$data);
       $response['message']="Location added successfully.";
       $response['code']=1;   
      } 
      else{
          $response['message']="You have already added this location.";
          $response['code']=0;
      }
      
      return json_encode($response);
    }
    
    public function checker($data){
        $this->db->select('id');
        $this->db->where('location_name',$data['location_name']);
        $this->db->group_start();
        $this->db->or_where('latitude',$data['latitude']);
        $this->db->where('longitude',$data['longitude']);
        $this->db->group_end();
        $this->db->group_start();
        $this->db->where('retailer_id',$data['retailer_id']);
        $this->db->group_end();     
        $this->db->from($this->delivery_point);
        $count= $this->db->count_all_results();
        
        return $count;
    }
    
    public function fetch_all($q_data){
        $this->db->select('id AS location_id,location_name,latitude,longitude,is_active');
        $this->db->where('retailer_id',$q_data['retailer_id']);
        $this->db->from($this->delivery_point);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        
      return json_encode($data);          
    }
    
    public function remove($data){
        $this->db->where('id',$data['id']);
        $this->db->delete('delivery_points');
        $num= $this->db->affected_rows();
        
        if($num>0){
            $response['message'] = "Delivery point deleted successfully.";
            $response['code'] = 1;
        }
        else{
          $response['message'] = "No delivery point was deleted.";
            $response['code'] = 0;  
        }
        
        return json_encode($response);
    }
}
