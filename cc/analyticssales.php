<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Duka pepe | Analytics Sales</title>

        <?php include("../links.php") ?>

        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search orders:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });
                if (localStorage.getItem('code') == 1) {
                    loadAnalytics();
                } else {

                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });
                }

                Ordersadd();

            });

            function getYesterdaysDate() {
                var date = new Date();
                date.setDate(date.getDate() - 1);
                var final = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
                return final;
            }

            function loadAnalytics() {
                console.log(localStorage.getItem('access_token'));
                var today = new Date();
                var todayFinal = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                var formData = {'access_token': localStorage.getItem('access_token'), 'start_date': getYesterdaysDate(), 'end_date': todayFinal};
                var url = base_url + "analytics/sales_volume";
                $('#example').DataTable({
                    "destroy": true,
                    "ajax": {
                        "url": url,
                        "data": formData,
                        "type": "post",
                        "dataSrc": function (json) {
                            return json;
                        },
                        "processing": true,
                        "serverSide": true,
                        "pagingType": "simple",
                        language: {
                            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                        }

                    }, "columns": [
                        {"data": "no_items"},
                        {"data": "sku_name"},
                        {"data": "business_name"},
                        {"data": "product_description"},
                        {"data": "date_range"},
                    ]
                });
            }




            function ajaxmodaladd() {
                $(".status-progress-add").hide();
                $('#access_token').val(localStorage.getItem('access_token'));
                $('#modaldate').modal('show');
            }

            function Ordersadd() {
                $('#formfilter').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-add").show();
                    var url = base_url + "analytics/sales_volume";
                    var formData = {
                        start_date: $('#start_date').val(),
                        end_date: $('#end_date').val(),
                        access_token: localStorage.getItem('access_token')
                    }
                    console.log(formData);
                    $('#example').DataTable({
                        "destroy": true,
                        "ajax": {
                            "url": url,
                            "data": formData,
                            "type": "post",
                            "dataSrc": function (json) {
                                return json;
                            },
                            "processing": true,
                            "serverSide": true,
                            "pagingType": "simple",
                            language: {
                                paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                            }

                        }, "columns": [
                            {"data": "no_items"},
                            {"data": "sku_name"},
                            {"data": "business_name"},
                            {"data": "product_description"},
                            {"data": "date_range"},
                        ]
                    });

                    $('#modaldate').modal('hide');
                    $(".status-progress-add").hide();
                });
            }



        </script>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->

                        <!-- /user menu -->

                        <?php include("../sidebar.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Sales Analytics</span></h4>
                            </div>

                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                                <li class="active">Sales Analytics</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title"></h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Filter</a>
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            </div>

                            <table class="table" id="example">
                                <thead>
                                    <tr>

                                        <th>No of items</th>

                                        <th>Sku name</th>
                                        <th>Business Name</th>
                                        <th>Product Description</th>
                                        <th>Date range</th>


                                    </tr>
                                </thead>

                            </table>
                        </div>
                        <!-- /basic datatable -->

                        <style>
                            .modal{
                                z-index: 20;
                            }
                            .modal-backdrop{
                                z-index: 10;
                            }
                        </style>



                        <!-- Vertical form modal -->
                        <div id="modaldate" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title"></h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formfilter" enctype="multipart/form-data">



                                        <input type="hidden" name="access_token" id="access_token"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Start Date:</label>
                                                <input type="text" id="start_date" name="start_date" class="form-control daterange-single" >

                                            </div>
                                            <div class="form-group">

                                                <label>End Date:</label>
                                                <input type="text" id="end_date" name="end_date" class="form-control daterange-single" >

                                            </div>


                                        </div>





                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">filter</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->



                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
