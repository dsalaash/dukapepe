<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Duka pepe | DC Stock</title>

    <?php include("../links.php") ?>

    <!-- /theme JS files -->
    <script>
        $(document).ready(function () {
            $(".status-progress").hide();
            $.extend($.fn.dataTable.defaults, {
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search Stock:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                },
                drawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                },
                preDrawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                }
            });

            if (localStorage.getItem('code') == 1) {
                loadStock();
            } else {

                new PNotify({
                    title: 'Primary notice',
                    text: 'Error: You are not authorised to view this page.',
                    addclass: 'bg-warning'
                });

            }
            document.getElementById("businessName").innerHTML = localStorage.getItem('supplier_name_stock');


            Stockadd();
            unitPriceUpdate();
            unitBpUpdate();
            loadSkuspinner();
            loadProductsSpinner();
            stockNew();
            Vatedit();
            getSuppliers();
            TransfertoDC();
            Stockupdatedetails();
            $('.js-example-basic-multiple').select2();

        });

        function loadProductsSpinner() {
            var formData = {
                'access_token': localStorage.getItem('access_token')
            };
            var url = base_url + "products/fetch_all";
            $.post(url, formData,
                function (data) {

                    var datas = JSON.parse(data);
                    // console.log(datas);
                    var model = $('#selectpro');
                    model.empty();

                    $.each(datas, function (index, element) {
                        model.append("<option value='" + element.product_id + "'>" + element.product_name + "</option>");
                    });
                });
        }

        function loadSkuspinner() {
            var formData = {
                'access_token': localStorage.getItem('access_token')
            };
            var url = base_url + "sku/fetch_all";
            $.post(url, formData,
                function (data) {

                    var datas = JSON.parse(data);
                    // console.log(datas);
                    var model = $('#selectsku');
                    model.empty();

                    $.each(datas, function (index, element) {
                        model.append("<option value='" + element.sku_id + "'>" + element.sku_name + "</option>");
                    });
                });
        }


        function getSuppliers() {
            var formData = {
                'access_token': localStorage.getItem('access_token')
            };
            var url = base_url + "supplier/fetch_all";

            $.post(url, formData,
                function (data) {

                    var datas = JSON.parse(data);

                    var model = $('#selectsupplier_id');
                    model.empty();

                    $.each(datas, function (index, element) {
                        model.append("<option value='" + element.supplier_id + "'>" + element.business_name + "</option>");
                    });
                });
        }


//         function loadStock() {
//             // console.log(localStorage.getItem('supplier_id_stock'));
//             var formData = {
//                 'access_token': localStorage.getItem('access_token'),
//                 'supplier_id': localStorage.getItem('supplier_id_stock')
//             };
//             var url = base_url + "stock/supplier_stock";
//             $('#example').DataTable({
//                 "destroy": true,
//                 "ajax": {
//                     "url": url,
//                     "data": formData,
//                     "type": "post",
//                     "dataSrc": function (json) {
//                         // console.log(json);
//                         return json;
//                     },
//                     "processing": true,
//                     "serverSide": true,
//                     "pagingType": "simple",
//                     language: {
//                         paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
//                     }

//                 }, "columns": [

//                     {"data": "product_name"},
//                     {"data": "product_description"},
//                     {"data": "sku_name"},
//                     {"data": "weight"},
//                     {"data": "unit_bp"},
//                     {"data": "unit_cost"},
//                     {"data": "quantity"},
//                     {"data": "vat"},
//                     {
//                         "data": "stock_id",
//                         orderable: false,
//                         searchable: false,
//                         render: function (data, type, full, meta) {
//                             var links = '<ul class="icons-list"><li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>\n\
//                                         <ul class="dropdown-menu dropdown-menu-right">\n\
// <li><a href="javascript: dcTodctransfer(' + data + ', ' + full.quantity + ')">Dc to Dc transfer</a></li>\n\
// <li><a href="javascript: addStock(' + data + ')">Update stock</a></li>\n\
// <li><a href="javascript: updateBp(' + data + ')">Update Buying Price</a></li>\n\
// <li><a href="javascript: updatePrice(' + data + ')">Update price</a></li>\n\
// <li><a href="javascript: updateDetails(' + data + ')">Update Details</a></li>\n\
// <li><a href="javascript: updateVat(' + data + ')">Update VAT</a></li>\n\
// \n\<li><a onclick="return confirm(\'Are you sure you want to delete this?\')" href="javascript: ajaxdelete(' + data + ')" >delete stock</a></a></li>\n\
// \n\
// </ul></li></ul>'
//                             return links;

//                         }
//                     }
//                 ]
//             });

//         }
function loadStock() {
            // console.log(localStorage.getItem('supplier_id_stock'));
            var formData = {
                'access_token': localStorage.getItem('access_token'),
                'supplier_id': localStorage.getItem('supplier_id_stock')
            };
            var url = base_url + "stock/supplier_stock";
        
            $.ajax({
               type:'POST',
               url: url,
               data: formData,
               success:function(data){
                // console.log(data);
                var stock_row="";   
                    
                // var no =0;  
                $('#example').dataTable().fnClearTable();
                $('#example').dataTable().fnDestroy();
                $.each(JSON.parse(data), function(k, v) {
                       // no=no+1;
                      stock_row+="<tr><td>"+v.product_name+"</td><td>"+v.product_description+"</td><td>"+v.sku_name+"</td><td>"+v.unit_bp+"</td><td>"+v.unit_cost+"</td><td>"+v.quantity+"</td><td>"+v.vat+"</td><td><div class='dropdown'><button class='btn btn-flat no-border dropdown-toggle dropleft' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><span class='glyphicon glyphicon-list'></span></button><div class='dropdown-menu dropdown-menu-right' aria-labelledby='dropdownMenuButton'><ul class='list-group '><li class='list-group-item'><a class='dropdown-item text-muted' href='javascript: dcTodctransfer("+v.stock_id+")'>Dc to Dc transfer</a></li>\n\
                      <li class='list-group-item'><a class='dropdown-item text-muted' href='javascript: updateBp("+v.stock_id+")'>Update Buying Price</a></li>\n\
                      <li class='list-group-item'><a class='dropdown-item text-muted' href='javascript: updatePrice("+v.stock_id+")'>Update price</a></li>\n\
                      <li class='list-group-item'><a class='dropdown-item text-muted' href='javascript: updateDetails("+v.stock_id+")'>Update Details</a></li>\n\
                      <li class='list-group-item'><a class='dropdown-item text-muted' href='javascript: updateVat("+v.stock_id+")'>Update VAT</a></li>\n\
                      <li class='list-group-item'><a class='dropdown-item text-muted' href='javascript: DivideLargeSku("+v.product_id+","+v.sku_id+","+v.supplier_id+","+v.quantity+","+v.unit_cost+","+v.unit_bp+")'>Divide to small SKU</a></li>\n\
                      <li class='list-group-item'><a class='dropdown-item text-muted' href='javascript: CombineSmallSku("+v.product_id+","+v.sku_id+","+v.supplier_id+","+v.quantity+")'>Combine to large SKU</a></li>\n\
                      </ul></div></div></td></tr>";
                    });  
                    $("#example").append(stock_row);
                    $(".status-progress-universal").hide();
                    $('#example').DataTable( {
                        // dom: 'Bfrtip',
                    });
            
                },
               error:function(data){
                    console.log('error loading data');
               }        

            });

        }


         function DivideLargeSku(product_id,sku_id,supplier_id,quantity) {
            // alert(quantity);
            $('#divide_product_id').val(product_id);
            $('#divide_supplier_id').val(supplier_id);
            $('#divide_sku_id').val(sku_id);
            $('#availableQty').val(quantity);
            $('#divideQty').val('');
            $('#selling_price').val('');
            $('#divideToken').val(localStorage.getItem('access_token'));
            $('#dividemodal').modal('show');
            document.getElementById("submitdivide").disabled = false;
            $(".status-progress-universal").show();
            $('#formdivide').submit(function (e) {
                e.preventDefault();
                // $("#submitdivide").addClass("disabled");
                var availableqty = document.getElementById("availableQty").value;
                var qtyToDivide = document.getElementById("divideQty").value;
                if (qtyToDivide > availableqty) {
                    alert('Quantity to divide cannot be bigger than available quantity');
                } else {
                    $.ajax({
                        url: base_url + "stock/DivideLargeSku",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            // console.log(data);
                            document.getElementById("submitdivide").disabled = true;
                            $('#dividemodal').modal('hide');
                            $(".status-progress-universal").hide();
                            loadStock();
                            var dt = JSON.parse(data);
                            var message = dt['message'];
                           
                            if ((dt['code']) == 1) {
                                
                                new PNotify({
                                    title: 'Success Message',
                                    text: message,
                                    addclass: 'bg-success'
                                });

                            } else {

                                new PNotify({
                                    title: 'Failure Message',
                                    text: message,
                                    addclass: 'bg-warning'
                                });
                            }
                        },
                        error: function () {
                            alert('error');
                        }
                    });
                }
            });

            
        }
        function CombineSmallSku(product_id,sku_id,supplier_id,quantity) {
            $('#combine_product_id').val(product_id);
            $('#combine_supplier_id').val(supplier_id);
            $('#combine_sku_id').val(sku_id);
            $('#available_combine_qty').val(quantity);
            $('#quantity_to_combine').val('');
            $('#combineToken').val(localStorage.getItem('access_token'));
            $('#combinemodal').modal('show');
            document.getElementById("submitcombine").disabled = false;
            $('#formcombine').submit(function (e) {
                e.preventDefault();
                // $("#submitdivide").addClass("disabled");
                
                var availableqty = document.getElementById("available_combine_qty").value;
                var qtyToCombine = document.getElementById("quantity_to_combine").value;
                if (qtyToCombine > availableqty) {
                    alert('Quantity to combine cannot be bigger than available quantity');
                } else {
                    $.ajax({
                        url: base_url + "stock/CombineSmallSku",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            var dt = JSON.parse(data);
                            var message = dt['message'];
                            document.getElementById("submitcombine").disabled = true;
                            $('#combinemodal').modal('hide');
                            $(".status-progress-universal").hide();
                            
                            if ((dt['code']) == 1) {
                                // location.reload();
                                loadStock();
                                new PNotify({
                                    title: 'Success Message',
                                    text: message,
                                    addclass: 'bg-success'
                                });

                            } else {

                                new PNotify({
                                    title: 'Failure Message',
                                    text: message,
                                    addclass: 'bg-warning'
                                });
                            }
                        },
                        error: function () {
                            alert('error');
                        }
                    });
                }
            });

            
        }


        function ajaxdelete(id) {
            $(".status-progress").show();
            $(".status-progress-add").hide();
            var url = base_url + "stock/delete";
            var formData = {
                'stock_id': id,
                'access_token': localStorage.getItem('access_token')
            };

            $.post(url, formData, function (json) {
                obj = JSON.parse(json);
                new PNotify({
                    text: obj['message'],
                    addclass: 'bg-success'
                });

                $(".status-progress").hide();
                loadStock();

            });
        }


        function addStock(id) {
            $(".status-progress").show();
            var url = base_url + "stock/fetch";
            var formData = {
                'stock_id': id,
                'access_token': localStorage.getItem('access_token')
            };
            $.post(url, formData, function (json) {
                obj = JSON.parse(json);
                var count = obj.length;
                for (i = 0; i < count; i++) {
                    var obj_data = obj[i];
                    $('#stockTokenAdd2').val(localStorage.getItem('access_token'));
                    $('#stockIdAdd2').val(obj_data['stock_id']);
                    $('#quantity').val(obj_data['quantity']);
                    $('#vat').val(obj_data['vat']);
                }
                $(".status-progress-add").hide();

                $('#modalAddstock').modal('show');
            });

        }

        function dcTodctransfer(stockID, quantity) {
            $(".status-progress-transfer").hide();
            $('#stockID').val(stockID);
            $('#stockQuantity').val(quantity);
            $('#dctransfertoken').val(localStorage.getItem('access_token'));
            $('#modalTransfer').modal('show');
        }


        function updatePrice(id) {
            $(".status-progress").show();
            var url = base_url + "stock/fetch";
            var formData = {
                'stock_id': id,
                'access_token': localStorage.getItem('access_token')
            };
            $.post(url, formData, function (json) {
                obj = JSON.parse(json);
                var count = obj.length;
                for (i = 0; i < count; i++) {
                    var obj_data = obj[i];
                    $('#stockTokene2').val(localStorage.getItem('access_token'));
                    $('#stocksid2').val(id);
                    $('#unit_cost').val(obj_data['unit_cost']);
                    // $('#vat').val(obj_data['vat']);
                }
                $(".status-progress").hide();
                $(".status-progress-update-price").hide();
                $('#modalUnitstock').modal('show');
            });

        }
         function updateDetails(id) {

            $(".status-progress").show();
            var url = base_url + "stock/fetchitem";
            var formData = {
                'stock_id': id,
                'access_token': localStorage.getItem('access_token')
            };
            $.post(url, formData, function (json) {
                // console.log(json);
                var parse = JSON.parse(json);
                // console.log(obj);
                  $.each(parse['itemdetails'], function(k, v) {
                    $('#access_token_details').val(localStorage.getItem('access_token'));
                    
                     var model = $('#details_selectpro');
                    model.empty();
                         $.each(parse['products'], function(k2, v2) {
                            if(v2.product_id==v.product_id)
                            {
                               model.append("<option value='" + v2.product_id + "' selected ='selected'>" + v2.product_name + "</option>");
                            }
                            else
                            {
                             model.append("<option value='" + v2.product_id + "'>" + v2.product_name + "</option>");
                            }
                           
                         });
                         $('#details_selectpro').val(v.product_id); // Select the option with a value of 'US'
                         $('#details_selectpro').trigger('change'); 

                         var model2 = $('#details_selectsku');
                    model2.empty();
                         $.each(parse['skus'], function(k2, v2) {
                            if(v2.product_id==v.product_id)
                            {
                               model2.append("<option value='" + v2.sku_id + "' selected ='selected'>" + v2.sku_name + "</option>");
                            }
                            else
                            {
                             model2.append("<option value='" + v2.sku_id + "' selected ='selected'>" + v2.sku_name + "</option>");
                            }
                           
                         });
                         $('#details_selectsku').val(v.sku_id); // Select the option with a value of 'US'
                         $('#details_selectsku').trigger('change'); 
                    
                  });
                // for (i = 0; i < count; i++) {
                //     var obj_data = obj[i];
                //    
                // }
                $("#detailsstock").val(id);
                $(".status-progress").hide();
                $(".status-progress-update-price").hide();
                $('#modalupdatedetails').modal('show');
            });

        }
        function updateBp(id) {
            $(".status-progress").show();
            var url = base_url + "stock/fetch";
            var formData = {
                'stock_id': id,
                'access_token': localStorage.getItem('access_token')
            };
            $.post(url, formData, function (json) {
                // console.log(json);
                obj = JSON.parse(json);
                var count = obj.length;
                for (i = 0; i < count; i++) {
                    var obj_data = obj[i];
                    $('#stockTokene_bp').val(localStorage.getItem('access_token'));
                    $('#stock_id_bp').val(id);
                    $('#unit_bp').val(obj_data['unit_bp']);
                    // $('#vat').val(obj_data['vat']);
                }
                $(".status-progress").hide();
                $(".status-progress-update-price").hide();
                $('#modalUnitbp').modal('show');
            });

        }


        function updateVat(id) {
            $(".status-progress").show();
            var url = base_url + "stock/fetch";
            var formData = {
                'stock_id': id,
                'access_token': localStorage.getItem('access_token')
            };
            $.post(url, formData, function (json) {
                obj = JSON.parse(json);
                var count = obj.length;
                for (i = 0; i < count; i++) {
                    var obj_data = obj[i];
                    $('#stockToken').val(localStorage.getItem('access_token'));
                    $('#stockId').val(id);
                    //$('#unit_cost').val(obj_data['unit_cost']);
                    $('#vat').val(obj_data['vat']);
                }
                $(".status-progress").hide();
                $(".status-progress-edit-vat").hide();
                $('#modalVatstock').modal('show');
            });

        }


        function ajaxmodaladd() {
            $(".status-progress-add").hide();
            $('#supplier_id2').val(localStorage.getItem('supplier_id_stock'));
            $('#supToken2').val(localStorage.getItem('access_token'));
            $('#modalStockNew').modal('show');
        }

        function Stockadd() {
            $('#formstockadd').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-add").show();
                $.ajax({
                    url: base_url + "/stock/direct_update",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress-add").hide();
                        loadStock();
                        $('#modalAddstock').modal('hide');
                        var data = JSON.parse(data);
                        new PNotify({

                            text: date['message'],
                            addclass: 'bg-success'
                        });
                    },
                    error: function () {
                    }
                });
            });
        }
          function Stockupdatedetails() {
            $('#formupdatedetails').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-add").show();
                $.ajax({
                    url: base_url + "/stock/updatestockDetails",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                      $("#submit").removeClass("disabled");
                            $(".status-progress-transfer").hide();
                            loadStock();
                            $('#modalupdatedetails').modal('hide');
                            var data = JSON.parse(data);
                            new PNotify({

                                text: data['message'],
                                addclass: 'bg-success'
                            });
                    },
                    error: function () {
                    }
                });
            });
        }


        function TransfertoDC() {
            $('#formTransfer').submit(function (e) {
                e.preventDefault();
                $("#submitTransfer").addClass("disabled");
                $(".status-progress-transfer").show();
                var quantity = document.getElementById("quantitystck").value;
                var stockQuantity = document.getElementById("stockQuantity").value;
                // console.log('Qantity');
                // console.log(quantity);
                // console.log('StockQantity');
                // console.log(stockQuantity);
                if (quantity >= stockQuantity) {
                    alert('You do not have enough quantity to transfer');
                    $(".status-progress-transfer").hide();
                    $('#modalTransfer').modal('hide');
                } else {
                    $.ajax({
                        url: base_url + "/stock/dc_to_dc_transfer",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-transfer").hide();
                            loadStock();
                            $('#modalTransfer').modal('hide');
                            var data = JSON.parse(data);
                            new PNotify({

                                text: data['message'],
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {
                        }
                    });
                }
            });
        }

        function Vatedit() {
            $('#formvatedit').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-edit-vat").show();
                $.ajax({
                    url: base_url + "/stock/update_vat",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress-edit-vat").hide();
                        loadStock();
                        $('#modalVatstock').modal('hide');
                        //console.log(data[0]['message']);
                        new PNotify({

                            text: 'VAT updated successfully',
                            addclass: 'bg-success'
                        });
                    },
                    error: function () {
                    }
                });
            });
        }

        function unitPriceUpdate() {
            $('#formunitprice').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-update-price").show();
                $.ajax({
                    url: base_url + "/stock/update_price",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress-update-price").hide();
                        loadStock();
                        $('#modalUnitstock').modal('hide');
                        // console.log(data['message']);
                        new PNotify({

                            text: 'Price update successfully',
                            addclass: 'bg-success'
                        });
                    },
                    error: function () {
                    }
                });
            });
        }
function unitBpUpdate() {
            $('#formunitbuyingprice').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-update-price").show();
                // var formdata= new FormData(this);
                // console.log(formdata);
                $.ajax({
                    url: base_url + "/stock/update_bp",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        // console.log(data);
                        $("#submit").removeClass("disabled");
                        $(".status-progress-update-price").hide();
                        // loadStock();
                        $('#modalUnitbp').modal('hide');
                       loadStock();
                        new PNotify({

                            text: 'Buying Price updated successfully',
                            addclass: 'bg-success'
                        });
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            });
        }
        function stockNew() {
            $('#formstockNew').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-add").show();
                $.ajax({
                    url: base_url + "/stock/new_product",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress-add").hide();
                        loadStock();
                        $('#modalStockNew').modal('hide');

                        new PNotify({

                            text: 'New stock has been added successfully',
                            addclass: 'bg-success'
                        });
                    },
                    error: function () {
                    }
                });
            });

        }


        function activateCategory(id) {
            $(".status-progress").show();
            var url = base_url + "/category/status";
            var formData = {
                'category_id': id,
                'access_token': localStorage.getItem('access_token'),
                'is_active': 1
            };
            $.post(url, formData, function (json) {
                var data = JSON.parse(json);
                new PNotify({

                    text: data['message'],
                    addclass: 'bg-info'
                });
                loadCategoryUpdate();
                $(".status-progress").hide();
            });
        }

        function deactivateCategory(id) {
            $(".status-progress").show();
            var url = base_url + "/category/status";
            var formData = {
                'category_id': id,
                'access_token': localStorage.getItem('access_token'),
                'is_active': 0
            };
            $.post(url, formData, function (json) {
                var data = JSON.parse(json);
                new PNotify({

                    text: data['message'],
                    addclass: 'bg-warning'
                });
                loadCategoryUpdate();
                $(".status-progress").hide();
            });
        }

    </script>

</head>

<body>

<?php include("../topbar.php") ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->

                <!-- /user menu -->

                <?php include("../sidebargen.php") ?>
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Stock</span>
                        </h4>
                    </div>

                </div>


            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Basic datatable -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"><p id="businessName"></p>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <!-- <li><a data-action="collapse"></a></li> -->
                                <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-success"><i
                                    class="glyphicon"></i>Add Stock</a>
                            </ul>
                        </div>
                        </h5>
                    </div>
                    <!-- <div class="panel-heading">
                        <h5 class="panel-title">Stock for <span id="businessName"></span></h5> -->
                        <!-- <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>

                            </ul>
                        </div> -->
                        <!-- <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-success pull-right"><i
                                    class="glyphicon glyphicon-pencil"></i>Add Stock</a>
                    </div> -->

                    <div class="panel-body">
                        
                        <img class="text-center center-block status-progress" src="../assets/loader/loader.gif"/>
                    <table class="table" id="example">
                        <thead>
                        <tr>


                            <th>Product Name</th>
                            <th>Product Description</th>
                            <th>Sku Name</th>
                            <!-- <th>Weight</th> -->
                            <th>Unit BP</th>
                            <th>Unit Price</th>
                            <th>Quantity</th>
                            <th>VAT (%)</th>
                            <!--<th>Status</th>-->
                            <th>Action</th>
                        </tr>
                        </thead>

                    </table>
                    </div>
                </div>
                <!-- /basic datatable -->
                  <!-- Vertical form modal -->
                <div id="modalVatstock" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Update VAT</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formvatedit"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="stock_id" id="stockId"/>
                                <input type="hidden" name="access_token" id="stockToken"/>
                                <div class="modal-body">

                                    <div class="form-group">

                                        <label>VAT (%):</label>
                                        <input name="vat" id="vat" class="form-control" placeholder="e.g 16"/>

                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Update
                                    </button>
                                    <img class="text-center center-block status-progress-edit-vat"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->


                <!-- Vertical form modal -->
                <div id="modalAddstock" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Update stock</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formstockadd"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="stock_id" id="stockIdAdd2"/>
                                <input type="hidden" name="access_token" id="stockTokenAdd2"/>
                                <div class="modal-body">
                                    <div class="form-group">

                                        <label>Quantity</label>
                                        <input type="number" name="quantity" id="quantity" class="form-control"
                                               placeholder="Quantity"/>

                                    </div>
                                    <!--  <div class="form-group">

                                         <label>VAT (%):</label>
                                         <input name="vat" id="vat" class="form-control" placeholder="e.g 16" />

                                     </div> -->


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Update
                                    </button>
                                    <img class="text-center center-block status-progress-add"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->


                  <!-- modal to break bulk products -->
                <div id="dividemodal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Subdivide product to smaller SKU</h5>
                            </div>
                            <form role="form" class="form-validate " method="POST" id="formdivide"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="divide_sku_id" id="divide_sku_id"/>
                                <input type="hidden" name="divide_product_id" id="divide_product_id"/>
                                <input type="hidden" name="divide_supplier_id" id="divide_supplier_id"/>
                                <input type="hidden" name="access_token" id="divideToken"/>
                                <div class="modal-body">
                                    <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Available Quantity:</label>
                                        <input name="availableQty" id="availableQty" class="form-control" readonly="" />
                                    </div>
                                    </div>
                                    <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Quantity to Divide:</label>
                                        <input type="number" name="divideQty" id="divideQty" class="form-control" placeholder="Enter quantity" required="" />
                                    </div>
                                    </div>
                                    <!-- <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Buying Price</label>
                                            <input type="number" name="buying_price" id="buying_price" class="form-control" placeholder="Set buying price">
                                        </div>
                                    </div> -->
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label>Selling Price</label>
                                            <input type="number" name="selling_price" id="selling_price" required="" class="form-control" placeholder="Small SKU selling price">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary btn-block" id="submitdivide" value="add">Divide
                                    </button>
                                    <img class="text-center center-block status-progress-universal"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /end of modal to break bulk products -->
                <!-- modal to combine products in small skus to big sku-->
                <div id="combinemodal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Combine products in small SKU to bigger SKU</h5>
                            </div>
                            <form role="form" class="form-validate " method="POST" id="formcombine"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="combine_sku_id" id="combine_sku_id"/>
                                <input type="hidden" name="combine_product_id" id="combine_product_id"/>
                                <input type="hidden" name="combine_supplier_id" id="combine_supplier_id"/>
                                <input type="hidden" name="access_token" id="combineToken"/>
                                <div class="modal-body">
                                    <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Available Quantity:</label>
                                        <input name="available_combine_qty" id="available_combine_qty" class="form-control" readonly="" />
                                    </div>
                                    </div>
                                    <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>Quantity to Combine:</label>
                                        <input type="number" name="quantity_to_combine" id="quantity_to_combine" class="form-control" placeholder="Enter quantity to combine" required="" />
                                    </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary btn-block" id="submitcombine">Combine
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /end of modal to combine products in small skus to big sku -->


                <!-- Vertical form modal -->
                <div id="modalupdatedetails" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Update Stock Details</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formupdatedetails"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="detailsstock" id="detailsstock"/>
                                <input type="hidden" name="access_token_details" id="access_token_details"/>
                                <div class="modal-body">
                                     <div class="form-group">
                                        <label>Select with search</label>
                                        <select id="details_selectsku" name="details_sku_id" class="form-control js-example-basic-multiple">
                                                        <option value="0">Select sku</option>
                                        </select>
                                    </div>
                                    <div class="form-group">

                                        <label>Product:</label>
                                        <select id="details_selectpro" name="details_product_id" class="form-control js-example-basic-multiple">
                                            <option value="0">Select Product</option>
                                        </select>

                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Update
                                    </button>
                                    <img class="text-center center-block status-progress-update-price"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->

                <!-- Vertical form modal -->
                <div id="modalVatstock" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Update VAT</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formvatedit"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="stock_id" id="stockId"/>
                                <input type="hidden" name="access_token" id="stockToken"/>
                                <div class="modal-body">

                                    <div class="form-group">

                                        <label>VAT (%):</label>
                                        <input name="vat" id="vat" class="form-control" placeholder="e.g 16"/>

                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Update
                                    </button>
                                    <img class="text-center center-block status-progress-edit-vat"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->


                <!-- Vertical form modal -->
                <div id="modalAddstock" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Update stock</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formstockadd"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="stock_id" id="stockIdAdd2"/>
                                <input type="hidden" name="access_token" id="stockTokenAdd2"/>
                                <div class="modal-body">
                                    <div class="form-group">

                                        <label>Quantity</label>
                                        <input type="number" name="quantity" id="quantity" class="form-control"
                                               placeholder="Quantity"/>

                                    </div>
                                    <!--  <div class="form-group">

                                         <label>VAT (%):</label>
                                         <input name="vat" id="vat" class="form-control" placeholder="e.g 16" />

                                     </div> -->


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Update
                                    </button>
                                    <img class="text-center center-block status-progress-add"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->


                <!-- Vertical form modal -->
                <div id="modalUnitstock" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Update Unit Price</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formunitprice"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="stock_id" id="stocksid2"/>
                                <input type="hidden" name="access_token" id="stockTokene2"/>
                                <div class="modal-body">
                                    <div class="form-group">

                                        <label>Unit Price</label>
                                        <input type="number" name="unit_cost" id="unit_cost" class="form-control"
                                               placeholder="Unit cost"/>

                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Update
                                    </button>
                                    <img class="text-center center-block status-progress-update-price"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->
                <!-- Vertical form modal -->
                <div id="modalUnitbp" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Update Unit BP</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" name="formunitbuyingprice" id="formunitbuyingprice"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="stock_id_bp" id="stock_id_bp"/>
                                <input type="hidden" name="access_token_bp" id="stockTokene_bp"/>
                                <div class="modal-body">
                                    <div class="form-group">

                                        <label>Unit BP</label>
                                        <input type="number" name="unit_bp" id="unit_bp" class="form-control"
                                               placeholder="Unit BP"/>

                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Update BP
                                    </button>
                                    <img class="text-center center-block status-progress-update-price"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->



                <!-- Vertical form modal -->
                <div id="modalTransfer" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">DC TO DC TRANSFER</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formTransfer"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="stock_id" id="stockID"/>
                                <input type="hidden" name="access_token" id="dctransfertoken"/>
                                <input type="hidden" name="stockQuantity" id="stockQuantity"/>
                                <div class="modal-body">

                                    <div class="form-group">

                                        <label>Select Supplier:</label>
                                        <select required id="selectsupplier_id" name="recipient_supplier"
                                                class="form-control">
                                            <option value="0">Select Supplier</option>
                                        </select>

                                    </div>

                                    <div class="form-group">

                                        <label>Quantity</label>
                                        <input type="number" name="quantity" id="quantitystck" class="form-control"
                                               placeholder="Qunatity" required/>

                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submitTransfer" value="add">Transfer
                                    </button>
                                    <img class="text-center center-block status-progress-transfer"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->


                <!-- Vertical form modal -->
                <div id="modalStockNew" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">New Stock Product form</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formstockNew"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="supplier_id" id="supplier_id2"/>
                                <input type="hidden" name="access_token" id="supToken2"/>
                                <div class="modal-body">

                                    <div class="form-group">
                                        <label>Select with search</label>
                                        <select id="selectsku" name="sku_id" class="form-control js-example-basic-multiple">
                                                        <option value="0">Select sku</option>
                                        </select>
                                    </div>
                                    <div class="form-group">

                                        <label>Product:</label>
                                        <select id="selectpro" name="product_id" class="form-control js-example-basic-multiple">
                                            <option value="0">Select Product</option>
                                        </select>

                                    </div>
                                    <div class="form-group">nu0

                                        <label>Unit Price:</label>
                                        <input name="unit_cost" id="unit_cost" class="form-control"
                                               placeholder="Unit cost"/>

                                    </div>
                                     <div class="form-group">

                                        <label>Unit Buying Price:</label>
                                        <input name="unit_bp" id="unit_bp" class="form-control"
                                               placeholder="Unit Buying Price"/>

                                    </div>
                                    <div class="form-group">

                                        <label>Quantity:</label>
                                        <input name="quantity" id="quantity" class="form-control"
                                               placeholder="Quantity"/>

                                    </div>
                                    <div class="form-group">

                                        <label>VAT (%):</label>
                                        <input name="vat" id="vat" class="form-control" placeholder="e.g 16"/>

                                    </div>

                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                    <img class="text-center center-block status-progress-add"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->


                <!-- Footer -->
                <div class="footer text-muted">
                    <?php include("../footer.php") ?>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
