<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Duka pepe | Category analytics</title>
<?php include("../links.php") ?>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet">
    <!-- /theme JS files -->
    <script>
      if(localStorage.getItem('access_token')=="")
          {
           window.location = "http://localhost/dukapepe_portal/dukapepe/";
          }
       
        $(document).ready(function () {
          $('.js-example-basic-multiple').select2();
            $(".status-progress").hide();
              var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "order/load_category_analytics";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(response){
                        var arr = JSON.parse(response);

                        console.log(response);
                           var categories_row="";   
                           var no =0;    
                        $.each(arr['categories'], function(k, v) {
                               no=no+1;
                               console.log(v);
                               var categorytotal=arr['cat'+v.category_id];
                               var totalPurchase=arr['cat'+v.totalPurchases];
                              categories_row+="<tr><td>"+no+"</td><td>"+v.category_name+"</td><td>"+categorytotal+"</td><td>"+v.totalPurchase+"</td><td>"+v.totalPurchase+"</td><td>"+v.timestamp+"</td><td></td></tr>";

                            //   {"data": "image_url", orderable: false, searchable: false, render: function (data, type, full, meta) {
                            //     return '<img src="' + data + '" class=" width="70" height="70" alt="">';
                            // }},
                            });  
                            $("#userstablebody").append(categories_row);
                            $('#example').DataTable( {
                                dom: 'Bfrtip',
                                buttons: [
                                    'copy', 'csv', 'excel', 'pdf', 'print'
                                ],
                                
                            } );             
                                },
                       error:function(response){
                                  console.log(response);
                       }        
                });     
            });
    </script>
</head>
<body>
<?php include("../topbar.php") ?>
<!-- Page container -->
<div class="page-container">
   <!-- Vertical form modal -->
    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">
                <!-- User menu -->
                <!-- /user menu -->
                <?php include("../sidebargen.php") ?>
            </div>
        </div>
        <!-- /main sidebar -->
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Sale analytics by category</span>
                        </h4>
                    </div>
                </div>
            </div>
            <!-- /page header -->
            <!-- Content area -->
            <div class="content">
                <div class="content clearfix">
                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">All Agents In the system</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body table-responsive">
                                <!--<a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Orders</a>-->
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                      <table class="display nowrap" id="example" style="margin-bottom: 150px;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <!-- <th>Category image</th> -->
                            <th>Category</th>
                            <th>Total Sales</th>
                            <th>Total Purchases</th>
                            <th>Margins</th>
                            <th>Category creation date</th>
                        </tr>
                        </thead>
                        <tbody id="userstablebody">
                        </tbody>
                    </table>
                    </div>
                        </div>
                    </div>
                </div>
                <!-- Footer -->
                <div class="footer text-muted">
                    <?php include("../footer.php") ?>
                </div>
                <!-- /footer -->
            </div>
            <!-- /content area -->
        </div>
        <!-- /main content -->
    </div>
    <!-- /page content -->
    <script type="text/javascript">
       $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
    </script>
</div>
<!-- /page container -->
</body>
</html>
