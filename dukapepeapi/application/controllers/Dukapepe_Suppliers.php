<?php

header('Access-Control-Allow-Origin: *');
class Dukapepe_Suppliers extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('Dukapepe_Suppliers_Model');
    }
    public function add(){
      // $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      // if($access_response['code']==1){
        $data=array();

        $business_name= $this->input->post('business_name');
        $phone= $this->input->post('phone');
        $mobile= $this->input->post('mobile');
        $email= $this->input->post('email');
        $kra_pin= $this->input->post('kra_pin');
        $address= $this->input->post('address');
        $bank= $this->input->post('bank');
        $account_number= $this->input->post('account_number');

        $data['business_name']=$business_name;
        $data['phone']=$phone;
        $data['mobile']=$mobile;
        $data['email']=$email;
        $data['kra_pin']=$kra_pin;
        $data['address']=$address;
        $data['bank']=$bank;
        $data['account_number']=$account_number;
        $response = $this->Dukapepe_Suppliers_Model->add($data);
      //   }
      // else{
      //  $response= json_encode($access_response);   
      // }
        echo $response;  
    }
    
    public function update_supplier(){
    // $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
    //     if($access_response['code']==1){
         $data['supplier_id'] = $this->input->post('supplier_id_edit');   
         $data['business_name'] = $this->input->post('business_name_edit');  
         $data['phone'] = $this->input->post('phone_edit');
         $data['mobile'] = $this->input->post('mobile_edit');
         $data['email'] = $this->input->post('email_edit');
         $data['kra_pin'] = $this->input->post('kra_pin_edit'); 
         $data['address'] = $this->input->post('address_edit');
         $data['bank'] = $this->input->post('bank_edit');
         $data['account_number'] = $this->input->post('account_number_edit');     
        // $data['name']= $this->input->post('name');
            
        $response= $this->Dukapepe_Suppliers_Model->update_supplier($data);
        // }
        // else{
        //   $response= json_encode($access_response);   
        // }
        echo $response;
      }
    public function fetch_all(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      // if($access_response['code']==1){
        $response= $this->Dukapepe_Suppliers_Model->fetch_all();
      //   }
      // else{
      //  $response= json_encode($access_response);   
      // }
        echo $response;  
    }
    
    public function details(){
      //   $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      // if($access_response['code']==1){
        $supplier_id=$this->input->post('supplier_id');
        $response= $this->Dukapepe_Suppliers_Model->details($supplier_id);
      //   }
      // else{
      //  $response= json_encode($access_response);   
      // }
        echo $response;  
    }

    
    
    public function delete(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $product_id= $this->input->post('product_id');
        $response= $this->Products_Model->delete($product_id);
        }
        
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
}
