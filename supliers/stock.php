<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka pepe | Stock</title>

        <?php include("../links.php") ?>
        
        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search Stock:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });

                if (localStorage.getItem('code') == 1) {
                    loadStock();
                } else {

                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });

                }


                Stockadd();
                unitPriceUpdate();
                loadSkuspinner();
                loadProductsSpinner();
                stockNew();
                Vatedit();

            });

            function loadProductsSpinner() {
                var formData = {
                    'access_token': localStorage.getItem('access_token')
                };
                var url = base_url + "products/fetch_all";
                $.post(url, formData,
                        function (data) {

                            var datas = JSON.parse(data);
                            console.log(datas);
                            var model = $('#selectpro');
                            model.empty();

                            $.each(datas, function (index, element) {
                                model.append("<option value='" + element.product_id + "'>" + element.product_name + "</option>");
                            });
                        });
            }

            function loadSkuspinner() {
                var formData = {
                    'access_token': localStorage.getItem('access_token')
                };
                var url = base_url + "sku/fetch_all";
                $.post(url, formData,
                        function (data) {

                            var datas = JSON.parse(data);
                            console.log(datas);
                            var model = $('#selectsku');
                            model.empty();

                            $.each(datas, function (index, element) {
                                model.append("<option value='" + element.sku_id + "'>" + element.sku_name + "</option>");
                            });
                        });
            }


            function loadStock() {
                console.log(localStorage.getItem('supplier_id'));
                var formData = {'access_token': localStorage.getItem('access_token'), 'supplier_id': localStorage.getItem('supplier_id')};
                var url = base_url + "stock/supplier_stock";
                $('#example').DataTable({
                    "destroy": true,
                    "ajax": {
                        "url": url,
                        "data": formData,
                        "type": "post",
                        "dataSrc": function (json) {
                            return json;
                        },
                        "processing": true,
                        "serverSide": true,
                        "pagingType": "simple",
                        language: {
                            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                        }

                    }, "columns": [
                        
                        {"data": "product_name"},
                        {"data": "product_description"},
                        {"data": "sku_name"},
                        {"data": "weight"},
                        {"data": "unit_cost"},
                        {"data": "quantity"},
                        {"data": "vat"},
                        {"data": "stock_id", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                var links = '<ul class="icons-list"><li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>\n\
                                        <ul class="dropdown-menu dropdown-menu-right">\n\
<li><a href="javascript: addStock(' + data + ')">Update stock</a></li>\n\
<li><a href="javascript: updatePrice(' + data + ')">Update price</a></li>\n\
<li><a href="javascript: updateVat(' + data + ')">Update VAT</a></li>\n\
\n\<li><a onclick="return confirm(\'Are you sure you want to delete this?\')" href="javascript: ajaxdelete(' + data + ')" >delete stock</a></a></li>\n\
\n\
</ul></li></ul>'
                                return links;

                            }}
                    ]
                });

            }
            
            
            function ajaxdelete(id) {
                $(".status-progress").show();
                $(".status-progress-add").hide();
                var url = base_url + "stock/delete";
                var formData = {
                    'stock_id': id,
                    'access_token': localStorage.getItem('access_token')
                };

                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    new PNotify({
                        text: obj['message'],
                        addclass: 'bg-success'
                    });

                    $(".status-progress").hide();
                    loadStock();

                });
            }


            function addStock(id) {
                $(".status-progress").show();
                var url = base_url + "stock/fetch";
                var formData = {
                    'stock_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];
                        $('#stockToken').val(localStorage.getItem('access_token'));
                        $('#stockId').val(obj_data['stock_id']);
                        $('#quantity').val(obj_data['quantity']);
                        $('#vat').val(obj_data['vat']);
                    }
                    $(".status-progress").hide();
                    $('#modalAddstock').modal('show');
                });

            }


            function updatePrice(id) {
                $(".status-progress").show();
                var url = base_url + "stock/fetch";
                var formData = {
                    'stock_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];
                        $('#stockTokene2').val(localStorage.getItem('access_token'));
                        $('#stocksid2').val(id);
                        $('#unit_cost').val(obj_data['unit_cost']);
                        // $('#vat').val(obj_data['vat']);
                    }
                    $(".status-progress").hide();
                     $(".status-progress-update-price").hide();
                    $('#modalUnitstock').modal('show');
                });

            }


             function updateVat(id) {
                $(".status-progress").show();
                var url = base_url + "stock/fetch";
                var formData = {
                    'stock_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];
                        $('#stockToken').val(localStorage.getItem('access_token'));
                        $('#stockId').val(id);
                        //$('#unit_cost').val(obj_data['unit_cost']);
                        $('#vat').val(obj_data['vat']);
                    }
                    $(".status-progress").hide();
                    $(".status-progress-edit-vat").hide();
                    $('#modalVatstock').modal('show');
                });

            }


            function ajaxmodaladd() {
                $(".status-progress-add").hide();
                $('#supplier_id2').val(localStorage.getItem('supplier_id'));
                $('#supToken2').val(localStorage.getItem('access_token'));
                $('#modalStockNew').modal('show');
            }

            function Stockadd() {
                $('#formstockadd').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "/stock/direct_update",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-add").hide();
                            loadStock();
                            $('#modalAddstock').modal('hide');

                            new PNotify({
                               
                                text: 'Quantity updated successfully',
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {}
                    });
                });
            }

            function Vatedit(){
                $('#formvatedit').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-edit-vat").show();
                    $.ajax({
                        url: base_url + "/stock/update_vat",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-edit-vat").hide();
                            loadStock();
                            $('#modalVatstock').modal('hide');
                            //console.log(data[0]['message']);
                            new PNotify({
                               
                                text: 'VAT updated successfully',
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {}
                    });
                });
            }
            
            function unitPriceUpdate() {
                $('#formunitprice').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-update-price").show();
                    $.ajax({
                        url: base_url + "/stock/update_price",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-update-price").hide();
                            loadStock();
                            $('#modalUnitstock').modal('hide');
                            console.log(data['message']);
                            new PNotify({
                               
                                text: 'Price update successfully',
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {}
                    });
                });
            }

            function stockNew() {
                $('#formstockNew').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "/stock/new_product",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-add").hide();
                            loadStock();
                            $('#modalStockNew').modal('hide');

                            new PNotify({
                              
                                text: 'New stock has been added successfully',
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {}
                    });
                });

            }


            function activateCategory(id) {
                $(".status-progress").show();
                var url = base_url + "/category/status";
                var formData = {
                    'category_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 1
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                      
                        text: data['message'],
                        addclass: 'bg-info'
                    });
                    loadCategoryUpdate();
                    $(".status-progress").hide();
                });
            }

            function deactivateCategory(id) {
                $(".status-progress").show();
                var url = base_url + "/category/status";
                var formData = {
                    'category_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 0
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                       
                        text: data['message'],
                        addclass: 'bg-warning'
                    });
                    loadCategoryUpdate();
                    $(".status-progress").hide();
                });
            }

        </script>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                       
                        <!-- /user menu -->

                        <?php include("../sidebarSupplier.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Stock</span></h4>
                            </div>

                        </div>

                        
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">Stock</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                       
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-pencil"></i>Add Stock</a>
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            </div>

                            <table class="table" id="example">
                                <thead>
                                    <tr>

                                        
                                        <th>Product name</th>
                                        <th>Product Description</th>
                                        <th>Sku name</th>
                                        <th>Weight</th>
                                        <th>Unit cost</th>
                                        <th>Quantity</th>
                                        <th>VAT (%)</th>
                                        <!--<th>Status</th>-->
                                        <th>Action</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                        <!-- /basic datatable -->

                        <!-- Vertical form modal -->
                        <div id="modalVatstock" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Update VAT</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formvatedit" enctype="multipart/form-data">
                                        <input type="hidden" name="stock_id" id="stockId"/>
                                        <input type="hidden" name="access_token" id="stockToken"/>
                                        <div class="modal-body">

                                            <div class="form-group">

                                                <label>VAT (%):</label>
                                                <input name="vat" id="vat" class="form-control" placeholder="e.g 16" />

                                            </div>


                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Update</button>
                                            <img class="text-center center-block status-progress-edit-vat"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                         <!-- Vertical form modal -->
                        <div id="modalAddstock" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Update stock</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formstockadd" enctype="multipart/form-data">
                                        <input type="hidden" name="stock_id" id="stockId"/>
                                        <input type="hidden" name="access_token" id="stockToken"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Quantity</label>
                                                <input type="number" name="quantity" id="quantity" class="form-control" placeholder="Quantity" />

                                            </div>
                                           <!--  <div class="form-group">

                                                <label>VAT (%):</label>
                                                <input name="vat" id="vat" class="form-control" placeholder="e.g 16" />

                                            </div> -->


                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Update</button>
                                            <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->



                        <!-- Vertical form modal -->
                        <div id="modalUnitstock" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Update Unit cost</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formunitprice" enctype="multipart/form-data">
                                        <input type="hidden" name="stock_id" id="stocksid2"/>
                                        <input type="hidden" name="access_token" id="stockTokene2"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Unit Cost</label>
                                                <input type="number" name="unit_cost" id="unit_cost" class="form-control" placeholder="Unit cost" />

                                            </div>


                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Update</button>
                                            <img class="text-center center-block status-progress-update-price"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->




                        <!-- Vertical form modal -->
                        <div id="modalStockNew" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">New Stock Product form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formstockNew" enctype="multipart/form-data">
                                        <input type="hidden" name="supplier_id" id="supplier_id2"/>
                                        <input type="hidden" name="access_token" id="supToken2"/>
                                        <div class="modal-body">
                                           
                                            <div class="form-group">
                                        <label>Select with search</label>
                                        <select id="selectsku" name="sku_id" class="form-control">
                                            
                                        </select>
                                    </div>
                                            <div class="form-group">

                                                <label>Product:</label>
                                                <select id="selectpro" name="product_id" class="form-control">
                                                    <option value="0">Select Product</option>
                                                </select>

                                            </div>
                                            <div class="form-group">

                                                <label>Unit Cost:</label>
                                                <input name="unit_cost" id="unit_cost" class="form-control" placeholder="Unit cost" />

                                            </div>
                                            <div class="form-group">

                                                <label>Quantity:</label>
                                                <input name="quantity" id="quantity" class="form-control" placeholder="Quantity" />

                                            </div>
                                            <div class="form-group">

                                                <label>VAT (%):</label>
                                                <input name="vat" id="vat" class="form-control" placeholder="e.g 16" />

                                            </div>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                            <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
