<div class="sidebar-user">
    <div class="category-content">
        <div class="media">
            <a href="#" class="media-left">
                <div id="placehere">
                    
                </div>
                <!--<img src="../assets/images/placeholder.jpg" class="img-circle img-sm" alt="">-->
            </a>
            <div class="media-body">
                <span class="media-heading text-semibold"><p id="nameUser"></p></span>

            </div>
            <script>
                document.getElementById("nameUser").innerHTML = localStorage.getItem('retailer_name');
            </script>
            <script type="text/javascript">
                if (localStorage.getItem('image_url')) {
                    var elem = document.createElement("img");
                    elem.setAttribute("src", localStorage.getItem('image_url'));
                    elem.setAttribute("height", "768");
                    elem.setAttribute("width", "1024");
                    elem.setAttribute("alt", "Flower");
                    document.getElementById("placehere").appendChild(elem);
                }else{
                    var elem = document.createElement("img");
                    elem.setAttribute("src", "../assets/images/placeholder.jpg");
                    elem.setAttribute("height", "768");
                    elem.setAttribute("width", "1024");
                    elem.setAttribute("alt", "Flower");
                    document.getElementById("placehere").appendChild(elem);
                }

            </script>

            <div class="media-right media-middle">
                <ul class="icons-list">
                    <li>
                        <a href="#"><i class="icon-cog3"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>