<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_Type
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class User_type extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    public function add(){
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $user_type_name= $this->input->post('name');
    $response= $this->User_Type_Model->add($user_type_name);
    }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function update(){
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $user_type_id= $this->input->post('id');
    $user_type_name= $this->input->post('name');
    $data=array();
    $data['id']=$user_type_id;
    $data['name']=$user_type_name;
    $response= $this->User_Type_Model->edit($data);
    }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function delete(){
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $user_type_id= $this->input->post('id');
    $response= $this->User_Type_Model->delete($user_type_id);
    }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function fetch(){
   $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $user_type_id= $this->input->post('id');
    $response= $this->User_Type_Model->view($user_type_id);
    }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;   
    }
    public function fetch_all(){
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $response= $this->User_Type_Model->view_all();
    }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;  
    }
   
}
