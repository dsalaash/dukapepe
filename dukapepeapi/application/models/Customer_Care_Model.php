<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Customer_Care_Model
 *
 * @author mwamb
 */
class Customer_Care_Model extends CI_Model{
    //put your code here
    public $customer_care="customer_care";
    
    public function register_cc($data){
        $count = $this->newChecker($data);
        if($count==0){
            $this->db->insert($this->customer_care,$data);
            $response['message'] = "Customer care added successfully.";
            $response['code'] = 1;
        }
        else{
            $response['message'] = "Error: Customer care Already exist.";
            $response['code'] = 0;
        }
        return json_encode($response);
    }
    public function newChecker($data){
        $this->db->select('cc_id');
        $this->db->where('phone',$data['phone']);
        if($data['email']==""){
        $this->db->or_where('email',$data['email']);
        }
        $this->db->from($this->customer_care);
        $count= $this->db->count_all_results();
        return $count;
    }

    public  function update_cc($data){
        $count= $this->checkUpdate($data);
        if($count==0){
        $supplier_id=$data['cc_id'];
        unset($data['cc_id']);
        $this->db->where('cc_id',$supplier_id);
        $this->db->update($this->customer_care,$data);
        $response['message'] = "Customer care details updated successfully.";
        $response['code'] = 1;
        }
        else{
        $response['message'] = "Failed to update customer care details.";
        $response['code'] = 0;   
        }
        
        return json_encode($response);
    }
    
    
    public function checkUpdate($data){
        $this->db->select('cc_id');
        $this->db->where('phone',$data['phone']);
        $this->db->where('cc_id !=',$data['cc_id']);
        $this->db->from($this->suppliers);
        $count= $this->db->count_all_results();
        return $count;
    }
    
    public function fetch($data){
        $this->db->select('cc_id,first_name,last_name,phone,email,gender,id_number,dob,is_active');
        $this->db->where('cc_id',$data['cc_id']);
        $this->db->from($this->customer_care);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return json_encode($response);
    }
    
    public function fetch_all(){
        $this->db->select('cc_id,first_name,last_name,phone,email,gender,id_number,dob,is_active');
        $this->db->from($this->customer_care);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return json_encode($response);
    }
    
    public function update_profile($data){
        $cc_id=$data['cc_id'];
        unset($data['cc_id']);
        $this->db->where('cc_id',$cc_id);
        $this->db->update($this->customer_care,$data);
        
        $response['message'] = "Customer care profile updated successfully.";
        $response['code'] = 1;
        
        return json_encode($response);
    }
}
