<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
header('Access-Control-Allow-Origin: *');
class Analytics extends CI_Controller{
    
public  function registration()
{
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token')); 
      if($access_response['code']==1){
        $response = $this->Analytics_Model->registration();
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
}

 public function dashboard()
 {
  $data['retailer']=$this->input->post('retailer');
  $data['startdate']=$this->input->post('startdate');
  $data['enddate']=$this->input->post('enddate');
  // echo json_encode($data);
  $response = $this->Analytics_Model->dashboard($data);
  echo $response;
 }
 public function todaysales()
 {
  $data['retailer']=$this->input->post('retailer');
  $data['date']=$this->input->post('date');
  $response = $this->Analytics_Model->todaysales($data);
  echo $response;
 }
 public function todaysalessuper()
 {
   $data['retailer']=$this->input->post('retailer');
  $data['date']=$this->input->post('date');
   $response = $this->Analytics_Model->todaysalessuper($data);
   echo $response;
 }
 public function todaymarginssuper()
 {
  $data['retailer']=$this->input->post('retailer');
   $data['date']=$this->input->post('date');
   $response = $this->Analytics_Model->todaymarginssuper($data);
   echo $response;
 }
 public function todaymargins()
 {
  $data['retailer']=$this->input->post('retailer');
   $data['date']=$this->input->post('date');
   $response = $this->Analytics_Model->todaymargins($data);
   echo $response;
 }
  public function todaystocks()
 {
   $response = $this->Analytics_Model->todaystocks($this->input->post('date'));
   echo $response;
 }
 public function retailer_products()
 {
   $response = $this->Analytics_Model->retailer_products($this->input->post('retailer'));
   echo $response;
 }
public function periodic_brand_analysis()
{
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token')); 
      $data['start_date'] = $this->input->post('start_date'); 
      $data['end_date'] =  $this->input->post('end_date'); 
      $data['manufacturer_id'] =  $this->input->post('manufacturer_id');  
      if($access_response['code']==1){
        $response = $this->Analytics_Model->periodic_brand_analysis($data);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
}
public function periodic_general()
 {
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token')); 
      $data['start_date'] = $this->input->post('start_date'); 
      $data['end_date'] =  $this->input->post('end_date'); 
      if($access_response['code']==1)
      {
        $response = $this->Analytics_Model->periodic_general($data);
      }
      else
      {
        $response= json_encode($access_response);   
      }
      echo $response;

 } 
 public function periodic_customer()
 {
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token')); 
      $data['start_date'] = $this->input->post('start_date'); 
      $data['end_date'] =  $this->input->post('end_date'); 
      if($access_response['code']==1)
      {
        $response = $this->Analytics_Model->periodic_customer($data);
      }
      else
      {
        $response= json_encode($access_response);   
      }
      echo $response;

 } 
 public function dailyreport()
 {
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token')); 
      $data['start_date'] = $this->input->post('start_date'); 
      // $data['end_date'] =  $this->input->post('end_date'); 
      if($access_response['code']==1)
      {
        $response = $this->Analytics_Model->daily_general($data);
      }
      else
      {
        $response= json_encode($access_response);   
      }
      echo $response;

 } 
public function periodic_dc()
 {
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1)
      {
        $response = $this->Analytics_Model->periodic_dc();
      }
      else
      {
       $response= json_encode($access_response);   
      }
      echo $response;

 } 
public function brand_dc_periodic()
 {
   
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1)
      {
        $response = $this->Analytics_Model->brand_dc_periodic();
      }
      else
      {
       $response= json_encode($access_response);   
      }
      echo $response;
 } 
public function sales_details_this_year()
{
  $data['dc']=$this->input->post('dc');
  $data['start_date']=$this->input->post('start_date');
  $data['end_date']=$this->input->post('end_date');
  $response=$this->Analytics_Model->sales_details_this_year($data);
  echo $response;  
}
public function customer_details_this_year()
{
   $response=$this->Analytics_Model->customer_details_this_year();
    echo $response;  
}
public function order_details_this_year()
{
   $response=$this->Analytics_Model->order_details_this_year();
    echo $response;  
}
     public  function genman_dashboard_index()
     {
    $response=$this->Analytics_Model->genman_dashboard_index();
    echo $response;    
     
    }
     public function top_items()
     {
       $response=$this->Analytics_Model->top_items();
       echo $response;    
     }
      public function genman_dashboard_index_this_month()
   {
      $response=$this->Analytics_Model->genman_dashboard_index_this_month();
    echo $response; 
   }

   public function genman_dashboard_analytics_by_month()
   {
    $data['month']= $this->input->post('month');
    $response=$this->Analytics_Model->genman_dashboard_analytics_by_month($data);
    echo $response; 
   }

    
    
    public function retailers_registered(){
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
    
        $data['start_date'] = $this->input->post('start_date');
        $data['end_date'] = $this->input->post('end_date');
        $response = $this->Analytics_Model->retailers_registered($data);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    
    public function sales_volume(){
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
     $data['start_date'] = $this->input->post('start_date');
     $data['end_date'] = $this->input->post('end_date');
     if($this->input->post('product_id')!=null){
     $data['product_id'] = $this->input->post('product_id');
     }
     $response = $this->Analytics_Model->sales_volume($data);
     }
      else{
       $response= json_encode($access_response);   
      }   
    echo $response;   
    }
    public function load_category_analytics()
    {
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));
      $data['start_date'] = $this->input->post('startdate').' 01:00:00'; 
      $data['end_date'] =  $this->input->post('enddate').' 23:59:00'; 
      // if($access_response['code']==1)
      // {
         
        $response= $this->Analytics_Model->load_category_analytics($data);
      // }
      // else
      // {
      //   $response=json_encode($access_response);
      // }
      echo $response;
    }
    public function load_product_analytics()
    {
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));
      $data['start_date'] = $this->input->post('startdate').' 01:00:00'; 
      $data['end_date'] =  $this->input->post('enddate').' 23:59:00'; 
      // if($access_response['code']==1)
      // {
         
        $response= $this->Analytics_Model->load_product_analytics($data);
      // }
      // else
      // {
      //   $response=json_encode($access_response);
      // }
      echo $response;
    }
    public function load_agents_report()
    {
      $data['start_date'] = $this->input->post('startdate').' 01:00:00'; 
      $data['end_date'] =  $this->input->post('enddate').' 23:59:00'; 
      $response= $this->Analytics_Model->load_agents_report($data);
      echo $response;
    }

    
}