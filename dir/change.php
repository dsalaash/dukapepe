<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Duka pepe | Change Password</title>

    <?php include("../links.php") ?>
    <!-- /theme JS files -->
    <script>
        $(document).ready(function () {
            $(".status-progress").hide();
            $.extend($.fn.dataTable.defaults, {
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                },
                drawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                },
                preDrawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                }
            });

            if (localStorage.getItem('code') == 1) {
                ajaxedit();
            } else {

                new PNotify({
                    title: 'Primary notice',
                    text: 'Error: You are not authorised to view this page.',
                    addclass: 'bg-warning'
                });

            }
//                ajaxedit();
            passwordchange();
        });

        function passwordchange() {
            $('#formpassword').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress").show();

                $.ajax({
                    url: base_url + "/user/change_password",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (json) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress").hide();
                        var data = JSON.parse(json);
                        //obj = JSON.parse(json['message']);
                        new PNotify({
                            text: data['message'],
                            addclass: 'bg-success'
                        });
                    },
                    error: function () {
                    }
                });
            });
        }

        function ajaxedit() {


            $('#retainerPhone').val(localStorage.getItem('phone'));

        }

    </script>

</head>

<body>

<?php include("../topbar.php") ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->

                <!-- /user menu -->
                <?php include("../sidebardir.php") ?>
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span
                                    class="text-semibold">Change Password</span></h4>
                    </div>

                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                        <li class="active">Change Password</li>
                    </ul>

                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Basic datatable -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Change password</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form role="form" class="form-validate" method="POST" id="formpassword"
                              enctype="multipart/form-data">

                            <div class="modal-body">

                                <div class="form-group">

                                    <label>Phone:</label>
                                    <input name="phone" id="retainerPhone" class="form-control" placeholder="Phone"/>

                                </div>
                                <div class="form-group">

                                    <label>Password:</label>
                                    <input type="password" name="password" id="password" class="form-control"
                                           placeholder="Password"/>

                                </div>


                            </div>
                            <div class="modal-footer">

                                <button type="submit" class="btn btn-primary" id="submit" value="add">Change password
                                </button>
                            </div>
                            <img class="text-center center-block status-progress" src="../assets/loader/loader.gif"/>
                        </form>

                    </div>

                </div>
                <!-- /basic datatable -->


                <!-- Footer -->
                <div class="footer text-muted">
                    <?php include("../footer.php") ?>

                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
