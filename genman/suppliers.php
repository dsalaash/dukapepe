<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka pepe | Suppliers</title>

        <?php include("../links.php") ?>
        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search Suppliers:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });

                if (localStorage.getItem('code') == 1) {
                    loadSuppliers();
                } else {

                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });

                }

                $("select").change(function () {
                    $("select option:selected").each(function () {
                        if ($(this).attr("value") == "4") {
                            $(".box").hide();
                            $(".cc").show();
                        }
                        if ($(this).attr("value") == "3") {
                            $(".box").hide();
                            $(".supplier").show();
                        }
                        if ($(this).attr("value") == "2") {
                            $(".box").hide();
                            $(".director").show();
                        }
                        if ($(this).attr("value") == "choose") {
                            $(".box").hide();
                            $(".choose").show();
                        }
                    });
                }).change();

            });
            function loadSuppliers() {
                console.log(localStorage.getItem('access_token'));
                var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "supplier/fetch_all";
                $('#example').DataTable({
                    "destroy": true,
                    "ajax": {
                        "url": url,
                        "data": formData,
                        "type": "post",
                        "dataSrc": function (json) {
                            return json;
                        },
                        "processing": true,
                        "serverSide": true,
                        "pagingType": "simple",
                        language: {
                            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                        }

                    }, "columns": [
                        {"data": "first_name"},
                        {"data": "business_name"},
                        {"data": "phone"},
                        {"data": "email"},
                        {"data": "location_name"},
                        {"data": "availability", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                if (data == 1) {
                                    return '<span class="label label-primary">Available</span>';
                                } else {
                                    return '<span class="label label-primary">Not available</span>';
                                }
                            }},
                        {"data": "is_active", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                if (data == 1) {
                                    return '<a href="javascript: deactivateSupplier(' + full.supplier_id + ')" class="btn btn-xs btn-warning"><i class="icon-lock2"></i>De activate</a>';
                                } else {
                                    return '<a href="javascript: activateSupplier(' + full.supplier_id + ')" class="btn btn-xs btn-success"><i class="icon-unlocked2"></i>Activate</a>';
                                }
                            }},
                        {"data": "supplier_id", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                var links = '<ul class="icons-list"><li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>\n\
                                        <ul class="dropdown-menu dropdown-menu-right">\n\
                                    <li><a href="javascript: ajaxmodaledit(' + data + ')" ><i class="icon-pencil5"></i>Details</a></li>\n\
                                    <li><a href="javascript: ajaxmodaledit2(' + data + ')" ><i class="glyphicon glyphicon-pencil"></i>Edit</a></li>\n\
                                    \n\
                                    \n\
                                    </ul></li></ul>';
                                return links;
                            }}
                    ]
                });
            }
            function ajaxmodaledit(id) {
                $(".status-progress").show();
                var url = base_url + "supplier/fetch";
                var formData = {
                    'supplier_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];

                        document.getElementById("first_name2").innerHTML = obj_data['first_name'];
                        document.getElementById("last_name2").innerHTML = obj_data['last_name'];
                        document.getElementById("business_name2").innerHTML = obj_data['business_name'];
                        document.getElementById("phone2").innerHTML = obj_data['phone'];
                        document.getElementById("email2").innerHTML = obj_data['email'];
                        document.getElementById("location_name2").innerHTML = obj_data['location_name'];
                        document.getElementById("latitude2").innerHTML = obj_data['latitude'];
                        document.getElementById("longitude2").innerHTML = obj_data['longitude'];
                        if (obj_data['availability'] == 1) {
                            document.getElementById("availability").innerHTML = '<span class="label label-primary">Available</span>';
                        } else {
                            document.getElementById("availability").innerHTML = '<span class="label label-primary">Not available</span>';
                        }
                    }
                    $(".status-progress").hide();
                    $('#modalCat').modal('show');
                });

            }

            function ajaxmodalsup(id,businessname){
                localStorage.setItem('supplier_id_stock', id);
                localStorage.setItem('supplier_name_stock', businessname);
                window.location = "dcstock.php";
            }


            function ajaxmodaledit2(id) {
                $(".status-progress").show();
                var url = base_url + "supplier/fetch";
                var formData = {
                    'supplier_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {

                        var obj_data = obj[i];
                        // console.log(obj_data['phone']);
                        $('#supplier_id').val(id);
                        $('#access_token').val(localStorage.getItem('access_token'));
                        $('#first_name').val(obj_data['first_name']);
                        $('#last_name').val(obj_data['last_name']);
                        $('#business_name').val(obj_data['business_name']);
                        $('#mobilePhone').val(obj_data['phone']);
                        $('#emailadd').val(obj_data['email']);
                        $('#location_name').val(obj_data['location_name']);
                        $('#latitude').val(obj_data['latitude']);
                        $('#longitude').val(obj_data['longitude']);

                    }
                    $(".status-progress").hide();
                    $('#modalSup').modal('show');
                });

            }


            function activateSupplier(id) {
                $(".status-progress").show();
                var url = base_url + "supplier/status";
                var formData = {
                    'supplier_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 1
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                        title: 'Update Notice',
                        text: data['message'],
                        addclass: 'bg-success'
                    });
                    loadSuppliers();
                    $(".status-progress").hide();
                });
            }

            function deactivateSupplier(id) {
                $(".status-progress").show();
                var url = base_url + "supplier/status";
                var formData = {
                    'supplier_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 0
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                        title: 'Update Notice',
                        text: data['message'],
                        addclass: 'bg-success'
                    });
                    loadSuppliers();
                    $(".status-progress").hide();
                });
            }

        </script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyB8WgY93rMVkcDEn3Z64CUcSh3Jx_UMeH4"></script>
        <script>
            google.maps.event.addDomListener(window, 'load', initialize);
            function initialize() {
                var input = document.getElementById('location_name');
                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.addListener('place_changed', function () {
                    var place = autocomplete.getPlace();
                    // place variable will have all the information you are looking for.
                    $('#latitude').val(place.geometry['location'].lat());
                    $('#longitude').val(place.geometry['location'].lng());
                    console.log(place.geometry['location'].lat());
                    console.log(place.geometry['location'].lng());
                });
            }
        </script>
    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->

                        <!-- /user menu -->
                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Suppliers</span></h4>
                            </div>
                            <div class="heading-elements">
                                <a href="add-supplier.php" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus-circle"></i> Add A Supplier</a>
                            </div>
                        </div>
                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                                <li class="active">Suppliers</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->
                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <!-- <div class="panel-heading">
                                <h5 class="panel-title">Suppliers</h5>
                                <div class="heading-elements">
                                    <a href="add-supplier.php" class="btn btn-primary btn-sm pull-right"><i class="fa fa-plus-circle"></i> Add A Supplier</a>
                                </div>
                            </div> -->

                            <div class="panel-body">
<!--                                <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add a SKU</a>-->
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            

                            <table class="table" id="example">
                                <thead>
                                    <tr>
                                        <th>Fist name</th>
                                        <th>Business name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Location name</th>
                                        <th>Availability</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                            </table>
                            </div>
                        </div>
                        <!-- /basic datatable -->

                        <!-- Vertical form modal -->
                        <div id="modalCat" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Supplier Details</h5>
                                    </div>
                                    <table class="table table-bordered table-striped">

                                        <tr>
                                            <th>First name</th>
                                            <td><p id="first_name2"></p></td>
                                        </tr>
                                        <tr>
                                            <th>Last name</th>
                                            <td><p id="last_name2"></p></td>
                                        </tr>
                                        <tr>
                                            <th>Business name</th>
                                            <td><p id="business_name2"></p></td>
                                        </tr>
                                        <tr>
                                            <th>Phone</th>
                                            <td><p id="phone2"></p></td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td><div id="email2"></div></td>
                                        </tr>
                                        <tr>
                                            <th>Location name</th>
                                            <td><div id="location_name2"></div></td>
                                        </tr>
                                        <tr>
                                            <th>Latitude</th>
                                            <td><p id="latitude2"></p></td>
                                        </tr>
                                        <tr>
                                            <th>Longitude</th>
                                            <td><p id="longitude2"></p></td>
                                        </tr>
                                        <tr>
                                            <th>Availability</th>
                                            <td><p id="availability"></p></td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->
                        <style>
                            .modal{
                                z-index: 20;   
                            }
                            .modal-backdrop{
                                z-index: 10;        
                            }
                        </style>

                        <!-- this is an edit modal -->
                        <div id="modalSup" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Supplier Edit</h5>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" class="form-validate" method="POST" id="formAdd" enctype="multipart/form-data">
                                            <input name="access_token" id="access_token" type="hidden" />
                                            <input name="supplier_id" id="supplier_id" type="hidden" />
                                            <p id="demo"></p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" required name="first_name" id="first_name" class="form-control" placeholder="First name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" required name="last_name" id="last_name" class="form-control" placeholder="Last name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="email" required name="email" id="emailadd" class="form-control" placeholder="Email">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-mention text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" required name="phone" id="mobilePhone" class="form-control" placeholder="Phone number">
                                                        <div class="form-control-feedback">
                                                            <i class="glyphicon glyphicon-earphone text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row" style="margin-bottom: 15px;">

                                                <div class="col-md-6">
                                                    <select required name="user_type"  class="form-control select-search">
                                                        <optgroup label="Select Access type">
                                                            <option value="3">Supplier</option>
                                                        </optgroup>
                                                    </select>
                                                </div>


                                            </div>
                                            <div class="cc box">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="number"  name="id_number" class="form-control" placeholder="Id number">
                                                            <div class="form-control-feedback">
                                                                <i class="icon-user-check text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">

                                                            <select name="gender"  class="select">
                                                                <optgroup label="Select Gender">
                                                                    <option value="female">Female</option>
                                                                    <option value="male">Male</option>
                                                                </optgroup>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text"  name="dob" class="form-control daterange-single" value="01/01/1990">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-calendar text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="supplier box">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text" id="location_name" required name="location_name" class="form-control" placeholder="Location Name">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input name="business_name" id="business_name" required  type="text" class="form-control" placeholder="Business name">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-home text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text"  id="latitude" required name="latitude" class="form-control" placeholder="Latitude">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text"  id="longitude" required name="longitude" class="form-control" placeholder="Longitude">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="choose box"></div>
                                            <div class="director box"></div>
                                            <div class="text-right">
                                                <img class="status-progress"  src="../assets/loader/loader.gif"/>
                                                <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10">Update supplier</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>

                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

        <script>

            var x = document.getElementById("demo");

            function showPosition(position) {

                $('#latitude').val(position.coords.latitude);
                $('#longitude').val(position.coords.longitude);
            }
            function ajaxmodaladd() {
                // $(".status-progress-add").hide();
                // $('#catTokenSup').val(localStorage.getItem('access_token'));
                $('#addmodal').modal('show');
            }



            $('#formAdd').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress").show();

                $.ajax({
                    url: base_url + "supplier/update_profile",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress").hide();

                        var data = JSON.parse(data);
                        $(".status-progress").hide();
                        $('#modalSup').modal('hide');
                        loadSuppliers();
                        new PNotify({
                            title: 'Registration Notice',
                            text: data['message'],
                            addclass: 'bg-success'
                        });
                    },
                    error: function (data) {
                        new PNotify({
                            title: 'Registration Notice',
                            text: data['message'],
                            addclass: 'bg-warning'
                        });
                        $('#modalSup').modal('hide');
                        loadSuppliers();
                        $(".status-progress").hide();
                    }
                });


            });
        </script>


    </body>
</html>
