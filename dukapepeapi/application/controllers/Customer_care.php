<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Customer_care
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Customer_care extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function fetch(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
          $data['cc_id']= $this->input->post('cc_id');
          $response= $this->Customer_Care_Model->fetch($data);
        }
      else{
          $response= json_encode($access_response);   
      }
    echo $response;
    }
    public function fetch_all(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $response= $this->Customer_Care_Model->fetch_all();
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    
    public function update_profile(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
         $data['cc_id'] = $this->input->post('cc_id');   
         $data['first_name'] = $this->input->post('first_name');  
         $data['last_name'] = $this->input->post('last_name');
         $data['gender'] = $this->input->post('gender');
         $data['id_number'] = $this->input->post('id_number');
         $data['dob'] = $this->input->post('dob');  
         $data['email'] = $this->input->post('email');  
            
            
        $response= $this->Customer_Care_Model->update_profile($data);
        }
        else{
          $response= json_encode($access_response);   
        }
        echo $response;   
    }
}
