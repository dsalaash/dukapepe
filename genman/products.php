<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka pepe | Products</title>

        <?php include("../links.php") ?>
        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search Products:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });

                if (localStorage.getItem('code') == 1) {
                    loadProducts();
                } else {

                    new PNotify({
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });

                }

                update();
                AddProducts();
                addImages();
                var formData = {
                    'access_token': localStorage.getItem('access_token')
                };
                var url = base_url + "sub_category/fetch_all";

                $.post(url, formData,
                        function (data) {

                            var datas = JSON.parse(data);

                            var model = $('#selectsubcat');
                            model.empty();

                            $.each(datas, function (index, element) {
                                model.append("<option value='" + element.id + "'>" + element.sub_category + "</option>");
                            });
                        });

                $.post(url, formData,
                        function (data) {

                            var datas = JSON.parse(data);

                            var model = $('#sub_cat_id');
                            model.empty();

                            $.each(datas, function (index, element) {
                                model.append("<option value='" + element.id + "'>" + element.sub_category + "</option>");
                            });
                        });



            });
            function loadProducts() {
                console.log(localStorage.getItem('access_token'));
                var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "products/fetch_all";
                $('#example').DataTable({
                    "destroy": true,
                    "ajax": {
                        "url": url,
                        "data": formData,
                        "type": "post",
                        "dataSrc": function (json) {
                            return json;
                        },
                        "processing": true,
                        "serverSide": true,
                        "pagingType": "simple",
                        language: {
                            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                        }

                    }, "columns": [
                        {"data": "sub_category_name"},
                        {"data": "product_name"},
                        {"data": "product_description"},
                        {"data": "product_id", orderable: false, searchable: false, render: function (data, type, full, meta) {


                                var links = '<ul class="icons-list"><li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>\n\
                                        <ul class="dropdown-menu dropdown-menu-right">\n\
<li><a href="javascript: ajaxmodaledit(' + data + ')" >edit</a></li>\n\
<li><a href="javascript: ajaxmodaladdimage(' + data + ')" >add images</a></a></li>\n\
\n\<li><a href="javascript: ajaxmodalviewimage(' + data + ')" >view images</a></a></li>\n\
\n\<li><a onclick="return confirm(\'Are you sure you want to delete this?\')" href="javascript: ajaxdelete(' + data + ')" >delete product</a></a></li>\n\
\n\
\n\
</ul></li></ul>';
                                return links;


                            }}
                    ]
                });

            }


            function ajaxdelete(id) {
                $(".status-progress").show();
                $(".status-progress-add").hide();
                var url = base_url + "products/delete";
                var formData = {
                    'product_id': id,
                    'access_token': localStorage.getItem('access_token')
                };

                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    new PNotify({
                        text: obj['message'],
                        addclass: 'bg-success'
                    });

                    $(".status-progress").hide();
                    loadProducts();

                });
            }

            function ajaxmodalviewimage(id) {
                $(".status-progress").show();
                $(".status-progress-add").hide();
                var url = base_url + "products/get_product_images ";
                var formData = {
                    'product_id': id,
                    'access_token': localStorage.getItem('access_token')
                };

                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    $('#tablemy tbody').html('');
                    $("#tBody").empty();
                    var trHTML = '';
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];
                        obj2 = obj_data['image_url'];
                        console.log(obj2);

                        trHTML +=
                                '<tr><td>'
                                + '<img src="' + obj2 + '" alt="No Image" style="width: 50px; height: 58px;">'
                                + '</td><td>'
                                + '<a onclick="return confirm(\'Are you sure you want to delete this?\')" href="javascript: ajaxmodaldeleteimage(' + obj_data['pi_id'] + ')" class="btn btn-xs btn-warning">Delete</a>'
                                + '</td></tr>';






                    }
                    $('#tBody').append(trHTML);
                    $(".status-progress").hide();
                    $('#modalimages').modal('show');
                });
            }

            function ajaxmodaldeleteimage(id) {
                $(".status-progress").show();
                $(".status-progress-add").hide();
                var url = base_url + "products/delete_image";
                var formData = {
                    'pi_id': id,
                    'access_token': localStorage.getItem('access_token')
                };

                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    new PNotify({
                        text: obj['message'],
                        addclass: 'bg-success'
                    });

                    $(".status-progress").hide();

                    $('#modalimages').modal('hide');
                });
            }


            function ajaxmodaledit(id) {
                $(".status-progress").show();
                $(".status-progress-add").hide();
                var url = base_url + "products/getProductDetails";
                var formData = {
                    'product_id': id,
                    'access_token': localStorage.getItem('access_token')
                };

                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    console.log(obj['product_name']);
                    $('#catToken').val(localStorage.getItem('access_token'));
                    $('#product_id').val(obj['product_id']);
                    $('#product_name').val(obj['product_name']);
                    $('#product_description').val(obj['product_description']);
                    $('#sub_cat_id').select2().val(obj['sub_category_id']).trigger("change");

                    $(".status-progress").hide();

                    $('#modalCat').modal('show');
                });

            }

            function ajaxmodaladdimage(id) {
                $(".status-progress").show();
                $(".status-progress-add").hide();
                $('#proToken').val(localStorage.getItem('access_token'));
                $('#productIDpro').val(id);
                $(".status-progress").hide();
                $('#modalformimages').modal('show');


            }


            function ajaxmodaladd() {
                $(".status-progress-add").hide();
                $('#catToken2').val(localStorage.getItem('access_token'));
                $('#modalCatAdd').modal('show');
            }

            function AddProducts() {
                $('#formcategoryAdd').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "products/add",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-add").hide();
                            loadProducts();
                            $('#modalCatAdd').modal('hide');
                            var data = JSON.parse(data);
                            new PNotify({
                                text: data['message'],
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {}
                    });
                });
            }

            function update() {
                $('#formcategory').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress").show();
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "products/update",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $('#modalCat').modal('show');
                            $("#submit").removeClass("disabled");
                            $(".status-progress").hide();
                            $(".status-progress-add").hide();

                            loadProducts();
                            $('#modalCat').modal('hide');
                            var data = JSON.parse(data);
                            new PNotify({
                                text: data['message'],
                                addclass: 'bg-success'
                            });

                            console.log(data);
                        },
                        error: function () {}
                    });
                });
            }

            function addImages() {
                $('#formimages').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress").show();
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "products/save_image",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {

                            $("#submit").removeClass("disabled");
                            $(".status-progress").hide();
                            $(".status-progress-add").hide();

                            loadProducts();
                            $('#modalformimages').modal('hide');
                            var data = JSON.parse(data);
                            new PNotify({
                                text: 'Image added successfully',
                                addclass: 'bg-success'
                            });

                            console.log(data);
                        },
                        error: function () {}
                    });
                });
            }

        </script>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->

                        <!-- /user menu -->
                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Products</span></h4>
                            </div>
                            <div class="heading-elements">
                                <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>Add a Product</a>
                            </div>

                        </div>


                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <!-- <div class="panel-heading">
                                <h5 class="panel-title">Products </h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div> -->

                            <div class="panel-body">
                                
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            

                            <table class="table" id="example">
                                <thead>
                                    <tr>

                                        <th>Sub category name</th>
                                        <th>Product name</th>
                                        <th>Product description</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>

                            </table>
                        </div>
                        </div>
                        <!-- /basic datatable -->

                        <!-- Vertical form modal -->
                        <div id="modalCat" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Products edit form</h5>
                                    </div>

                                    <form role="form" class="form-validate" method="POST" id="formcategory" enctype="multipart/form-data">

                                        <input type="hidden" name="product_id" id="product_id"/>
                                        <input type="hidden" name="access_token" id="catToken"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Sub Category:</label>
                                                <select required id="sub_cat_id" name="sub_cat_id" class="select-search">
                                                    <option value="0">Select Sub Category</option>
                                                </select>

                                            </div>
                                            <div class="form-group">

                                                <label>Product Name:</label>
                                                <input required type="text" name="product_name" id="product_name" class="form-control" placeholder="Product name" />

                                            </div>
                                            <div class="form-group">

                                                <label>Product Description:</label>
                                                <input required type="text" name="product_description" id="product_description" class="form-control" placeholder="Product description" />

                                            </div>
                                            <div class="form-group">

                                                <label>Upload Image:</label>
                                                <input type="file" name="image_url" class="file-input" data-show-caption="true" data-show-upload="false">

                                            </div>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Update</button>
                                            <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <!-- Vertical form modal -->
                        <div id="modalCatAdd" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Products Add</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formcategoryAdd" enctype="multipart/form-data">
                                        <input type="hidden" name="access_token" id="catToken2"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Sub category:</label>
                                                <select required id="selectsubcat" name="sub_cat_id" class="select-search">
                                                    <option value="0">Select Sub Category</option>
                                                </select>

                                            </div>
                                            <div class="form-group">

                                                <label>Product Name:</label>
                                                <input required type="text" name="product_name" id="product_name2" class="form-control" placeholder="Product name" />

                                            </div>
                                            <div class="form-group">

                                                <label>Product Description:</label>
                                                <input required type="text" name="product_description" id="product_description2" class="form-control" placeholder="Product description" />

                                            </div>
                                            <div class="form-group">

                                                <label>Upload Image:</label>
                                                <input type="file" name="image_url" class="file-input" data-show-caption="true" data-show-upload="false">

                                            </div>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                            <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <!-- Vertical form modal -->
                        <div id="modalformimages" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Add Images</h5>
                                    </div>

                                    <form role="form" class="form-validate" method="POST" id="formimages" enctype="multipart/form-data">
                                        <input type="hidden" name="product_id" id="productIDpro"/>
                                        <input type="hidden" name="access_token" id="proToken"/>
                                        <div class="modal-body">

                                            <div class="form-group">

                                                <label>Upload Image:</label>
                                                <input type="file" name="image_url" class="file-input" multiple="multiple" data-show-caption="true" data-show-upload="false">

                                            </div>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Update</button>
                                            <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <div id="modalimages" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Images</h5>
                                    </div>

                                    <div class="modal-body"><img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                                        <table id="tablemy" class="table table-bordered table-striped">

                                            <thead>
                                                <tr>
                                                    <th>Product Image</th>

                                                    <th>Action</th>

                                                </tr>
                                            </thead>
                                            <tbody id="tBody"></tbody>

                                        </table>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->




                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
