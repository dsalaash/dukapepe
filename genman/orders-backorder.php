<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka pepe | Orders Back order</title>

        <?php include("../links.php") ?>
        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search orders:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });

                if (localStorage.getItem('code') == 1) {
                    loadOrders();
                } else {

                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });

                }


                Ordersadd();
                unitPriceUpdate();
                stockNew();

            });

            function loadOrders() {
                console.log(localStorage.getItem('access_token'));
                var formData = {'access_token': localStorage.getItem('access_token'), 'status_id': '4'};
                var url = base_url + "order/get_order_by_status";
                $('#example').DataTable({
                    "destroy": true,
                    "ajax": {
                        "headers": {
                            'Accept': 'application/json'
                        },
                        "url": url,
                        "data": formData,
                        "type": "post",
                        "dataSrc": function (json) {
                            return json;
                        },
                        "processing": true,
                        "serverSide": true,
                        "pagingType": "simple",
                        language: {
                            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                        }

                    }, "columns": [
                        {"data": "order_id"},
                        {"data": "retailer_name"},
                        {"data": "phone"},
                        {"data": "email"},
                        {"data": "total_cost"},
                        {"data": "timestamp"},
                        {"data": "payment_status", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                if (data == 'Not paid') {
                                    return '<span class="label label-warning">Not Paid</span>';
                                } else {
                                    return '<span class="label label-primary">Paid</span>';
                                }

                            }},
                        {orderable: false, searchable: false, render: function (data, type, full, meta) {
                                var links = '<ul class="icons-list"><li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>\n\
                                        <ul class="dropdown-menu dropdown-menu-right">\n\
<li><a href="javascript: updateStatus(' + full['order_id'] + ',2)">Packed</a></li>\n\
<li><a href="javascript: updateStatus(' + full['order_id'] + ',3)">On transit</a></li>\n\
\n\
</ul></li></ul>';
                                return links;
                            }}
                    ]
                });

            }


            function addOrders(id) {
                $(".status-progress").show();
                var url = base_url + "stock/fetch";
                var formData = {
                    'stock_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];
                        $('#stockToken').val(localStorage.getItem('access_token'));
                        $('#stockId').val(obj_data['stock_id']);
                        $('#quantity').val(obj_data['quantity']);
                    }
                    $(".status-progress").hide();
                    $('#modalAddstock').modal('show');
                });

            }


            function updatePrice(id) {
                $(".status-progress").show();
                var url = base_url + "stock/fetch";
                var formData = {
                    'stock_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];
                        $('#stockToken').val(localStorage.getItem('access_token'));
                        $('#stockId').val(obj_data['stock_id']);
                        $('#unit_cost').val(obj_data['unit_cost']);
                    }
                    $(".status-progress").hide();
                    $('#modalUnitstock').modal('show');
                });

            }


            function ajaxmodaladd() {
                $(".status-progress-add").hide();
                $('#supplier_id').val(localStorage.getItem('supplier_id'));
                $('#modalOrdersNew').modal('show');
            }

            function Ordersadd() {
                $('#formstockadd').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "/stock/new_stock ",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-add").hide();
                            loadOrders();
                            $('#modalAddstock').modal('hide');

                            new PNotify({
                                title: 'Add Orders Notice',
                                text: 'Quantity updated successfully',
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {}
                    });
                });
            }
            
            function unitPriceUpdate() {
                $('#formunitprice').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "/stock/update_price ",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-add").hide();
                            loadOrders();
                            $('#modalUnitstock').modal('hide');

                            new PNotify({
                                title: 'Notice',
                                text: 'Unit price updated successfully',
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {}
                    });
                });
            }

            function stockNew() {
                $('#formstockNew').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "/stock/new_product ",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-add").hide();
                            loadOrders();
                            $('#modalOrdersNew').modal('hide');

                            new PNotify({
                                title: 'Notice',
                                text: 'New Orders added successfully',
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {}
                    });
                });

            }


            function activateCategory(id) {
                $(".status-progress").show();
                var url = base_url + "/category/status";
                var formData = {
                    'category_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 1
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                        title: 'Update Notice',
                        text: data['message'],
                        addclass: 'bg-info'
                    });
                    loadCategoryUpdate();
                    $(".status-progress").hide();
                });
            }

            function deactivateCategory(id) {
                $(".status-progress").show();
                var url = base_url + "/category/status";
                var formData = {
                    'category_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 0
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                        title: 'Update Notice',
                        text: data['message'],
                        addclass: 'bg-warning'
                    });
                    loadCategoryUpdate();
                    $(".status-progress").hide();
                });
            }

        </script>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                       
                        <!-- /user menu -->

                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Orders Received</span></h4>
                            </div>

                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                                <li class="active">Orders Received</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">Orders</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <!--<a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Orders</a>-->
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            </div>

                            <table class="table" id="example">
                                <thead>
                                    <tr>
                                        <th>Order Id</th>
                                        <th>Retailer Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Total Cost</th>
                                       
                                        <th>Timestamp</th>
                                        <th>Status</th>
                                         <th>Action</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                        <!-- /basic datatable -->

                        <!-- Vertical form modal -->
                        <div id="modalAddstock" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Add stock form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formstockadd" enctype="multipart/form-data">
                                        <input type="hidden" name="stock_id" id="stockId"/>
                                        <input type="hidden" name="access_token" id="stockToken"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Quantity</label>
                                                <input name="quantity" id="quantity" class="form-control" placeholder="Quantity" />

                                            </div>


                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->



                        <!-- Vertical form modal -->
                        <div id="modalUnitstock" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Update Unit cost</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formunitprice" enctype="multipart/form-data">
                                        <input type="hidden" name="stock_id" id="stockId"/>
                                        <input type="hidden" name="access_token" id="stockToken"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Unit Cost</label>
                                                <input name="unit_cost" id="unit_cost" class="form-control" placeholder="Unit cost" />

                                            </div>


                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->




                        <!-- Vertical form modal -->
                        <div id="modalOrdersNew" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">New Orders Product form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formstockNew" enctype="multipart/form-data">
                                        <input type="hidden" name="supplier_id" id="supplier_id"/>
                                        <input type="hidden" name="access_token" id="supToken"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Sku:</label>
                                                <select id="selectsku" name="sku_id" class="select-search">
                                                    <option value="0">Select Sku</option>
                                                </select>

                                            </div>
                                            <div class="form-group">

                                                <label>Product:</label>
                                                <select id="selectpro" name="product_id" class="select-search">
                                                    <option value="0">Select Product</option>
                                                </select>

                                            </div>
                                            <div class="form-group">

                                                <label>Unit Cost:</label>
                                                <input name="unit_cost" id="unit_cost" class="form-control" placeholder="Unit cost" />

                                            </div>
                                            <div class="form-group">

                                                <label>Quantity:</label>
                                                <input name="quantity" id="quantity" class="form-control" placeholder="Quantity" />

                                            </div>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                            <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
