<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Payment_Option
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Payment_method extends CI_Controller{
     //put your code here
    public function __construct() {
        parent::__construct();
    }
    public function add(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
    $name= $this->input->post('name');
    $response= $this->Payment_Method_Model->add($name);
    }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    public function update(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
    $id= $this->input->post('id');
    $name= $this->input->post('name');
    $data=array();
    $data['id']=$id;
    $data['name']=$name;
    $response= $this->Payment_Method_Model->update($data);
    }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;    
    }
    public function fetch(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
    $id= $this->input->post('id');
    $response= $this->Payment_Method_Model->fetch($id);
    }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;   
    }
    
    public function fetch_all(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
    $response= $this->Payment_Method_Model->fetch_all($this->input->post('user_type'));
    }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
   
}
