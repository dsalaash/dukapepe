<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

        <script type="text/javascript" src="assets/js/core/app.js"></script>
        <script type="text/javascript" src="assets/js/pages/login.js"></script>
        <!-- /theme JS files -->
        <script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/jquery_ui/interactions.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/form_select2.js"></script>


        <script type="text/javascript" src="assets/js/plugins/notifications/jgrowl.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>
        <script type="text/javascript" src="assets/js/plugins/pickers/anytime.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/pickers/pickadate/picker.js"></script>
        <script type="text/javascript" src="assets/js/plugins/pickers/pickadate/picker.date.js"></script>
        <script type="text/javascript" src="assets/js/plugins/pickers/pickadate/picker.time.js"></script>
        <script type="text/javascript" src="assets/js/plugins/pickers/pickadate/legacy.js"></script>
        <script type="text/javascript" src="assets/js/plugins/notifications/pnotify.min.js"></script>


        <script type="text/javascript" src="assets/js/pages/picker_date.js"></script>
         <script type="text/javascript" src="assets/js/load_image.js"></script>
        <script type="text/javascript" src="assets/js/configs.js"></script>

    </head>

    <body class="login-container">

        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $("select").change(function () {
                    $("select option:selected").each(function () {
                        if ($(this).attr("value") == "4") {
                            $(".box").hide();
                            $(".cc").show();
                        }
                        if ($(this).attr("value") == "3") {
                            $(".box").hide();
                            $(".supplier").show();
                        }
                        if ($(this).attr("value") == "2") {
                            $(".box").hide();
                            $(".director").show();
                        }
                        if ($(this).attr("value") == "choose") {
                            $(".box").hide();
                            $(".choose").show();
                        }
                    });
                }).change();

            });


        </script>




        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content">



                        <!-- Registration form -->
                        <form role="form" class="form-validate" method="POST" id="formAdd" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-6 col-lg-offset-3">
                                    <div class="panel registration-form">
                                        <div class="panel-body">
                                            <div class="text-center">
                                                <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                                                <h5 class="content-group-lg">Create account <small class="display-block">All fields are required</small></h5>
                                            </div>

                                            <p id="demo"></p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" name="first_name" class="form-control" placeholder="First name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" name="last_name" class="form-control" placeholder="Last name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="email" name="email" class="form-control" placeholder="Email">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-mention text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="number" name="phone" class="form-control" placeholder="Phone number">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="latitude" name="latitude" class="form-control" placeholder="Latitude">
                                                <input type="hidden" id="longitude" name="longitude" class="form-control" placeholder="Longitude">
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="password" name="password" class="form-control" placeholder="Create password">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-lock text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <select name="user_type" class="form-control select-search">
                                                        <optgroup label="Select Access type">
                                                            <option value="4">Customer care</option>
                                                            <option value="3">Supplier</option>
                                                            <option value="2">Director</option>
                                                        </optgroup>
                                                    </select>
                                                </div>


                                            </div>
                                            <div class="cc box">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="number" name="id_number" class="form-control" placeholder="Id number">
                                                            <div class="form-control-feedback">
                                                                <i class="icon-user-check text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">

                                                            <select name="gender" class="select">
                                                                <optgroup label="Select Gender">
                                                                    <option value="female">Female</option>
                                                                    <option value="male">Male</option>

                                                                </optgroup>
                                                            </select>

                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text" name="dob" class="form-control daterange-single" value="01/01/1990">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-calendar"></i>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="row supplier box">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" name="location_name" class="form-control" placeholder="Location Name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input name="business_name" type="text" class="form-control" placeholder="Business name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="choose box"></div>
                                            <div class="director box"></div>






                                            <div class="text-right">
                                                
                                                <button type="submit" class="btn btn-link"><i class="icon-arrow-left13 position-left"></i> Back to login form</button>
                                                <img class="status-progress"  src="assets/loader/loader.gif"/>
                                                <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b> Create account</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- /registration form -->


                        <!-- Footer -->
                        <div class="footer text-muted text-center">
                            &copy; 2015. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
        <script>
       
            var x = document.getElementById("demo");
//            function getLocation() {
//                if (navigator.geolocation) {
//                    navigator.geolocation.getCurrentPosition(showPosition);
//                } else {
//                    x.innerHTML = "Geolocation is not supported by this browser.";
//                }
//            }
            function showPosition(position) {

                $('#latitude').val(position.coords.latitude);
                $('#longitude').val(position.coords.longitude);
            }



            $('#formAdd').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress").show();
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                    $.ajax({
                        url: base_url + "user/register",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress").hide();
                            
                            var data = JSON.parse(data);
                            new PNotify({
                                title: 'Registration Notice',
                                text: data['message'],
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {}
                    });

                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            });
        </script>

    </body>
</html>
