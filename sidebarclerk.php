<?php
$arry_uri=explode('/', $_SERVER['REQUEST_URI']);
$dashboardactive=false;
$inventoryactive=false;
if($arry_uri['3']=="dash-today.php" || $arry_uri['3']=="todaymargins.php")
{
$dashboardactive=true;
$inventoryactive=false;
}
else if($arry_uri['3']=="stocklevels.php" || $arry_uri['3']=="purchases.php" || $arry_uri['3']=="purchasehistory.php")
{
$dashboardactive=false;
$inventoryactive=true;
}
?>
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
            <!-- Main -->
            <li class="navigation-header"><span>Navigation</span> <i class="icon-menu" title="Main pages"></i></li>
             <li><a href="dcretailers.php" class="active"><i class="fa fa-store-alt"></i> My Retailers</a></li>
             <li class="<?php echo ($dashboardactive ? 'active' : '') ?>">
                <a href="#"><i class="fa fa-home"></i> <span>Dashboard</span></a>
                <ul>
                    <li><a href="dash-today.php">Daily sales</a></li>
                    <li><a href="todaymargins.php">Daily Margins</a></li>                    
                </ul>
            </li>
            <li class="<?php echo ($inventoryactive ? 'active' : '') ?>">
                <a href="#"><i class="fas fa-chart-line"></i> <span>Inventory control</span></a>
                <ul>
                    <li><a href="stocklevels.php">Stock Levels</a></li>
                    <li><a href="purchases.php">Record Purchase</a></li>
                    <li><a href="purchasehistory.php">Purchase History</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navigation -->
