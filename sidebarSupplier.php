<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
            <!-- Main -->
            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
            <li id="stock"><a href="stock.php"><i class="glyphicon glyphicon-check"></i> <span>Stock</span></a></li>
            <!--<li id="stock"><a href="orders-sup.php"><i class="glyphicon glyphicon-bell"></i> <span>Orders</span></a></li>-->
            <li>
                <a href="#"><i class="glyphicon glyphicon-bell"></i> <span>Orders</span></a>
                <ul>
                    <li><a href="orders.php">Requested Orders</a></li>
                    <li><a href="orders-processing.php">Pending Orders</a></li>
                    <li><a href="orders-packed.php">Packed Orders</a></li>
                    <!--<li><a href="orders-not-assigned-transport.php">Orders Waiting Delivery</a></li>-->
                    <li><a href="orders-delivered.php">Delivered</a></li>
                    <li><a href="orders-backorder.php">Unfullfilled Orders</a></li>
                    <!--<li><a href="orders-backorder.php">Back order</a></li>-->

                </ul>
            </li>
<!--            <li>
                <a href="#"><i class="glyphicon glyphicon-yen"></i> <span>Payments</span></a>
                <ul>
                    <li><a href="order-payments.php">Mpesa Payments</a></li>
                </ul>
            </li>-->
        </ul>
    </div>
</div>
<!-- /main navigation -->