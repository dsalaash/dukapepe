
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("../links.php") ?>
    <!-- /theme JS files -->

</head>

<body>
   <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->

                        <!-- /user menu -->

                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->
                <script type="text/javascript">
                     if(localStorage.getItem('access_token')=="")
                          {
                           window.location = "http://test.duka-pepe.com/dukapepe_portal/dukapepe/";
                          }
                      $(document).ready(function ()
                       {
                        //fech initial analytics year
                          
                           var formData = {'access_token': localStorage.getItem('access_token')};
                           var url = base_url + ="Analytics/top_items";
                           $.ajax({
                           type:'POST',
                           url: url,
                           data: formData,
                           success:function(data)
                           {
                             var datas = JSON.parse(data);
                             console.log(datas['ngong_top_selling']);
                             console.log(datas['kitengela_top_selling']);
                             console.log(datas['fedha_top_selling']);
                            var kite_row="";   
                          
                            $.each(datas['kitengela_top_selling'], function(k, v) {
                               
                              kite_row+="<tr><td>"+v.product_name+"</td><td>"+v.no_items+"</td><td>"+v.sku_name+"</td></tr>";
                               
                            });  
                            $("#kitengela").append(kite_row);
                            $("#example1").DataTable();


                             var ngong_row="";   
                             $.each(datas['ngong_top_selling'], function(k, v) {
                               ngong_row+="<tr><td>"+v.product_name+"</td><td>"+v.no_items+"</td><td>"+v.sku_name+"</td></tr>";
                              
                            });  
                            $("#ngong").append(ngong_row);
                            $("#example2").DataTable();


                              var fedha_row="";   
                              $.each(datas['fedha_top_selling'], function(k, v) {
                               
                              fedha_row+="<tr><td>"+v.product_name+"</td><td>"+v.no_items+"</td><td>"+v.sku_name+"</td></tr>";
                               
                            });  
                            $("#fedha").append(fedha_row);
                            $("#example3").DataTable();

                            },
                           error:function(data)
                           {
                            console.log(data);
                           }        

                       });
                    });
                        ///fech intial analytics year

                         //fech initial analytics year
                          
                           

// $( "#datepicker" ).datepicker({
//   altFormat: "yy-mm-dd"
// });
// // Getter
// var altFormat = $( ".selector" ).datepicker( "option", "altFormat" );
// // Setter
// $( "#datepicker" ).datepicker( "option", "altFormat", "yy-mm-dd" );
//   });          
 </script>


<!-- Modal  retailer distribution-->
<div class="modal fade" id="dcretailersdistribution" tabindex="-1" role="dialog" aria-labelledby="DC Retailer " aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DC -Retailer Distribution</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="containerdcretailersdistribution" style="width:100%; height:400px;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal  retailer distribution-->
<!-- Modal  order distribution-->
<div class="modal fade" id="dcordersdistribution" tabindex="-1" role="dialog" aria-labelledby="DC order Distribution" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DC - Order Distribution</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

         <div id="containerdcordersdistribution" style="width:100%; height:400px;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal  order distribution-->
<!-- Modal  sales distribution-->
<div class="modal fade" id="dcsalesdistribution" tabindex="-1" role="dialog" aria-labelledby="DC order Distribution" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DC -Sales Dstribution</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        
         <div id="containerdcsalesdistribution" style="width:100%; height:400px;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal  sales distribution-->

            <!-- Main content -->
            <div class="content-wrapper">
                <!-- //customerdistribution modal-->

               <!--  ///customerdistribution modal -->

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
                        </div>

                        <div class="heading-elements">
                            <div class="heading-btn-group">
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                            </div>
                        </div>
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-gear position-left"></i>
                                    More Analytics
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="montlyanalytics.php"><i class="icon-user-lock"></i>Monthly</a></li>
                                    <li><a href="annual.php"><i class="icon-statistics"></i>Annual</a></li>
                                    <li><a href="topitems.php"><i class="icon-statistics"></i>topitems</a></li>
                                    <!-- <li><a href="#"><i class="icon-statistics"></i>DC Monthly Comparisons</a></li>
                                    <li><a href="#"><i class="icon-statistics"></i>5 best Selling Items Pe DC</a></li> -->
                                    <!-- <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="icon-gear"></i> All settings</a></li> -->
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">

                   

            <!-- Main content -->
                <div class="row">
                        <div class="col-lg-6">

                            <!-- Traffic sources -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">Top Selling in Kitengela DC</h6>
                                    <div class="heading-elements">
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                     <table class="table datatable-basic" id="example1">
                        <thead>
                        <tr>

                            <th>Item Name</th>
                            <th>Total Sales</th>
                            <th>Sku Name</th>
                        </tr>
                        </thead>
                        <tbody id="kitengela">
                            
                        </tbody>

                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">

                            <!-- Sales stats -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">Top Selling in Ngong DC</h6>
                                    <div class="heading-elements">
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                     <table class="table datatable-basic" id="example2">
                        <thead>
                        <tr>

                            <th>Item Name</th>
                            <th>Total Sales</th>
                            <th>Sku Name</th>
                            
                        </tr>
                        </thead>
                        <tbody id="ngong">
                            
                        </tbody>

                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- second row -->
                    <div class="row">
                        <div class="col-lg-6">

                            <!-- Traffic sources -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">Top Selling in Fedha DC</h6>
                                    <div class="heading-elements">
                                     
                                    </div>
                                </div>
                                <div class="panel-body">
                                     <table class="table datatable-basic" id="example3">
                        <thead>
                        <tr>

                             <th>Item Name</th>
                            <th>Total Sales</th>
                            <th>Sku Name</th>
                        </tr>
                        </thead>
                        <tbody id="fedha">
                            
                        </tbody>

                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">

                            <!-- Sales stats -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">Top Selling In Bomet</h6>
                                    <div class="heading-elements">
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                     <table class="table datatable-basic" id="example4">
                        <thead>
                        <tr>

                            <th>Item Name</th>
                            <th>Total Sales</th>
                            <th>Sku Name</th>
                        </tr>
                        </thead>
                        <tbody id="userstablebody">
                            
                        </tbody>

                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /main charts -->


                    <!-- Dashboard content -->
                   
                    <!-- /dashboard content -->


                    <!-- Footer -->
               <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
