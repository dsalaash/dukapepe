<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Duka pepe | suppliers</title>

<?php include("../links.php") ?>
<!-- /theme JS files -->
<script>
$(document).ready(function () {
    $(".status-progress").hide();
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Search DukaPepe suppliers:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    if (localStorage.getItem('code') == 1) {
        fetch_dukapepe_suppliers();
    } else {

        new PNotify({
            title: 'Primary notice',
            text: 'Error: You are not authorised to view this page.',
            addclass: 'bg-warning'
        });

    }

    update();
    Add();
    // console.log(localStorage);

});




function Add() {

// supplier_access_token
    $('#supplier_form').submit(function (e) {
        // alert('this');
        e.preventDefault();
        
        $("#submit").addClass("disabled");
        $(".status-progress-add").show();
        $.ajax({
            url: base_url + "Dukapepe_Suppliers/add",
            type: "POST",
            data: new FormData(this),
            //Setting these to false because we are sending a multipart request
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                $("#submit").removeClass("disabled");
                $(".status-progress-add").hide();
                $("#supplier_form")[0].reset();
                fetch_dukapepe_suppliers();
                $('#addsupplier').modal('hide');
                var data = JSON.parse(data);
                if (data['code'] == 1) {
                    new PNotify({
                        text: data['message'],
                        addclass: 'bg-success'
                    });
                } else {
                    new PNotify({
                        text: data['message'],
                        addclass: 'bg-success'
                    });
                }

            },
            error: function () {}
        });
    });
}






function fetch_dukapepe_suppliers() {
    // console.log(localStorage.getItem('access_token'));
    var formData = {'access_token': localStorage.getItem('access_token')};
    var url = base_url + "Dukapepe_Suppliers/fetch_all";
    $('#example').DataTable({
        "destroy": true,
        "ajax": {
            "url": url,
            "data": formData,
            "type": "post",
            "dataSrc": function (json) {
                return json;
            },
            "processing": true,
            "serverSide": true,
            "pagingType": "simple",
            language: {
                paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
            }

        }, "columns": [
            // {"data": "first_name"},
            {"data": "business_name"},
            {"data": "phone"},
            {"data": "email"},
            {"data": "account_number"},

            {"data": "id", orderable: false, searchable: false, render: function (data, type, full, meta) {
                    var links = '<ul class="icons-list"><li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>\n\
                            <ul class="dropdown-menu dropdown-menu-right">\n\
                        <li><a href="javascript: supplierdetails(' + data + ')" ><i class="icon-pencil5"></i>Details</a></li>\n\
                        <li><a href="javascript: supplieredit(' + data + ')" ><i class="glyphicon glyphicon-pencil"></i>Edit</a></li>\n\
                        \n\
                        \n\
                        </ul></li></ul>';
                    return links;
                }}
        ]
    });
}

function supplieredit(id) {
    $(".status-progress").show();
    var url = base_url + "dukapepe_suppliers/details";
    var formData = {
        'supplier_id': id
    };
    $.post(url, formData, function (json) {
        obj = JSON.parse(json);
        var count = obj.length;
        for (i = 0; i < count; i++) {

            var obj_data = obj[i];
            // console.log(obj_data['phone']);
            $('#supplier_id_edit').val(obj_data['id']);
            $('#business_name_edit').val(obj_data['business_name']);
            $('#phone_edit').val(obj_data['phone']);
            $('#mobile_edit').val(obj_data['mobile']);
            $('#email_edit').val(obj_data['email']);
            $('#kra_pin_edit').val(obj_data['kra_pin']);
            $('#address_edit').val(obj_data['address']);
            $('#bank_edit').val(obj_data['bank']);
            $('#account_number_edit').val(obj_data['account_number']);
        }
        $(".status-progress").hide();
        $('#modaledit').modal('show');
    });

}
// $('#editform').submit(function (e) {
//     e.preventDefault();
//     $("#update").addClass("disabled");
//     $(".status-progress").show();
//     $.ajax({
//         url: base_url + "dukapepe_suppliers/update_supplier",
//         type: "POST",
//         data: new FormData(this),
//         //Setting these to false because we are sending a multipart request
//         contentType: false,
//         cache: false,
//         processData: false,
//         success: function (data) {
//             $("#update").removeClass("disabled");
//             $(".status-progress").hide();

//             var data = JSON.parse(data);
//             $(".status-progress").hide();
//             $('#modaledit').modal('hide');
//             fetch_dukapepe_suppliers();
//             $("#editform")[0].reset();
//             new PNotify({
//                 title: 'Registration Notice',
//                 text: data['message'],
//                 addclass: 'bg-success'
//             });
//         },
//         error: function (data) {
//             new PNotify({
//                 title: 'Registration Notice',
//                 text: data['message'],
//                 addclass: 'bg-warning'
//             });
//             $("#editform")[0].reset();
//             $('#modaledit').modal('hide');
//             fetch_dukapepe_suppliers();
//             $(".status-progress").hide();
//         }
//     });


// });


function ajaxmodaladd() {
    $(".status-progress-add").hide();
    // $('#access_token').val(localStorage.getItem('access_token'));
    $('#addsupplier').modal('show');
}

function supplierdetails(id) {
    $(".status-progress").show();
    var url = base_url + "dukapepe_suppliers/details";
    var formData = {
        'supplier_id': id
    };
    $.post(url, formData, function (json) {
        obj = JSON.parse(json);
        // alert(obj);
        var count = obj.length;
        for (i = 0; i < count; i++) {
            var obj_data = obj[i];

            document.getElementById("business_name2").innerHTML = obj_data['business_name'];
            document.getElementById("phone2").innerHTML = obj_data['phone'];
            document.getElementById("mobile2").innerHTML = obj_data['mobile'];
            document.getElementById("email2").innerHTML = obj_data['email'];
            document.getElementById("kra_pin2").innerHTML = obj_data['kra_pin'];
            document.getElementById("address2").innerHTML = obj_data['address'];
            document.getElementById("account_number2").innerHTML = obj_data['account_number'];
        }
        $(".status-progress").hide();
        $('#modaldetails').modal('show');
    });

}



function update() {
    $('#editform').submit(function (e) {
        // alert('working');
        e.preventDefault();
        $("#updatebtn").addClass("disabled");
        // $(".status-progress").show();
        // $(".status-progress-add").show();
        $.ajax({
            url: base_url + "dukapepe_suppliers/update_supplier",
            type: "POST",
            data: new FormData(this),

            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                // $('#modalCat').modal('show');
                $("#updatebtn").removeClass("disabled");
                $(".status-progress").hide();
                // $(".status-progress-add").hide();
                $('#modaledit').modal('hide');
                fetch_dukapepe_suppliers();
                 $("#editform")[0].reset();
                var data = JSON.parse(data);
                // console.log(data);
                if (data['code'] == 1) {
                    new PNotify({
                        text: data['message'],
                        addclass: 'bg-success'
                    });
                } else {
                    new PNotify({
                        text: data['message'],
                        addclass: 'bg-success'
                    });
                }

                // console.log(data);
            },
            error: function () {}
        });
    });
}

</script>

</head>

<body>

<?php include("../topbar.php") ?>


<!-- Page container -->
<div class="page-container">

<!-- Page content -->
<div class="page-content">

    <!-- Main sidebar -->
    <div class="sidebar sidebar-main">
        <div class="sidebar-content">

            <!-- User menu -->

            <!-- /user menu -->
            <?php include("../sidebargen.php") ?>
        </div>
    </div>
    <!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-default">
            <div class="page-header-content">
                <div class="page-title">
                    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">DukaPepe Suppliers</span></h4>
                </div>
                <div class="heading-elements">
                    <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-success"> Add a new supplier</a>
                </div>
                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
            </div>

            <div class="breadcrumb-line">
                <ul class="breadcrumb">
                    <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                    <li class="active">DukaPepe suppliers</li>
                </ul>

            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

            <!-- Basic datatable -->
            <div class="panel panel-flat">
                <table class="table" id="example">
                    <thead>
                        <tr>
                            <th>Business</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Account</th>
                            <th>Action</th>
                        </tr>
                    </thead>


                </table>
            </div>
            <!-- /basic datatable -->



            <!-- Vertical form modal -->
            <div id="addsupplier" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Suppliers form</h5>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <form role="form" class="form-horizontal" method="POST" name="supplier_form" id="supplier_form" enctype="multipart/form-data">
                            <input type="hidden" name="access_token" id="access_token"/>
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <label for="business_name">Business name: <span style="color: red;">*</span></label>
                                        <input type="text" required name="business_name" id="business_name" class="form-control" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="phone">Phone:</label>
                                        <input type="text" name="phone" id="phone" class="form-control" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="mobile">Mobile number:</label>
                                        <input type="text" name="mobile" id="mobile" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <label for="email">Email:</label>
                                        <input type="email" name="email" id="email" class="form-control" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="kra_pin">KRA Pin:</label>
                                        <input  type="text" name="kra_pin" id="kra_pin" class="form-control" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="address">Address :</label>
                                        <textarea name="address" id="address" class="form-control" placeholder="Business Address" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label for="bank">Bank</label>
                                        <select class="form-control" name="bank" id="bank">
                                            <option value="" selected="selected">Select supplier bank</option>
                                            <option value="Barclays">Barclays</option>
                                            <option value="DTB">DTB</option>
                                            <option value="NIC">NIC</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="account_number">Account number</label>
                                        <input type="text" name="account_number" id="account_number" class="form-control" placeholder="Business A/C number">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        
                                    </div>
                                    <div class="col-sm-6">
                                        <button type="submit" name="submit" class="btn btn-primary btn-block" id="submit">Save supplier </button>
                                        <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /vertical form modal -->




            <!-- this is an edit modal -->
            <div id="modaledit" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title">Supplier Edit</h5>
                        </div>
                        <div class="modal-body">
                            <form role="form" class="form-horizontal" method="POST" name="editform" id="editform" enctype="multipart/form-data">
                            <input type="hidden" name="access_token_edit" id="access_token_edit"/>
                            <input name="supplier_id_edit" id="supplier_id_edit" type="hidden" />
                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <label for="business_name_edit">Business name: <span style="color: red;">*</span></label>
                                        <input type="text" required name="business_name_edit" id="business_name_edit" class="form-control" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="phone_edit">Phone:</label>
                                        <input type="text" name="phone_edit" id="phone_edit" class="form-control" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="mobile_edit">Mobile number:</label>
                                        <input type="text" name="mobile_edit" id="mobile_edit" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <label for="email_edit">Email:</label>
                                        <input type="email" name="email_edit" id="email_edit" class="form-control" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="kra_pin_edit">KRA Pin:</label>
                                        <input  type="text" name="kra_pin_edit" id="kra_pin_edit" class="form-control" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="address_edit">Address :</label>
                                        <textarea name="address_edit" id="address_edit" class="form-control" placeholder="Business Address" ></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label for="bank_edit">Bank</label>
                                        <select class="form-control" name="bank_edit" id="bank_edit">
                                            <option value="" selected="selected">Select supplier bank</option>
                                            <option value="1">Barclays</option>
                                            <option value="2">DTB</option>
                                            <option value="3">NIC</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="account_number_edit">Account number</label>
                                        <input type="text" name="account_number_edit" id="account_number_edit" class="form-control" placeholder="Business A/C number">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <button type="submit" name="updatebtn" class="btn btn-primary btn-block" id="updatebtn">Update supplier </button>
                                        <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>

                    </div>
                </div>
            </div>



            



            <div id="modaldetails" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title">Supplier Details</h5>
                        </div>
                        <table class="table table-bordered table-striped">

                            <tr>
                                <th>Business name</th>
                                <td><p id="business_name2"></p></td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td><p id="phone2"></p></td>
                            </tr>
                            <tr>
                                <th>Mobile number</th>
                                <td><p id="mobile2"></p></td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td><p id="email2"></p></td>
                            </tr>
                            <tr>
                                <th>KRA pin</th>
                                <td><div id="kra_pin2"></div></td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td><div id="address2"></div></td>
                            </tr>
                            <tr>
                                <th>Account number</th>
                                <td><p id="account_number2"></p></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /vertical form modal -->

            <!-- Footer -->
            <div class="footer text-muted">
                <?php include("../footer.php") ?>
            </div>
            <!-- /footer -->

        </div>
        <!-- /content area -->

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
