<?php

$arry_uri=explode('/', $_SERVER['REQUEST_URI']);
$ordersactive=false;
if($arry_uri['3']=="dashboard.php" || $arry_uri['3']=="generalanalytics.php")
{
$ordersactive=true;
}
?><!-- Main navigation -->
<script type="text/javascript">
     if(localStorage.getItem('user_type')==5)
    {
     window.location = "genman/dashboard.php";
    }
    else if(localStorage.getItem('user_type')==8)
    {
     window.location = "dcclerks/dcretailers.php";   
    }
</script>
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">
            <!-- Main -->
            <li class="navigation-header"><span>Navigation</span> <i class="icon-menu" title="Main pages"></i></li>
            <li>
                <a href="#"><i class="glyphicon glyphicon-plane"></i> <span>Dashboard</span></a>
                <ul>
                    <li><a href="dash-today.php">Today sales</a></li>
                    <li><a href="todaymargins.php">Today Margins</a></li>
                   <!--  <li><a href="cumalitivemargins.php">Agents Performances</a></li> -->
                    <li><a href="dashboardsales.php">Daily Dashboard</a></li>
                    <!-- <li><a href="activetransactions.php">Filtered Sales</a></li> -->
                   
                    
                </ul>
            </li>
             <li class="<?php echo ($ordersactive ? 'active' : '') ?>">
                <a href="#"><i class="glyphicon glyphicon-shopping-cart"></i> <span>Analytics</span></a>
                <ul>
                    <li><a href="dashboard.php" class="active">Current </a></li>
                    <li><a href="generalanalytics.php">General</a></li>
                    <!-- <li><a href="dcanalytics.php"> </a></li>
                    <li><a href="orders-not-assigned-transport.php">Orders not assigned for transport</a></li>
                    <li><a href="brandanalytics.php">Brands</a></li> -->
                    <!--<li><a href="orders-backorder.php">Back order</a></li>-->

                </ul>
            </li>
            
           
        </ul>
    </div>
</div>
<!-- /main navigation -->