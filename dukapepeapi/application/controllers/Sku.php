<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sku
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Sku extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();

    }
    public function add(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['sku_name']= $this->input->post('sku_name');
        $data['weight']= $this->input->post('weight');
        $data['sku_type']= $this->input->post('sku_type');
        $response= $this->Sku_Model->add($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function update(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['sku_id']= $this->input->post('sku_id');
        $data['sku_name']= $this->input->post('sku_name');
        $data['sku_type']= $this->input->post('skuType');
        $data['weight']= $this->input->post('weight');
        $response= $this->Sku_Model->update($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function fetch(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['sku_id']= $this->input->post('sku_id');
        $response= $this->Sku_Model->fetch($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function fetch_all(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $response= $this->Sku_Model->fetch_all();
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function status(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['sku_id']= $this->input->post('sku_id');
        $data['is_active']= $this->input->post('is_active');
        $response= $this->Sku_Model->status($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
}
