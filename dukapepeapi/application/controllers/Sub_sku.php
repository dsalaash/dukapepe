<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sku
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Sub_sku extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('SubSku_Model');
        

    }
    public function add(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['larger_sku']= $this->input->post('larger_sku');
        $data['smaller_sku']= $this->input->post('smaller_sku');
        $data['factor']= $this->input->post('factor');
        $response= $this->SubSku_Model->add($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function update(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['larger_sku']= $this->input->post('edit_larger_sku');
        $data['smaller_sku']= $this->input->post('edit_smaller_sku');
        $data['factor']= $this->input->post('edit_factor');
        $response= $this->Sku_Model->update($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function fetch(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['sku_id']= $this->input->post('sku_id');
        $response= $this->SubSku_Model->fetch($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function fetch_all(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $response= $this->SubSku_Model->fetch_all();
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function status(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['sku_id']= $this->input->post('sku_id');
        $data['is_active']= $this->input->post('is_active');
        $response= $this->Sku_Model->status($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
}
