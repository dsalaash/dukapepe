<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka pepe | Retailers</title>
   
       

        <?php include("../links.php") ?>
        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                if (localStorage.getItem('code') == 1) {
                    loadRetailers();
                } else {
                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });
                }
                $("#formAddcustomer").on("submit", function(e) 
               {
                var light_2 = $('body');
                $(light_2).block({
                                message: '<i class="icon-spinner2 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'none'
                                }
                            });  
             e.preventDefault();
            
             var formData = {'access_token': localStorage.getItem('access_token'),'retailer_name':$("#first_name").val()+" "+$("#last_name").val(),'email':$("#email").val(),'phone':"+254"+$("#mobilePhone").val(),'gender':$("#gender").val(),'location_name':$("#location_name").val(),'latitude':$("#latitude").val(),'longitude':$("#longitude").val(),'reference_no':"",'supplier':$("#supplier_id_tag").val()};
                // console.log(formData);
              $.ajax({
                       type:'POST',
                       url:  base_url + "retailer/clerkregistercustomer",
                       data: formData,
                       success:function(data){
                       
                         new PNotify({
                                    text: "Operation Succssful",
                                    addclass: 'bg-success'
                                });
                             window.location=window.location.href;      
                                },
                       error:function(data){

                                  // console.log(data);
                       }        

                   });
         
  
           });  
               
                // if (localStorage.getItem('code') == 1) {
                //     loadRetailers();
                // } else {

                //     new PNotify({
                //         title: 'Primary notice',
                //         text: 'Error: You are not authorised to view this page.',
                //         addclass: 'bg-warning'
                //     });

                // }

                

            });
            // loadRetailers();
            // editretailer();
            function loadRetailers() {
                // console.log(localStorage.getItem('access_token'));
                var formData = {'access_token': localStorage.getItem('access_token'),'phone':localStorage.getItem('phone'),'retailer':localStorage.getItem('retailer_id')};
                // console.log(formData);
                var url = base_url + "retailer/fetch_all_by_dc";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                        // console.log(data);
                           var parse =JSON.parse(data);
                           
                           $("#supplier_id_tag").val("");
                          $("#supplier_id_tag").val(parse['supplier']);
                           var user_row="";   
                            
                           var no =0;    
                        $.each(parse['retailers'], function(k, v) {
                               no=no+1;
                               var retailer_id = v.retailer_id;
                              user_row+="<tr><td>"+no+"</td><td>"+v.retailer_name+"</td><td>"+v.phone+"</td><td>"+v.dc+"</td><td>"+"<a class='btn-xs btn-primary' onclick='retaileredit("+retailer_id+")'>EDIT</a>"+"</td></tr>";
                               
                            });  
                            $("#retailers").append(user_row);
                            $("#example4").DataTable();

                                        
                                },
                       error:function(data){

                                  // console.log(data);
                       }        
                });
            }
            function retaileredit(id) {
                $(".status-progress").show();
                var url = base_url + "retailer/fetch";
                var formData = {
                    'retailer_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                // alert(formData);
                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];
                        // console.log(obj_data['phone']);
                        $('#retailer_id').val(id);
                        $('#access_token').val(localStorage.getItem('access_token'));
                        $('#retailer_name').val(obj_data['retailer_name']);
                        $('#phone').val(obj_data['phone']);
                        $('#mpesa_number').val(obj_data['mpesa_number']);
                        $('#email').val(obj_data['email']);
                        $('#referred_by').val(obj_data['referred_by']);
                    }

                    $(".status-progress").hide();
                    $('#editretailer').modal('show');
                });

            }

    </script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyB8WgY93rMVkcDEn3Z64CUcSh3Jx_UMeH4"></script>
        <script>
            google.maps.event.addDomListener(window, 'load', initialize);
            function initialize() {
                var input = document.getElementById('location_name');
                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.addListener('place_changed', function () {
                    var place = autocomplete.getPlace();
                    // place variable will have all the information you are looking for.
                    $('#latitude').val(place.geometry['location'].lat());
                    $('#longitude').val(place.geometry['location'].lng());
                    console.log(place.geometry['location'].lat());
                    console.log(place.geometry['location'].lng());
                });
            }
        </script>
             <style type="text/css">
            
  .pac-container {
    z-index: 1051 !important;
}
</style>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">
            <input name="supplier_id_tag" id="supplier_id_tag" type="hidden" />
            <div id="modalSup" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Register Customer</h5>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" class="form-validate" method="POST" id="formAddcustomer" enctype="multipart/form-data">
                                            <input name="access_token" id="access_token" type="hidden" />

                                            
                                            <p id="demo"></p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" required name="first_name" id="first_name" class="form-control" placeholder="First name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" required name="last_name" id="last_name" class="form-control" placeholder="Last name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="email" required name="email" id="email" class="form-control" placeholder="Email">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-mention text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" required name="mobilephone" id="mobilePhone" class="form-control" placeholder="Phone number e.g 701717592">
                                                        <div class="form-control-feedback">
                                                            <i class="glyphicon glyphicon-earphone text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                      

                                            <div class="supplier box">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group has-feedback">
                                                            <input type="text" id="location_name" required name="location_name" class="form-control" placeholder="Location Name">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>

                                                   
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="hidden"  id="latitude" required name="latitude" class="form-control" placeholder="Latitude">
                                                            <div class="form-control-feedback">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="hidden"  id="longitude" required name="longitude" class="form-control" placeholder="Longitude">
                                                            <div class="form-control-feedback">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>


                                            <div class="choose box"></div>
                                            <div class="director box"></div>






                                            <div class="text-right">
                                                <img class="status-progress"  src="../assets/loader/loader.gif"/>
                                                <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10">Register</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>


            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->

                        <!-- /user menu -->
                        <?php include("../sidebarclerk.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">My retailers</span></h4>
                            </div>
                            <div class="heading-elements">
                                <button class="btn btn-primary" data-toggle='modal' data-target='#modalSup'>Register Customer</button>
                            </div>
                        </div>
                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>
                                <li class="active">My retailers</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                          <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <!-- <div class="panel-heading">
                                <h5 class="panel-title">All Users In the system</h5>
                                
                            </div>
 -->
                            <div class="panel-body table-responsive">
                                <!--<a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Orders</a>-->
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            


                          
                      <table class="table datatable-basic" id="example4" style="margin-bottom: 150px;">
                        <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <!-- <th>Email</th> -->
                                        <th>DC</th>
                                        <th>Action</th>
                                        
                                    </tr>
                        </thead>
                        <tbody id="retailers">
                            
                        </tbody>

                    </table>
                    </div>
                        </div>
                    </div>
                </div>
                    

                <!-- Basic datatable -->

                       
                        <style>
                            .modal-backdrop{
                                z-index: 10;        
                            }​
                        </style>
                        <div id="modalSup" style="z-index: 20;   
                             " class="modal fade">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Register Customer</h5>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" class="form-validate" method="POST" id="formAdd" enctype="multipart/form-data">
                                            <!-- <input name="access_token" id="access_token" type="hidden" /> -->
                                            <input name="supplier_id" id="supplier_id" type="hidden" />
                                            <p id="demo"></p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" required name="first_name" id="first_name" class="form-control" placeholder="First name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" required name="last_name" id="last_name" class="form-control" placeholder="Last name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="email" required name="email" id="emailadd" class="form-control" placeholder="Email">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-mention text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" required name="phone" id="mobilePhone" class="form-control" placeholder="Phne in format 701717592">
                                                        <div class="form-control-feedback">
                                                            <i class="glyphicon glyphicon-earphone text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            
                                            <div class="cc box">
                                                <div class="row">
                                                    
                                                      
                                                    


                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text"  name="dob" class="form-control daterange-single" value="01/01/1990">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-calendar text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="supplier box">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text" id="location_name" required name="location_name" class="form-control" placeholder="Location Name">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input name="business_name" id="business_name" required  type="text" class="form-control" placeholder="Business name">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-home text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text"  id="latitude" required name="latitude" class="form-control" placeholder="Latitude">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text"  id="longitude" required name="longitude" class="form-control" placeholder="Longitude">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>


                                            <div class="choose box"></div>
                                            <div class="director box"></div>






                                            <div class="text-right">


                                                <img class="status-progress"  src="../assets/loader/loader.gif"/>
                                                <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10">Edit supplier</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div id="editretailer" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title">Edit retailer information</h5>
                        </div>
                        <div class="modal-body">
                            <form role="form" class="form-validate" method="POST" id="formEditRetailer" enctype="multipart/form-data">
                                <input name="access_token" id="access_token" type="hidden" />
                                <input name="retailer_id" id="retailer_id" type="hidden" />
                                <p id="demo"></p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <label for="retailer_name" class="control-label">Retailer Names</label>
                                            <input type="text" required name="retailer_name" id="retailer_name" class="form-control" placeholder="Retailer Names">
                                            <div class="form-control-feedback">
                                                <i class="icon-user-check text-muted"></i>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <label for="phone" class="control-label">Retailer phone number</label>
                                            <input type="text" required name="phone" id="phone" class="form-control" placeholder="Contact phone number">
                                            <div class="form-control-feedback">
                                                <i class="glyphicon glyphicon-earphone text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <label for="mpesa_number" class="control-label">Mpesa number</label>
                                            <input type="text" required name="mpesa_number" id="mpesa_number" class="form-control" placeholder="Mpesa phone number">
                                            <div class="form-control-feedback">
                                                <i class="glyphicon glyphicon-earphone text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <label for="email" class="control-label">Retailer Email</label>
                                            <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                            <div class="form-control-feedback">
                                                <i class="icon-mention text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group has-feedback">
                                            <label for="referred_by" class="control-label">Reffered by</label>
                                            <input type="text" required name="referred_by" id="referred_by" class="form-control" placeholder="Reffered by">
                                            <div class="form-control-feedback">
                                                <i class="glyphicon glyphicon-user text-muted"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <!-- <img class="status-progress"  src="../assets/loader/loader.gif"/> -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10">Update retailer information</button>
                                `   </div>
                                </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
                        <div id="modalLink" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Link Retailer</h5>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" class="form-validate" method="POST" id="formRetailer" enctype="multipart/form-data">
                                            <input name="access_token" id="access_tokenID" type="hidden" />
                                            <input name="retailer_id" id="retailerId" type="hidden" />

                                            <div class="row">

                                                <div class="form-group has-feedback">

                                                    <select id="selectsubcat" name="supplier_id"  class="select-search">
                                                        <optgroup label="Select Supplier">
                                                            


                                                        </optgroup>
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="text-right">
                                                <img class="status-progress"  src="../assets/loader/loader.gif"/>
                                                <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10">Link Retailer</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- Vertical form modal -->
                        <div id="modalLinkedRetailers" class="modal fade">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Retailers</h5>
                                    </div>

                                    <div class="modal-body">
                                        
                                    </div>


                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>

                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

        <script>

            var x = document.getElementById("demo");

            function showPosition(position) {

                $('#latitude').val(position.coords.latitude);
                $('#longitude').val(position.coords.longitude);
            }

            $('#formEditRetailer').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress").show();
              var formdata = new FormData(this);
              formdata.append('access_token',localStorage.getItem('access_token'));
                $.ajax({
                    url: base_url + "retailer/update_profile",
                    type: "POST",
                    data: formdata,
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress").hide();
                        var data = JSON.parse(data);
                        $(".status-progress").hide();
                        $('#editretailer').modal('hide');
                        
                        window.location.reload();
                        new PNotify({
                            title: 'Update Notice',
                            text: data['message'],
                            addclass: 'bg-success'
                        });

                    },
                    error: function (data) {
                        new PNotify({
                            title: 'Update Notice',
                            text: data['message'],
                            addclass: 'bg-warning'
                        });
                        $('#editretailer').modal('hide');
                        loadRetailers();
                        $(".status-progress").hide();
                    }
                });

            });

            $('#formAdd').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress").show();

                $.ajax({
                    url: base_url + "supplier/update_profile",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress").hide();

                        var data = JSON.parse(data);
                        $(".status-progress").hide();
                        $('#modalSup').modal('hide');
                        loadRetailers();
                        new PNotify({
                            title: 'Registration Notice',
                            text: data['message'],
                            addclass: 'bg-success'
                        });
                    },
                    error: function (data) {
                        new PNotify({
                            title: 'Registration Notice',
                            text: data['message'],
                            addclass: 'bg-warning'
                        });
                        $('#modalSup').modal('hide');
                        $(".status-progress").hide();
                        loadRetailers();
                    }
                });


            });
        </script>


    </body>
</html>
