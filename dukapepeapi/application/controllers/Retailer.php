<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Retailers
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Retailer extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->library('encryption');
        $this->load->library('image');
    }
    public function register(){
        
        $retailer_name =  $this->input->post('retailer_name');
        $phone =  $this->input->post('phone');
        $mpesa_number =  $this->input->post('mpesa_number');
        $email =  $this->input->post('email');
        $pass =  $this->input->post('password');
        $location_name =  $this->input->post('location_name');
        $latitude =  $this->input->post('latitude');
        $longitude =  $this->input->post('longitude');
        
        if($this->input->post('reference_no')!=null)
        {
        $cc_id =  $this->input->post('reference_no');
        }
        else{
         $cc_id = "";   
        }
        $user_type=1;
        $password = $this->encryption->encrypt($pass);
        //create an array
        $retailer_data=array();
        $retailer_data['retailer_name'] = $retailer_name;
        $retailer_data['phone'] = $phone;
        $retailer_data['mpesa_number'] = $mpesa_number;
        $retailer_data['email'] = $email;
        $retailer_data['password'] = $password;
        $retailer_data['location_name'] = $location_name;
        $retailer_data['latitude'] = $latitude;
        $retailer_data['longitude'] = $longitude;
        $retailer_data['user_type'] = $user_type;
        $retailer_data['referred_by'] = $cc_id;
//     call method to save data
        $response=$this->Retailer_Model->register($retailer_data);
        
        echo $response;
    }
    public function clerkregistercustomer(){
        
        $retailer_name =  $this->input->post('retailer_name');
        $phone =  $this->input->post('phone');
        $mpesa_number =  $this->input->post('phone');
        $email =  $this->input->post('email');
        $pass =  "1234";
        $location_name =  $this->input->post('location_name');
        $latitude =  $this->input->post('latitude');
        $longitude =  $this->input->post('longitude');
        $supplier =  $this->input->post('supplier');
        
        if($this->input->post('reference_no')!=null)
        {
        $cc_id =  $this->input->post('reference_no');
        }
        else{
         $cc_id = "";  
        }
        $user_type=1;
        $password = $this->encryption->encrypt($pass);
        //create an array
        $retailer_data=array();
        $retailer_data['retailer_name'] = $retailer_name;
        $retailer_data['phone'] = $phone;
        $retailer_data['mpesa_number'] = $mpesa_number;
        $retailer_data['email'] = $email;
        $retailer_data['password'] = $password;
        $retailer_data['location_name'] = $location_name;
        $retailer_data['latitude'] = $latitude;
        $retailer_data['longitude'] = $longitude;
        $retailer_data['user_type'] = $user_type;
        $retailer_data['referred_by'] = $cc_id;
        
//     call method to save data
        $response=$this->Retailer_Model->clerkregistercustomer($retailer_data,$supplier);
        
        echo $response;
    }
    
    public function activate(){
        
        $user_data=array();
        $phone= $this->input->post('phone');
        $activation_code= $this->input->post('activation_code');
        $user_data['phone']=$phone;
        $user_data['activation_code']=$activation_code;
        $response=$this->Retailer_Model->activate_new_account($user_data);
        
        echo $response;    
    }
    
    public function resend_activation(){
        
        $data=array();
        $phone= $this->input->post('phone');
        $data['phone']=$phone;
        $response= $this->Retailer_Model->resend_activation($data);
        
        echo $response;
    }

    
    public function new_password(){
        
        $data=array();
        $data['phone']= $this->input->post('phone');
        $pass=$this->input->post('password');
        $password = $this->encryption->encrypt($pass);
        $data['password']=$password;
        
        $response= $this->Retailer_Model->new_password($data);
        
        echo $response;
    }
    
    // public function update_profile(){
    //     $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
    //     if($access_response['code']==1){  
    //     $retailer_id= $this->input->post('retailer_id');
    //     $email= $this->input->post('email');
    //     if($this->input->post('image_url')!=null && $this->input->post('image_url')!=""){
    //     $image_url= $this->image->mobile_image();
    //     $data['image_url']=$image_url;
    //     }
    //     if($this->input->post('mpesa_number')!=null && $this->input->post('mpesa_number')!=""){
    //     $mpesa_number= $this->input->post('mpesa_number');
    //     $data['mpesa_number'] = $mpesa_number;
    //     }
    //     $data['retailer_id']=$retailer_id;
    //     $data['email'] = $email;
    //     $response= $this->Retailer_Model->update_profile($data);
    //     }
    //     else{
    //         $response= json_encode($access_response);   
    //     }
    //     echo $response;
        
    // }
    public function update_profile(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data['retailer_id'] = $this->input->post('retailer_id');
         $data['retailer_name'] = $this->input->post('retailer_name');   
         $data['phone'] = $this->input->post('phone');  
         $data['mpesa_number'] = $this->input->post('mpesa_number');
         $data['email'] = $this->input->post('email');
         $data['referred_by'] = $this->input->post('referred_by');         
        $response= $this->Retailer_Model->update_profile($data);
         
        }
        else{
          $response= json_encode($access_response);   
        }
        echo $response;   
   }
    
    public function fetch_all(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        if($this->input->post('is_approved')!=null){
            $data['is_approved'] = $this->input->post('is_approved');   
        }  
        $response= $this->Retailer_Model->fetch_all($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response; 
        
        
    }
    public function fetch_all_by_dc()
    {
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1)
        {
            
            $data['clerk'] = $this->input->post('retailer');   
            $response= $this->Retailer_Model->fetch_all_by_dc($data);
        }
        else
        {
            $response= json_encode($access_response);   
        }
        echo json_encode($response); 
           
    }
    public function approve(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['retailer_id'] = $this->input->post('retailer_id');
        $data['is_approved'] = $this->input->post('approve');
        $response= $this->Retailer_Model->approve($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response; 
        
         
    }
    public function fetch(){
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $retailer_id= $this->input->post('retailer_id');
        $data['retailer_id'] = $retailer_id;
        $response= $this->Retailer_Model->get_details($data);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;

    }
    
    public function link_retailer(){
            $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['retailer_id'] = $this->input->post('retailer_id');
        $data['supplier_id'] = $this->input->post('supplier_id');
        $response= $this->Retailer_Model->link_retailer($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response; 
        
         
    }
    
    public function unlink_retailer(){
            $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['id'] = $this->input->post('id');
        $response= $this->Retailer_Model->unlink_retailer($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response; 
    }
    
    public function linked_suppliers(){
            $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $retailer_id= $this->input->post('retailer_id');
        $response= json_encode($this->Retailer_Model->linked_suppliers($retailer_id));
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response; 
    }
}
