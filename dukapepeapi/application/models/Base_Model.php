<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Base_Model
 *
 * @author mwamb
 */
class Base_Model extends CI_Model{
    //put your code here
    
    public $image_table="image_url";
    
    public function getImageUrl(){
        echo base_url()."images/";
        $this->db->select('base_image_url');
        $this->db->from($this->image_table);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        
        return $data[0]['base_image_url'];
    }
}
