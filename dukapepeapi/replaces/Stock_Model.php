<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Stock_Model
 *
 * @author mwamb
 */
class Stock_Model extends CI_Model{
    //put your code here
    public $stock='stock';
    public $stock_history='stock_history';
    public $price_history='price_history';
    //0 stock changes
     public function return_all_on_closure()
    {
        $received_json= $this->input->post('order');
        $data= json_decode($received_json,true);
        $response_data= $this->Stock_Model->return_all_on_closure($data);

    }
    public function get_qty_stock_by_id($data){
        $this->db->select('quantity');
        $this->db->where('stock_id',$data['stock_id']);
        $this->db->from($this->stock);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        $currentqty=$response[0]['quantity'];
        if($currentqty>0)
        {
          $this->db->query('UPDATE stock SET quantity = (quantity-1) WHERE stock_id ='.$data["stock_id"]);
          return "proceed";
        }
        else if($currentqty==0)
        {
          return "zero";
        }
        else
        {
          return "dontproceed";
        }
        
    }
    public function return_qty_to_stock($data)
    {

        $this->db->query('UPDATE stock SET quantity = (quantity+1) WHERE stock_id ='.$data["stock_id"]);  
        $this->db->select('quantity');
        $this->db->where('stock_id',$data['stock_id']);
        $this->db->from($this->stock);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return $response[0]['quantity'];  
     
    }
     public function return_all_to_store($data)
    {

        $this->db->query('UPDATE stock SET quantity = (quantity+'.$data["stock_qty"].') WHERE stock_id ='.$data["stock_id"]);  
        $this->db->select('quantity');
        $this->db->where('stock_id',$data['stock_id']);
        $this->db->from($this->stock);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return $response[0]['quantity'];  
     
    }
    ///0 stock changes
    
    public function new_product($data){
        $response=array();
        $count= $this->checkNew($data);
        if($count==0){
            $this->db->insert($this->stock,$data);
            // add to item history
            $stock_id= $this->getStockId($data);
            $history_data=array();
            $history_data['stock_id']=$stock_id;
            $history_data['quantity']=$data['quantity'];
            $this->add_stock($history_data);
            //price history
            $price_data=array();
            $price_data['stock_id']=$stock_id;
            $price_data['unit_cost']=$data['unit_cost'];
            $price_data['vat']=$data['vat'];
            $this->add_price($price_data);
            
            $response['message']="Product added to stock successfully.";
            $response['code']=1;
        }
        else{
         $response['message']="Similar product exist under same supplier and skus, Check ant try again.";
         $response['code']=0;   
        }
        return json_encode($response);
    }
    
    public function getStockId($data){
        $this->db->select('stock_id');
        $this->db->where('product_id',$data['product_id']);
        $this->db->where('sku_id',$data['sku_id']);
        $this->db->where('supplier_id',$data['supplier_id']);
        $this->db->from($this->stock);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return $response[0]['stock_id'];
    }

    public function checkNew($data){
        $this->db->select('stock_id');
        $this->db->where('product_id',$data['product_id']);
        $this->db->where('sku_id',$data['sku_id']);
        $this->db->where('supplier_id',$data['supplier_id']);
        $this->db->from($this->stock);
        $count=$this->db->count_all_results();
     return $count;
    }
    
    public function add_stock($data){
    $this->db->insert($this->stock_history,$data);
    }
    public function add_price($data){
    $this->db->insert($this->price_history,$data);
    }
    public function currentStock($data){
        $this->db->select('quantity');
        $this->db->where('stock_id',$data['stock_id']);
        $this->db->from($this->stock);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return $response[0]['quantity'];
    }
    
    public function new_stock($data){
       $current_stock= $this->currentStock($data);
       $total_stock=$current_stock+$data['quantity'];
       
       //add to history
       $this->add_stock($data);
       //update stock
       $stock=array();
       $stock['quantity']=$total_stock;
       $this->db->where('stock_id',$data['stock_id']);
       $this->db->update($this->stock,$stock);
       $response=array();
       $response['message']="Stock updated successfully.";
       $response['code']=1;
       
       return json_encode($response);
    }
    
    public function update_price($data){
        $this->db->insert($this->price_history,$data);
        // update price 
        $stock_id=$data['stock_id'];
        unset($data['stock_id']);
       $this->db->where('stock_id',$stock_id);
       $this->db->update($this->stock,$data);
       $rows= $this->db->affected_rows();
       $response=array();
       if($rows>0){
       $response['message']="Price updated successfully.";
       $response['code']=1;
       }
       else{
       $response['message']="Error while updating price, unknown stock item.";
       $response['code']=0;    
       }
       
       return json_encode($response);
    }
    public function update_bp($data){
        $this->db->insert($this->price_history,$data);
        // update price 
        $stock_id=$data['stock_id'];
        unset($data['stock_id']);
       $this->db->where('stock_id',$stock_id);
       $this->db->update($this->stock,$data);
       $rows= $this->db->affected_rows();
       $response=array();
       if($rows>0){
       $response['message']="Buying Price updated successfully.";
       $response['code']=1;
       }
       else{
       $response['message']="Error while updating price, unknown stock item.";
       $response['code']=0;    
       }
       
       return json_encode($response);
    }
    public function supplier_stock($passed_data){
        $this->db->select('stock_id,stock_id AS bp,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,unit_cost,vat,quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
        $this->db->where($this->stock.'.supplier_id',$passed_data['supplier_id']);
        $this->db->from($this->stock);
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        
        //get reviews
        $reviews = $this->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        
        $i++;
        }
        return json_encode($data);
    }
    public function get_stock($passed_data){
        $this->db->select('stock_id,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,unit_cost,stock.vat AS vat,quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,sub_categories.sub_cat_id AS sub_category_id,sub_categories.sub_cat_name AS sub_category_name,'
                . 'supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
        $this->db->where('products.sub_cat_id',$passed_data['sub_category_id']);
        if(isset($passed_data['retailer_id'])){
           $this->db->where('retailer_suppliers.retailer_id',$passed_data['retailer_id']);   
        }
        $this->db->from($this->stock);
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('sub_categories','sub_categories.sub_cat_id=products.sub_cat_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        if(isset($passed_data['retailer_id'])){
        $this->db->join('retailer_suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id');
        }
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        
        //get reviews
        $reviews = $this->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        
        $i++;
        }
        return json_encode($data);
    }
    public function fetch($passed_data){
        $this->db->select('stock_id,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,stock.unit_bp,unit_cost,vat,quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
        $this->db->where('stock_id',$passed_data['stock_id']);
        if(isset($passed_data['retailer_id'])){
           $this->db->where('retailer_suppliers.retailer_id',$passed_data['retailer_id']);   
       }
        $this->db->from($this->stock);
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        
        if(isset($passed_data['retailer_id'])){
        $this->db->join('retailer_suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id');
        }
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
            // product images
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        //get reviews
        $reviews = $this->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        
        $i++;
        }
        return json_encode($data);
    }
    public function fetch_all($input){
       $this->db->select('stock_id,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,unit_cost,vat,quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
       if(isset($input['retailer_id'])){
           $this->db->where('retailer_suppliers.retailer_id',$input['retailer_id']);   
       }
        $this->db->from($this->stock);
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        if(isset($input['retailer_id'])){
        $this->db->join('retailer_suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id');
        }
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
            // images
        $images = $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
            //get reviews
        $reviews = $this->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        $i++;
        }
        return json_encode($data); 
    }
    public function fetch_stock_history($data){
        $this->db->select('quantity,timestamp');
        $this->db->where('stock_id',$data['stock_id']);
        $this->db->from($this->stock_history);
        $this->db->order_by('timestamp DESC');
        $returned_data=$this->db->get();
        $response=$returned_data->result_array();
        return json_encode($response);
    }
    public function delete_stock_history($data){
      $response=array();
      $array_data= $this->stock_history_quantity($data);
      $quantity=$array_data[0]['quantity'];
      $data['stock_id']=$array_data[0]['stock_id'];
      $stock_quantity= $this->currentStock($data);
      if($quantity>$stock_quantity){
          $response['message']='Error, Products already issued.';
          $response['code']=0;
      }
      else{
          //update stock
          $q_array=array();
          $q_array['quantity']=$stock_quantity-$quantity;
          $this->db->where('stock_id',$data['stock_id']);
          $this->db->update($this->stock,$q_array);
          //stock history data
          $this->db->where('id',$data['id']);
          $this->db->delete($this->stock_history);
          $response['message']='Product quantities deleted and stock updated accordingly.';
          $response['code']=1;
      }
      return json_encode($response);
    }
    
    public function stock_history_quantity($data){
        $this->db->select('stock_id,quantity');
        $this->db->where('id',$data['id']);
        $this->db-from($this->stock_history);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return $response;
    }
    
    public function direct_update($data){
      $stock_id=$data['stock_id'];
      unset($data['stock_id']);
      
      $this->db->where('stock_id',$stock_id);
      $this->db->update($this->stock,$data);
      $rows = $this->db->affected_rows();
      $response=array();
      if($rows>0){
      $response['message']="Stock updated successfully.";
      $response['code']=1;
      }
      else{
      $response['message']="Failed to update stock quantity.";
      $response['code']=0;    
      }
      return json_encode($response);
    }
    
    
       public function recommended($check_array){
           // get common retailer shopped items    
       $this->db->select('stock.stock_id AS stock_id,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,stock.unit_cost AS unit_cost,ordered_products.vat AS vat,SUM(ordered_products.quantity) AS quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
        $this->db->where('orders.retailer_id',$check_array['retailer_id']);
        
        if(isset($check_array['retailer_id'])){
           $this->db->where('retailer_suppliers.retailer_id',$check_array['retailer_id']);   
        }
        
        $this->db->from($this->stock);
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('ordered_products','ordered_products.stock_id=stock.stock_id');
        $this->db->join('orders','ordered_products.order_id=orders.order_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
       
        if(isset($check_array['retailer_id'])){
        $this->db->join('retailer_suppliers','suppliers.supplier_id=retailer_suppliers.supplier_id');
        } 
        
        $this->db->group_by('stock_id');
        $this->db->order_by('quantity','DESC');
        $this->db->limit(10);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
            // product_images
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        //get reviews
        $reviews = $this->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        
        $i++;
        }
        return json_encode($data);  
       }
       
    public function review($data){
        $review_data=$this->check_review($data);
        if(count($review_data)==0){ // insert as new entry
            $this->db->insert('reviews',$data); 
            $response['message'] = "Reviewed successfully.";
            $response['code'] = 1;
        }
        else{ // update entry
            $this->db->where('review_id',$review_data[0]['review_id']);
            $this->db->update('reviews',$data);
            $response['message'] = "Review updated successfully.";
            $response['code'] = 1;
        }
        return json_encode($response);
    }
    
    public function check_review($data){
        $this->db->select('review_id');
        $this->db->where('retailer_id',$data['retailer_id']);
        $this->db->where('stock_id',$data['stock_id']);
        $this->db->from('reviews');
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        return $response;
    }
    
    
    public function get_reviews($stock_id){
        $this->db->select();
        $this->db->where('stock_id',$stock_id);
        $this->db->from('reviews');
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        
        return $response;
    }
    
    public function delete($stock_id){
     if($this->check_associated($stock_id)==0 && $this->check_associated2($stock_id)==0 && $this->check_associated3($stock_id)==0 ){
         $this->db->where('stock_id',$stock_id);
         $this->db->delete('stock');
         $num= $this->db->affected_rows();
     }
     else{
      $num=0;   
     }
        $response= $this->Status->response($num);
        
        return json_encode($response);
     }
     
     public function check_associated($stock_id){
     $this->db->select('order_id');
     $this->db->where('stock_id',$stock_id);
     $this->db->from('ordered_products');
     $num= $this->db->count_all_results();
     return $num;
     }
     public function check_associated2($stock_id){
     $this->db->select('promotion_id');
     $this->db->where('stock_id',$stock_id);
     $this->db->from('promotions');
     $num= $this->db->count_all_results();
     return $num;
     }
     
     public function check_associated3($stock_id){
     $this->db->select('review');
     $this->db->where('stock_id',$stock_id);
     $this->db->from('reviews');
     $num= $this->db->count_all_results();
     return $num;
     }
     
     public function update_vat($data){
       $stock_id = $data['stock_id'];
       
       unset($data['stock_id']);
       
       $this->db->where('stock_id',$stock_id);
       
       $this->db->update($this->stock,$data);
       $num = $this->db->affected_rows();
       
       if($num>0){
           $response['message'] = $num." Stock items updated.";
           $response['code'] = 1;
       }
       else{
          $response['message'] = "No record updated.";
          $response['code'] = 0; 
       }
       
       return json_encode($response);
     }
     
     
     public function dc_to_dc_transfer($data){
         $product_info = $this->dc_from($data);
         $rem = ($product_info['quantity'])-$data['no_transfer'];
         if($rem >= 0){
           // success operation
          $res1 = $this->update_from($data, $product_info);
           
          $res2 = $this->update_to($data, $product_info);
          
          if($res1['code']==1 && $res2['code']==1){
           $response['message'] =  "Succcess, transfer made successfully.";
            $response['code']   = 1;    
           }
           else{
            $response['message'] =  "Failed, error while transfering. Close all applications and restart.";
            $response['code']   = 0;   
           }
         }
         else{
             //fail operation
            $response['message'] =  "Failed, the stock is less than the number to be transferred.";
            $response['code']   = 0;
         }
         
         
         return json_encode($response);
     }
     
     
     public function dc_from($data){
         $res=0;
         $this->db->select('quantity,product_id,sku_id');
         $this->db->where('stock_id',$data['stock_id']);
//         $this->db->where('supplier_id',$data['from']);
         $this->db->from('stock');
         $returned_data= $this->db->get();
         $response=$returned_data->result_array();
         if(count($response)>0){
         $res=$response[0];
         }
         
         return $res;
     }
     
     
     public function update_from($passed_data,$product_info){
        $stock_id  = $passed_data['stock_id'];
        $rem=$product_info['quantity']-$passed_data['no_transfer'];
        
        $from['quantity'] = $rem;
        
        $this->db->where('stock_id',$stock_id);
        $this->db->update('stock',$from);
        $num = $this->db->affected_rows(); 
        if($num>0){
           $response['message'] = " Stock items updated.";
           $response['code'] = 1;
       }
       else{
            $response['message'] = "No changes detected";
          $response['code'] = 0; 
       
       }
       
       return $response;
     }
     
     
     public function update_to($passed_data, $product_info){
         $product_id  = $product_info['product_id'];
         $sku_id  = $product_info['sku_id'];
         $supplier_id  = $passed_data['to'];
         $rem=$passed_data['no_transfer'];
          $this->db->query('UPDATE stock SET quantity=quantity+'.$rem.' WHERE '
        . 'product_id='.$product_id.' && sku_id='.$sku_id.' && supplier_id='.$supplier_id );
        $num = $this->db->affected_rows(); 
        
        if($num>0){
           $response['message'] = " Stock items updated.";
           $response['code'] = 1;
       }
       else{
           // new insertion
           $new['product_id']= $product_id;
           $new['supplier_id']= $supplier_id;
           $new['sku_id']= $sku_id;
           $new['unit_cost']= 0;
           $new['vat']= 0;
           $new['quantity']= $rem;
           $new['is_active']= 1;
           
           $this->db->insert('stock',$new);
           
          $response['message'] = "Product inserted successfully.";
          $response['code'] = 0; 
       }
       
       return $response;
     }
}
