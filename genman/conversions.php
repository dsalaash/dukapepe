<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Duka pepe | Logistic Conversions</title>

        <?php include("../links.php") ?>

        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Filter:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });
                if (localStorage.getItem('code') == 1) {

                } else {

                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });
                }


            });

        </script>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->

                        <!-- /user menu -->

                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Logistic Conversions</span></h4>
                            </div>

                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                                <li class="active">Logistic Conversions</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <!-- <div class="panel-heading">
                                <h5 class="panel-title">Logistic Conversions</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div> -->

                            <div class="panel-body">
                                <!--<a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Stock</a>-->
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                                <form role="form" class="form-validate" method="POST" id="formstockadd" enctype="multipart/form-data">
                                    <input type="hidden" name="stock_id" id="stockId"/>
                                    <input type="hidden" name="access_token" id="stockToken"/>
                                    <div class="modal-body">
                                        <div class="form-group">

                                            <label>Item measurement</label>
                                            <input name="quantity" id="quantity" class="form-control" placeholder="Item measurement e.g ml" />

                                        </div>
                                        <div class="form-group">

                                            <label>Weight in kilograms</label>
                                            <input name="quantity" id="quantity" class="form-control" placeholder="Weight in kilograms" />

                                        </div>


                                    </div>
                                    <div class="modal-footer">

                                        <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                    </div>
                                </form>
                            </div>


                        </div>
                        <!-- /basic datatable -->

                        <!-- Vertical form modal -->
                        <div id="modalAddstock" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Add stock form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formstockadd" enctype="multipart/form-data">
                                        <input type="hidden" name="stock_id" id="stockId"/>
                                        <input type="hidden" name="access_token" id="stockToken"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Quantity</label>
                                                <input name="quantity" id="quantity" class="form-control" placeholder="Quantity" />

                                            </div>


                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->



                        <!-- Vertical form modal -->
                        <div id="modalUnitstock" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Update Unit cost</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formunitprice" enctype="multipart/form-data">
                                        <input type="hidden" name="stock_id" id="stockId"/>
                                        <input type="hidden" name="access_token" id="stockToken"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Unit Cost</label>
                                                <input name="unit_cost" id="unit_cost" class="form-control" placeholder="Unit cost" />

                                            </div>


                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->




                        <!-- Vertical form modal -->
                        <div id="modalStockNew" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">New Stock Product form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formstockNew" enctype="multipart/form-data">
                                        <input type="hidden" name="supplier_id" id="supplier_id"/>
                                        <input type="hidden" name="access_token" id="supToken"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Sku:</label>
                                                <select id="selectsku" name="sku_id" class="select-search">
                                                    <option value="0">Select Sku</option>
                                                </select>

                                            </div>
                                            <div class="form-group">

                                                <label>Product:</label>
                                                <select id="selectpro" name="product_id" class="select-search">
                                                    <option value="0">Select Product</option>
                                                </select>

                                            </div>
                                            <div class="form-group">

                                                <label>Unit Cost:</label>
                                                <input name="unit_cost" id="unit_cost" class="form-control" placeholder="Unit cost" />

                                            </div>
                                            <div class="form-group">

                                                <label>Quantity:</label>
                                                <input name="quantity" id="quantity" class="form-control" placeholder="Quantity" />

                                            </div>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                            <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->

                        <!-- Vertical form modal -->
                        <div id="modalorderDetails" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Order details</h5>
                                    </div>

                                    <div class="modal-body">
                                        <table class="table">

                                            <thead>
                                                <tr>
                                                    <th>Product name</th>
                                                    <th>Product description</th>
                                                    <th>Order Quantity</th>
                                                    <th>Unit cost</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tBody"></tbody>
                                        </table>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
