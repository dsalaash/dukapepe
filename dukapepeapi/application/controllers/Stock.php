<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Stock
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Stock extends CI_Controller{
    //
  public function new_purchase()
  {
    $data['stock_id']=$this->input->post('stock');
    $data['quantity']=$this->input->post('quantity'); 
    $data['bp']=$this->input->post('bp'); 
    $data['sp']=$this->input->post('sp'); 
    $data['retaier']=$this->input->post('clerk'); 
    $response_data= $this->Stock_Model->new_purchase($data);
    echo  $data['retaier'];
    // echo $response_data;
   
  }
public function check_new_purchase_total()
  {
    $data['stock_id']=$this->input->post('stock');
    $data['quantity']=$this->input->post('quantity'); 
    $data['bp']=$this->input->post('bp'); 
    $data['sp']=$this->input->post('sp');
    $total=0;
    $i=0;
     while($i<sizeof($data['stock_id']))
        {
          $total=$total+($data['quantity'][$i]*$data['bp'][$i]);
          $i++;
        }
    $response['code']=1;
    $response['total']=$total;
    $response['size']=$_POST;
     echo  json_encode($response);
 
  }
  public function updatestockDetails()
  {
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token_details'));    
     if($access_response['code']==1)
     {
       $data['product_id']=$this->input->post('details_product_id');
       $data['sku_id']=$this->input->post('details_sku_id');
       // $stock=$this->input->post('detailsstock');
       $stock=$this->input->post('detailsstock');
       $response= $this->Stock_Model->updatestockDetails($data,$stock);
     }
     else
        {
            $response= json_encode($access_response);   
        }
       // echo json_encode($data);
        echo $response;
  }
  public function updatepurchaseload()
  {
     $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
     if($access_response['code']==1)
     {
       $data['purchase']=$this->input->post('purchase');
       $response= $this->Stock_Model->updatepurchaseload($data);
     }
     else
        {
            $response= json_encode($access_response);   
        }
        echo $response;
  }
   public function closingstock()
  {
      
       $response= $this->Stock_Model->closingstock();
    
  }
    public function updatepurchase()
    {
          $access_response=$this->Management_Model->validate_request($this->input->post('access_token_pup'));    
         if($access_response['code']==1)
         {
              
               $data['unit_bp']= $this->input->post('up_bp');
               $data['unit_cost']= $this->input->post('up_sp');
               $data['quantity']= $this->input->post('up_qty');
               $response= $this->Stock_Model->updatepurchase($data,$this->input->post('purchase'),$this->input->post('up_oldqty'),$this->input->post('stock_idpup'));
         }
         else
            {
                $response= json_encode($access_response);   
            }
        echo $response;
    }
    public function get_all_activetransactions()
    {
      $data['retailer']=$this->input->post('retailer');  
      $response_data= $this->Stock_Model->get_all_activetransactions($data);
      echo json_encode($response_data);      
    }
    public function seetransactionsitems()
    {
      $data['retailer']=$this->input->post('retailer');  
      $response_data= $this->Stock_Model->seetransactionsitems($data);
      echo json_encode($response_data);    
    }
    public function DivideLargeSku()
    {
        $data=array();
        $data['product_id']= $this->input->post('divide_product_id');
        $data['sku_id']= $this->input->post('divide_sku_id');
        $data['supplier_id']= $this->input->post('divide_supplier_id');
        $data['divideQty']= $this->input->post('divideQty');
        $data['selling_price'] = $this->input->post('selling_price');
        $response= $this->Stock_Model->divide_large_sku($data);
       
        echo $response;  
    }
    public function getSkuFactor()
    {
      $data['larger_sku'] =  $this->input->post('large_sku');
      $response = $this->Stock_Model->getSkuFactor($data);
      echo $response;
    }
    public function CombineSmallSku()
    {
        $data=array();
        $data['product_id']= $this->input->post('combine_product_id');
        $data['sku_id']= $this->input->post('combine_sku_id');
        $data['supplier_id']= $this->input->post('combine_supplier_id');
        $data['quantity_to_combine']= $this->input->post('quantity_to_combine');
        $response= $this->Stock_Model->combine_small_sku($data);
       
        echo $response;
    }
    //
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    //0 stock changes
    //0 stock changes
    public function return_all_on_closure()
    {
        $received_json= $this->input->post('order');
        $data= json_decode($received_json,true);
        $response_data= $this->Stock_Model->return_all_on_closure($data);

    }
    public function get_qty_stock_by_id()
    {
     $stock_item_id=$this->input->post('stock_id');
     $data['stock_id']=$stock_item_id;
     $data['retailer_id']=$this->input->post('retailer_id');
     $response_data= $this->Stock_Model->get_qty_stock_by_id($data);
      
     echo $response_data;     
    }
    public function clearcache()
    {
      $response_data= $this->Stock_Model->clearcache();   
      echo $response_data;
    }
    public function return_qty_to_stock()
    {
     $stock_item_id=$this->input->post('stock_id');
     $data['stock_id']=$stock_item_id;
     $data['retailer_id']=$this->input->post('retailer_id');
     $response_data= $this->Stock_Model->return_qty_to_stock($data);
     if($response_data>0)
     {
          echo "reduced";
     }   
     else{
        echo "failed";
     }  
    }
     public function return_all_to_store()
    {
     $stock_item_id=$this->input->post('stock_id');
     $stock_item_qty=$this->input->post('stock_qty');
      $data['retailer_id']=$this->input->post('retailer_id');
     $data['stock_id']=$stock_item_id;
     $data['stock_qty']=$stock_item_qty;
     $response_data= $this->Stock_Model->return_all_to_store($data);
     if($response_data>0)
     {
       echo "reduced";
     }   
     else{
       echo "failed";
     }     
    }
     //0 stock changes
    //0 stock changes
     public function new_product(){
     $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
     if($access_response['code']==1){
        $data=array();
     $data['product_id']= $this->input->post('product_id');
     $data['supplier_id']= $this->input->post('supplier_id');
     $data['sku_id']= $this->input->post('sku_id');
     $data['unit_cost']= $this->input->post('unit_cost');
     $data['unit_bp']= $this->input->post('unit_bp');
     
     $data['quantity']= $this->input->post('quantity');
     $data['vat']= $this->input->post('vat');
     
     $response_data= $this->Stock_Model->new_product($data);
     }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function new_stock(){
     $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
     $stock_items_id=$this->input->post('stock_id');
     $quantity= $this->input->post('quantity');
     
     $data['stock_id']=$stock_items_id;
     $data['quantity']=$quantity;
     
     $response_data= $this->Stock_Model->new_stock($data);
     }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;   
    }
    
    public function update_price(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['stock_id']= $this->input->post('stock_id');
        $data['unit_cost']= $this->input->post('unit_cost');
        $response= $this->Stock_Model->update_price($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
     public function update_bp(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token_bp'));    
        if($access_response['code']==1){
        $data=array();
        $data['stock_id']= $this->input->post('stock_id_bp');
        $data['unit_bp']= $this->input->post('unit_bp');
        $response= $this->Stock_Model->update_bp($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;  
    }
    public function supplier_stock(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['supplier_id']= $this->input->post('supplier_id');
        $response= $this->Stock_Model->supplier_stock($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function supplier_stockdc(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['retailer']= $this->input->post('retailer');
        
        $response= $this->Stock_Model->supplier_stockdc($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function purchasesdc(){
        // $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        // if($access_response['code']==1){
        $data=array();
        $data['retailer']= $this->input->post('retailer');
        $response= $this->Stock_Model->supplier_purchasesdc($data);
        // }
        // else{
        //     $response= json_encode($access_response);   
        // }
        echo $response;
    }
    public function get_stock(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['sub_category_id']= $this->input->post('sub_category_id');
        if($this->input->post('retailer_id')!=null){
            $data['retailer_id'] = $this->input->post('retailer_id');   
        } 
        $response= $this->Stock_Model->get_stock($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
    public function fetch(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['stock_id']= $this->input->post('stock_id');
        
        if($this->input->post('retailer_id')!=null){
            $data['retailer_id'] = $this->input->post('retailer_id');   
        }
        $response= $this->Stock_Model->fetch($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function fetchitem()
    {
      // $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      //   if($access_response['code']==1)
      //   {
              $data=array();
              $data['stock_id']=$this->input->post('stock_id');
              $response= $this->Stock_Model->fetchitem($data);
        // }
        // else
        // {
        //         $response= json_encode($access_response);   
        // }
        echo $response; 
    }
    public function fetch_all(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        if($this->input->post('retailer_id')!=null){
            $data['retailer_id'] = $this->input->post('retailer_id');   
        }
        $response= $this->Stock_Model->fetch_all($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function fetch_stock_history(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['stock_id']= $this->input->post('stock_id');
        $response= $this->Stock_Model->fetch_stock_history($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function delete_stock_history(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['id']= $this->input->post('id');
        $response= $this->Stock_Model->delete_stock_history($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function direct_update(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['stock_id']= $this->input->post('stock_id');
        $data['quantity']= $this->input->post('quantity');
        $response= $this->Stock_Model->direct_update($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function recommended(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $retailer_id= $this->input->post('retailer_id');
        $data['retailer_id']=$retailer_id;
        
        $response= $this->Stock_Model->recommended($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
    public function review(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $stock_id= $this->input->post('stock_id');
        $retailer_id= $this->input->post('retailer_id');
        $rate= $this->input->post('rate');
        $title= $this->input->post('title');
        $message= $this->input->post('message');
        
        $data['stock_id'] = $stock_id;
        $data['retailer_id'] = $retailer_id;
        $data['rate'] = $rate;
        $data['title'] = $title;
        $data['message'] = $message;
        
        $response= $this->Stock_Model->review($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
        public function delete(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $stock_id= $this->input->post('stock_id');
        $response= $this->Stock_Model->delete($stock_id);
        }
        
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
    public function update_vat(){
        $data['stock_id']= $this->input->post('stock_id');
        $data['vat']= $this->input->post('vat');
        
        $response = $this->Stock_Model->update_vat($data);
        
        echo $response;
    }
    
    public function dc_to_dc_transfer(){
         $data['to'] = $this->input->post('recipient_supplier');
         $data['stock_id'] = $this->input->post('stock_id');
         $data['no_transfer'] = $this->input->post('quantity');
         
         $response = $this->Stock_Model->dc_to_dc_transfer($data);
         
         echo $response;
         
     }
}
