<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Delivery_points
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Delivery_point extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function add(){
      $data=array();
      $retailer_id = $this->input->post('retailer_id');
      $location_name = $this->input->post('location_name');
      $latitude = $this->input->post('latitude');
      $longitude = $this->input->post('longitude');
      $data['retailer_id']=$retailer_id;
      $data['location_name']=$location_name;
      $data['latitude'] = $latitude;
      $data['longitude'] = $longitude;
      $response= $this->Delivery_Point_Model->add($data);
      echo $response;
    }
    
    public function fetch_all(){
        $data=array();
        $retailer_id= $this->input->post('retailer_id');
        $data['retailer_id'] = $retailer_id;
        
        $response= $this->Delivery_Point_Model->fetch_all($data);
        echo $response;
    }
    
    public function remove(){
        $data['id'] = $this->input->post('id');
        $response = $this->Delivery_Point_Model->remove($data);
        
        echo $response;
    }
}
