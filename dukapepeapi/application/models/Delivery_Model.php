<?php



/*

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */



/**

 * Description of Delivery_Model

 *

 * @author mwamb

 */

class Delivery_Model extends CI_Model{

    //put your code here

    

    public $deliveries="deliveries";

    public $notify_url="";

    

    public function get_deliveries($data){

//        echo "Assigned status: ".$data['not_assigned'];

        $this->db->select('deliveries.order_id,phone,email,latitude,longitude,vehicle_no,retailer_name,location_name,paid AS payment_status_id,payment_codes.status AS payment_status,orders.timestamp as date_ordered');

        if(isset($data['order_id'])){

        $this->db->where('orders.order_id',$data['order_id']);

        }

        if(isset($data['vehicle_no'])){

        $this->db->where('vehicle_no',$data['vehicle_no']);

        }

        if(isset($data['not_assigned']) && $data['not_assigned']==1){

        $this->db->where('vehicle_no',"");

        }

        else if(isset($data['not_assigned']) && $data['not_assigned']==0){

        $this->db->where('vehicle_no !=', "");

        }

        $this->db->from('orders');

        $this->db->join('retailers','orders.retailer_id=retailers.retailer_id','left');

        $this->db->join('deliveries','deliveries.order_id=orders.order_id','left');

        $this->db->join('delivery_points','deliveries.delivery_point_id=delivery_points.id','left');

        $this->db->join('payment_codes','orders.paid=payment_codes.id');

        $returned_data= $this->db->get();

        $response=$returned_data->result_array();

        

        return json_encode($response);

    }

    

    public function get_order_details($order_id){

        $this->db->select('orders.order_id AS order_id,retailer_name,phone,email,location_name,latitude,longitude,total_cost,delivery_cost,paid AS payment_status_id,payment_codes.status AS payment_status');

        $this->db->where('orders.order_id',$order_id);

        $this->db->from('orders');

        $this->db->join('retailers','retailers.retailer_id=orders.retailer_id');

        $this->db->join('deliveries','deliveries.order_id=orders.order_id');

        $this->db->join('delivery_points','retailers.retailer_id=delivery_points.retailer_id');

        $this->db->join('payment_codes','orders.paid=payment_codes.id');

        $returned_data= $this->db->get();

        $data=$returned_data->result_array();

        

        // format the output

        $today=date("Y-m-d");

        $time = date("Y-m-d").' '.date("h:i:sa");

        $now=date("h:i:sa");

        $new_time = date("H:i:sa", strtotime('+6 hours'));

        $do = str_replace('-', '', $time);

        $do = str_replace(':', '', $do);

        $do = str_replace(' ', '', $do);

        $response['date']=$today;

        $response['do']=$order_id;

        $response['address'] = $data[0]['location_name'];

        $response['delivery_time'] =$now." - ".$new_time; 

        $response['deliver_to'] = $data[0]['retailer_name'];

        $response['phone'] = $data[0]['phone'];

        $response['notify_email'] =NOTIFY_EMAIL; 

        $response['notify_url'] =NOTIFY_URL;

        $response['instructions'] = "Order : ".$data[0]['order_id'].", Call the customer";

        $response['zone'] = "East";

        

      return $response;  

    }

    

    public function  get_items($order_id){

        $items=array();

        $item['sku']="SKU324";

        $item['desc']="Sample Generic item";

        $item['qty']=1;

        

        array_push($items, $item);

        return $items;

    }



    public function assign_vehicle($data){

           $order_id=$data['order_id'];

           unset($data['order_id']);

           $this->db->where('order_id',$order_id);

           $this->db->update($this->deliveries,$data);

           $response['message'] = "Order no. ".$order_id." was assigned to vehicle ".$data['vehicle_no']." successfully.";

           $response['code'] = 1;

           

           return $response;

       }

       

       public function new_logistics_settings($data){

           if($this->checkSettings($data)==0){

               $this->db->insert('delivery_settings',$data) ; 

               $num= $this->db->affected_rows();

               if($num>0){

                  $response['message'] = "Record added successfully.";

                  $response['code'] = 1;  

               }

               else{

               $response['message'] = "No change detected";

               $response['code'] = 0;

               }

           }

           else{

               $response['message'] = "Settings already set";

               $response['code'] = 0;

           }

           

           return json_encode($response);

       }

       

       public function checkSettings($data){

           $this->db->select('id');

           $this->db->where('supplier_id',$data['supplier_id']);

           $this->db->where('delivery_mode_id', $data['delivery_mode_id']);

           $this->db->from('delivery_settings');

           $count= $this->db->count_all_results();

           

           return $count;

        }

       

       public function new_delivery_zone($data){

           if($this->checkZones($data)==0){

               $this->db->insert('delivery_zones',$data) ; 

               $num= $this->db->affected_rows();

               if($num>0){

                  $response['message'] = "Record added successfully.";

                  $response['code'] = 1;  

               }

               else{

               $response['message'] = "No change detected";

               $response['code'] = 0;

               }

           }

           else{

               $response['message'] = "Delivery Zone already set";

               $response['code'] = 0;

           }

           

           return json_encode($response);

       }

       

       

       

       

       public function checkZones($data){

         $this->db->select('id');

           $this->db->where('supplier_id',$data['supplier_id']);

           $this->db->where('delivery_mode_id', $data['delivery_mode_id']);

           $this->db->where('zone_id', $data['zone_id']);

           $this->db->from('delivery_zones');

           $count= $this->db->count_all_results();

           

           return $count;

           

       }

       

       public function update_logistic_settings($data){

           $id=$data['id'];

           unset($data['id']);

           

           $this->db->where('id',$id);

           $this->db->update('delivery_settings',$data);

           $num= $this->db->affected_rows();

           if($num>0){

               $response['message'] = "Settings updated successfully";

               $response['code'] =1;

           }

           else{

               $response['message'] = "No record updated";

               $response['code'] =0;

           }

           return json_encode($response);

       }

       

       public function fetch_all_logistic_settings($data){

           $this->db->select('delivery_settings.id AS id,delivery_mode_id,delivery_modes.name AS delivery_mode,minimum_cost,max_load,delivery_settings.supplier_id AS supplier_id, business_name,phone,location_name');

           if(isset($data['id'])){

               $this->db->where('delivery_settings.id',$data['id']);   

           }

           if(isset($data['supplier_id'])){

               $this->db->where('delivery_settings.supplier_id',$data['supplier_id']);   

           }

           $this->db->from('delivery_settings');

           $this->db->join('delivery_modes','delivery_modes.id=delivery_settings.delivery_mode_id');

           $this->db->join('suppliers','suppliers.supplier_id=delivery_settings.supplier_id');

           $returned_data = $this->db->get();

           $response=$returned_data->result_array();

           

           return json_encode($response);   

       }

       

       public function update_delivery_zones($data){

           $id=$data['id'];

           unset($data['id']);

           $this->db->where('id',$id);

           $this->db->update('delivery_zones',$data);

           $num= $this->db->affected_rows();

           if($num>0){

               $response['message'] = "Delivery zone detals updated successfully";

               $response['code'] =1;

           }

           else{

               $response['message'] = "No record updated";

               $response['code'] =0;

           }

           return json_encode($response);

       }

       public function fetch_delivery_zones($data){

           $this->db->select('delivery_zones.id AS id,zones.name AS zone_name,delivery_modes.name AS delivery_mode,delivery_mode_id,zone_id,min_distance,max_distance,rate,delivery_zones.supplier_id AS supplier_id, business_name,phone,location_name');

           if(isset($data['id'])){

            $this->db->where('delivery_zones.id',$data['id']);   

           }

           if(isset($data['supplier_id'])){

            $this->db->where('delivery_zones.supplier_id',$data['supplier_id']);   

           }

           $this->db->from('delivery_zones');

           $this->db->join('zones','zones.id=delivery_zones.zone_id');

           $this->db->join('delivery_modes','delivery_modes.id=delivery_zones.delivery_mode_id');

           $this->db->join('suppliers','suppliers.supplier_id=delivery_zones.supplier_id');

           $returned_data= $this->db->get();

           $response=$returned_data->result_array();

           

           return json_encode($response);

       }

       

       

       public function view_zones(){

           $this->db->select('id,name');

           $this->db->from('zones');

           $returned_data= $this->db->get();

           $response=$returned_data->result_array();

           

           return json_encode($response);

       }

       

       public function view_delivery_modes(){

           $this->db->select('id,name');

           $this->db->from('delivery_modes');

           $returned_data= $this->db->get();

           $response=$returned_data->result_array();

           

           return json_encode($response); 

       }

       

       

       public function delivered($order_id){

           $data['status_id'] = 4;

           $this->db->where('order_id',$order_id);

           $this->db->update('orders',$data);

           

           // notify customer care

           $cc_numbers= $this->getcc_phone();

           $i=0;

           

           $message = $this->get_retailer($order_id);

           while($i< count($cc_numbers)){

          // $this->SendSMS->action_send($cc_numbers[$i],$message);

           $i++;

           }

           // send email with ordered_items

           $this->delivered_items($order_id);

       }

       

       public function getcc_phone(){

           $phone_numbers=array();

           $this->db->select('phone');

           $this->db->where('is_notification_number',1);

           $this->db->from('customer_care');

           $returned_data= $this->db->get();

           $data=$returned_data->result_array();

           if(count($data)>0){

        $i=0;

        $nos="";

        while($i<count($data)){

            if(($i+1)==count($data)){

         $nos.=$data[$i]['phone']; 

            }

            else{

             $nos.=$data[$i]['phone'].",";     

            }

         $i++;

        }

        $phone_numbers= explode(",", $nos);

        }

        

        return $phone_numbers;

       }

       

       public function get_retailer_name($order_id){

           $this->db->select('retailer_name,phone');

           $this->db->where('order_id',$order_id);

           $this->db->from('retailers');

           $this->db->join('orders','orders.retailer_id=retailers.retailer_id');

           $returned_data = $this->db->get();

           $data=$returned_data->result_array();

        //   print_r($data);

           return $data;

       }

       public function get_retailer($order_id){

           $data= $this->get_retailer_name($order_id);

        //   print_r($data); 

           if(count($data)==0){

           $message="Items in order no:".$order_id." delivered to unknown retailer.";

           }

           else{

               $message="Items under order no:".$order_id." have been delivered to ".$data[0]['retailer_name'].".\nCall ".$data[0]['phone']." to confirm.";

           }

           

           return $message;

       }

       

       public function all_data($arry){

           $data['data'] = $arry;

           $this->db->insert('test_post',$data);

       }

       

        public function  delivered_items($order_id){

            $retailer__details=$this->get_retailer_name($order_id);
            $retailer_name=$retailer__details[0]['retailer_name'];
            $phone=$retailer__details[0]['phone'];
            $this->db->select('products.product_name AS product,products.product_id AS product_id,skus.sku_name AS sku,stock.sku_id AS sku_id,stock.product_id AS product_id,ordered_products.quantity AS quantity,ordered_products.unit_cost AS unit_cost, ordered_products.timestamp AS date_ordered');
            $this->db->where('ordered_products.order_id',$order_id);
            $this->db->from('ordered_products');
            $this->db->join('stock','stock.stock_id=ordered_products.stock_id');
            $this->db->join('products','stock.product_id=products.product_id');
            $this->db->join('skus','skus.sku_id=stock.sku_id');
            $returned_data = $this->db->get();
            $data=$returned_data->result_array();
            
            return $data;

        }

        

        public function deliver_order($data){

//                    {"order_details":

//                    {"source_address":"Nairobi",

//                    "source_latitude":12.324667,

//                    "source_longitude":2.1121,

//                    "destination_address":"Keroka",---

//                    "destination_longitude":8.35456,---

//                    "destination_latitude":2.3434,---

//                    "phone":"+254725627847",---

//                    "email":"mwas@gmail.com",---

//                    "call_back_url":"http.www.google.com",

//                    "expected_pickup_time":"11:00pm",--

//                    "expected_delivery_time":"10:21 pm",--

//                    "special_notes":"Handle with alot of care",--

//                    "items":[{"name":"Soda","quantity":1,"cost":100},{"name":"Chocolate","quantity":2,"cost":50}]}}

//                    "order_id"}

            // get items

            $this->db->select('ordered_products.quantity AS quantity,ordered_products.unit_cost AS cost,CONCAT(product_name," ",sku_name) AS name');

            $this->db->where('order_id',$data['order_id']);

            $this->db->from('ordered_products');

            $this->db->join('stock','stock.stock_id=ordered_products.stock_id');

            $this->db->join('products','products.product_id=stock.product_id');

            $this->db->join('skus','skus.sku_id=stock.sku_id');

            $returned_data= $this->db->get();

            $items = $returned_data->result_array();

            if(count($items)>0){

            // get retailer info

            $this->db->select('retailer_name,phone,email,IFNULL(location_name,"Unknown Destination") AS destination_address,IFNULL(latitude,"Unknown Destination") AS destination_latitude,IFNULL(longitude,"Unknown Longitude") AS destination_longitude');

            $this->db->where('orders.order_id',$data['order_id']);

            $this->db->from('orders');

            $this->db->join('retailers','orders.retailer_id=retailers.retailer_id','left');

            $this->db->join('delivery_points','retailers.retailer_id=delivery_points.retailer_id','left');

            $this->db->join('deliveries','delivery_points.id=deliveries.delivery_point_id','left');

            $this->db->limit(1);

            

            $retailer_data= $this->db->get();

            $retailer_info = $retailer_data->result_array();

            

            // get supplier or DC info

            $this->db->select('suppliers.supplier_id AS supplier_id,location_name AS source_address,latitude AS source_latitude,longitude AS source_longitude');

            $this->db->where('ordered_products.order_id',$data['order_id']);

            $this->db->from('ordered_products');

            $this->db->join('stock','stock.stock_id=ordered_products.stock_id');

            $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');

            $this->db->limit(1);

            $source_data = $this->db->get();

            $source_info=$source_data->result_array();

            

            // get callback url

            $this->db->select('url');

            $this->db->where('app','callback');

            $this->db->from('ryda_urls');

            $url = $this->db->get();

            $url_data = $url->result_array();

            

             unset($data['order_id']);

             $res1 = array_merge($data,$retailer_info[0]);

             $response = array_merge($res1,$source_info[0]);

             $response['call_back_url'] = $url_data[0]['url'];

             $response['items'] = $items;

             

             $final_data['order_details'] = $response;

            }

            else{

            $final_data['order_details'] = 0;    

            }

           return json_encode($final_data); 

        }

        

        public function get_delivery_url(){

            $this->db->select('url');

            $this->db->where('app','deliver_order');

            $this->db->from('ryda_urls');

            $url = $this->db->get();

            $url_data = $url->result_array();

            

            if(count($url_data)>0){

                $url=$url_data[0]['url'];

            }

            else{

                $url_data="http://test.duka-pepe.com/rydaapi/index.php/orders/request_assignment";

            }

            return $url;

        }





//        Later reference *******************************************************

//        

//        public function  delivered_items($order_id){

//            $retailer__details=$this->get_retailer_name($order_id);

//            $retailer_name=$retailer__details[0]['retailer_name'];

//            

//            $this->db->select('products.product_name AS product,skus.sku_name AS sku, ordered_products.quantity AS quantity,ordered_products.unit_cost AS unit_cost, ordered_products.timestamp AS date_ordered');

//            $this->db->where('ordered_products.order_id',$order_id);

//            $this->db->from('ordered_products');

//            $this->db->join('stock','stock.stock_id=ordered_products.stock_id');

//            $this->db->join('products','stock.product_id=products.product_id');

//            $this->db->join('skus','skus.sku_id=stock.sku_id');

//            $returned_data = $this->db->get();

//           $data=$returned_data->result_array();

//           

//           $output="Hi ".$retailer_name.",\n Your order ".$order_id." have been delivered successfully. Below are items delivered:\n";

//           $i=0;

//           $overal_cost=0;

//           $counter=0;

//           while($i<count($data)){

//               $counter++;

//               $product=$data[$i]['product'];

//               $sku=$data[$i]['sku'];

//               $quantity=$data[$i]['quantity'];

//               $unit_cost=$data[$i]['unit_cost'];

//               $total_line_cost=$quantity*$unit_cost;

//               $date_ordered=$data[$i]['date_ordered'];

//               

//              $output.=$counter.". ".$product." ".$sku." @ Ksh ".$unit_cost." X ".$quantity." = Ksh. ".$total_line_cost."\n";

//               

//               $overal_cost+=$total_line_cost;

//               $i++;

//           }

//           $output.="\nProducts cost :\t\t\t Ksh. ".$overal_cost."\n";

//           

//           // GET DELIVERY COST

//           $this->db->select('delivery_cost');

//           $this->db->where('order_id',$order_id);

//           $this->db->from('deliveries');

//           $rdata = $this->db->get();

//           $data_del=$rdata->result_array();

//         //get retailer information 

//           $delivery_cost=$data_del[0]['delivery_cost'];

//           $output.="Delivery cost :\t\t\t Ksh. ".$delivery_cost."\n";

//           $overal_cost+=$delivery_cost;

//          $output.=" \n"

//                  . "TOTAL COST :\t\t\t Ksh. ".$overal_cost."\n"; 

//          

//          $output.="Receipt Prepared by DUKAPEPE LIMITED.";

//          $this->SendMail->action_send('mwambage@gmail.com',$output,'Tuko Chonjo');

//          return $output;

//        }

}
