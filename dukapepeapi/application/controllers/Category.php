<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Category
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Category extends CI_Controller{
   public function __construct() { 
         parent::__construct(); 
         $this->load->helper(array('form', 'url'));
         $this->load->library('image');
      }
     
      public function add(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $data=array();
        //upload image and get file_name
        $image_url= $this->image->upload_image();
        if(!$image_url==""){
        $category_name= $this->input->post('category_name');
        $category_description= $this->input->post('category_description');
//        $image_url= $this->input->post('image_url');
        $data['category_name']=$category_name;
        $data['category_description']=$category_description;
        $data['image_url']=$image_url;
        $response= $this->Category_Model->add($data);
        }
        else{
            $response=array();
            $response['message']="Error Uploading Image, Category not added";
            $response['code']=0;
        }
//        end
      }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    
    public function getProductDetails(){
         $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $data=array();
        $category_id= $this->input->post('category_id');
        $data['category_id']=$category_id;
        $response= $this->Category_Model->getProductDetails($data);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
        
    }
    public function fetch(){
     $access_response=$this->Management_Model->validate_request($this->input->post('access_token')); 
     
      if($access_response['code']==1){
          $data=array();
          if($this->input->post('is_active')!=null){
           $data['is_active'] =  $this->input->post('is_active');
          }
        $response= $this->Category_Model->fetch($data);
      }
        else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    
    public function update(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){

        $data=array();
        $category_id= $this->input->post('category_id');
        $category_name= $this->input->post('category_name');
        $category_description= $this->input->post('category_description');
        //upload image and get file_name
       // if($this->input->post('image_url')!=null){
        $image_url= $this->image->upload_image();
        if(!$image_url==""){
         $data['image_url']=$image_url;   
        }
       // }
        $data['category_id']=$category_id;
        $data['category_name']=$category_name;
        $data['category_description']=$category_description;
        
        $response= $this->Category_Model->update($data);
         }else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    
    public function status(){
        
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $data=array();
       $category_id= $this->input->post('category_id');
       $is_active=$this->input->post('is_active');
       $data['category_id']=$category_id;
       $data['is_active']=$is_active;
       
       $response= $this->Category_Model->status($data);
        }
        else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
    
    public function delete(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $data=array();
       $category_id= $this->input->post('category_id');
       $response= $this->Category_Model->delete($category_id);
        }
        else{
       $response= json_encode($access_response);   
      }
        echo $response;    
    }
}
