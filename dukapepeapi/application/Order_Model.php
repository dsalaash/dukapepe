<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Order_Model
 *
 * @author mwamb
 */
class Order_Model extends CI_Model{
    //put your code here
    public $order="orders";
    public $order_supplier="supplier_orders";
    public $ordered_products="ordered_products";
    
     public function get_order_count_by_status()
       {
            $query = $this->db->query('SELECT * FROM orders WHERE status_id =1');
           
           return $query->num_rows();
       }
 public function linuxtestsms()
       { 
         return $this->SendSMS->action_send('+254701717592','hello');
       }
    public function place_order($order_data,$order_id,$payment_method){
        $response=array();
        $stock_data=$order_data['order_details'];
        $retailer_id=$order_data['retailer_id'];
        $agent_id=-1;
        $cachinguser =$retailer_id;
        if (array_key_exists("agent_id",$order_data))
              {
               $agent_id=$order_data['agent_id'];
                $cachinguser=$order_data['agent_id'];
              }
            else
              {
             $agent_id=-1;
              }
        $delivery_data=$order_data['delivery'];

        
        
        $i=0;
        $total_cost=0;
        $total_vat=0;
        $total_bp=0;
        while($i<count($stock_data)){
            $this->db->select('unit_bp');
            $this->db->from('stock');
            $this->db->where('stock_id', $stock_data[$i]['stock_id']);
            $returned= $this->db->get();
            $returned_bp2=$returned->result_array();
            $total_cost+=($stock_data[$i]['unit_cost']*$stock_data[$i]['quantity']);  
            $total_bp+=($returned_bp2[0]['unit_bp']*$stock_data[$i]['quantity']);
            $total_vat+=$stock_data[$i]['vat']*$stock_data[$i]['quantity']; 
            $i++;
         }
//         check payment
        if($this->check_paid($order_id)==0){
        $data['paid']=0;   
        }
        else{
        $data['paid']=1;     
        }
         
        $data['retailer_id']=$retailer_id;
        $data['total_cost']=$total_cost;
        $data['order_id']=$order_id;
        $data['payment_method']=$payment_method;
        $data['vat']=$total_vat;
        $data['agent_id']=$agent_id;
        
        if ($payment_method>1) {
            $data['paid']=2;   
        }
        
        $delivery_data['order_id']=$order_id;
                
         if($this->checkStock($data)==0){
        $added_records=$this->new_order($data);
        // update delivery cost
        $this->add_delivery_cost($delivery_data);
        
        
        $j=0;
        $supplier_orders=array();
        while($j<count($stock_data)){
            $this->db->select('unit_bp');
            $this->db->from('stock');
            $this->db->where('stock_id', $stock_data[$j]['stock_id']);
            $returned= $this->db->get();
            $returned_bp1=$returned->result_array();
            $product_cost=($stock_data[$j]['unit_cost']*$stock_data[$j]['quantity']);
            $product_bp = ($returned_bp1[0]['unit_bp']*$stock_data[$j]['quantity']);
            $supplier_id=$stock_data[$j]['supplier_id'];
            $product_vat=($stock_data[$j]['vat']*$stock_data[$j]['quantity']);
            $k=0;
            $counter=0;
            while($k<count($supplier_orders)){
              if($supplier_orders[$k]['supplier_id']==$supplier_id){
                 $supplier_orders[$k]['cost_of_sales'] =  $supplier_orders[$k]['cost_of_sales']+ $product_bp;
                 $supplier_orders[$k]['total_cost']=$supplier_orders[$k]['total_cost'] + $product_cost;
                 $supplier_orders[$k]['vat']=$supplier_orders[$k]['vat'] + $product_vat;
                 $counter++;
              }
              $k++;
            }
            if($counter==0){ //Add as a new product
              $new_product=array();
              $new_product['supplier_id']=$supplier_id;
              $new_product['total_cost']=$product_cost;
              $new_product['cost_of_sales']=$product_bp;
              $new_product['order_id']=$order_id;
              $new_product['vat']=$product_vat;
              
              array_push($supplier_orders, $new_product);  
            }
        $j++;
        }
               
       
        $this->new_product_order($stock_data,$order_id,$cachinguser);
        $this->new_supplier_order($supplier_orders,$retailer_id);
        
        $phone_number= $this->getphone($retailer_id);
        $response['message']="Order placed successfully.";
        $response['code']=1;
        $response['order_id']=$order_id;
        $response['total_cost']=$total_cost;
        $response['vat']=$total_vat;
        
        $response['delivery_cost']=$delivery_data['delivery_cost'];
        $response['delivery_vat']=$delivery_data['vat'];
        $response['phone']=$phone_number;
        
        
    }
    else{
        $response['message']="Order has already been placed.";
        $response['code']=0;
    }
    
    return json_encode($response);
    }
    public function new_supplier_order($supplier_orders,$retailer){
     $i=0;
    while($i<count($supplier_orders)){
        $so_info=$supplier_orders[$i];
        $this->db->insert($this->order_supplier,$so_info);
        //
        $phone = $this->getSupplierPhone($so_info['supplier_id']);
        $retailerinfo = $this->getRetailerPhone($retailer);
        $order = $this->getSupplierItems($so_info,$retailerinfo);
        // send sms notification upon placing order
        $this->SendSMS->action_send($phone,$order);
//        $this->SendSMS->action_send("+254741403621",$order);
        $i++;
    }   
        
    }
    
    public function new_product_order($order_data,$order_id,$cachinguser){
    $i=0;
    while($i<count($order_data)){
        $product_info=array();
        $this->db->select('unit_bp');
        $this->db->from('stock');
        $this->db->where('stock_id', $order_data[$i]['stock_id']);
        $returned= $this->db->get();
        $returned_bp=$returned->result_array();
        $product_info['order_id']=$order_id;
        $product_info['stock_id']=$order_data[$i]['stock_id'];
        $product_info['quantity']=$order_data[$i]['quantity'];
        $product_info['unit_cost']=$order_data[$i]['unit_cost'];
         $product_info['cost_of_sales']=$returned_bp[0]['unit_bp'];
        $product_info['vat']=$order_data[$i]['vat'];
        
        $this->db->insert($this->ordered_products,$product_info);
       $this->remove_stock($order_data[$i]['stock_id'],$order_data[$i]['quantity'],$cachinguser);
        $i++;
    }
        
    }
    
    public function remove_stock($stock_id,$quantity,$cachinguser){
        $this->db->query('UPDATE stock SET quantity = (quantity-'.$quantity.') WHERE stock_id ='.$stock_id);
            $this->db->where('retailer_id',263);
            $this->db->where('stock_id', $stock_id);
               $this->db->delete('current_transactions_items');
               $this->db->select('*');
               $this->db->where('retailer_id', 263);
               $this->db->from('current_transactions_items');
               if($this->db->count_all_results()==0)
                  {
                   $this->db->where('retailer_id', 263);
                   $this->db->delete('order_transactions');
                  }  
    }
    
    public function new_order($data){
        $this->db->insert($this->order,$data);
        $num = $this->db->affected_rows();
        return $num;
    }
    public function checkStock($data){
        $this->db->select('order_id');
//        $this->db->where('retailer_id',$data['retailer_id']);
//        $this->db->where('total_cost',$data['total_cost']);
        $this->db->where('order_id',$data['order_id']);
//        $this->db->where('status_id',1); NO NEED
        $this->db->from($this->order);
        $count= $this->db->count_all_results();
        return $count;
    }
    
    public function update_order_status($data){
        $order_id=$data['order_id'];
        unset($data['order_id']);
        $this->db->where('order_id',$order_id);
        $this->db->update($this->order,$data);
        $rows = $this->db->affected_rows();
        $response=array();
        if($rows>0){
        $response['message']="Status update successfully.";
        $response['code']=1;
        }
        else{
        $response['message'] = "Failed to update status.";
        $response['code'] = 0;
        }
        
       return json_encode($response);
    }
    
    public function payment($data){
        $response=array();
        $counter= $this->checkPayment($data);
        if($counter==0){
        $this->db->insert('payments',$data);
        $response['message']="Payments made successfully.";
        $response['code']=1;
        }
        else{
         $response['message']="Payment already used in another order.";
        $response['code']=0;   
        }
        return json_encode($response);
    }
    public function checkPayment($data){
        $this->db->select('payment_id');
        $this->db->where('transaction_id',$data['transaction_id']);
        $this->db->from('payments');
        $counter= $this->db->count_all_results();
        return $counter;
    }
    
    public function myorders($data){
         $this->db->select('order_id,total_cost,vat,orders.status_id AS status_id,order_status.status_name AS status,paid AS payment_status_id,payment_codes.status AS payment_status,orders.is_active AS is_active,orders.timestamp AS timestamp,payment_methods.name AS payment_method');  
         $this->db->where('retailer_id',$data['retailer_id']);
         $this->db->from($this->order);
         $this->db->join('order_status', $this->order.'.status_id=order_status.status_id');
         $this->db->join('payment_codes','orders.paid=payment_codes.id');
         $this->db->join('payment_methods','payment_methods.id=orders.payment_method');
         $this->db->order_by('timestamp DESC');
         $returned_data= $this->db->get();
         $response_data=$returned_data->result_array();
          $i=0;
         while ($i< count($response_data)){
                $delivery_costs=$this->get_delivery_cost($response_data[$i]['order_id']);
                $response_data[$i]['delivery_cost'] = $delivery_costs['delivery_cost'];
                $response_data[$i]['delivery_vat'] = $delivery_costs['vat'];
             $i++;
         }
         return json_encode($response_data);
       }
    
    public function order_status($data){
           $this->db->select($this->order.'.status_id AS status_id,order_status.status_name AS status_name,payment_methods.name AS payment_method');
           $this->db->where('order_id',$data['order_id']);
           $this->db->from($this->order);
           $this->db->join('order_status','orders.status_id=order_status.status_id');
         $this->db->join('payment_methods','payment_methods.id=orders.payment_method');
           $returned_data = $this->db->get();
           $response= $returned_data->result_array();
           return json_encode($response);
       }
             
    public function getphone($retailer_id){
           $this->db->select('phone');
           $this->db->where('retailer_id',$retailer_id);
           $this->db->from('retailers');
           $returned_data= $this->db->get();
           $data=$returned_data->result_array();
           if(count($data)>0){
           return $data[0]['phone'];
           }
           else{
               return 0;
           }
       }
       
    public function order_details($passed_data){
        $this->db->select('stock.stock_id AS stock_id,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,stock.unit_cost AS unit_cost,stock.vat AS vat,stock.quantity AS quantity,stock.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight,supplier_code,first_name,last_name,business_name,availability,latitude,longitude');
        $this->db->where($this->ordered_products.'.order_id',$passed_data['order_id']);
        $this->db->from($this->ordered_products);
        $this->db->join('stock','stock.stock_id='.$this->ordered_products.'.stock_id');
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
        $delivery_costs=$this->get_delivery_cost($passed_data['order_id']);
        $data[$i]['delivery_cost'] = $delivery_costs['delivery_cost'];
        $data[$i]['delivery_vat'] = $delivery_costs['vat'];
        
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        
        //get reviews
        $reviews= $this->Stock_Model->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        $i++;
        }
        return json_encode($data);   
           
       }
       
    public function get_order_by_status($status_id){
         $this->db->select('retailer_name, phone,email,orders.order_id AS order_id,orders.total_cost AS product_cost,orders.vat AS vat,deliveries.vat AS delivery_vat,delivery_cost,(orders.total_cost+orders.vat+deliveries.vat+delivery_cost) AS total_cost, orders.status_id AS status_id,'
                 . 'order_status.status_name AS status,paid AS payment_status_id,payment_codes.status AS payment_status,'
                 . 'vehicle_no,location_name,latitude,longitude,'
                 . 'orders.is_active AS is_active,orders.timestamp AS timestamp,payment_methods.name AS payment_method');
         $this->db->where('orders.status_id',$status_id);
         $this->db->from($this->order);
         $this->db->join('order_status', $this->order.'.status_id=order_status.status_id');
         $this->db->join('retailers', $this->order.'.retailer_id=retailers.retailer_id');
         $this->db->join('payment_codes','orders.paid=payment_codes.id');
         $this->db->join('deliveries','orders.order_id=deliveries.order_id','left');
         $this->db->join('delivery_points','delivery_points.id=deliveries.delivery_point_id','left');
         $this->db->join('payment_methods','payment_methods.id=orders.payment_method');
         $this->db->order_by('timestamp DESC');
         $returned_data= $this->db->get();
         $response_data=$returned_data->result_array();
//         
         $i=0;
         while ($i< count($response_data)){
             $supplier_info=$this->getsupplier($response_data[$i]['order_id']);
             $response_data[$i]['supplier_id'] = $supplier_info['supplier_id'];
             $response_data[$i]['distribution_center'] = $supplier_info['business_name'];
             $response_data[$i]['dc_location'] = $supplier_info['location_name'];
             $i++;
         }
         return json_encode($response_data); 
       } 
       
       public function getsupplier($order_id){
           $this->db->select('suppliers.supplier_id AS supplier_id,business_name,location_name');
           $this->db->where('supplier_orders.order_id',$order_id);
           $this->db->from('supplier_orders');
           $this->db->join('suppliers','suppliers.supplier_id=supplier_orders.supplier_id');
           $this->db->limit(1);
           $returned_data= $this->db->get();
         $response_data=$returned_data->result_array();
         
         return $response_data[0];
       }
       public function update_supplier_order_status($data){
        $so_id=$data['so_id'];
        unset($data['so_id']);
        $this->db->where('so_id',$so_id);
        $this->db->update('supplier_orders',$data);
        $rows = $this->db->affected_rows();
        if($rows>0){
            $response['message'] = "Status updated successfully.";
            $response['code'] = 1;
        }
        else{
            $response['message'] = "Failed to update supplier status.";
            $response['code'] = 0;
        }
        return json_encode($response);
        }
        
    public function get_supplier_orders($data){
//        print_r($data);
    //get supplier items
    $this->db->select('so_id AS supplier_order_id,supplier_orders.supplier_id AS supplier_id,order_id,total_cost,supplier_orders.vat AS vat,status_id,supplier_status.name AS status,supplier_orders.timestamp AS timestamp,first_name,last_name,business_name,availability,latitude,longitude');
    if(array_key_exists('supplier_id',$data)){
    $this->db->where('supplier_orders.supplier_id',$data['supplier_id']);
    }
    if(array_key_exists('status_id', $data)){
    $this->db->where('status_id',$data['status_id']);
    }
    if(array_key_exists('supplier_order_id', $data)){
    $this->db->where('so_id',$data['supplier_order_id']);
    }
    if(array_key_exists('order_id', $data)){
    $this->db->where('order_id',$data['order_id']);
    }
    $this->db->from('supplier_orders');
    $this->db->join('supplier_status','supplier_status.id=supplier_orders.status_id');
    $this->db->join('suppliers','suppliers.supplier_id=supplier_orders.supplier_id');
    $this->db->order_by('timestamp DESC');
    $returned_data= $this->db->get();
    $supplier_orders=$returned_data->result_array();
    $counter= count($supplier_orders);
    $i=0;
         while($i<$counter){
            $product_details= $this->get_sub_order_details($supplier_orders[$i]['order_id'],$supplier_orders[$i]['supplier_id']);
            $supplier_orders[$i]['product_details']=$product_details;
            $i++;   
         }
     return json_encode($supplier_orders);   
    }
    
    public function get_sub_order_details($order_id,$supplier_id){
        $this->db->select('ordered_products.stock_id AS stock_id,stock.product_id AS product_id,stock.sku_id AS sku_id,'
                . 'ordered_products.quantity AS ordered_quantity,ordered_products.unit_cost AS ordered_unit_cost,'
                . 'product_name,product_description,sku_name,weight,stock.unit_cost AS unit_cost,ordered_products.vat AS vat,'
                . 'stock.quantity AS quantity,stock.is_active AS is_active, ordered_products.timestamp AS timestamp,'
                . 'supplier_code,latitude,longitude');
        $this->db->where('stock.supplier_id',$supplier_id);
        $this->db->where('ordered_products.order_id',$order_id);
        $this->db->from('ordered_products');
        $this->db->join('stock','stock.stock_id='.$this->ordered_products.'.stock_id');
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $this->db->join('suppliers','suppliers.supplier_id=stock.supplier_id');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        //get reviews
        $reviews= $this->Stock_Model->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        $i++;
        }
        return $data;   
         
       }
       
       public function getSupplierPhone($supplier_id){
           $this->db->select('phone');
           $this->db->where('supplier_id',$supplier_id);
           $this->db->from('suppliers');
           $returned_data= $this->db->get();
           $data=$returned_data->result_array();
           if(count($data)>0){
           return $data[0]['phone'];
           }
           else{
               return 0;
           }    
       }
       public function getRetailerPhone($retailer){
           $this->db->select('retailers.phone,retailers.retailer_name,suppliers.business_name');
           $this->db->where('retailers.retailer_id',$retailer);
           $this->db->from('retailer_suppliers');
           $this->db->join('retailers','retailers.retailer_id=retailer_suppliers.retailer_id');
            $this->db->join('suppliers','retailer_suppliers.supplier_id=suppliers.supplier_id');
           $returned_data= $this->db->get();
           $data=$returned_data->result_array();
           return $data[0];
           // if(count($data)>0){
           // return $data[0]['phone'];
           // }
           // else{
           //     return 0;
           // }    
       }
       public function getSupplierItems($so_info,$retailer){
           $response="New Order placed from ".$retailer['retailer_name']."(".$retailer['phone'].") For ".$retailer['business_name']." \n";
           $this->db->select('product_name,sku_name,ordered_products.quantity AS quantity');
           $this->db->where('stock.supplier_id',$so_info['supplier_id']);
           $this->db->where('ordered_products.order_id',$so_info['order_id']);
           $this->db->from('ordered_products');
           $this->db->join('stock','ordered_products.stock_id=stock.stock_id');
           $this->db->join('products','products.product_id=stock.product_id');
           $this->db->join('skus','stock.sku_id=skus.sku_id');
           $returned_data= $this->db->get();
           $data=$returned_data->result_array();
           $i=0;
           while($i< count($data)){
            $response.=$data[$i]['product_name'].":".$data[$i]['quantity']." X ".$data[$i]['sku_name']."\n" ;  
               $i++;
           }
         return $response;  
       }
       
       public function get_delivery_options($data){
         // check zone
            $available_modes=array();
            $returned_data = $this->db->query('SELECT `delivery_zones`.`id` AS `id`, `delivery_mode_id`, `zone_id`, `rate`, `delivery_modes`.`name` AS `delivery_mode`, '
                    . '`zones`.`name` AS `zone_name` '
                    . 'FROM `delivery_zones` '
                    . 'JOIN `delivery_modes` ON `delivery_modes`.`id`=`delivery_zones`.`delivery_mode_id` '
                    . 'JOIN `zones` ON `delivery_zones`.`zone_id`=`zones`.`id` '
                    . 'WHERE '.$data['distance'].' >= min_distance AND '.$data['distance'].' <= max_distance && delivery_zones.supplier_id='.$data['supplier_id'].' '
                    . 'ORDER BY delivery_mode_id ASC');

            $zone_mode=$returned_data->result_array();
            $i=0;
            while($i<count($zone_mode)){
               $settings_data= $this->delivery_settings($zone_mode[$i]['delivery_mode_id'],$data['weight'],$data['supplier_id']);
               if(count($settings_data)>0){
                $minimum_cost=$settings_data[0]['minimum_cost'];
   //           actual cost=mass*distance*rate;
                $actual_cost=$data['weight']*$data['distance']*$zone_mode[$i]['rate'];
                $cost=round($minimum_cost+$actual_cost);
                // add it to output
                $mode['dmz_id'] = $zone_mode[$i]['id'];
                $mode['delivery_mode'] = $zone_mode[$i]['delivery_mode'];
                $mode['zone'] = $zone_mode[$i]['zone_name'];
                $mode['cost'] = $cost;
                array_push($available_modes, $mode);
                break;
               }
                $i++;
            }
         return json_encode($available_modes);
       }
       
       public function delivery_settings($mode_id,$weight,$supplier_id){
           $returned_data = $this->db->query('SELECT `minimum_cost` FROM `delivery_settings` WHERE `delivery_mode_id` = '.$mode_id.' AND '.$weight.' <= max_load AND supplier_id='.$supplier_id.'');
           $response=$returned_data->result_array();
           return $response;
       }
       public function add_delivery_cost($delivery_data){
           $this->db->insert('deliveries',$delivery_data);
       }
       
        public function get_delivery_cost($order_id){
           $this->db->select('delivery_cost,vat');
           $this->db->where('order_id',$order_id);
           $this->db->from('deliveries');
           $returned_data= $this->db->get();
           $data=$returned_data->result_array();
           if(count($data)>0){
               $cost=$data[0];
           }
           else{
               $cost=array();
           }
           return $cost;
       }
       
       public function cancel_order($data){
           $message="Hi, \nKindly stop working on order number ".$data['order_id'].". It has been cancelled by Retailer.\n  Sorry for any inconvenience.";
           $this->db->where('order_id',$data['order_id']);
           $this->db->where('retailer_id',$data['retailer_id']);
           $this->db->where('status_id',1);
           $this->db->delete('orders');
           $rows = $this->db->affected_rows();
           if($rows>0){
               //supplier_orders table
           $this->db->where('order_id',$data['order_id']);
           $this->db->delete('supplier_orders');
              // ordered products
           $this->db->where('order_id',$data['order_id']);
           $this->db->delete('ordered_products');
           
           $response['message'] = "Order cancelled successfully.";
           $response['code'] = 1;
           // Notify Supplier && customer care
           $this->db->select('phone');
           $this->db->where('order_id',$data['order_id']);
           $this->db->from('suppliers');
           $this->db->join('supplier_orders','suppliers.supplier_id=supplier_orders.supplier_id');
           $data_suppliers= $this->db->get();
           $suppliers=$data_suppliers->result_array();
           $i=0;
           while($i< count($suppliers)){
            
            $this->SendSMS->action_send($suppliers[$i]['phone'],$message);
            $i++;
           }
                   
           }
           else{
           $response['message'] = "Order cannot be cancelled.";
           $response['code'] = 0;    
           }
           
           return json_encode($response);
       }
       
       public function  check_payment($data){
           $this->db->select('order_id,paid AS payment_status_id,payment_codes.status AS payment_status');
           if(isset($data['order_id'])){
           $this->db->where('order_id',$data['order_id']);
           }
           if(isset($data['paid_status'])){
           $this->db->where('paid',$data['paid_status']);   
           }
           $this->db->from('orders');
           $this->db->join('payment_codes','orders.paid=payment_codes.id');
           $returned_data= $this->db->get();
           $response=$returned_data->result_array();
           
           return json_encode($response);
       }
       
       public function fetch_mpesa_payment($data){
           $this->db->select();
           if(isset($data['order_id'])){
           $this->db->where('order_id',$data['order_id']);
           }
           $this->db->from('mpesa_payments');
           $returned_data= $this->db->get();
           $response=$returned_data->result_array();
           
           return json_encode($response);
       }
       
       public function getRandomID($retailerID){
        $datekey=date('Ymd-his')."-".$retailerID;
        return $datekey;
       }
       
       public function check_paid($order_id){
           $this->db->select('id');
           $this->db->where('order_id',$order_id);
           $this->db->where('trx_status','Success');
           $this->db->from('mpesa_payments');
           $count= $this->db->count_all_results();
           
           return $count;
       }
       
       public function approve_payment($data){
           $order_id = $data['order_id'];
           unset($data['order_id']);
           
           $this->db->where('order_id',$order_id);
           $this->db->update('orders',$data);
           $num= $this->db->affected_rows();
           if($num>0){
               //send sms
               $this->getRetailer($order_id);
               //response
               $response['message'] = "Order payments approved successfully.";
               $response['code'] = 1;
           }
           else{
             $response['message'] = "Confirmation failed.";
               $response['code'] = 0;  
           }
           
           return json_encode($response);
       }
       
       public function getRetailer($order_id){
           $this->db->select('retailer_name,phone,(total_cost+orders.vat+deliveries.vat+delivery_cost) AS total_paid');
           $this->db->where('orders.order_id',$order_id);
           $this->db->from('orders');
           $this->db->join('retailers','retailers.retailer_id=orders.retailer_id');
           $this->db->join('deliveries','deliveries.order_id=orders.order_id');
           $returned_data= $this->db->get();
           $data=$returned_data->result_array();
           $retailer_name = $data[0]['retailer_name'];
           $phone = $data[0]['phone'];
           $total_paid = $data[0]['total_paid'];
           
           $message = "Dear ".$retailer_name.",\n"
                   . "Your order has been received and is being processed.Please ensure payment upon delivery. Kindly ignore this message if you have already paid. We thank you for your support ";
           
           $this->SendSMS->action_send($phone,$message);
       }
       
       public function gettodeleteOrders(){
           $this->db->select("order_id");
           
           $this->db->group_start();
//           $this->db->where("retailer_id",1);
//           $this->db->or_where("retailer_id",23);
//           $this->db->or_where("retailer_id",25);
//           $this->db->or_where("retailer_id",46);
//           $this->db->or_where("retailer_id",48);
//           $this->db->or_where("retailer_id",28);
//           $this->db->or_where("retailer_id",37);
//           $this->db->or_where("retailer_id",20);
//           $this->db->or_where("retailer_id",40);
//           $this->db->or_where("retailer_id",57);
//           $this->db->or_where("retailer_id",60);
//           $this->db->or_where("retailer_id",44);
           $this->db->group_end();
           
           $this->db->where("paid",2);
           
           $this->db->from('orders');
           $returned_data = $this->db->get();
           
           $recs = $returned_data->result_array();
           
           $records=$i=0;
           while($i<count($recs)){
              $order_id=$recs[$i]['order_id'] ;
              $records+=$this->delete_order($order_id);
               $i++;
           }
          
           $message=$records." Records deleted.";
           
           return $message;
       }
       
       public function delete1($order_id){
           $num= $this->delete_order($order_id);
           if($num>0){
               $response['code'] = 1;
               $response['message'] = $num." Records deleted permanently.";
           }
           else{
               $response['code'] = 0;
               $response['message'] = "No record was deleted.";
           }
           
           return json_encode($response);
       }
       
       public function delete_order($order_id){
           $this->db->where("order_id",$order_id);
           $this->db->delete("deliveries");
           
           $this->db->where("order_id",$order_id);
           $this->db->delete("ordered_products");
           
           $this->db->where("order_id",$order_id);
           $this->db->delete("supplier_orders");
           
           $this->db->where("order_id",$order_id);
           $this->db->delete("orders");
           
           $num = $this->db->affected_rows();
           if($num>0){
               $response['code'] = 1;
               $response['message'] = $num." Records deleted permanently.";
           }
           else{
               $response['code'] = 0;
               $response['message'] = "No record was deleted.";
           }
           return $num;
       }
}