<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Director
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Director extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function fetch(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
          $data['director_id']= $this->input->post('director_id');
          $response= $this->Director_Model->fetch($data);
        }
      else{
          $response= json_encode($access_response);   
      }
    echo $response;
    }
    public function fetch_all(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $response= $this->Director_Model->fetch_all();
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    
    public function update_profile(){
       $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
          $data['director_id'] = $this->input->post('director_id');
          $data['first_name'] = $this->input->post('first_name');
          $data['last_name'] = $this->input->post('last_name');
          $data['email'] = $this->input->post('email');
          
        $response= $this->Director_Model->update_profile($data);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response; 
    }
}
