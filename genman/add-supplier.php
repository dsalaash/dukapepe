<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka pepe | Suppliers</title>

        <?php include("../links.php") ?>
        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $('#access_token').val(localStorage.getItem('access_token'));
            });
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyB8WgY93rMVkcDEn3Z64CUcSh3Jx_UMeH4"></script>
        <script>
            google.maps.event.addDomListener(window, 'load', initialize);
            function initialize() {
                var input = document.getElementById('location_name');
                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.addListener('place_changed', function () {
                    var place = autocomplete.getPlace();
                    // place variable will have all the information you are looking for.
                    $('#latitude').val(place.geometry['location'].lat());
                    $('#longitude').val(place.geometry['location'].lng());
                    console.log(place.geometry['location'].lat());
                    console.log(place.geometry['location'].lng());
                });
            }
        </script>
    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->

                        <!-- /user menu -->
                        <?php include("../sidebar.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Suppliers</span></h4>
                            </div>


                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                                <li class="active">Suppliers</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">



                        <style>
                            .modal{
                                z-index: 20;   
                            }
                            .modal-backdrop{
                                z-index: 10;        
                            }
                        </style>

                        <!-- add a new supplier -->
                        <div class="panel">
                            <div class="panel-body">
                                <form role="form" class="form-validate form-horizontal" method="POST" id="supplieradd" enctype="multipart/form-data">
                                            <input name="access_token" id="access_token" type="hidden" />
                                            <p id="demo"></p>
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <div class="form-group has-feedback">
                                                        <label for="first_name">First Name :</label>
                                                        <input type="text" required name="first_name" id="first_name" class="form-control" placeholder="First name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group has-feedback">
                                                        <label for="last_name">Last Name :</label>
                                                        <input type="text" required name="last_name" id="last_name" class="form-control" placeholder="Last name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group has-feedback">
                                                        <label for="email">Supplier Email Address :</label>
                                                        <input type="email" required name="email" id="emailadd" class="form-control" placeholder="Email">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-mention text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">

                                                <div class="col-md-4">
                                                    <div class="form-group has-feedback">
                                                        <label for="phone">Phone Number :</label>
                                                        <input type="text" required name="phone" id="mobilePhone" class="form-control" placeholder="Phone number">
                                                        <div class="form-control-feedback">
                                                            <i class="glyphicon glyphicon-earphone text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group has-feedback">
                                                        <label for="business_name">Business Name :</label>
                                                        <input name="business_name" id="business_name" required  type="text" class="form-control" placeholder="Business name">
                                                        <div class="form-control-feedback">
                                                            <i class="glyphicon glyphicon-home text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="supplier box">
                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <div class="form-group has-feedback">
                                                            <label for="location_name">Location Name :</label>
                                                            <input type="text" id="location_name" required name="location_name" class="form-control" placeholder="Location Name">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group has-feedback">
                                                            <label for="latitude">Latitude :</label>
                                                            <input type="text"  id="latitude" required name="latitude" class="form-control" placeholder="Latitude" readonly="true">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group has-feedback">
                                                            <label for="longitude">longitude :</label>
                                                            <input type="text"  id="longitude" required name="longitude" class="form-control" placeholder="Longitude" readonly="true">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="choose box"></div>
                                            <div class="director box"></div>
                                            <div class="text-right">
                                            <img class="status-progress"  src="../assets/loader/loader.gif"/>
                                            <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b>Save supplier</button>
                                            <!-- <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b>Add new supplier</button> -->
                                            </div>
                                        </form>


                            </div>
                        </div>

                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>

                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

        <script>

            var x = document.getElementById("demo");

            function showPosition(position) {

                $('#latitude').val(position.coords.latitude);
                $('#longitude').val(position.coords.longitude);
            }
            $('#supplieradd').submit(function (e) {
                e.preventDefault();
                // $("#submit").addClass("disabled");
                $(".status-progress").show();

                $.ajax({
                    url: base_url + "supplier/new_supplier",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        console.log(data);
                        $("#submit").removeClass("disabled");
                        $(".status-progress").hide();
                        var data = JSON.parse(data);
                        $(".status-progress").hide();
                        // console.log(data['newdc']);
                        window.location.reload();
                        new PNotify({
                            title: 'Registration Notice',
                            text: data['message'],
                            addclass: 'bg-success'
                        });


                    },
                    error: function (data) {
                        new PNotify({
                            title: 'Registration Notice',
                            text: data['message'],
                            addclass: 'bg-warning'
                        });
                    }
                });


            });
        </script>


    </body>
</html>