<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Duka pepe | Orders History</title>

        <?php include("../links.php") ?>

        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search orders:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });
                if (localStorage.getItem('code') == 1) {
                    loadAssignTransport();
                } else {

                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });
                }

                loadVehicles();
                vehicleassign();

            });
            function loadAssignTransport() {
                console.log(localStorage.getItem('access_token'));
                var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "delivery/get_deliveries";
                $('#example').DataTable({
                    "destroy": true,
                    "ajax": {
                        "url": url,
                        "data": formData,
                        "type": "post",
                        "dataSrc": function (json) {
                            return json;
                        },
                        "processing": true,
                        "serverSide": true,
                        "pagingType": "simple",
                        language: {
                            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                        }

                    }, "columns": [
                        {"data": "vehicle_no"},
                        {"data": "order_id"},
                        {"data": "phone"},
                        {"data": "email"},
                        {"data": "retailer_name"},
                        {"data": "location_name"},
                        {"data": "date_ordered"},
                        
                    ]
                });
            }


            function assignVehicle(id) {
                $(".status-progress").show();
                $('#accesstoken').val(localStorage.getItem('access_token'));
                $('#orderId').val(id);
                $('#modalAddstock').modal('show');
                $(".status-progress").hide();

            }


            function vehicleassign() {
                $('#formvehicleassign').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "delivery/assign_vehicle",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-add").hide();
                            loadOrders();
                            $('#modalAddstock').modal('hide');
                            new PNotify({
                                text: data['message'],
                                addclass: 'bg-success'
                            });
                        },
                        error: function () {}
                    });
                });
            }

            function loadVehicles() {
                var formData = {
                    'access_token': localStorage.getItem('access_token')
                };
                var url = base_url + "delivery/get_all_vehicles";
                $.post(url, formData,
                        function (data) {

                            var datas = JSON.parse(data);
                            console.log(datas);
                            var model = $('#selectcat');
                            model.empty();

                            $.each(datas, function (index, element) {
                                model.append("<option value='" + element.detrack_id + "'>" + element.name + "</option>");
                            });
                        });
            }

        </script>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                        
                        <!-- /user menu -->

                        <?php include("../sidebar.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Orders History</span></h4>
                            </div>

                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                                <li class="active">Order History</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">Order History</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <!--<a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Orders</a>-->
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            </div>

                            <table class="table" id="example">

                                <thead>
                                    <tr>
                                        <th>Vehicle number</th>
                                        <th>Order Id</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                       
                                        <th>Retailer name</th>
                                        <th>Location name</th>
                                        <th>Date ordered</th>
                                       
                                    </tr>
                                </thead>

                            </table>
                        </div>
                        <!-- /basic datatable -->

                        <!-- Vertical form modal -->
                        <div id="modalAddstock" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Add stock form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formvehicleassign" enctype="multipart/form-data">
                                        <input type="hidden" name="order_id" id="orderId"/>
                                        <input type="hidden" name="access_token" id="accesstoken"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Select Vehicle</label>
                                                <select required id="selectcat" name="vehicle_no" class="select-search">
                                                    <option value="0">Select Vehicle</option>
                                                </select>

                                            </div>


                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->

                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
