<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Duka pepe | DC Stock</title>

    <?php include("../links.php") ?>

    <!-- /theme JS files -->
    <script>
       
        $(document).ready(function () {
            $( ".assign_supplies_modal" ).click(function() {
  alert( "Handler for .click() called." );
});
            $(".status-progress").hide();
              var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url+ "Manufacturer/fetchManufacturers";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                        var manufacturer_row="";   
                        $.each(JSON.parse(data), function(k, v) {
                               
                              manufacturer_row+="<tr><td>"+v.manufacturer_id+"</td><td>"+v.name+"</td><td>"+v.mode_delivery+"</td><td><div class='btn-group'><button type='button' class='actionbtn dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><span><i class='icons-list' aria-hidden='true'></i><i class='fa fa-caret-down' aria-hidden='true'></i></span></button><ul class='dropdown-menu dropdown-menu-right'><li><a href='#'>Edit</a></li><li><a href='#'>Delete</a></li><li><a href='javascript: assign_supplies_modal("+v.manufacturer_id+")' class='assign_supplies_modal'>Supplies</a></li></ul></div></div></td></tr>";
                               
                            });  
                            $("#userstablebody").append(manufacturer_row);
                            $("#example").DataTable();

                                        
                                },
                       error:function(data){

                                  console.log(data);
                       }      
                       }) 

                //////////////
                  //////////////////
           $("#add_man_form").on("submit", function(e) 
           {
             e.preventDefault();
             var name=$("#manufacturer_name").val();
             var formData = {'access_token': localStorage.getItem('access_token'),'name':name};
             var url = base_url + "Manufacturer/addManufacturer";
             // var url = "http://192.168.191.2/dukapepeapi/index.php/Manufacturer/addManufacturer";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                        var response = JSON.parse(data);
                        if(response['code']==1)
                        {
                            new PNotify({
                                    text: response['message'],
                                    addclass: 'bg-success'
                                });
                        }
                        else
                        {

                        }
                         
                                        
                                },
                        
                       error:function(data){

                                  console.log(data);
                       }        

                   });

  
           });  
          ///fetch all products
          //////////////////////////////////////
                //////////////////
           $("#add_man_supplies_form").on("submit", function(e) 
           {
             e.preventDefault();
             var id=$("#manufacturer_id").val();
             var products=$("#supplies_list").val();
             var formData = {'access_token': localStorage.getItem('access_token'),'manufacturer_id':id, 'products':products};
             var url = base_url + "Manufacturer/addSupplies";
             // var url = "http://192.168.191.2/dukapepeapi/index.php/Manufacturer/addManufacturer";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                        var response = JSON.parse(data);
                        if(response['code']==1)
                        {
                             new PNotify({
                                text: response['message'],
                                addclass: 'bg-success'
                            });
                        }
                        else
                        {
                             new PNotify({
                                text: response['message'],
                                addclass: 'bg-success'
                            });
                        }
                                        
                                },
                        
                       error:function(data){

                                  console.log(data);
                       }        

                   });
                      
  
           });  
          ////////////////////////////////////
          var formData = {'access_token': localStorage.getItem('access_token')};
            var url = base_url + "products/fetch_all_products";
             // var url = "http://192.168.191.2/dukapepeapi/index.php/Manufacturer/addManufacturer";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var datas = JSON.parse(data);
                         var model = $('#supplies_list');
                         model.empty();
                         $.each(datas, function (index, element) {
                                model.append("<option value='" + element.product_id + "'>" + element.product_name + "</option>");
                            });
                                          
                                        
                                },
                        
                       error:function(data){

                                  console.log(data);
                       }        

                   });
          ///fetch all products
             

        });
        
        
      function assign_supplies_modal(id)
      {
        $("#modalmanAddsupplies").modal();
        $("#manufacturer_id").val(id);

        
      }   
       

    </script>

</head>

<body>

<?php include("../topbar.php") ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- Vertical form modal -->
                        <div id="modalmanAdd" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Manufacturers form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="add_man_form" enctype="multipart/form-data">

                                        <input type="hidden" name="access_token" id="catToken2"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Manufacturer name:</label>
                                                <input required name="manufacturer_name" id="manufacturer_name" class="form-control" placeholder="Manufacturer name" />

                                            </div>
                                                                                      

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->
                         <!-- Vertical form modal -->
                        <div id="modalmanAddsupplies" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Manufacturers-Product Assignment</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="add_man_supplies_form" enctype="multipart/form-data">

                                        <input type="hidden" name="access_token" id="catToken2"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Manufacturer name:</label>
                                                <input type="hidden" required name="manufacturer_id" id="manufacturer_id" class="form-control"/>

                                            </div>
                                             
                                               <label>Manufacturer Supplies:</label> 
                                                <select  class="js-example-basic-multiple select-search" name="states[]" multiple="multiple" id="supplies_list" >
                                                  <option value="AL">Alabama</option>
                                                   
                                                  <option value="WY">Wyoming</option>
                                               
                                               
                                                
                                                </select>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->

                <!-- /user menu -->

                <?php include("../sidebar.php") ?>
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Dukapepe Brands</span>
                        </h4>
                    </div>
                </div>
            </div>
            <!-- /page header -->
            <!-- Content area -->
            <div class="content">
                <div class="content clearfix">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                 <div class="panel-body">
                                <a href="#" class="btn btn-success" data-toggle="modal" data-target="#modalmanAdd"><i class="glyphicon glyphicon-plus"></i>Add Manufacturer</a>
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            </div>
                            
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body table-responsive">
                                <!--<a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Orders</a>-->
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            


                          
                           <table class="table datatable-basic" id="example">
                        <thead>
                        <tr>

                            <th>Id</th>
                            <th>Manufacturer Names</th>
                            <th>Mode Of Delivery</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="userstablebody">
                            
                        </tbody>

                    </table>
                    </div>
                        </div>
                    </div>
                </div>
                    

                <!-- Basic datatable -->
                <!-- <div class="panel ">
                   


                    <table class="table datatable-basic" id="example">
                        <thead>
                        <tr>

                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Approved</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="userstablebody">
                            
                        </tbody>

                    </table>
                </div> -->
                <!-- /basic datatable -->

              


               


               
               


               


                <!-- Footer -->
                <div class="footer text-muted">
                    <?php include("../footer.php") ?>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
