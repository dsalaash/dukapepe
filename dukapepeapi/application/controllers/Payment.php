<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Payment
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Payment extends CI_Controller{
    //put your code here
     public function __construct() {
        parent::__construct();
        $this->load->library('mpesa');
    }
    
    public function initiate_payments(){
        $phone= $this->input->post('phone');
        $amount= $this->input->post('amount');
        $order_id=$this->input->post('order_id');
        $merchant_transaction_id= $this->mpesa->generateRandomString();
        $password=$this->mpesa->getPasword();
        $response_data = $this->mpesa->processCheckOutRequest($password,MERCHANT_ID, $merchant_transaction_id,$order_id,$amount,$phone);
        $response_message = $response_data['message'];
        if($response_message!=""){
            $this->SendSMS->action_send($phone,$response_message);
        }
        else{
            $trx_id=$response_data['trx_id'];
            $response = $this->check_payment_status($password,$trx_id,$merchant_transaction_id);
            echo json_encode($response);
            $counter=0;
            while(1==1){
                $response = $this->check_payment_status($password,$trx_id,$merchant_transaction_id);
                echo json_encode($response);
                sleep(3);
                $counter++;
                echo 'trx_status'.$response['trx_status'];
                if($response['trx_status']=="Success" OR $response['trx_status']=="Failed" ){
                    //print_r($transaction_details);
                    $this->Payment_Model->save_mpesa_transaction($response);

                    if($response['trx_status']=="Success"){
                       $this->Payment_Model->update_order($order_id);
                       $message="Payment for Order Number : ".$order_id." Made successfully.";    
                    }
                    else{
                   $message="Error while making payments: Description: ".$response['description'];        
                    }
                $this->SendSMS->action_send($phone,$message);
                break;
                }
                else{
                    
                }
            }
        }
        
    }
    public function get_details(){
     $pass = $this->input->post('pass');
     $trx_id_num = $this->input->post('trx_id');
     $mti = $this->input->post('mti'); 
    $response = $this->check_payment_status($pass,$trx_id_num,$mti);
    echo json_encode($response); 
    }
    
    public function check_payment_status($pass,$trx_id_num,$mti){
        $response = $this->mpesa->statusRequest($pass,MERCHANT_ID,$trx_id_num, $mti); 
        return $response;
    }
public function checkout_response(){
      //GET TRANSACTION RESPONSE FROM SAFARICOM, METHOD WAS SET TO POST
    
$msisdn = $this->input->post('MSISDN');

$amount = $this->input->post('AMOUNT');

$mpesa_trx_id = $this->input->post('MPESA_TRX_ID');

$mpesa_trx_date = $this->input->post('MPESA_TRX_DATE');

$trx_status = $this->input->post('TRX_STATUS');

$return_code = $this->input->post('RETURN_CODE');

$description = $this->input->post('DESCRIPTION');

$order_id = $this->input->post('MERCHANT_TRANSACTION_ID');

$enc_params = $this->input->post('ENC_PARAMS');

$trx_id = $this->input->post('TRX_ID');  

$transaction_details=array();
$transaction_details['msisdn']        = "".$msisdn;
$transaction_details['amount']        = "".$amount;
$transaction_details['mpesa_trx_id']  = "".$mpesa_trx_id;
$transaction_details['mpesa_trx_date']= "".$mpesa_trx_date;
$transaction_details['trx_status']    = "".$trx_status;
$transaction_details['return_code']   = "".$return_code;
$transaction_details['description']   = "".$description;
$transaction_details['order_id']      = "".$order_id;
$transaction_details['enc_params']    = "".$enc_params;
$transaction_details['trx_id']        = "".$trx_id;

//print_r($transaction_details);
$this->Payment_Model->save_mpesa_transaction($transaction_details);
if($trx_status!='Failed'){ //suceeded
//update order as paid    
$this->Payment_Model->update_order($order_id);
$message="Payment made successfully for order number :".$order_id;
}
else{
    $message="Payment failed. Reason : ".$description;
}

$this->SendSMS->action_send($msisdn,$message);
}

public function sleeper(){
    $counter=0;
    while(1==1){
    sleep(2);
    echo 'did it : '.$counter;
    $counter++;
    if($counter>3){
        break;
    }
    }
}

}
