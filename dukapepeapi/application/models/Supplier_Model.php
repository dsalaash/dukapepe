<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Supplier_Model
 *
 * @author mwamb
 */
class Supplier_Model extends CI_Model{
    //put your code here
    public $suppliers="suppliers";
    
    public function __construct() {
        parent::__construct();
        $this->load->library('mpesa');
    }

    public function register_supplier($data){
        $count = $this->newChecker($data);
        if($count==0){
            $this->db->insert($this->suppliers,$data);
            $response['message'] = "Supplier added successfully.";
            $response['code'] = 1;
            // $response['newdc'] = $row;
        }
        else{
            $response['message'] = "Error: Supplier Already exist.";
            $response['code'] = 0;
        }
        return json_encode($response);
    }
    public function newChecker($data){
        $this->db->select('supplier_id');
        $this->db->where('phone',$data['phone']);
        if($data['email']==""){
        $this->db->or_where('email',$data['email']);
        }
        $this->db->from($this->suppliers);
        $count= $this->db->count_all_results();
        return $count;
    }

    public  function update_supplier($data){
        $count= $this->checkUpdate($data);
        if($count==0){
        $supplier_id=$data['supplier_id'];
        unset($data['supplier_id']);
        $this->db->where('supplier_id',$supplier_id);
        $this->db->update($this->suppliers,$data);
        $response['message'] = "Supplier details updated successfully.";
        $response['code'] = 1;
        }
        else{
        $response['message'] = "Failed to update supplier.";
        $response['code'] = 0;   
        }
        
        return json_encode($response);
    }
    public function get_supplier($data){
        $row = $this->db->select("*")->limit(1)->order_by('supplier_id',"DESC")->get("suppliers");
        $dat = $row->result_array();

        // $dcdata['supplier_details'] = $dat;
        return json_encode($dat);
    }
    public function checkUpdate($data){
        $this->db->select('supplier_id');
        $this->db->where('phone',$data['phone']);
        $this->db->where('supplier_id !=',$data['supplier_id']);
        $this->db->from($this->suppliers);
        $count= $this->db->count_all_results();
        return $count;
    }
    public function get_details($data){
        $this->db->select('supplier_id,first_name,last_name,business_name,phone,email,location_name,latitude,longitude,availability,is_active');
        $this->db->where('supplier_id',$data['supplier_id']);
        $this->db->from($this->suppliers);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        
        return json_encode($response);
    }
    public function get_all(){
       $this->db->select('supplier_id,first_name,last_name,business_name,phone,email,location_name,latitude,longitude,availability,is_active');
        $this->db->from($this->suppliers);
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
        
        return json_encode($response);
    }
    
    public function is_available($data){
        $supplier_id=$data['supplier_id'];
        unset($data['supplier_id']);
        $this->db->where('supplier_id',$supplier_id);
        $this->db->update($this->suppliers,$data);
        
        $response['message'] = "Supplier availability updated successfully.";
        $response['code'] = 1;
        
        return json_encode($response);
    }
    
    public function update_profile($data){
        $supplier_id=$data['supplier_id'];
        unset($data['supplier_id']);
        $this->db->where('supplier_id',$supplier_id);
        $this->db->update($this->suppliers,$data);
        $response['message'] = "Profile updated successfully.";
        $response['code'] = 1;
        
        return json_encode($response);
    }
    
    public function my_orders($data){
        $this->db->select('so_id,order_id,total_cost,payment_codes.id AS payment_status_id,payment_codes.status AS payment_status');
        $this->db->where('supplier_id',$data['supplier_id']);
        if(isset($data['is_paid'])){
            $this->db->where('payment_status',$data['is_paid']);
        }
        $this->db->from('supplier_orders');
        $this->db->join('payment_codes','payment_codes.id=supplier_orders.payment_status');
        $returned_data = $this->db->get();
        $response=$returned_data->result_array();
      return $response;  
    }
    
    public function update_supplier_payments($order_info){
        $counter=$updated=0;
        $data=$order_info['orders'];
        $invoice_no= $this->mpesa->generateRandomString();
        $invoice_data=$order_info['other_info'];
        $invoice_data['invoice_no'] = $invoice_no;
        while($counter<count($data)){
            $so_id=$data[$counter]['so_id'];
            unset($data[$counter]['so_id']);
            $data[$counter]['invoice_no'] = $invoice_data;
            $this->db->where('so_id',$so_id);
            $this->db->update('supplier_orders',$data[$counter]);
            if($this->db->affected_rows()>0){$updated++;}
            $counter++;
        }
        if($updated==$counter){
            $message="Orders paid successfully.";
            $code=1;
        }
        else{
            $message=$updated." orders paid for while ".($counter-$updated)." were not paid for";
            $code=0;
        }
      $response['message'] = $message;
      $response['code'] = $code;
      
      return json_encode($response);
    }
    
    public function save_invoice($data){
        $this->db->insert('invoices',$data);
    }
    
}
