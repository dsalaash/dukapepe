<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Shopping
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Shopping extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    public  function recent_orders(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $retailer_id= $this->input->post('retailer_id');
//        $retailer_id=1;
        $data['retailer_id']=$retailer_id;
        
        $response= $this->Shopping_Model->recent_orders($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
    public function categories(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $response= $this->Shopping_Model->categories();
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function promotions(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $response= $this->Shopping_Model->promotions();
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function recommended(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $response= $this->Shopping_Model->recommended();
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function last_orders(){
       $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
       $retailer_id= $this->input->post('retailer_id');
       $data['retailer_id']=$retailer_id;
       $response= $this->Shopping_Model->last_orders($data);
       }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
}
