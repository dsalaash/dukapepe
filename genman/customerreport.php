
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("../links.php") ?>
    <!-- /theme JS files -->

</head>

<body>
   <?php include("../topbar.php") ?>
     <!-- Page container -->
        <div class="page-container">
     <!-- Page content -->
            <div class="page-content">
              <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">
                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->
                <script type="text/javascript">
                   if(localStorage.getItem('access_token')=="")
                        {
                         window.location = "http://test.duka-pepe.com/dukapepe_portal/dukapepe/";
                        }
                      $(document).ready(function ()
                       {
                        $("#select_timeframe").change(function (){
                          var timeframe = $("#select_timeframe").val();
                         
                          if(timeframe==1)
                          {
                              
                          }
                          else if(timeframe==2)
                          {
                              var now = new Date();
                              var yr = now.getFullYear();
                              var month = now.getMonth()+1;
                              var day = now.getDate();
                              var startdate = yr+"-01-01";
                              var enddate = yr+"-"+month+"-"+day;
                              $("#datepickerstart").val(startdate);
                              $("#datepickerend").val(enddate);
                          }
                           else if(timeframe==3)
                          {
                              var now = new Date();
                              var yr = now.getFullYear();
                              var month = now.getMonth()+1;
                              var day = now.getDate();
                              var startdate = yr+"-"+month+"-01";
                              var enddate = yr+"-"+month+"-"+day;
                              $("#datepickerstart").val(startdate);
                              $("#datepickerend").val(enddate);
                          }
                          else if(timeframe==4)
                          {
                              var now = new Date();
                              var yr = now.getFullYear()-1;
                              var month = now.getMonth();
                              var day = now.getDate();
                              var no_days = new Date(yr, month, 0).getDate(); 
                              var startdate = yr+"-01-01";
                              var enddate = yr+"-12-31";
                              $("#datepickerstart").val(startdate);
                              $("#datepickerend").val(enddate);
                          }
                           else if(timeframe==5)
                          {
                              var now = new Date();
                              var yr = now.getFullYear();
                              var month = now.getMonth();
                              var day = now.getDate();
                              var no_days = new Date(yr, month, 0).getDate(); 
                              var startdate = yr+"-"+month+"-01";
                              var enddate = yr+"-"+month+"-"+no_days;
                              $("#datepickerstart").val(startdate);
                              $("#datepickerend").val(enddate);
                          }
                         
                         });
                        
                        $('#formsearchperiod').submit(function (e) {
                            var light_2 = $('body');
                            $(light_2).block({
                                            message: '<i class="icon-spinner2 spinner"></i>',
                                            overlayCSS: {
                                                backgroundColor: '#fff',
                                                opacity: 0.8,
                                                cursor: 'wait'
                                            },
                                            css: {
                                                border: 0,
                                                padding: 0,
                                                backgroundColor: 'none'
                                            }
                                        });  
                          e.preventDefault();
                          var startdate = $("#datepickerstart").val();
                          var enddate = $("#datepickerend").val();
                          var formData = {'access_token': localStorage.getItem('access_token'), 'start_date':startdate , 'end_date':enddate};
                          var url = base_url+ "Analytics/periodic_customer";
                           $.ajax({
                           type:'POST',
                           url: url,
                           data: formData,
                           success:function(data)
                           {
                               console.log(data);
                               var response = JSON.parse(data);
                               console.log(response['new_customers']);
                               // document.getElementById("selected_period_sales").innerHTML = "";
                               // document.getElementById("selected_period_orders").innerHTML = "";
                               // document.getElementById("new_customers").innerHTML = "";
                               // document.getElementById("o_customers").innerHTML = "";
                               // document.getElementById("c_customers").innerHTML = "";
                               // $("#selected_period_sales").append(response["total_sales"]);
                               // $("#selected_period_orders").append(response["total_orders"]);
                               // $("#new_customers").append(response["total_newcustomers"]);
                               // $("#o_customers").append(response["o_customers"]);
                               // $("#c_customers").append(response["c_customers"]);
                               var retailers_list="<table class='table' id='example1'><thead><tr><th>Retailer Name</th><th>Phone</th><th>Mpesa Phone</th><th>DC</th></tr></thead><tbody>";
                               $.each(response['new_customers'], function (k, v) {
                               retailers_list+="<tr><td>"+v.retailer_name
                                                +"</td><td>"+v.phone
                                                +"</td><td>"+v.mpesa_number
                                                +"</td><td>"+v.business_name
                                                +"</td></tr>";
                               
                              });
                              retailers_list+="</tbody></table>";
                               document.getElementById("retailers_list_general").innerHTML = "";
                              $("#retailers_list_general").append(retailers_list);
                              $("#example1").DataTable( {"processing": true} );
                              //filling the top selling items

                                  var retailers_listall="<table class='table' id='example2'><thead><tr><th>Retailer Name</th><th>Phone</th><th>Mpesa Phone</th><th>DC</th></tr></thead><tbody>";
                               $.each(response['all_customers'], function (k, v) {
                               retailers_listall+="<tr><td>"+v.retailer_name
                                                +"</td><td>"+v.phone
                                                +"</td><td>"+v.mpesa_number
                                                +"</td><td>"+v.business_name
                                                +"</td></tr>";
                               
                              });
                              retailers_listall+="</tbody></table>";
                              document.getElementById("brandlistperformance").innerHTML = "";
                             $("#brandlistperformance").append(retailers_listall);
                             $("#example2").DataTable();

                                 var retailers_listorders="<table class='table' id='example3'><thead><tr><th>Retailer Name</th><th>Phone</th><th>Total for Orders</th><th>DC</th></tr></thead><tbody>";
                               $.each(response['all_customers'], function (k, v) {
                               retailers_listorders+="<tr><td>"+v.retailer_name
                                                +"</td><td>"+v.phone
                                                +"</td><td>"+response['retailer_totals'+v.retailer_id]
                                                +"</td><td>"+v.business_name
                                                +"</td></tr>";
                               
                              });
                              retailers_listorders+="</tbody></table>";
                              document.getElementById("topselling_list_general").innerHTML = "";
                              $("#topselling_list_general").append(retailers_listorders);
                              $("#example3").DataTable( {"processing": true} );
                              $(light_2).unblock();
                              //filling the top selling items

                              



                         
                              // document.getElementById("topselling_list_general").innerHTML = "";
                              // $("#topselling_list_general").append(retailers_list);
                              // $("#example2").DataTable( {"processing": true} );
                              // ///filll dc ranks div
                              // var dc_sales_list="<table class='table' id='example3'><thead><tr><th>DC</th><th>Total Sales(Ksh)</th><th>Percentage</th></tr></thead><tbody>";
                              // $.each(response['list_of_dcs'], function (k, v) 
                              //     {
                              //       var supp_id = v.supplier_id;
                              //         $.each(response['dcsales'+supp_id], function (index, element) 
                              //                   { 
                              //                     var value_to_parse= (parseFloat(element.totalcost)/parseFloat(response['total_sales']))*100;
                              //                     var per =round(value_to_parse, 2);
                              //                     if(element.business_name==v.business_name)
                              //                     {
                              //                             dc_sales_list+="<tr><td>"+element.business_name+"</td><td>"+element.totalcost+"</td><td>"+per+"</td></tr>";
                              //                     }
                              //                });
                              //      });
                              //   dc_sales_list+="</tbody></table>";
                                // document.getElementById("dc_sales_performances_list").innerHTML = "";
                                // $("#dc_sales_performances_list").append(dc_sales_list);
                                // $("#example3").DataTable( {"processing": true} );
                               ///percentages
                                  //  $(function () {Highcharts.chart('container_percentages_pie', {
                                  //                              chart: {
                                  //     type: 'bar'
                                       
                                  // },
                                  // title: {
                                  //     text: 'Sales Distribution For Period above Selected'
                                  // },
                                  // subtitle: {
                                  //     text: 'Source: duka-pepe.com'
                                  // },
                                  // xAxis: {
                                  //     categories: response['dcs'],
                                  //     crosshair: true
                                  // },
                                  // yAxis: {
                                  //     min: 0,
                                  //     title: {
                                  //         text: 'Sales Amount (Ksh)'
                                  //     }
                                  // },
                                  // tooltip: {
                                  //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                  //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                  //         '<td style="padding:0"><b>{point.y:.1f} Ksh</b></td></tr>',
                                  //     footerFormat: '</table>',
                                  //     shared: true,
                                  //     useHTML: true
                                  // },
                                  // plotOptions: {
                                  //     column: {
                                  //         pointPadding: 0.0,
                                  //         borderWidth: 0
                                  //     }
                                  // },
                                  // series: [ {
                                  //     name: 'Dc-Sales',
                                  //     colorByPoint: true,
                                  //     data: response['dcs_list_of_sales']

                                  // }]
                                                                                 
                                  //                                                 });
                                  //                                     });

                               //bar sales
                                   ///percentages
                                    //  $(function () {Highcharts.chart('container_values_bar', {
                                    //                                         chart: {
                                    //     type: 'column'
                                         
                                    // },
                                    // title: {
                                    //     text: 'Customer Registration Distribution For Period Above'
                                    // },
                                    // subtitle: {
                                    //     text: 'Source: duka-pepe.com'
                                    // },
                                    // xAxis: {
                                    //     categories: response['dcs'],
                                    //     crosshair: true
                                    // },
                                    // yAxis: {
                                    //     min: 0,
                                    //     title: {
                                    //         text: 'Sales Amount (Ksh)'
                                    //     }
                                    // },
                                    // tooltip: {
                                    //     headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                    //     pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                    //         '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                                    //     footerFormat: '</table>',
                                    //     shared: true,
                                    //     useHTML: true
                                    // },
                                    // plotOptions: {
                                    //     column: {
                                    //         pointPadding: 0.0,
                                    //         borderWidth: 0
                                    //     }
                                    // },
                                    // series: [ {
                                    //     name: 'Dc-New Customers',
                                    //     colorByPoint: true,
                                    //     data: response['dcs_no_cutomers']

                                    // }]
                                    //  });
                                    //                                     });

                                                               //bar sales
                                   $(light_2).unblock();
                           },
                           error:function(data)
                           {
                            console.log(data);
                           }        

                            });
                        });
                        //fech initial analytics year
      
                      $( "#datepickerstart" ).datepicker( { dateFormat: 'yy-mm-dd' } );
                      $( "#datepickerend" ).datepicker( { dateFormat: 'yy-mm-dd' } );
                      $( "#datepicker" ).change(function() {});
                        
                        var url = base_url + "Manufacturer/fetchManufacturers";
                         var formData = {'access_token': localStorage.getItem('access_token')};

                        $.ajax({
                           type:'POST',
                           url: url,
                           data: formData,
                           success:function(data)
                           {
                            var datas = JSON.parse(data);
                            console.log(datas);
                            var model = $('#select_brand');
                            model.empty();
                            model.append("<option value='none'>Select Brand</option>");
                            $.each(datas, function (index, element) {
                                model.append("<option value='" + element.manufacturer_id+ "'>" + element.name + "</option>");
                            });
                           },
                           error:function(data)
                           {
                            console.log(data);
                           }        

                            });

                        $('#select_brand').change(function()
                        {
                         var brandid = $("#select_brand").val();
                    
                         var url3 = base_url + "analytics/periodic_brand_analysis";
                         var startdate = "";
                         var enddate = "";
                         var startdate = $("#datepickerstart").val();
                         var enddate = $("#datepickerend").val();
                         var formData = {'access_token': localStorage.getItem('access_token') , 'manufacturer_id':brandid ,'start_date':startdate,'end_date':enddate};
                        $.ajax({
                           type:'POST',
                           url: url3,
                           data: formData,
                           success:function(data)
                           {
console.log(data);
                            var response = JSON.parse(data);
                            console.log(response);
var brand_row=" <table class='table datatable-basic' id='example1'><thead><tr><th>Product</th><th>DC</th><th>Sku</th><th>No Of Items</th><th>Unit Cost</th><th>Total Sales</th><th>Time</th></tr></thead><tbody id='brandtablebody'>";

                                           $.each(response['brand_list'], function (k, v) {
                               brand_row+="<tr><td>"+v.product_name
+"</td><td>"+v.business_name
+"</td><td>"+v.sku_name
+"</td><td>"+v.no_items

+"</td><td>"+v.unit_cost
+"</td><td>"+v.total_cost
+"</td><td>"+v.time
+"</td></tr>";
                               
                            });
                             brand_row+="  </tbody></table>";

                              
                             
                           },
                           error:function(data)
                           {

                            console.log(data);

                           }        
                        });
                        
                        });


}); 
function round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
} 

 </script>


<!-- Modal  retailer distribution-->
<div class="modal fade" id="dcretailersdistribution" tabindex="-1" role="dialog" aria-labelledby="DC Retailer " aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DC -Retailer Distribution</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="containerdcretailersdistribution" style="width:100%; height:400px;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal  retailer distribution-->
<!-- Modal  order distribution-->
<div class="modal fade" id="dcordersdistribution" tabindex="-1" role="dialog" aria-labelledby="DC order Distribution" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DC - Order Distribution</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

         <div id="containerdcordersdistribution" style="width:100%; height:400px;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal  order distribution-->
<!-- Modal  sales distribution-->
<div class="modal fade" id="dcsalesdistribution" tabindex="-1" role="dialog" aria-labelledby="DC order Distribution" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DC -Sales Dstribution</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
         <div id="containerdcsalesdistribution" style="width:100%; height:400px;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal  sales distribution-->

            <!-- Main content -->
            <div class="content-wrapper">
                <!-- //customerdistribution modal-->

               <!--  ///customerdistribution modal -->

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
                        </div>

                        <div class="heading-elements">
                            <div class="heading-btn-group">
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                            </div>
                        </div>
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-gear position-left"></i>
                                    More Analytics
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="#"><i class="icon-user-lock"></i>Monthly</a></li>
                                    <li><a href="#"><i class="icon-statistics"></i>Daily</a></li>
                                    <li><a href="#"><i class="icon-statistics"></i>DC Monthly Comparisons</a></li>
                                    <li><a href="#"><i class="icon-statistics"></i>5 best Selling Items Pe DC</a></li>
                                    <!-- <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="icon-gear"></i> All settings</a></li> -->
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">

                   <!-- Support tickets -->
                            <div class="panel panel-flat">
                                <div class="panel-heading" >
                                    <h6 class="panel-title">
                                    <div >
                                      <div class="row">
                                       
                                      </div>
                                      <div class="row">
                                       <form class="heading-form" id="formsearchperiod" action="#">
                                          <div class="col-lg-3"><label>Select TimeFrame</label>
                                           <!--  <select class="form-control" id="select_timeframe">
                                           <option value="1">Custom<option>
                                          <option value="2">This Year<option>
                                          <option value="3">This Month<option>
                                          <option value="4">Last Year<option>
                                          <option value="5">Last Month<option>
                                          <option value="6">Custom<option>
                                        </select> -->
                                         </div>
                                         <div class="col-lg-3"><label>Start Date</label>
                                         <input type='text' class="form-control" id="datepickerstart" onkeypress="return false;"/></div>
                                          <div class="col-lg-3"> <label>End Date</label>
                                         <input type='text' class="form-control" id="datepickerend" onkeypress="return false;"/></div>
                                          <div class="col-lg-3">&nbsp;<br>
                                         <input type='submit' class="btn btn-flat form-control" id="loadperiodicgeneral" value="Load Report" /></div>
                                          
                                       
                                        
                                       
                                       </form>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="table-responsive">
                                   <!--  <table class="table table-xlg text-nowrap">
                                        <tbody>
                                            <tr>
                                                <td class="col-md-4">
                                                   <div class="media-left media-middle">
                                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-home"></i></a>
                                                    </div>

                                                    <div class="media-left">
                                                        <h5 class="text-semibold no-margin" id="selected_period_sales">
                                                            
                                                        </h5>
                                                        <small class="display-block no-margin">Total Sales</small>
                                                    </div>
                                                </td>
                                                  <td class="col-md-4">
                                                    <div class="media-left media-middle">
                                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-home2"></i></a>
                                                    </div>

                                                    <div class="media-left">
                                                        <h5 class="text-semibold no-margin" id="selected_period_dcs">
                                                            
                                                        </h5>
                                                        <small class="display-block no-margin">Total No Of DCs</small>
                                                    </div>
                                                </td>
                                                  <td class="col-md-4">
                                                    <div class="media-left media-middle">
                                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-cart"></i></a>
                                                    </div>

                                                    <div class="media-left">
                                                        <h5 class="text-semibold no-margin" id="selected_period_orders">
                                                            
                                                        </h5>
                                                        <small class="display-block no-margin">Total Orders</small>
                                                    </div>
                                                </td>

                                                <td class="col-md-3">
                                                    <div class="media-left media-middle">
                                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-user-plus"></i></a>
                                                    </div>

                                                    <div class="media-left">
                                                        <h5 class="text-semibold no-margin" id="new_customers">
                                                            
                                                        </h5>
                                                        <small class="display-block no-margin">Total New Customers</small>
                                                    </div>
                                                </td>

                                                <td class="col-md-3">
                                                    <div class="media-left media-middle">
                                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-user"></i></a>
                                                    </div>

                                                    <div class="media-left">
                                                        <h5 class="text-semibold no-margin" id="o_customers">
                                                            
                                                        </h5>
                                                        <small class="display-block no-margin">O.N Customers</small>
                                                    </div>
                                                </td>
                                                <td class="col-md-3">
                                                    <div class="media-left media-middle">
                                                        <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-xs btn-icon"><i class="icon-user-lock"></i></a>
                                                    </div>

                                                    <div class="media-left">
                                                        <h5 class="text-semibold no-margin" id="c_customers">
                                                            
                                                        </h5>
                                                        <small class="display-block no-margin">C.N Cutomers</small>
                                                    </div>
                                                </td>

                                                
                                            </tr>
                                        </tbody>
                                    </table>     -->
                                </div>

                                <div class="table-responsive">
                                    <!-- Rounded colored tabs -->
                    <div class="row">
                        

                        <div class="col-md-12">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title"></h6>
                                   <!--  <div class="heading-elements">
                                        <ul class="icons-list">
                                            <li><a data-action="collapse"></a></li>
                                            <li><a data-action="reload"></a></li>
                                            <li><a data-action="close"></a></li>
                                        </ul>
                                    </div> -->
                                </div>

                                <div class="panel-body">
                                    <div class="tabbable">
                                        <ul class="nav nav-tabs bg-slate nav-tabs-component nav-justified">
                                         
                                            <li><a href="#colored-rounded-justified-tab2" data-toggle="tab">All Customers</a></li>
                                            <li><a href="#colored-rounded-justified-tab3" data-toggle="tab">New DC Customers</a></li>
                                             <li><a href="#colored-rounded-justified-tab4" data-toggle="tab">Active Customers Per Dc</a></li>
                                              <li><a href="#colored-rounded-justified-tab5" data-toggle="tab">Customer Purchase Ranks</a></li>
                                            
                                        </ul>

                                        <div class="tab-content">
                                            <div class="tab-pane active" id="colored-rounded-justified-tab1">
                                             
                                            </div>

                                            <div class="tab-pane" id="colored-rounded-justified-tab2">
                                                 <div class="row">
          
          <div class="col-lg-12">

            <!-- Traffic sources -->
            <div class="panel panel-flat">
              <div class="panel-heading">
                <h6 class="panel-title">All Customers In The System</h6>
                <div class="heading-elements">
                </div>
              </div>
              <div class="panel-body" id="brandlistperformance">


               
                            
                      
              </div>
            </div>
          </div>
        </div>
                                            </div>

                                            <div class="tab-pane" id="colored-rounded-justified-tab3">
                                                 <!-- Basic datatable -->
                                                  <div class="panel panel-flat">
                                                      <div class="panel-heading">
                                                          <h5 class="panel-title">List Of New Retailers</h5>
                                                          <div class="heading-elements">
                                                              <!-- <ul class="icons-list">
                                                                  <li style="color: white;"><a  class='btn btn-sm btn-success'><i class='glyphicon glyphicon-gift'></i> See DC Distributions</a></a></li>

                                                              </ul> -->
                                                          </div>
                                                      </div>

                                                      <div class="panel-body" id="retailers_list_general">
                                                     
                                                      </div>
                                                  </div>
                        <!-- /basic datatable -->
                                                
                                            </div>

                                            <div class="tab-pane" id="colored-rounded-justified-tab4">
                                              <!--  <div id="dukapepe_dc_retailer_comparisons" style="width:100%; height:700px;"></div> -->
                                                <div class="panel panel-flat">
                                                      <div class="panel-heading">
                                                          <h5 class="panel-title">Top Selling Items</h5>
                                                          <div class="heading-elements">
                                                              <!-- <ul class="icons-list">
                                                                  <li style="color: white;"><a  class='btn btn-sm btn-success'><i class='glyphicon glyphicon-gift'></i> See DC Distributions</a></a></li>

                                                              </ul> -->
                                                          </div>
                                                      </div>

                                                      <div class="panel-body" id="topselling_list_general">
                                                     
                                                      </div>
                                                </div>
                                              <!-- /basic datatable -->
                                            </div>
                                            <div class="tab-pane" id="colored-rounded-justified-tab5">
                                                     <div class="panel panel-flat">
                                                      <div class="panel-heading">
                                                          <h5 class="panel-title">DC Customer And Sales Ranks</h5>
                                                          <div class="heading-elements">
                                                              <!-- <ul class="icons-list">
                                                                  <li style="color: white;"><a  class='btn btn-sm btn-success'><i class='glyphicon glyphicon-gift'></i> See DC Distributions</a></a></li>

                                                              </ul> -->
                                                          </div>
                                                      </div>

                                                      <div class="panel-body" id="dcranks_list_general">
                                                         <div class="row">
                                                                        <div class="col-lg-6">

                                                                          <!-- Traffic sources -->
                                                                          <div class="panel panel-flat">
                                                                            <div class="panel-heading">
                                                                              <h6 class="panel-title">Customer Reg Distributions</h6>
                                                                          
                                                                              
                                                                              
                                                                            </div>
                                                                            <div class="panel-body">
                                                                              <div id="container_values_bar" style="width:100%; height:400px;"></div>

                                                                              
                                                                            </div>
                                                                          </div>
                                                                        </div>
                                                                        <div class="col-lg-6">

                                                                          <!-- Traffic sources -->
                                                                          <div class="panel panel-flat">
                                                                            <div class="panel-heading">
                                                                              <h6 class="panel-title">Sales Distributions</h6>
                                                                              <div class="heading-elements">
                                                                              </div>
                                                                            </div>
                                                                            <div class="panel-body">
                                                                              <div id="container_percentages_pie" style="width:100%; height:500px;"></div>


                                                                             
                                                                                          
                                                                                    
                                                                            </div>
                                                                          </div>
                                                                        </div>

                                                      </div>
                                                      <div class="row">
                                                                        <div class="col-lg-12">

                                                                          <!-- Traffic sources -->
                                                                          <div class="panel panel-flat">
                                                                            <div class="panel-heading">
                                                                              <h6 class="panel-title">DC Sales Performances</h6>
                                                                          
                                                                              
                                                                              
                                                                            </div>
                                                                            <div class="panel-body" id="dc_sales_performances_list">

                                                                              
                                                                            </div>
                                                                          </div>
                                                                        </div>
                                                                       

                                                      </div>
                                                </div>
                                              <!-- /basic datatable -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /rounded colored tabs -->



                                </div>
                            </div>
                            <!-- /support tickets -->




                    <!-- Dashboard content -->
                   
                    <!-- /dashboard content -->


                    <!-- Footer -->
               <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
