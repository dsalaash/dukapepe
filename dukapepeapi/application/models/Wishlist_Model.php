<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Wishlist_Model
 *
 * @author mwamb
 */
class Wishlist_Model extends CI_Model{
         //put your code here
    public $wishlist_table="wishlist";
    
    public function add($data){
        $response=array();
        $count= $this->check_new($data);
     if($count==0){
        
        $this->db->insert($this->wishlist_table,$data);  
        $response['message']="Wishlist item added successfully.";
        $response['code']=1; 
     }
     else{
      $this->db->where('retailer_id',$data['retailer_id']);
      $this->db->where('stock_id',$data['stock_id']);
      $this->db->delete($this->wishlist_table);
      $response['message']="Item removed successfully from wish list.";
      $response['code']=0;    
         
     }
     
     return json_encode($response);
    }
    public function check_new($data){
        $this->db->select('wishlist_id');
        $this->db->where('retailer_id',$data['retailer_id']);
        $this->db->where('stock_id',$data['stock_id']);
        $this->db->from($this->wishlist_table);
        $count= $this->db->count_all_results();
        
        return $count;
    }
    public function fetch_all(){
       $this->db->select('wishlist_id,'.$this->wishlist_table.'.stock_id AS stock_id,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,unit_cost,quantity,wishlist.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight');
        $this->db->from($this->wishlist_table);
        $this->db->join('stock',$this->wishlist_table.'.stock_id=stock.stock_id');
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        //get reviews
        $reviews= $this->Stock_Model->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        $i++;
        }
        return json_encode($data); 
    }
    
    public function mywishlist($input_data){
       $this->db->select('wishlist_id,'.$this->wishlist_table.'.stock_id AS stock_id,stock.product_id AS product_id,stock.supplier_id AS supplier_id,stock.sku_id AS sku_id,unit_cost,quantity,wishlist.is_active AS is_active,'
                . 'product_name,product_description,sku_name,weight');
        $this->db->where($this->wishlist_table.'.retailer_id',$input_data['retailer_id']);
        $this->db->from($this->wishlist_table);
        $this->db->join('stock',$this->wishlist_table.'.stock_id=stock.stock_id');
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('skus','skus.sku_id=stock.sku_id');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        //get reviews
        $reviews= $this->Stock_Model->get_reviews($data[$i]['stock_id']);
        $data[$i]['reviews'] = $reviews;
        $i++;
        }
        return json_encode($data); 
    }
    
    public function status($data){
    $wishlist_id=$data['wishlist_id'];
    unset($data['wishlist_id']);
    $this->db->where('wishlist_id',$wishlist_id);
    $this->db->update($this->wishlist_table,$data);
    $response=array();
    $response['message']="Status toggled successfully.";
    $response['code']=1;

    return json_encode($response);
    }
}
