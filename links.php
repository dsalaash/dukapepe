<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="../assets/css/core.css" rel="stylesheet" type="text/css">
<link href="../assets/css/components.css" rel="stylesheet" type="text/css">
<link href="../assets/css/colors.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../assets/css/icons/fontawesome/css/all.css">

<!-- /global stylesheets -->

<!-- Core JS files -->
<script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script type="text/javascript" src="../assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/forms/selects/select2.min.js"></script>

<script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>

<script type="text/javascript" src="../assets/js/core/app.js"></script>

<script type="text/javascript" src="../assets/js/plugins/notifications/pnotify.min.js"></script>

<script type="text/javascript" src="../assets/js/pages/components_notifications_pnotify.js"></script>
<script type="text/javascript" src="../assets/js/load_image.js"></script>
<script type="text/javascript" src="../assets/js/configs.js"></script>

<script type="text/javascript" src="../assets/js/plugins/uploaders/fileinput.min.js"></script>

<script type="text/javascript" src="../assets/js/pages/uploader_bootstrap.js"></script>

<script type="text/javascript" src="../assets/js/plugins/notifications/bootbox.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/notifications/sweet_alert.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/daterangepicker.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/anytime.min.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/pickadate/picker.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/pickadate/picker.date.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/pickadate/picker.time.js"></script>
<script type="text/javascript" src="../assets/js/plugins/pickers/pickadate/legacy.js"></script>
<script type="text/javascript" src="../assets/js/date.js"></script>
<!-- <script type="text/javascript" src="serviceWorker.min.js"></script> -->


<script type="text/javascript" src="../assets/js/pages/picker_date.js"></script>
<!-- <script type="text/javascript" src="..assets/js/plugins/notifications/pnotify.min.js"></script> -->
    <script type="text/javascript" src="../assets/js/plugins/loaders/progressbar.min.js"></script>

<script type="text/javascript" src="../assets/js/pages/components_modals.js"></script>

<script type="text/javascript" src="../assets/js/core/libraries/jquery_ui/interactions.min.js"></script>

<script type="text/javascript" src="../assets/js/pages/form_select2.js"></script>
<script type="text/javascript" src="../assets/js/pages/invoice_archive.js"></script>
<link href="../assets/dist/js/select2.min.css" rel="stylesheet" />
<!-- <script src="../assets/dist/js/select2.full.js"></script> -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet">