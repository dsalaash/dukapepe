<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Suppliers
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Supplier extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    public function new_supplier()
    {
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    

            if($access_response['code']==1){

                $data['first_name'] = $this->input->post('first_name');   
                $data['last_name'] = $this->input->post('last_name');   
                $data['business_name'] = $this->input->post('business_name');   
                $data['phone'] = $this->input->post('phone');   
                $data['email'] = $this->input->post('email');   
                $data['location_name'] = $this->input->post('location_name');   
                $data['supplier_code'] = 'Dukapepe';   
                $data['latitude'] = $this->input->post('latitude'); 
                $data['longitude'] = $this->input->post('longitude'); 
                $response = $this->Supplier_Model->register_supplier($data);
                $supplierUrl = 'http://test.duka-pepe.com/ryda/rydaapi/index.php/dcs/new_supplier';
                // $supplierUrl = 'http://localhost/ryda/rydaapi/index.php/dcs/new_supplier';

                 // $supplierUrl = 'http://team.rydapepe.com/rydaapi/index.php/dcs/new_supplier';
                $dc_url = curl_init($supplierUrl);
                $dcdata = $this->Supplier_Model->get_supplier($data);
                // $post_dc = ($dcdata);
                //attach encoded JSON string to the POST fields
                curl_setopt($dc_url, CURLOPT_POSTFIELDS, $dcdata);
                //set the content type to application/json
                curl_setopt($dc_url, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                //return response instead of outputting
                curl_setopt($dc_url, CURLOPT_RETURNTRANSFER, true);

                //execute the POST request
                $result = curl_exec($dc_url);
                //close cURL resource
                curl_close($dc_url);
           }
            else{
               $response= json_encode($access_response);   
            }
           echo $result;
    }

    public function fetch(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $supplier_id= $this->input->post('supplier_id');
        $data['supplier_id'] = $supplier_id;
        $response= $this->Supplier_Model->get_details($data);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    public function fetch_all(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $response= $this->Supplier_Model->get_all();
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    
   public function update_profile(){
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
         $data['supplier_id'] = $this->input->post('supplier_id');   
         $data['first_name'] = $this->input->post('first_name');  
         $data['last_name'] = $this->input->post('last_name');
         $data['business_name'] = $this->input->post('business_name');
         $data['location_name'] = $this->input->post('location_name');
         $data['latitude'] = $this->input->post('latitude'); 
         $data['longitude'] = $this->input->post('longitude');
         $data['availability'] = $this->input->post('availability');   
         $data['email'] = $this->input->post('email');   
            
            
        $response= $this->Supplier_Model->update_profile($data);
        }
        else{
          $response= json_encode($access_response);   
        }
        echo $response;   
   } 
   
   public function update_supplier_payments(){ 
//       $access_token=$this->input->post('access_token');
//        $access_response=$this->Management_Model->validate_request($this->input->post($access_token));    
//        if($access_response['code']==1){
        $data= json_decode($this->input->post('order_info'),true);
        $response= $this->Supplier_Model->update_supplier_payments($data);
//        }
//        else{
//          $response= json_encode($access_response);   
//        }
        echo $response;  
   }
   
   
   public function generate_update_supplier_payments(){
//       $data['access_token'] = "asegvdrnhg";
       
       $other_info['invoice_no']='45tfr657u67y';
       $other_info['supplier_id'] = '4565';
       $other_info['total_cost'] = 870;
       $other_info['initiated_by']=6;
       $other_info['initiated_date'] = '20170502098734';
       $order_info=array();
       $order_info['orders']=array();
       $i=0;
       
        $ldata=array();
       while ($i<2){
           if($i==0){
       $ldata['so_id'] = 1;
       $ldata['invoice_no'] = '45tfr657u67y';
       $ldata['payment_status'] = 1;     
           }
           else{
       $ldata['so_id'] = 2;
       $ldata['invoice_no'] = '45tfr657u67y';
       $ldata['payment_status'] = 1;   
           }
           $i++;
         array_push($order_info['orders'], $ldata);  
       }
       
       $order_info['other_info'] = $other_info;
       $data['order_info'] = $order_info;
       
       echo json_encode($data);
       
   }
}
