<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class User extends CI_Controller{
    //put your code here
    
    public function __construct() {
        parent::__construct();
        
        $this->load->library('curl');
        $this->load->library('encryption');
        $this->load->library('randomizer');
        
    }
    public function getretaileragentlist()
{
  $result =$this->User_Model->getretaileragentlist();
      
      echo $result;
}
    //mult user log in
    public function get_all_system_users()
    {
      $result =$this->User_Model->get_all_system_users();
      
      echo $result;
    }
    public function get_all_active_agents()
    {
      $result =$this->User_Model->get_all_active_agents();
      echo $result;
    }
    public function get_all_deactivated_agents()
    {
      $result =$this->User_Model->get_all_deactivated_agents();
      echo $result;
    }
    public function linkclerk_to_dc()
    {
      $data['clerk'] = $this->input->post('clerk');
      $data['supplier'] = $this->input->post('supplier');
      $result =$this->User_Model->linkclerk_to_dc($data);;
      echo $result; 
    }

    public function get_all_clerks()
    {
       $result =$this->User_Model->get_all_clerks();
      
      echo $result;
    }
     public function linkedprimaries()
     {
      $data['user_id'] = $this->input->post('user_id');
      $result =$this->User_Model->linkedprimaries($data);;
      
      echo $result;
     }
    public function unlink_retailers()
    {
      $data['user_id'] = $this->input->post('user_id');
      $result = $this->User_Model->unlink_retailers($data);

      echo json_encode($result);
    }
     public function deactivateaccount()
     {
       $data['user_id'] = $this->input->post('user_id');
       $result =$this->User_Model->deactivateaccount($data);
       echo $result;
     }
    public function allprimariesfromdc()
    {
      $data['user_id'] = $this->input->post('user_id');
      $result =$this->User_Model->allprimariesfromdc($data);;
      
      echo $result;
    }
    public function agenttransactions()
    {
      $data['user_id'] = $this->input->post('user_id');
      $result =$this->User_Model->agenttransactions($data);;
      echo $result;
    }
    public function allprimarieslinkedtoagent()
    {
      $data['user_id'] = $this->input->post('user_id');
      $result =$this->User_Model->allprimarieslinkedtoagent($data);;
      
      echo $result;
    }
    public function getagentretailers()
    {
      $data['user_id'] = $this->input->post('retailer_id');
      $result =$this->User_Model->getagentretailers($data);
      echo $result;

    }
    public function linkprimaries()
    {
      $data['primaries'] = $this->input->post('primaries');
      $data['agent'] = $this->input->post('agent');
      $result =$this->User_Model->linkprimaries($data);;
      
      echo  $result;
    }
   
    public function transferlinkedprimaries()
    {
      $data['agent'] = $this->input->post('agent');
      $data['from'] = $this->input->post('from');
      $data['primaries'] = $this->input->post('primaries');
      $result =$this->User_Model->transferlinkedprimaries($data);;
      echo  $result;
    }
    
    
    public function assign_user_type()
    {
     $data['user_id'] = $this->input->post('user_id');
     $data['user_type_id'] = $this->input->post('user_type_id');
     $data['retailer_id'] = $this->input->post('retailer_id');
     
     $result['message'] = $this->User_Model->assign_user_type($data);
     echo json_encode($result);
    }
    //mult user
    public function register(){
     $first_name = $this->input->post('first_name');
     $last_name =  $this->input->post('last_name');
     $business_name = $this->input->post('business_name');
     $phone =  $this->input->post('phone');
     $email = $this->input->post('email');
     $location_name = $this->input->post('location_name');
     $latitude = $this->input->post('latitude');
     $longitude = $this->input->post('longitude');
     if($this->input->post('gender')!=null){
     $gender = $this->input->post('gender');
     }
     else{
      $gender="";   
     }
     if($this->input->post('id_number')!=null){
     $id_number = $this->input->post('id_number');
     }
     else{
      $id_number="";   
     }
     if($this->input->post('dob')!=null){
     $dob = $this->input->post('dob');
     }
     else{
      $dob="";   
     }
     if($this->input->post('user_type')!=null){
     $user_type=$this->input->post('user_type');
     }
     else{
     $user_type=3;    
     }
     $pass =  $this->input->post('password');
     $password = $this->encryption->encrypt($pass);

     $data=array();
      $data['first_name'] = $first_name;
      $data['last_name'] = $last_name;
      $data['phone'] = $phone;
      $data['email'] = $email;
      

      //array for validation and logins
      $login['phone'] = $phone;
      $login['password'] = $password;
      $login['user_type'] = $user_type;
      $login['is_active'] = 1;
      
      $validation['phone'] = $phone;
      $validation['activation_code'] = "default";
      $validation['is_active'] = 1;
      
     if($user_type==2){ // for directors
   $response= $this->Director_Model->register_director($data);
    }
     else if($user_type==4) { // customer care
        $data['gender'] =$gender;
        $data['id_number'] = $id_number;
        $data['dob'] = $dob;
        $response= $this->Customer_Care_Model->register_cc($data);
     } 
     else if($user_type==3){ // supplier
       $data['business_name'] = $business_name;
       $data['location_name'] = $location_name;
       $data['latitude'] = $latitude;
       $data['longitude'] = $longitude;
       
       //generate Random code
//       $supplier_code= $this->randomizer->getSupplierCode();
       $supplier_code="Dukapepe";
       $data['supplier_code'] = $supplier_code;
       $response = $this->Supplier_Model->register_supplier($data);
 
    
       if((json_decode($response,true)['code'])>0){               
        // get details
       $supplier_details = $this->Supplier_Model->get_details($data);
//       echo $supplier_details;
        //       get url
       $url_data['app'] = "new_dc";
       $get_url = $this->Supplier_Model->get_url($url_data);
    $this->curl->create($get_url);
    $this->curl->ssl(FALSE);
    $post_data = array ("key" => "1234","dc_data"=>$supplier_details);
    $json_output = $this->curl->simple_post($get_url, $post_data);
    $response = $json_output; 
     }
     
     }
     if(json_decode($response,true)['code']==1){
       $this->User_Model->save_logins($login);
       $this->User_Model->save_activation($validation);   
        }
     echo $response;
     }
     
     public function get_details(){
         
        $business_name = $this->input->post('business_name');
        $data['business_name'] = $business_name;
       $supplier_details = $this->Supplier_Model->get_details($data);
       
       echo $supplier_details;
       
     }
    public function update_director(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
           if($access_response['code']==1){
        $director_id= $this->input->post('director_id');
        $first_name = $this->input->post('first_name');
        $last_name =  $this->input->post('last_name');
        $phone = $this->input->post('phone');
        $email =  $this->input->post('email');

       $data=array();
       $data['director_id'] = $director_id;
       $data['first_name'] = $first_name;
       $data['last_name'] = $last_name;
       $data['phone'] = $phone;
       $data['email'] = $email;

      $response= $this->Director_Model->update_director($data);
      }
           else{
               $response= json_encode($access_response);   
           }
           echo $response;
    } 

    public function update_cc(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
           if($access_response['code']==1){
        $cc_id= $this->input->post('cc_id');
        $first_name = $this->input->post('first_name');
        $last_name =  $this->input->post('last_name');
        $phone = $this->input->post('phone');
        $email =  $this->input->post('email');
        $gender =  $this->input->post('gender');
        $id_number = $this->input->post('id_number');
        $dob =  $this->input->post('dob');

       $data=array();
       $data['cc_id'] = $cc_id;
       $data['first_name'] = $first_name;
       $data['last_name'] = $last_name;
       $data['gender'] = $gender;
       $data['id_number'] = $id_number;
       $data['dob'] = $dob;
       $data['phone'] = $phone;
       $data['email'] = $email;

      $response= $this->Customer_Care_Model->update_cc($data);
      }
           else{
               $response= json_encode($access_response);   
           }
           echo $response;
    } 

    public function update_supplier(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
           if($access_response['code']==1){
        $supplier_id= $this->input->post('supplier_id');
        $first_name = $this->input->post('first_name');
        $last_name =  $this->input->post('last_name');
        $phone = $this->input->post('phone');
        $email =  $this->input->post('email');
        $business_name =  $this->input->post('business_name');
        $latitude = $this->input->post('latitude');
        $longitude =  $this->input->post('longitude');

       $data=array();
       $data['supplier_id'] = $supplier_id;
       $data['first_name'] = $first_name;
       $data['last_name'] = $last_name;
       $data['business_name'] = $business_name;
       $data['latitude'] = $latitude;
       $data['longitude'] = $longitude;
       $data['phone'] = $phone;
       $data['email'] = $email;

      $response= $this->Supplier_Model->update_supplier($data);
      }
           else{
               $response= json_encode($access_response);   
           }
           echo $response;
    } 

    public function is_available(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
           if($access_response['code']==1){
        $supplier_id= $this->input->post('supplier_id');
        $availability= $this->input->post('availability');

        $data=array();

        $data['supplier_id'] = $supplier_id;
        $data['availability'] = $availability;

        $response = $this->Supplier_Model->is_available($data);
        }
           else{
               $response= json_encode($access_response);   
           }
           echo $response;

    }

    public function change_password(){
        $phone = $this->input->post('phone');
        $pass =  $this->input->post('password');
         if($this->isStrong($pass)){
        $password = $this->encryption->encrypt($pass);

        $data=array();
        $data['phone'] = $phone;
        $data['password'] = $password;

        $response= $this->User_Model->change_password($data);
         }
         else{
//             weak password?
        $res['message'] = "Weak Password, Please retry and set a strong password.Password must contain lowercase,uppercase, numbers and it must be atleast 8 characters long.";  
        $res['code'] = 0; 
        $response = json_encode($res); 
         }
        echo $response;
        }
    public function login(){
           if($this->input->post()!=null){
           $phone= $this->input->post('phone');
           $password= $this->input->post('password');
           if($phone!="" || $password!=""){
           $data=array();
           $data['phone']=$phone;
           $data['password']=$password;
           $response= $this->User_Model->login($data);

          echo $response;
       }
    }
    else{
        $this->load->view('login');
    }
   }

    public function logout(){
    $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
    $data=array();
    $phone= $this->input->post('phone');
    $access_token= $this->input->post('access_token');
    $data['phone']=$phone;
    $data['access_token']=$access_token;
    
    $response= $this->User_Model->logout($data);
    }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
}

public function updatepasswords(){
    
   $res = $this->User_Model->getoldpasswords();
   echo $res;
}


public function reset_password(){
    $data['phone'] = $this->input->post('phone');
    $response = $this->User_Model->reset_password($data);
    
    echo $response;
}

public function reset_pass2(){
 $data['access_token']  = $this->input->post('passcode');
 $password1 = $this->input->post('password1');
 $password2 = $this->input->post('password2');
 
 if($password1!=$password2){
  $res['message'] = "Password and repeat password are not the same. Kindly repeat again.";  
  $res['code'] = 0;
  $response = json_encode($res);
 }
 else{
     if($this->isStrong($password1)){
 $password = $this->encryption->encrypt($password1);
 $data['password'] = $password;
 
 $response = $this->User_Model->reset_pass2($data);
     }
     else{
    $res['message'] = "Weak Password, Please retry and set a strong password.Password must contain lowercase,uppercase, numbers and it must be atleast 8 characters long.";    
    $res['code'] = 0; 
    $response = json_encode($res);    
     }
 }
 
 echo $response;
}


   public function isStrong($password){
    $num=false;
    if(preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$#", $password)){
        $num=true;
    }   
    
    return $num;
   }
}
