<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Payment_Method_Model
 *
 * @author mwamb
 */
class Payment_Method_Model extends CI_Model{
    //put your code here
    public $payment_table="payment_methods";
    
    public function add($name){
     $response=array();
        $count= $this->check_new($name);
     if($count==0){
        $data=array();
        $data['name']=$name;
        $this->db->insert($this->payment_table,$data);  
        $response['message']="New payment method added successfully.";
        $response['code']=1; 
     }
     else{
      $response['message']="Error, failed to add new payment method, similar record exists.";
      $response['code']=0;    
         
     }
     
     return json_encode($response);
    }
    public function check_new($name){
        $this->db->select('id');
        $this->db->where('name',$name);
        $this->db->from($this->payment_table);
        $count= $this->db->count_all_results();
        
        return $count;
    }

    public function update($data){
     $response=array();
     $count= $this->checkUpdate($data);
     if($count==0){
     $id=$data['id'];
     unset($data['id']);
     $this->db->where('id',$id);
     $this->db->update($this->payment_table,$data);
     
     $response['message']="Payment method details edited successfully.";
     $response['code']=1; 
     }
     else{
     $response['message']="Error, failed to edit payment method, similar record exists.";
     $response['code']=0;     
     }
      
     return json_encode($response);
    }
    
    public function checkUpdate($data){
     $id=$data['id'];
     $name=$data['name'];
     
     $this->db->select('id');
     $this->db->where('name',$name);
     $this->db->where('id!=',$id);
     $this->db->from($this->payment_table);
     $count= $this->db->count_all_results();
     
     return $count;    
    }
    
    public function fetch($id){
     $this->db->select('id,name,is_active');
     $this->db->where('id',$id);
     $this->db->from($this->payment_table);
     $returned_data=$this->db->get();
     $data=$returned_data->result_array();
     
     return json_encode($data);
    }
    public function fetch_all($type){
     $this->db->select('id,name,is_active');
    
     $this->db->from($this->payment_table);
     $this->db->order_by('name ASC');
     $returned_data= $this->db->get();
     $data=$returned_data->result_array();
     
     return json_encode($data);  
    }
}
