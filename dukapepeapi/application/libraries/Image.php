<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Image
 *
 * @author mwamb
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Image{
    public $CI;
    public function __construct() { 
    $this->CI=& get_instance();
    }
    public function  upload_image(){
          $config['upload_path']   = './images/'; 
         $config['allowed_types'] = 'gif|jpg|png|JPG|PNG|jpeg|JPEG|GIF'; 
         $config['max_size']      = 1000; 
         $config['max_width']     = 10240; 
         $config['max_height']    = 7680;  
         $this->CI->load->library('upload', $config);
         if ( ! $this->CI->upload->do_upload('image_url')) {
            $error = array('error' => $this->CI->upload->display_errors()); 
            echo json_encode($error);
         }
			
         else { 
            $data =$this->CI->upload->data(); 
         } 
         $url=$data['file_name'];
         return $url;
      }
      
    public function  mobile_image(){
        define('UPLOAD_DIR', 'images/');
	$img = $_POST['image_url'];
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
        $url= uniqid() . '.png';
	$file = UPLOAD_DIR .$url;
	$success = file_put_contents($file, $data);
        return $url;
      }
}
