<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Management_Model
 *
 * @author mwamb
 */
class Management_Model extends CI_Model{
    //put your code here
    public $login="login_details";
    
    public function validate_request($access_token){
        $response=array();
        $this->db->select('id');
        $this->db->where('access_token',$access_token);
        $this->db->where('is_logged',1);
        $this->db->from($this->login);
        $count= $this->db->count_all_results();
        if($count>0){
        $response['message']="Access Granted";
        $response['code']=1;
        }
        else{
        $response['message']="Access Denied";
        $response['code']=-1;   
        }
        
        return $response;
    }
}
