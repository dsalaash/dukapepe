<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Duka pepe | DC Sales <?php echo date("Y-m-d"); ?></title>
<?php include("../links.php") ?>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet">
    <!-- /theme JS files -->
    <script>
      if(localStorage.getItem('access_token')=="")
          {
           window.location = "http://localhost/dukapepe_portal/dukapepe/";
          }
       
        $(document).ready(function () {
          submitreturn();
          loadsalestoday();
          $('.js-example-basic-multiple').select2();
                     $( "#datepickerstart" ).datepicker( { dateFormat: 'yy-mm-dd' } );
                      $( "#datepickerend" ).datepicker( { dateFormat: 'yy-mm-dd' } );
                      $( "#datepicker" ).change(function() {});
           
            $(".status-progress").hide();

              var formData = {'access_token': localStorage.getItem('access_token'),'phone':localStorage.getItem('phone'),'retailer':localStorage.getItem('retailer_id')};
                var url = base_url + "analytics/todaysalessuper";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                                                var user_row="<table class='table table-bordered' id='example' style='margin-bottom: 150px;''><thead> <tr><th style='width: 10px;'>#</th> <th>Retailer Phone</th><th>Retailer Name</th><th>Product</th><th>Agent Name</th></tr></thead><tbody id='userstablebody'>";   
                            
                           var no =0; 
                          parse= JSON.parse(data);
                          console.log(parse);
                            var total_sales=0;
                          var agent_name="";
                        $.each(parse['sales_list'], function(k, v) {
                               no=no+1;
                             
                           
                              user_row+="<tr><td style='width: fit-content;'>"+no+"</td><td>"+v.phone+"</td><td>"+v.retailer_name+"</td><td>";
                            
                                     user_row+="<table class='table table-bordered'><tr><th>Product</th><th>Sku Name</th><th>Quantity</th><th>Sell</th></tr>";
                                     
                                     var sum_sp=0;
                                     agent_name=parse['agent_name'+v.order_id];
                                      $.each(parse['retailer_products'+v.order_id], function(k2, v2) {
                                       
                                      user_row+="<tr><td style='min-width:100px; max-width:100px;'>"+v2.product_name+"</td><td> "+v2.sku_name+" </td><td>"+v2.quantity+"</td><td >"+v2.total_unit_cost+"</td><tr>";
                                      sum_sp=parseInt(sum_sp)+parseInt(v2.total_unit_cost);
                                       
                                      });
                                      user_row+="</table>";
                                      total_sales=parseInt(total_sales)+parseInt(sum_sp);
                                       
                                   
                                           

                              user_row+="</td><td>"+agent_name+"</td></tr>";
                               
                            }); 
                            user_row+="</tbody></table>"; 
                            document.getElementById('today_total_sales').innerHTML="";
                           $('#today_total_sales').append(total_sales); 
                           document.getElementById('userstablebody').innerHTML="";
                            $("#userstablebody").append(user_row);
                          
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );


                                        
                                },
                       error:function(data){

                                  console.log(data);
                       }        

        });
           ////////////////////
          
             
            //////////////////    
           
            });
function loadsalestoday()
{
   $("#formsearchdaysales").on("submit", function(e) 
           {
                  document.getElementById('today_total_sales').innerHTML="";
                            document.getElementById('userstablebody').innerHTML="";
                var formData = {'access_token': localStorage.getItem('access_token'),'phone':localStorage.getItem('phone'),'retailer':localStorage.getItem('retailer_id'),'date':$('#datepickerstart').val()};
                // alert($('#datepickerstart').val());
                var url = base_url + "analytics/todaysalessuper";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                                               var user_row="<table class='table table-bordered' id='example' style='margin-bottom: 150px;''><thead> <tr><th style='width: 10px;'>#</th> <th>Retailer Phone</th><th>Retailer Name</th><th>Product</th><th>Order Total</th><th>Agent Name</th></tr></thead><tbody >";   
                            
                           var no =0; 
                          parse= JSON.parse(data);
                          // console.log(parse);
                          var total_sales=0;
                          var agent_name="";
                          $.each(parse['sales_list'], function(k, v) {
                                    no=no+1;
                                    user_row+="<tr><td style='width: fit-content;'>"+no+"</td><td>"+v.phone+"</td><td>"+v.retailer_name+"</td><td>";
                                     user_row+="<table class='table table-bordered'><tr><th>Product</th><th>Sku Name</th><th>Quantity</th><th>Sell</th><th>Action</th></tr>";
                                     var sum_sp=0;

                                     agent_name=parse['agent_name'+v.order_id];
                                      $.each(parse['retailer_products'+v.order_id], function(k2, v2) {
                                        var order=v2.id_order.split("-");
                                       var totalcurrentloop=parseInt(Math.round(v2.total_unit_cost))+parseInt(Math.round(v2.total_unit_vat));
                                      user_row+="<tr><td style='min-width:100px; max-width:100px;'>"+v2.product_name+"</td><td> "+v2.sku_name+" </td><td>"+v2.quantity+"</td><td >"+(Math.round(totalcurrentloop*v2.quantity)).toLocaleString()+"</td><td><a class='btn btn-success' href='javascript: returnitem("+order[0]+","+order[1]+","+v2.stock_id+","+v2.quantity+ ")'>Returned</a></td><tr>";
                                      sum_sp=parseInt(sum_sp)+parseInt(totalcurrentloop*v2.quantity);
                                        
                                      });
                                      user_row+="</table>";
                                      total_sales=parseInt(total_sales)+parseInt(sum_sp);
                                      console.log(total_sales);
                                      user_row+="</td><td>"+sum_sp+"</td><td>"+agent_name+"</td></tr>";
                               
                            });  

                        user_row+="</tbody></table>"; 
                           
                           $('#today_total_sales').append(total_sales.toLocaleString()); 
                         
                            $("#userstablebody").append(user_row);
                          
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );


                                        
                                },
                       error:function(data){

                                  console.log(data);
                       }        

        });
           });
}
                function linktoprimary(id)
                {
                  
                var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id};
                console.log(formData);
                var url = base_url + "user/allprimariesfromdc";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var user_row="";
                          $.each(JSON.parse(data), function(k, v) {
                                  user_row+="<option value='"+v.retailer_id+"'>"+v.retailer_name+"("+v.phone+")"+"</option>";
                               
                            });  
                           user_row+="";
                           document.getElementById('primaries').innerHTML="";
                           $("#primaries").append(user_row);
                           $("#agent").val(id);
                           $("#modallinks").modal();

                                        
                                },
                             error:function(data){

                                 new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

                   });
                }
                function viewtransactions(agent)
                {
                   var formData = {'access_token': localStorage.getItem('access_token'),'user_id':agent,'user_type_id':6};
                   var url = base_url + "user/agenttransactions";
                   $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                          var user_row="";
                          var pno=0;
                          $.each(JSON.parse(data), function(k, v) {
                            pno=pno+1;
                                  user_row+="<tr><td>"+pno+"</td><td>"+v.retailer_name+"</td><td>"+v.phone+"</td><td>"+v.dc+"</td></tr>";
                               
                            });  
                           user_row+="";
                           document.getElementById('agenttransactions').innerHTML="";
                           $("#agenttransactions").append(user_row);
                           $("#modaltransactions").modal();
                           $("#example3").DataTable();
                            },
                          error:function(data){
                               new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

        });

                }
                  function transferprimaries(id)
                
                {
                  
                var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id};
                console.log(formData);
                var url = base_url + "user/allprimarieslinkedtoagent";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var json= JSON.parse(data);

                        
                  console.log(json['fromname']);
                       var user_row ="";
                          $.each(json['primaries'], function(k, v) {
                                  user_row+="<option value='"+v.retailer_id+"'  selected>"+v.retailer_name+"("+v.phone+")"+"</option>";
                                  
                               
                            });  
                          
                           document.getElementById('primariestotransfer').innerHTML="";
                           $("#primariestotransfer").append(user_row);
                           $("#primariestotransfer").select2();
                          

                           var user_row1 ="";
                          $.each(json['to'], function(k, v) {
                                  user_row1+="<option value='"+v.retailer_id+"'>"+v.retailer_name+"("+v.phone+")"+"</option>";
                                  
                               
                            });  
                          
                           document.getElementById('to').innerHTML="";
                           $("#to").append(user_row1);
                           $("#primariestotransfer").select2();
                           $("#modallinkstransfer").modal();
                           $("#fromname").val(json['fromname']);
                           $("#fromid").val(json['fromid']);

                                        
                                },
                             error:function(data){

                                 new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

                   });
                }
                function unlinkprimary(id)
                {
                    var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':7};
                     var url = base_url + "user/assign_user_type";
                     $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         new PNotify({
                                    text: data,
                                    addclass: 'bg-success'
                                });
                                        
                                },
                       error:function(data){

                                  console.log(data);
                       }        

                        });
                }
                function returnitem(order1,order2,product,quantity)
                {
                  
                  // alert("Order"+order+"Product"+product+"quantity"+quantity);
                  $("#modalmakereeturn").modal('show');
                  $("#return_product").val(product);
                  $("#return_order").val(order1+"-"+order2);
                  $("#return_quantity").val(quantity);
                  $("#access_token_return").val(localStorage.getItem("access_token"));

                }
                    function submitreturn() {
            $('#formreturn').submit(function (e) {
                e.preventDefault();
                // $("#submit").addClass("disabled");
                $(".status-progress-edit-vat").show();
                $.ajax({
                    url: base_url + "/order/returnitem",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        var parse = JSON.parse(data);
                        if(parse['code']==1)
                        {
                           $("#formreturn").trigger('reset');
                           $("#modalmakereeturn").modal('hide');
                           $("#formsearchdaysales").trigger('submit');

                        }
                        else if(parse['code']==0)
                        {

                        }
                    },
                    error: function (data) {
                      console.log(data);
                    }
                });
            });
        }
                 function viewagentsales(id)
              
                {
                  var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':6};
                var url = base_url + "user/assign_user_type";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var msg=   JSON.parse(data);
                         new PNotify({
                                    text: msg['message'],
                                    addclass: 'bg-success'
                                });
                                        
                                },
                       error:function(data){
                               new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

        });
                }
                 function viewagentprimaries(id)
                {
                  var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':6};
                var url = base_url + "user/linkedprimaries";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                          var user_row="";
                          var pno=0;
                          $.each(JSON.parse(data), function(k, v) {
                            pno=pno+1;
                                  user_row+="<tr><td>"+pno+"</td><td>"+v.retailer_name+"</td><td>"+v.phone+"</td><td>"+v.dc+"</td></tr>";
                               
                            });  
                           user_row+="";
                           document.getElementById('primariestable').innerHTML="";
                           $("#primariestable").append(user_row);
                            $("#modalprimaries").modal();
                            $("#example2").DataTable();
                        
                                        
                                },
                       error:function(data){
                               new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

        });
                }
               
               
                
                
               

    </script>

</head>

<body>

<?php include("../topbar.php") ?>


<!-- Page container -->
<div class="page-container">
   <!-- Vertical form modal -->
                        <div id="modalprimaries" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Sales</h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="display nowrap" style="width:100%" id="example2" >
                        <thead>
                        <tr>

                            <th>#</th>
                            <th>Name</th>
                            <th>Phone</th>
                              <th>Distribution Center</th>
                            
                        </tr>
                        </thead>
                        <tbody id="primariestable">
                            
                        </tbody>

                    </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->
                        <!-- Vertical form modal -->
                        <div id="modaltransactions" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title" id="agentnametransaction">Agent Transactions</h5>
                                    </div>
                                    <div class="modal-body">
                                        <table class="display nowrap" style="width:100%" id="example3">
                        <thead>
                        <tr>
                            <th>#Order Id</th>
                            <th>Retailer</th>
                            <th>Phone</th>
                            <th>DC</th>
                            <th>Distribution Center</th>
                        </tr>
                        </thead>
                        <tbody id="agentnametransactions">
                            
                        </tbody>

                    </table>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                     <!-- Vertical form modal -->
                <div id="modalmakereeturn" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Update Stock Details</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formreturn"
                                  enctype="multipart/form-data">
                                
                                <input type="hidden" name="access_token_return" id="access_token_return"/>
                                <div class="modal-body">
                                     <div class="form-group">
                                       <label>Product: </label>
                                       <input type="text" name="return_product" class="form-control" id="return_product"/>
                                    </div>
                                    <div class="form-group">
                                       <label>Order Id: </label>
                                       <input type="text" name="return_order" class="form-control" id="return_order"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Quantity To reduce: </label>
                                        <input type="text" name="return_quantity" class="form-control" id="return_quantity"/>
                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Submit Return
                                    </button>
                                    <img class="text-center center-block status-progress-update-price"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->
                         <!-- Vertical form modal -->
                        <div id="modallinkstransfer" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Primary Customers' Transfer</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formtransferprimaries" enctype="multipart/form-data">
                                        <input type="hidden" name="agent" id="agent" value="" />
                                        <input type="hidden" name="access_token" id="catToken"/>
                                        <div class="modal-body">
                                          <div class="form-group">
                                            <label>From:</label>
                                                 <input type="text" id="fromname" value="" class="form-control">
                                                 <input type="text" id="fromid" value="" class="form-control">
                                            </div>

                                          <div class="form-group">
                                            <label>To:</label>
                                                 <select class="js-example-basic-multiple" id="to" name="to">
                                                 </select>
                                            </div>
                                            <div class="form-group">
                                              <label>Transfers:</label>
                                                 <select class="js-example-basic-multiple" id="primariestotransfer" name="primariestotransfer[]" multiple="multiple">
                                                 </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Complete Transfer</button>
                                           
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->

                <!-- /user menu -->

                     <?php include("../sidebar.php") ?>
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Dukapepe Daily DashBoard For Sale</span>
                        </h4>
                    </div>

                </div>


            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">
                <div class="content clearfix">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">Sales</h5>
                                <div class="heading-elements" id="today_total_sales" style="font-size: 30px; color:green;"">
                                       
                                </div>
                            </div>

                            <div class="panel-body table-responsive">
                               <div class="row">
                                       <form class="heading-form" id="formsearchdaysales" action="#">
                                         <!--  <div class="col-lg-3"><label>Select TimeFrame</label> -->
                                           <!--  <select class="form-control" id="select_timeframe">
                                           <option value="1">Custom<option>
                                          <option value="2">This Year<option>
                                          <option value="3">This Month<option>
                                          <option value="4">Last Year<option>
                                          <option value="5">Last Month<option>
                                          <option value="6">Custom<option>
                                        </select> -->
                                       <!--   </div> -->
                                         <div class="col-lg-3"><label>Day</label>
                                         <input type='text' class="form-control" id="datepickerstart" onkeypress="return false;" value="<?php  echo date('Y-m-d'); ?>" /></div>
                                         <!--  <div class="col-lg-3"> <label>End Date</label>
                                         <input type='text' class="form-control" id="datepickerend" onkeypress="return false;"/></div> -->
                                          <div class="col-lg-3">&nbsp;<br>
                                         <input type='submit' class="btn btn-success form-control" id="loadperiodicgeneral" value="Load Report" /></div>
                                          
                                       
                                        
                                       
                                       </form>
                                        </div>
                                <!--<a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Orders</a>-->
                                <hr/>
                                <hr/>
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            
<div class="panel " id="userstablebody">
                   


                   <!--  <table class="table datatable-basic" id="example">
                        <thead>
                        <tr>

                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Approved</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody >
                            
                        </tbody>

                    </table> -->
                </div>

                          
                      
                    
                    </div>
                        </div>
                    </div>
                </div>
                    

                <!-- Basic datatable -->
                
                <!-- /basic datatable -->

              




               
               


               


                <!-- Footer -->
                <div class="footer text-muted">
                    <?php include("../footer.php") ?>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
    <script type="text/javascript">
      
       $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
    </script>
   

</div>
<!-- /page container -->

</body>
</html>
