<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka pepe | categories</title>

        <?php include("../links.php") ?>
        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search Category:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });

                if (localStorage.getItem('code') == 1) {
                    loadCategory();
                } else {

                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });

                }

                update();
                AddCategory();
                console.log(localStorage);

            });
            function loadCategory() {
                console.log(localStorage.getItem('access_token'));
                var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "category/fetch";
                $('#example').DataTable({
                    "ajax": {
                        "url": url,
                        "data": formData,
                        "type": "post",
                        "dataSrc": function (json) {
                            return json;
                        },
                        "processing": true,
                        "serverSide": true,
                        "pagingType": "simple",
                        language: {
                            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                        }

                    }, "columns": [
                        {"data": "image_url", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                return '<a href="javascript: subcategorys(' + full.category_id + ')"><img src="' + data + '" class="img-bordered img-bordered-orange" width="70" height="70" alt=""></a>';
                            }},
                        {"data": "category_name"},
                        {"data": "category_description"},
                        {"data": "is_active", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                if (data == 1) {
                                    return '<a href="javascript: deactivateCategory(' + full.category_id + ')" class="btn btn-xs btn-warning"><i class="icon-lock2"></i>De activate</a>';

                                } else {
                                    return '<a href="javascript: activateCategory(' + full.category_id + ')" class="btn btn-xs btn-success"><i class="icon-unlocked2"></i>Activate</a>';
                                }

                            }},
                        {"data": "category_id", orderable: false, searchable: false, render: function (data, type, full, meta) {



                                var links = '<ul class="icons-list"><li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>\n\
                                        <ul class="dropdown-menu dropdown-menu-right">\n\
<li><a href="javascript: ajaxmodaledit(' + data + ')" >edit</a></li>\n\
\n\<li><a onclick="return confirm(\'Are you sure you want to delete this?\')" href="javascript: ajaxdelete(' + data + ')" >delete</a></a></li>\n\
\n\
\n\
</ul></li></ul>';
                                return links;

                            }}
                    ]
                });

            }


            function loadCategoryUpdate() {
                console.log(localStorage.getItem('access_token'));
                var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "category/fetch";
                $('#example').DataTable({
                    "destroy": true,
                    "ajax": {
                        "url": url,
                        "data": formData,
                        "type": "post",
                        "dataSrc": function (json) {
                            return json;
                        },
                        "processing": true,
                        "serverSide": true,
                        "pagingType": "simple",
                        "language": {
                            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                        }

                    }, "columns": [
                        {"data": "image_url", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                return '<a href="javascript: subcategorys(' + full.category_id + ')"><img src="' + data + '" class="img-bordered img-bordered-orange" width="70" height="70" alt=""></a>';
                            }},
                        {"data": "category_name"},
                        {"data": "category_description"},
                        {"data": "is_active", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                if (data == 1) {
                                    return '<a href="javascript: deactivateCategory(' + full.category_id + ')" class="btn btn-xs btn-warning"><i class="icon-lock2"></i>De activate</a>';

                                } else {
                                    return '<a href="javascript: activateCategory(' + full.category_id + ')" class="btn btn-xs btn-success"><i class="icon-unlocked2"></i>Activate</a>';
                                }

                            }},
                        {"data": "category_id", orderable: false, searchable: false, render: function (data, type, full, meta) {

                                var links = '<ul class="icons-list"><li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>\n\
                                        <ul class="dropdown-menu dropdown-menu-right">\n\
<li><a href="javascript: ajaxmodaledit(' + data + ')" >edit</a></li>\n\
\n\<li><a onclick="return confirm(\'Are you sure you want to delete this?\')" href="javascript: ajaxdelete(' + data + ')" >delete</a></a></li>\n\
\n\
\n\
</ul></li></ul>';
                                return links;

                            }}
                    ]
                });

            }


            function ajaxdelete(id) {
                $(".status-progress").show();
                $(".status-progress-add").hide();
                var url = base_url + "category/delete";
                var formData = {
                    'category_id': id,
                    'access_token': localStorage.getItem('access_token')
                };

                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    new PNotify({
                        text: obj['message'],
                        addclass: 'bg-success'
                    });

                    $(".status-progress").hide();
                    loadCategoryUpdate();

                });
            }


            function subcategorys(id) {
                $(".status-progress").show();
                $(".status-progress-add").hide();
                var url = base_url + "sub_category/fetch";
                var formData = {
                    'category_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                var trHTML = '';
                $("#tBody").empty();
                localStorage.setItem('category_id', id);
                console.log(localStorage.getItem('access_token'));
//                var url2 = 'http://localhost/dukapep/';
                var url2 = 'http://duka-pepe.softikoda.com/';
                window.location = url2 + "cc/subcategory_1.php";
//                $.post(url, formData, function (json) {
//                    obj = JSON.parse(json);
//                    var count = obj.length;
//                    for (i = 0; i < count; i++) {
//                        var obj_data = obj[i];
//                        console.log(obj_data['category_name']);
//                        trHTML +=
//                                '<tr><td>'
//                                + obj_data['sub_category']
//                                + '</td></tr>';
//                    }
//                    $('#tBody').append(trHTML);
//                    $(".status-progress").hide();
//                    $('#MyModalCat').modal('show');
//                });
            }

            function ajaxmodaledit(id) {
                $(".status-progress").show();
                $(".status-progress-add").hide();
                var url = base_url + "category/getProductDetails";
                var formData = {
                    'category_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];
                        // console.log(obj_data['category_name']);
                        $('#catToken').val(localStorage.getItem('access_token'));
                        $('#catId').val(obj_data['category_id']);
                        $('#catName').val(obj_data['category_name']);
                        $('#catDescription').val(obj_data['category_description']);
                    }
                    $(".status-progress").hide();
                    $('#modalCat').modal('show');
                });

            }


            function ajaxmodaladd() {
                $(".status-progress-add").hide();
                $('#catToken2').val(localStorage.getItem('access_token'));
                $('#modalCatAdd').modal('show');
            }

            function AddCategory() {
                $('#formcategoryAdd').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "category/add ",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $("#submit").removeClass("disabled");
                            $(".status-progress-add").hide();
                            loadCategoryUpdate();
                            $('#modalCatAdd').modal('hide');
                            var data = JSON.parse(data);
                            if (data['code'] == 1) {
                                new PNotify({
                                    text: data['message'],
                                    addclass: 'bg-success'
                                });
                            } else {
                                new PNotify({
                                    text: data['message'],
                                    addclass: 'bg-success'
                                });
                            }

                        },
                        error: function () {}
                    });
                });
            }

            function update() {
                $('#formcategory').submit(function (e) {
                    e.preventDefault();
                    $("#submit").addClass("disabled");
                    $(".status-progress").show();
                    $(".status-progress-add").show();
                    $.ajax({
                        url: base_url + "category/update",
                        type: "POST",
                        data: new FormData(this),
                        //Setting these to false because we are sending a multipart request
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            // $('#modalCat').modal('show');
                            $("#submit").removeClass("disabled");
                            $(".status-progress").hide();
                            $(".status-progress-add").hide();
                            loadCategoryUpdate();
                            $('#modalCat').modal('hide');
                            var data = JSON.parse(data);
                            if (data['code'] == 1) {
                                new PNotify({
                                    text: data['message'],
                                    addclass: 'bg-success'
                                });
                            } else {
                                new PNotify({
                                    text: data['message'],
                                    addclass: 'bg-success'
                                });
                            }

                            // console.log(data);
                        },
                        error: function () {}
                    });
                });
            }

            function activateCategory(id) {
                $(".status-progress").show();
                var url = base_url + "/category/status";
                var formData = {
                    'category_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 1
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                        text: data['message'],
                        addclass: 'bg-success'
                    });
                    loadCategoryUpdate();
                    $(".status-progress").hide();
                });
            }

            function deactivateCategory(id) {
                $(".status-progress").show();
                var url = base_url + "category/status";
                var formData = {
                    'category_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 0
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                        text: data['me5ssage'],
                        addclass: 'bg-success'
                    });
                    loadCategoryUpdate();
                    $(".status-progress").hide();
                });
            }

        </script>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->

                        <!-- /user menu -->
                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Categories</span></h4>
                            </div>
                            <div class="heading-elements">
                                 <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>Add a Category</a>
                            </div>

                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                                <li class="active">Categories</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <!-- <div class="panel-heading">
                                <h5 class="panel-title">Categories</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>

                                    </ul>
                                </div>
                            </div> -->

                            <div class="panel-body">
                               
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            

                            <table class="table" id="example">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Category name</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                        </div>
                        <!-- /basic datatable -->

                        <!-- Vertical form modal -->
                        <div id="modalCat" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Category form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formcategory" enctype="multipart/form-data">
                                        <input type="hidden" name="category_id" id="catId"/>
                                        <input type="hidden" name="access_token" id="catToken"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Category name:</label>
                                                <input required name="category_name" id="catName" class="form-control" placeholder="Category name" />

                                            </div>
                                            <div class="form-group">

                                                <label>Description:</label>
                                                <textarea required name="category_description" id="catDescription" class="form-control" placeholder="Description" ></textarea>

                                            </div>
                                            <div class="form-group">

                                                <label>Upload Image:</label>
                                                <input type="file" name="image_url" class="file-input" data-show-caption="true" data-show-upload="false">

                                            </div>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Update</button>
                                            <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <!-- Vertical form modal -->
                        <div id="modalCatAdd" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Category form</h5>
                                    </div>
                                    <form role="form" class="form-validate" method="POST" id="formcategoryAdd" enctype="multipart/form-data">

                                        <input type="hidden" name="access_token" id="catToken2"/>
                                        <div class="modal-body">
                                            <div class="form-group">

                                                <label>Category name:</label>
                                                <input required name="category_name" id="catName" class="form-control" placeholder="Category name" />

                                            </div>
                                            <div class="form-group">

                                                <label>Description:</label>
                                                <textarea required name="category_description" id="catDescription" class="form-control" placeholder="Description" ></textarea>

                                            </div>
                                            <div class="form-group">

                                                <label>Upload Image:</label>
                                                <input type="file" name="image_url" class="file-input" data-show-caption="true" data-show-upload="false">

                                            </div>

                                        </div>
                                        <div class="modal-footer">

                                            <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                            <img class="text-center center-block status-progress-add"  src="../assets/loader/loader.gif"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->

                        <!-- Vertical form modal -->
                        <div id="MyModalCat" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Sub categories</h5>
                                    </div>

                                    <div class="modal-body">
                                        <table class="table">

                                            <thead>
                                                <tr>

                                                    <th>Subcategory</th>

                                                </tr>
                                            </thead>
                                            <tbody id="tBody"></tbody>

                                        </table>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->


                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
