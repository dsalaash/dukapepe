<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka pepe | Payments</title>

        <?php include("../links.php") ?>
        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {
                $(".status-progress").hide();
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search payments:</span> _INPUT_',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                    },
                    drawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                    },
                    preDrawCallback: function () {
                        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                    }
                });

                if (localStorage.getItem('code') == 1) {
//                    loadSuppliers();
                } else {

                    new PNotify({
                        title: 'Primary notice',
                        text: 'Error: You are not authorised to view this page.',
                        addclass: 'bg-warning'
                    });

                }

            });
            function loadSuppliers() {
                console.log(localStorage.getItem('access_token'));
                var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "supplier/fetch_all";
                $('#example').DataTable({
                    "destroy": true,
                    "ajax": {
                        "url": url,
                        "data": formData,
                        "type": "post",
                        "dataSrc": function (json) {
                            return json;
                        },
                        "processing": true,
                        "serverSide": true,
                        "pagingType": "simple",
                        language: {
                            paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                        }

                    }, "columns": [
                        {"data": "first_name"},
                        {"data": "business_name"},
                        {"data": "phone"},
                        {"data": "email"},
                        {"data": "location_name"},
                        {"data": "availability", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                if (data == 1) {
                                    return '<span class="label label-primary">Available</span>';
                                } else {
                                    return '<span class="label label-primary">Not available</span>';
                                }

                            }},
                        {"data": "is_active", orderable: false, searchable: false, render: function (data, type, full, meta) {
                                if (data == 1) {
                                    return '<a href="javascript: deactivateSupplier(' + full.supplier_id + ')" class="btn btn-xs btn-info"><i class="icon-lock2"></i>De activate</a>';

                                } else {
                                    return '<a href="javascript: activateSupplier(' + full.supplier_id + ')" class="btn btn-xs btn-warning"><i class="icon-unlocked2"></i>Activate</a>';
                                }

                            }},
                        {"data": "supplier_id", orderable: false, searchable: false, render: function (data, type, full, meta) {

                                return '<a href="javascript: ajaxmodaledit(' + data + ')" class="btn btn-xs btn-primary"><i class="icon-pencil5"></i>Details</a>';

                            }}
                    ]
                });

            }

            function ajaxmodaledit(id) {
                $(".status-progress").show();
                var url = base_url + "/supplier/fetch";
                var formData = {
                    'supplier_id': id,
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    obj = JSON.parse(json);
                    var count = obj.length;
                    for (i = 0; i < count; i++) {
                        var obj_data = obj[i];
                        console.log(obj_data['unit_id']);
                        document.getElementById("firstname").innerHTML = obj_data['first_name'];
                        document.getElementById("lastname").innerHTML = obj_data['last_name'];
                        document.getElementById("businessname").innerHTML = obj_data['business_name'];
                        document.getElementById("phone").innerHTML = obj_data['phone'];
                        document.getElementById("email").innerHTML = obj_data['email'];
                        document.getElementById("locationname").innerHTML = obj_data['location_name'];
                        document.getElementById("latitude").innerHTML = obj_data['latitude'];
                        document.getElementById("longitude").innerHTML = obj_data['longitude'];
                        if (obj_data['availability'] == 1) {
                            document.getElementById("availability").innerHTML = '<span class="label label-primary">Available</span>';
                        } else {
                            document.getElementById("availability").innerHTML = '<span class="label label-primary">Not available</span>';
                        }
                    }
                    $(".status-progress").hide();
                    $('#modalCat').modal('show');
                });

            }

            function activateSupplier(id) {
                $(".status-progress").show();
                var url = base_url + "/supplier/status";
                var formData = {
                    'supplier_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 1
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                        title: 'Update Notice',
                        text: data['message'],
                        addclass: 'bg-info'
                    });
                    loadSuppliers();
                    $(".status-progress").hide();
                });
            }

            function deactivateSupplier(id) {
                $(".status-progress").show();
                var url = base_url + "/supplier/status";
                var formData = {
                    'supplier_id': id,
                    'access_token': localStorage.getItem('access_token'),
                    'is_active': 0
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                        title: 'Update Notice',
                        text: data['message'],
                        addclass: 'bg-warning'
                    });
                    loadSuppliers();
                    $(".status-progress").hide();
                });
            }

        </script>

    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->
                       
                        <!-- /user menu -->
                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Payments</span></h4>
                            </div>

                        </div>

                        <div class="breadcrumb-line">
                            <ul class="breadcrumb">
                                <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                                <li class="active">Payments</li>
                            </ul>

                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title">Payments</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
<!--                                <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add a SKU</a>-->
                                <!--<img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>-->
                            </div>

                            <table class="table table-lg invoice-archive">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Period</th>
                                        <th>Issued to</th>
                                        <th>Status</th>
                                        <th>Issue date</th>
                                        <th>Due date</th>
                                        <th>Amount</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>#0025</td>
                                        <td>February 2015</td>
                                        <td>
                                            <h6 class="no-margin">
                                                <a href="#">Rebecca Manes</a>
                                                <small class="display-block text-muted">Payment method: Skrill</small>
                                            </h6>
                                        </td>
                                        <td>
                                            <select name="status" class="select" data-placeholder="Select status">
                                                <option value="overdue">Overdue</option>
                                                <option value="hold" selected="selected">On hold</option>
                                                <option value="pending">Pending</option>
                                                <option value="paid">Paid</option>
                                                <option value="invalid">Invalid</option>
                                                <option value="cancel">Canceled</option>
                                            </select>
                                        </td>
                                        <td>
                                            April 18, 2015
                                        </td>
                                        <td>
                                            <span class="label label-success">Paid on Mar 16, 2015</span>
                                        </td>
                                        <td>
                                            <h6 class="no-margin text-bold">$17,890 <small class="display-block text-muted text-size-small">VAT $4,890</small></h6>
                                        </td>
                                        <td class="text-center">
                                            <ul class="icons-list">
                                                <li><a href="#" data-toggle="modal" data-target="#invoice"><i class="icon-file-eye"></i></a></li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-file-text2"></i> <span class="caret"></span></a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="#"><i class="icon-file-download"></i> Download</a></li>
                                                        <li><a href="#"><i class="icon-printer"></i> Print</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#"><i class="icon-file-plus"></i> Edit</a></li>
                                                        <li><a href="#"><i class="icon-cross2"></i> Remove</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>#0024</td>
                                        <td>February 2015</td>
                                        <td>
                                            <h6 class="no-margin">
                                                <a href="#">James Alexander</a>
                                                <small class="display-block text-muted">Payment method: Wire transfer</small>
                                            </h6>
                                        </td>
                                        <td>
                                            <select name="status" class="select" data-placeholder="Select status">
                                                <option value="overdue">Overdue</option>
                                                <option value="hold">On hold</option>
                                                <option value="pending">Pending</option>
                                                <option value="paid" selected="selected">Paid</option>
                                                <option value="invalid">Invalid</option>
                                                <option value="cancel">Canceled</option>
                                            </select>
                                        </td>
                                        <td>
                                            April 17, 2015
                                        </td>
                                        <td>
                                            <span class="label label-warning">5 days</span>
                                        </td>
                                        <td>
                                            <h6 class="no-margin text-bold">$2,769 <small class="display-block text-muted text-size-small">VAT $2,839</small></h6>
                                        </td>
                                        <td class="text-center">
                                            <ul class="icons-list">
                                                <li><a href="#" data-toggle="modal" data-target="#invoice"><i class="icon-file-eye"></i></a></li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-file-text2"></i> <span class="caret"></span></a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="#"><i class="icon-file-download"></i> Download</a></li>
                                                        <li><a href="#"><i class="icon-printer"></i> Print</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#"><i class="icon-file-plus"></i> Edit</a></li>
                                                        <li><a href="#"><i class="icon-cross2"></i> Remove</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>#0023</td>
                                        <td>February 2015</td>
                                        <td>
                                            <h6 class="no-margin">
                                                <a href="#">Jeremy Victorino</a>
                                                <small class="display-block text-muted">Payment method: Payoneer</small>
                                            </h6>
                                        </td>
                                        <td>
                                            <select name="status" class="select" data-placeholder="Select status">
                                                <option value="overdue">Overdue</option>
                                                <option value="hold">On hold</option>
                                                <option value="pending">Pending</option>
                                                <option value="paid" selected="selected">Paid</option>
                                                <option value="invalid">Invalid</option>
                                                <option value="cancel">Canceled</option>
                                            </select>
                                        </td>
                                        <td>
                                            April 17, 2015
                                        </td>
                                        <td>
                                            <span class="label label-primary">27 days</span>
                                        </td>
                                        <td>
                                            <h6 class="no-margin text-bold">$1,500 <small class="display-block text-muted text-size-small">VAT $1,984</small></h6>
                                        </td>
                                        <td class="text-center">
                                            <ul class="icons-list">
                                                <li><a href="#" data-toggle="modal" data-target="#invoice"><i class="icon-file-eye"></i></a></li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-file-text2"></i> <span class="caret"></span></a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="#"><i class="icon-file-download"></i> Download</a></li>
                                                        <li><a href="#"><i class="icon-printer"></i> Print</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#"><i class="icon-file-plus"></i> Edit</a></li>
                                                        <li><a href="#"><i class="icon-cross2"></i> Remove</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>


                                </tbody>
                            </table>


                        </div>
                        <!-- /basic datatable -->

                        <!-- Vertical form modal -->
                        <div id="modalCat" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Supplier Details</h5>
                                    </div>
                                    <table class="table table-bordered table-striped">

                                        <tr>
                                            <th>First name</th>
                                            <td><p id="firstname"></p></td>
                                        </tr>
                                        <tr>
                                            <th>Last name</th>
                                            <td><p id="lastname"></p></td>
                                        </tr>
                                        <tr>
                                            <th>Business name</th>
                                            <td><p id="businessname"></p></td>
                                        </tr>
                                        <tr>
                                            <th>Phone</th>
                                            <td><p id="phone"></p></td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td><div id="email"></div></td>
                                        </tr>
                                        <tr>
                                            <th>Location name</th>
                                            <td><div id="locationname"></div></td>
                                        </tr>
                                        <tr>
                                            <th>Latitude</th>
                                            <td><p id="latitude"></p></td>
                                        </tr>
                                        <tr>
                                            <th>Longitude</th>
                                            <td><p id="longitude"></p></td>
                                        </tr>
                                        <tr>
                                            <th>Availability</th>
                                            <td><p id="availability"></p></td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /vertical form modal -->





                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>

                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
