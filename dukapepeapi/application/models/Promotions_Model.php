<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Promotions_Model
 *
 * @author mwamb
 */
class Promotions_Model extends CI_Model{
    //put your code here
    public $promotions = "promotions";
    public function add($data){
     $response=array();
     $count= $this->checkNew($data);
     if($count==0){
         $this->db->insert($this->promotions,$data);
         $response['message']="Promotion added successfully.";
         $response['code']=1;
     }
     else{
        $response['message']="Error, Another active promotion exist";
        $response['code']=0; 
     }
     echo json_encode($response);
    }
    
    public function checkNew($data){
        $stock_id=$data['stock_id'];
        $this->db->select('promotion_id');
        $this->db->where('stock_id',$stock_id);
        $this->db->where('is_active',1);
        $this->db->from($this->promotions);
        $counter= $this->db->count_all_results();
        return $counter;
    }
    
    public function update($data){
        $response=array();
        $count= $this->checkUpdate($data);
        if($count==0){
            $promotion_id=$data['promotion_id'];
            unset($data['promotion_id']);
            $this->db->where('promotion_id',$promotion_id);
            $this->db->update($this->promotions,$data);
            $response['message']='Promotion updated successfully.';
            $response['code']=1;
        }
        else{
            $response['message']='Failed to add promotion, another promotion exist.';
            $response['code']=0;
        }
        
    }
    public function checkUpdate($data){
     $promotion_id=$data['promotion_id'];
     $stock_id=$data['stock_id'];
     
     $this->db->select('promotion_id');
     $this->db->where('stock_id',$stock_id);
     $this->db->where('is_active',1);
     $this->db->where('promotion_id !=',$promotion_id);
     $this->db->from($this->promotions);
     $counter= $this->db->count_all_results();
     return $counter;
    }
    
    public function supplier_promotions($supplier_id){
        $this->db->select('promotion_id,start_date,end_date,promotions.is_active AS is_active,product_name,product_description,pq_value,unit_initial,unit_name,quantity,unit_cost,'
                . 'stock.stock_id AS stock_id,stock.product_id AS product_id,stock.sku_id AS sku_id');
        $this->db->where('stock.supplier_id',$supplier_id);
        $this->db->from($this->promotions);
        $this->db->join('stock','stock.stock_id='.$this->promotions.'.stock_id');
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('sku','stock.sku_id=sku.sku_id');
        $this->db->join('product_quantities','product_quantities.pq_id=sku.pq_id');
        $this->db->join('units','units.unit_id=sku.unit_id');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        $i++;
        }
        return json_encode($data);
    }
    
    public function fetch_all(){
        $this->db->select('promotion_id,start_date,end_date,promotions.is_active AS is_active,product_name,product_description,pq_value,unit_initial,unit_name,quantity,unit_cost,CONCAT(first_name," ",last_name) AS supplier_name,business_name,'
                . 'stock.stock_id AS stock_id,stock.product_id AS product_id,stock.sku_id AS sku_id');
        $this->db->from($this->promotions);
        $this->db->join('stock','stock.stock_id='.$this->promotions.'.stock_id');
        $this->db->join('suppliers','stock.supplier_id=suppliers.supplier_id');
        $this->db->join('products','products.product_id=stock.product_id');
        $this->db->join('sku','stock.sku_id=sku.sku_id');
        $this->db->join('product_quantities','product_quantities.pq_id=sku.pq_id');
        $this->db->join('units','units.unit_id=sku.unit_id');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $i=0;
        while($i<count($data)){
        $images= $this->Products_Model->getImages($data[$i]['product_id']);
        $data[$i]['image_url']=$images;
        $i++;
        }
        return json_encode($data);
    }
    
    public function new_promotion($data){
        if($this->checkPromo($data)==0){
            $this->db->insert('promotion_message',$data);
            $num = $this->db->affected_rows();
            if($num>0){
                $response['message'] = "Promotion message added successfully.";
                $response['code'] = 1;
            }
            else{
             $response['message'] = "No changes detected.";
             $response['code'] = 0;   
            }
        }
        else{
             $response['message'] = "Similar promotion message already exist.";
             $response['code'] = 0;      
        }
        
        return json_encode($response);
    }
    
    public function checkPromo($data){
        $this->db->select('id');
        $this->db->where('message',$data['message']);
        $this->db->from('promotion_message');
        $count= $this->db->count_all_results();
        
        return $count;
    }
    
    public function update_promotion($data){
        $id= $data['id'];
        unset($data['id']);
        $this->db->where('id',$id);
        $this->db->update('promotion_message',$data);
        $num = $this->db->affected_rows();
            if($num>0){
                $response['message'] = "Promotion message updated successfully.";
                $response['code'] = 1;
            }
            else{
             $response['message'] = "No update detected.";
                $response['code'] = 0;   
            }
            
            
            return json_encode($response);
    }
    
    public function view_promotions($data){
        $this->db->select('id,message,is_active,timestamp');
        if(isset($data['id'])){
            $this->db->where('id',$data['id']);
        }
        if(isset($data['is_active'])){
            $this->db->where('is_active',$data['is_active']);
        }
        $this->db->from('promotion_message');
        $returned_data= $this->db->get();
        $response=$returned_data->result_array();
     
        return json_encode($response);
    }
}
