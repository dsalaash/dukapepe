<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_Type
 *
 * @author mwamb
 */
class User_Type_Model extends CI_Model{
      //put your code here
    public $user_type_table="user_types";
    
    public function add($user_type_name){
     $response=array();
        $count= $this->check_new($user_type_name);
     if($count==0){
        $data=array();
        $data['name']=$user_type_name;
        $this->db->insert($this->user_type_table,$data);  
        $response['message']="User type added successfully.";
        $response['code']=1; 
     }
     else{
      $response['message']="Error, failed to add user type, similar record exists.";
      $response['code']=0;    
         
     }
     
     return json_encode($response);
    }
    public function check_new($user_type_name){
        $this->db->select('id');
        $this->db->where('name',$user_type_name);
        $this->db->from($this->user_type_table);
        $count= $this->db->count_all_results();
        
        return $count;
    }

    public function edit($data){
     $response=array();
     $count= $this->check_edit($data);
     if($count==0){
     $user_type_id=$data['id'];
     unset($data['id']);
     $this->db->where('id',$user_type_id);
     $this->db->update($this->user_type_table,$data);
     
     $response['message']="User type name edited successfully.";
     $response['code']=1; 
     }
     else{
     $response['message']="Error, failed to edit user type name, similar record exists.";
     $response['code']=0;     
     }
      
     return json_encode($response);
    }
    
    public function check_edit($data){
     $user_type_id=$data['id'];
     $user_type_name=$data['name'];
     
     $this->db->select('id');
     $this->db->where('name',$user_type_name);
     $this->db->where('id!=',$user_type_id);
     $this->db->from($this->user_type_table);
     $count= $this->db->count_all_results();
     
     return $count;    
    }
    
    public function delete($user_type_id){
    $response=array();
    $count= $this->associated_records($user_type_id);
    if($count==0){
        $this->db->where('id',$user_type_id);
        $this->db->delete($this->user_type_table);
        $response['message']="User group deleted successfully.";
        $response['code']=1;  
    }
    else{
     $response['message']="Error, failed to delete a user group, similar record exists.";
     $response['code']=0;    
    }
    
    return json_encode($response);
    }
    
    public function associated_records($user_type_id){
        $this->db->select('id');
        $this->db->where('id',$user_type_id);
        $this->db->from('users');
        $count=$this->db->count_all_results();
        return $count; 
    }
    
    public function view($user_type_id){
     $this->db->select('id,name,is_active');
     $this->db->where('id',$user_type_id);
     $this->db->from($this->user_type_table);
     $returned_data=$this->db->get();
     $data=$returned_data->result_array();
     
     return json_encode($data);
    }
    public function view_all(){
      $this->db->select('id,name,is_active');
     $this->db->from($this->user_type_table);
     $this->db->order_by('name ASC');
     $returned_data= $this->db->get();
     $data=$returned_data->result_array();
     
     return json_encode($data);  
    }
}
