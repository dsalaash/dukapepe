<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Products
 *
 * @author mwamb
 */
class Products_Model extends CI_Model {
    //put your code here
    public $products='products';
    
    public function add($data){
        $response=array();
        $counter= $this->checkNew($data);
        if($counter==0){
            $image_data['image_url'] = $data['image_url'];
            unset($data['image_url']);
            $this->db->insert($this->products,$data);
            $product_id= $this->get_productID($data);
            $image_data['product_id'] = $product_id;
            // add image
            $this->save_image($image_data);
            
            $response['message']='Product added successfully.';
            $response['code']=1;
        }
        else{
          $response['message']='Product already exist';
          $response['code']=0;
        }
        return json_encode($response);
    }
    
    public function checkNew($data){
        $product_name=$data['product_name'];
        $this->db->select('product_id');
        $this->db->where('product_name',$product_name);
        $this->db->from($this->products);
        $counter=$this->db->count_all_results();
        return $counter;
    }
    
    public function get_productID($data){
        $this->db->select('product_id');
        $this->db->where('product_name',$data['product_name']);
        $this->db->from($this->products);
        $returnned_data= $this->db->get();
        $response=$returnned_data->result_array();
        return $response[0]['product_id'];
    }
    public function update($data){
        $image_data['image_url'] = $data['image_url'];
        $image_data['product_id'] = $data['product_id'];
        unset($data['image_url']);
        $response=array();
        $counter= $this->checkUpdate($data);
        if($counter==0){
            $product_id=$data['product_id'];
            unset($data['product_id']);
            $this->db->where('product_id',$product_id);
            $this->db->update($this->products,$data);
            
            // save image
            $this->save_image($image_data);
            
            $response['message']='Product information updated successfully.';
            $response['code']=1;
        }
        else{
            $response['message']='Error, Another product with the same name exist.';
            $response['code']=0;
        }
        
        return json_encode($response);
    }
    
    public function checkUpdate($data){
       $product_name=$data['product_name'];
       $sub_cat_id=$data['sub_cat_id'];
       $product_id=$data['product_id'];
       
       $this->db->select('product_id');
       $this->db->where('product_name',$product_name);
       $this->db->where('sub_cat_id',$sub_cat_id);
       $this->db->where('product_id!=',$product_id);
       $this->db->from($this->products);
       $count= $this->db->count_all_results();
       return $count;
    }
    public function getProductDetails($product_id){
        $response=array();
        $this->db->select('product_id,product_name,product_description,sub_cat_id');
        $this->db->where('product_id',$product_id);
        $this->db->from($this->products);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $response['product_id']=$data[0]['product_id'];
        $response['product_name']=$data[0]['product_name'];
        $response['product_description']=$data[0]['product_description'];
        $response['sub_category_id']=$data[0]['sub_cat_id'];
        $images= $this->getImages($product_id);
        $response['images']=$images;
        
        // get image urls
        return json_encode($response);
    }
    
    public function getImages($product_id){
        $base_url= base_url()."images/";
        $this->db->select('CONCAT("'.$base_url.'",image_url) AS image_url,pi_id');
        $this->db->where('product_id',$product_id);
        $this->db->where('is_active',1);
        $this->db->from('product_images');
        $this->db->limit(3);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        
        return $data;
    }
    public function fetch($sub_cat_id){
        $response=array();
        $this->db->select('product_id,product_name,product_description,products.sub_cat_id AS sub_cat_id,sub_cat_name');
        $this->db->where('sub_cat_id',$sub_cat_id);
        $this->db->from($this->products);
        $this->db->join('sub_categories','sub_categories.sub_cat_id=products.sub_cat_id');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $at=0;
        while($at< count($data)){
        $product=array();
        $product['product_id']=$data[$at]['product_id'];
        $product['product_name']=$data[$at]['product_name'];
        $product['product_description']=$data[$at]['product_description'];
        $product['sub_category_id']=$data[$at]['sub_cat_id'];
        $product['sub_category_name']=$data[$at]['sub_cat_name'];
        $images= $this->getImages($data[$at]['product_id']);
        $product['images']=$images;
        
        array_push($response, $product);
        $at++;
        }
        return json_encode($response);
    }
    public function fetch_all(){
        $response=array();
        $this->db->select('product_id,product_name,product_description,products.sub_cat_id AS sub_cat_id,sub_cat_name');
        $this->db->from($this->products);
        $this->db->join('sub_categories','sub_categories.sub_cat_id=products.sub_cat_id');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        $at=0;
        while($at< count($data)){
        $product=array();
        $product['product_id']=$data[$at]['product_id'];
        $product['product_name']=$data[$at]['product_name'];
        $product['product_description']=$data[$at]['product_description'];
        $product['sub_category_id']=$data[$at]['sub_cat_id'];
        $product['sub_category_name']=$data[$at]['sub_cat_name'];
        $images= $this->getImages($data[$at]['product_id']);
        $product['images']=$images;
        
        array_push($response, $product);
        $at++;
        }
        return json_encode($response);
    }
    
    public function save_images($data){
      $product_id=$data['product_id'];
      $images=$data['image_urls'];
      $i=0;
      while($i<count($images)){
          $image_url=$images[$i];
          $image_data=array();
          $image_data['product_id']=$product_id;
          $image_data['image_url']=$image_url;
          $this->save_image($image_data);
      }
      $response=array();
      $response['message']='Images saved successfully';
      $response['code']=1;
      return json_encode($response);
    }
    public function delete_image($image_id){
        $this->db->select('pi_id,image_url');
        $this->db->where('pi_id',$image_id);
        $this->db->from('product_images');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();

        $response = $this->delete_image_data($data);
        return $response;
    }
    
    public function save_image($image_data){
      $this->db->insert('product_images',$image_data);  
    }
    
    public function get_product_images($product_id){
      return json_encode($this->getImages($product_id));
    }
    
    public function delete($product_id){
     if($this->check_associated($product_id)==0){
         $this->db->where('product_id',$product_id);
         $this->db->delete('products');
         $num= $this->db->affected_rows();
         if($num>0){
//             delete in product images
                $this->db->select('pi_id,image_url');
                $this->db->where('product_id',$product_id);
                $this->db->from('product_images');
                $returned_data= $this->db->get();
                $data=$returned_data->result_array();
                
                $this->delete_image_data($data);
         }
}
     else{
      $num=0;   
     }
        $response= $this->Status->response($num);
        
        return json_encode($response);
     }

     public function delete_image_data($data){
        $counter=$i=0;
        while($i<count($data)){
        $this->db->where('pi_id',$data[$i]['pi_id']);
        $this->db->delete('product_images');
        $num= $this->db->affected_rows();
        $response=array();
        if($num>0){
        unlink('./images/'.$data[$i]['image_url']);
        $counter++;
        }
        $i++;
        }
        if($counter==0){
         $response['message']="No image deleted";
        $response['code']=0;       
        }
        else{
        $response['message']=$counter." Image (s) deleted successfully.";
        $response['code']=1;   
        }
        return json_encode($response);    
     }
     public function check_associated($product_id){
     $this->db->select('product_id');
     $this->db->where('product_id',$product_id);
     $this->db->from('stock');
     $num= $this->db->count_all_results();
     return $num;
     }
}
