<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Duka pepe | Promotions</title>

    <?php include("../links.php") ?>

    <!-- /theme JS files -->
    <script>
        $(document).ready(function () {
            $(".status-progress").hide();
            $.extend($.fn.dataTable.defaults, {
                autoWidth: false,
                dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Search promotions:</span> _INPUT_',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: {'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;'}
                },
                drawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                },
                preDrawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                }
            });
            if (localStorage.getItem('code') == 1) {
                loadPromotions();
            } else {

                new PNotify({
                    title: 'Primary notice',
                    text: 'Error: You are not authorised to view this page.',
                    addclass: 'bg-warning'
                });
            }

            promotionAdd();
            promotionsUpdate();


        });


        function loadPromotions() {
            console.log(localStorage.getItem('access_token'));
            var formData = {'access_token': localStorage.getItem('access_token')};
            var url = base_url + "promotions/view_promotions";
            $('#example').DataTable({
                "destroy": true,
                "ajax": {
                    "url": url,
                    "data": formData,
                    "type": "post",
                    "dataSrc": function (json) {
                        return json;
                    },
                    "processing": true,
                    "serverSide": true,
                    "pagingType": "simple",
                    language: {
                        paginate: {'next': 'Next &rarr;', 'previous': '&larr; Prev'}
                    }

                }, "columns": [
                    {"data": "message"},
                    {"data": "timestamp"},
                    {
                        "data": "is_active",
                        orderable: false,
                        searchable: false,
                        render: function (data, type, full, meta) {
                            if (data == 1) {

                                return '<span class="label label-primary">Active</span>';

                            } else {
                                return '<span class="label label-warning">In active</span>';
                            }

                        }
                    },
                    {
                        orderable: false, searchable: false, render: function (data, type, full, meta) {
                        var links = '<ul class="icons-list"><li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>\n\
                                        <ul class="dropdown-menu dropdown-menu-right">\n\
<li><a href="javascript: updatePromotionStatus(' + full['id'] + ')">Edit</a></li>\n\
\n\
</ul></li></ul>';
                        return links;
                    }
                    }
                ]
            });
        }


        function ajaxmodaladd() {
            $(".status-progress-add").hide();
            $('#accesstokenadd').val(localStorage.getItem('access_token'));
            $('#modalpromotionadd').modal('show');
        }

        function promotionAdd() {
            $('#formpromotionsadd').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-add").show();
                $.ajax({
                    url: base_url + "promotions/new_promotion",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress-add").hide();
                        loadPromotions();
                        $('#modalpromotionadd').modal('hide');
                        var data = JSON.parse(data);
                        if (data['code'] == 1) {
                            new PNotify({

                                text: data['message'],
                                addclass: 'bg-success'
                            });
                        } else {
                            new PNotify({

                                text: data['message'],
                                addclass: 'bg-warning'
                            });
                        }
                    },
                    error: function () {
                    }
                });
            });
        }


        function updatePromotionStatus(id) {
            $(".status-progress").show();
            var url = base_url + "promotions/view_promotions";
            var formData = {
                'id': id,
                'access_token': localStorage.getItem('access_token')
            };
            $.post(url, formData, function (json) {
                obj = JSON.parse(json);
                var count = obj.length;

                for (i = 0; i < count; i++) {
                    var obj_data = obj[i];
                    $('#accesstokenupdate').val(localStorage.getItem('access_token'));
                    $('#promotionIdupdate').val(obj_data['id']);
                    $('#messageupdate').val(obj_data['message']);
                    $('#is_activeupdate').val(obj_data['is_active']);
                }
                $(".status-progress").hide();
                $(".status-progress-add").hide();
                $('#modalPromotionUpdate').modal('show');
            });

        }


        function promotionsUpdate() {
            $('#formpromotionsUpdate').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress-add").show();
                $.ajax({
                    url: base_url + "promotions/update_promotion",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress-add").hide();
                        loadPromotions();
                        $('#modalPromotionUpdate').modal('hide');
                        var data = JSON.parse(data);
                        if (data['code'] == 1) {
                            new PNotify({

                                text: data['message'],
                                addclass: 'bg-success'
                            });
                        } else {
                            new PNotify({

                                text: data['message'],
                                addclass: 'bg-warning'
                            });
                        }
                    },
                    error: function () {
                    }
                });
            });
        }


    </script>

</head>

<body>

<?php include("../topbar.php") ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->

                <!-- /user menu -->

                <?php include("../sidebar.php") ?>
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span
                                    class="text-semibold">Promotions</span>
                        </h4>
                    </div>

                </div>

                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="../index.php"><i class="icon-home2 position-left"></i> Home</a></li>

                        <li class="active">Promotions</li>
                    </ul>

                </div>
            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Basic datatable -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">Promotions</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i
                                    class="glyphicon glyphicon-pencil"></i>Add Promotion</a>
                        <img class="text-center center-block status-progress" src="../assets/loader/loader.gif"/>
                    </div>

                    <table class="table" id="example">
                        <thead>
                        <tr>
                            <th>Message</th>
                            <th>Timestamp</th>
                            <th>Status</th>
                            <th>Edit</th>
                        </tr>
                        </thead>

                    </table>
                </div>
                <!-- /basic datatable -->


                <!-- Vertical form modal -->
                <div id="modalpromotionadd" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Add Promotion</h5>
                            </div>
                            <form role="form" class="form-validate" method="POST" id="formpromotionsadd"
                                  enctype="multipart/form-data">

                                <input type="hidden" name="access_token" id="accesstokenadd"/>
                                <div class="modal-body">

                                    <div class="form-group">

                                        <label>Message</label>
                                        <textarea rows="4" name="message" id="message" class="form-control"
                                                  placeholder="Message"></textarea>

                                    </div>
                                    <div class="form-group">

                                        <label>Select Status:</label>
                                        <select required id="isactive" name="is_active"
                                                class="form-control">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>

                                    </div>



                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                    <img class="text-center center-block status-progress-add"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->

                <!-- Vertical form modal -->
                <div id="modalPromotionUpdate" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title">Update Promotion</h5>
                            </div>

                            <form role="form" class="form-validate" method="POST" id="formpromotionsUpdate"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="id" id="promotionIdupdate"/>
                                <input type="hidden" name="access_token" id="accesstokenupdate"/>
                                <div class="modal-body">
                                    <div class="form-group">

                                        <label>Message</label>
                                        <textarea rows="4" name="message" id="messageupdate" class="form-control"
                                                  placeholder="Message"></textarea>

                                    </div>
                                    <div class="form-group">

                                        <label>Select Status:</label>
                                        <select required id="is_activeupdate" name="is_active"
                                                class="form-control">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>

                                    </div>


                                </div>
                                <div class="modal-footer">

                                    <button type="submit" class="btn btn-primary" id="submit" value="add">Save</button>
                                    <img class="text-center center-block status-progress-add"
                                         src="../assets/loader/loader.gif"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /vertical form modal -->

                <!-- Footer -->
                <div class="footer text-muted">
                    <?php include("../footer.php") ?>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
