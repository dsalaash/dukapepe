<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Duka pepe | Register</title>
        <style type="text/css">
            body
            {
                font-family: Arial;
                font-size: 10pt;
            }
            #password_strength
            {
                font-weight: bold;
            }
        </style>

        <?php include("../links.php") ?>
        <!-- /theme JS files -->
        <script>
            $(document).ready(function () {

                $(".status-progress").hide();
                $("select").change(function () {
                    $("select option:selected").each(function () {
                        if ($(this).attr("value") == "4") {
                            $(".box").hide();
                            $(".cc").show();
                        }
                        if ($(this).attr("value") == "3") {
                            $(".box").hide();
                            $(".supplier").show();
                        }
                        if ($(this).attr("value") == "2") {
                            $(".box").hide();
                            $(".director").show();
                        }
                        if ($(this).attr("value") == "choose") {
                            $(".box").hide();
                            $(".choose").show();
                        }
                    });
                }).change();

            });


        </script>


    </head>

    <body>

        <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->

                        <!-- /user menu -->
                        <?php include("../sidebar.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->


                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Page header -->
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Register new supplier</span></h4>
                            </div>

                        </div>

                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">

                        <form role="form" class="form-validate" method="POST" id="formAdd" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="panel">
                                        <div class="panel-body">


                                            <p id="demo"></p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" required name="first_name" class="form-control" placeholder="First name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="text" required name="last_name" class="form-control" placeholder="Last name">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-check text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="email" required name="email" class="form-control" placeholder="Email">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-mention text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="number" required name="phone" class="form-control" placeholder="Phone number">
                                                        <div class="form-control-feedback">
                                                            <i class="glyphicon glyphicon-earphone text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group has-feedback">
                                                        <input type="password" required name="password" class="form-control" placeholder="Create password" id="txtPassword" onkeyup="CheckPasswordStrength(this.value)">
                                                        <span id="password_strength"></span>
                                                        <script type="text/javascript">
                                                            function CheckPasswordStrength(password) {
                                                                var password_strength = document.getElementById("password_strength");

                                                                //TextBox left blank.
                                                                if (password.length == 0) {
                                                                    password_strength.innerHTML = "";
                                                                    return;
                                                                }

                                                                //Regular Expressions.
                                                                var regex = new Array();
                                                                regex.push("[A-Z]"); //Uppercase Alphabet.
                                                                regex.push("[a-z]"); //Lowercase Alphabet.
                                                                regex.push("[0-9]"); //Digit.
                                                                regex.push("[$@$!%*#?&]"); //Special Character.

                                                                var passed = 0;

                                                                //Validate for each Regular Expression.
                                                                for (var i = 0; i < regex.length; i++) {
                                                                    if (new RegExp(regex[i]).test(password)) {
                                                                        passed++;
                                                                    }
                                                                }

                                                                //Validate for length of Password.
                                                                if (passed > 2 && password.length > 8) {
                                                                    passed++;
                                                                }

                                                                //Display status.
                                                                var color = "";
                                                                var strength = "";
                                                                switch (passed) {
                                                                    case 0:
                                                                    case 1:
                                                                        strength = "Weak";
                                                                        color = "red";
                                                                        break;
                                                                    case 2:
                                                                        strength = "Good";
                                                                        color = "darkorange";
                                                                        break;
                                                                    case 3:
                                                                    case 4:
                                                                        strength = "Strong";
                                                                        color = "green";
                                                                        break;
                                                                    case 5:
                                                                        strength = "Very Strong";
                                                                        color = "darkgreen";
                                                                        break;
                                                                }
                                                                password_strength.innerHTML = strength;
                                                                password_strength.style.color = color;
                                                            }
                                                        </script>
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-lock text-muted"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <select required name="user_type"  class="form-control select-search">
                                                        <optgroup label="Select Access type">

                                                            <!--<option value="4">Customer care</option>-->
                                                            <option value="3">Supplier</option>
                                                            <!--                                                            <option value="2">Director</option>-->

                                                        </optgroup>
                                                    </select>
                                                </div>


                                            </div>
                                            <div class="cc box">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="number"  name="id_number" class="form-control" placeholder="Id number">
                                                            <div class="form-control-feedback">
                                                                <i class="icon-user-check text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">

                                                            <select name="gender"  class="select">
                                                                <optgroup label="Select Gender">
                                                                    <option value="female">Female</option>
                                                                    <option value="male">Male</option>

                                                                </optgroup>
                                                            </select>

                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text"  name="dob" class="form-control daterange-single" value="01/01/1990">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-calendar text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>

                                            <div class="supplier box">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text" id="pac-input" required name="location_name" class="form-control" placeholder="Location Name">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input name="business_name" required  type="text" class="form-control" placeholder="Business name">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-home text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text"  id="latitude" required name="latitude" class="form-control" placeholder="Latitude">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group has-feedback">
                                                            <input type="text"  id="longitude" required name="longitude" class="form-control" placeholder="Longitude">
                                                            <div class="form-control-feedback">
                                                                <i class="glyphicon glyphicon-map-marker text-muted"></i>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>


                                            <div class="choose box"></div>
                                            <div class="director box"></div>






                                            <div class="text-right">


                                                <img class="status-progress"  src="../assets/loader/loader.gif"/>
                                                <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b>Add new supplier</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>






                        <!-- Footer -->
                        <div class="footer text-muted">
                            <?php include("../footer.php") ?>

                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyB8WgY93rMVkcDEn3Z64CUcSh3Jx_UMeH4"></script>
        <script>
            google.maps.event.addDomListener(window, 'load', initialize);
            function initialize() {
                var input = document.getElementById('pac-input');
                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.addListener('place_changed', function () {
                    var place = autocomplete.getPlace();
                    // place variable will have all the information you are looking for.
                    $('#latitude').val(place.geometry['location'].lat());
                    $('#longitude').val(place.geometry['location'].lng());
                    console.log(place.geometry['location'].lat());
                    console.log(place.geometry['location'].lng());
                });
            }
        </script>
        <script>

            var x = document.getElementById("demo");

            $('#formAdd').submit(function (e) {
                e.preventDefault();
                $("#submit").addClass("disabled");
                $(".status-progress").show();
                var password_strength2 = document.getElementById("password_strength");
                if(password_strength2.innerHTML == "Weak"){
                    alert("Please make sure your password strength is not weak");
                    $(".status-progress").hide();
                }else{
                    $.ajax({
                    url: base_url + "user/register",
                    type: "POST",
                    data: new FormData(this),
                    //Setting these to false because we are sending a multipart request
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $("#submit").removeClass("disabled");
                        $(".status-progress").hide();

                        var data = JSON.parse(data);

                        new PNotify({
                            title: 'Registration Notice',
                            text: data['message'],
                            addclass: 'bg-success'
                        });
                    },
                    error: function (data) {
                        new PNotify({
                            title: 'Registration Notice',
                            text: data['message'],
                            addclass: 'bg-warning'
                        });
                        $(".status-progress").hide();
                    }
                });
                }
            });
        </script>

    </body>
</html>
