<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dukapepe Limited</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="assets/css/colors.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>

        <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

        <script type="text/javascript" src="assets/js/core/app.js"></script>

        <script type="text/javascript" src="assets/js/plugins/notifications/pnotify.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/components_notifications_pnotify.js"></script>
        <script type="text/javascript" src="assets/js/load_image.js"></script>
        <script type="text/javascript" src="assets/js/configs.js"></script>

        <script type="text/javascript" src="assets/js/plugins/uploaders/fileinput.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/uploader_bootstrap.js"></script>

        <script type="text/javascript" src="assets/js/plugins/notifications/bootbox.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/notifications/sweet_alert.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/components_modals.js"></script>

        <script type="text/javascript" src="assets/js/core/libraries/jquery_ui/interactions.min.js"></script>

        <script type="text/javascript" src="assets/js/pages/form_select2.js"></script>
        <script type="text/javascript" src="assets/js/pages/invoice_archive.js"></script>
<!-- dev changes -->
  <?php
        if(isset($_GET['reset']))
        {
            ?>
            <script type="text/javascript">
            $(".status-progresslog").show();
                var url = base_url + "/user/logout";
                var formData = {
                    'phone': localStorage.getItem('phone'),
                    'access_token': localStorage.getItem('access_token')
                };
                $.post(url, formData, function (json) {
                    var data = JSON.parse(json);
                    new PNotify({
                       
                        text: data['message'],
                        addclass: 'bg-success'
                    });
                    
                    localStorage.setItem('access_token',"")
                    $(".status-progresslog").hide();
                });
            </script>
            <?php
        }

        ?>


<!--<script type="text/javascript" src="assets/js/pages/components_notifications_pnotify.js"></script>-->
        <script>

            $(document).ready(function () {
                $("#status-login").hide();
            });
            $(function () {
                $('form').submit(function () {
                    $("#status-login").show();
                    var returned = false; // stops function from submitting form
                    var url = base_url + "user/login";

                    var s = $("#phone").val();

                    while (s.charAt(0) === '0')
                    {
                        s = s.substr(1);
                    }
                    var country = $("#selectcountry").val();
                    var loginPhone = country + s;


//                    var formData = {
//                        'phone': loginPhone,
//                        'password':$("#password").val()
//                    };

                    
                    var formData = $('form').serializeArray(); // serializing the json data 
                    formData.push({ name: "phone", value: loginPhone });
                   
                    console.log(formData);
                    $.post(url, formData).done(function (response) {
                        var data = JSON.parse(response);
                        lstorage(data); //call localstorage to store login credentials
                        if (data['code'] == 1) {
                            returned = true;
                            if (data['user_type'] == 1) {
                                
                                window.location = "cc/orders.php";
//                                window.location = "cc/category.php";

                            } else if (data['user_type'] == 2) { // if director
                                new PNotify({
                                    text: data['message'],
                                    addclass: 'bg-success'
                                });
                                 window.location = "cc/orders.php";
                            } else if (data['user_type'] == 3) { // if supplier
                                new PNotify({
                                    text: data['message'],
                                    addclass: 'bg-success'
                                });
                                window.location = "supliers/stock.php";
                                //console.log(localStorage);
                            } else if (data['user_type'] == 4) { // if customer care
                                new PNotify({
                                    text: data['message'],
                                    addclass: 'bg-success'
                                });
                                window.location = "dir/dashboard.php";
//                                window.location = "orders.html";
                            }
                             else if (data['user_type'] == 6) { // if customer care
                                new PNotify({
                                    text: data['message'],
                                    addclass: 'bg-success'
                                });
                                window.location = "genman/dashboard.php";
//                                window.location = "orders.html";
                            }
                              else if (data['user_type'] == 8) { // if customer care
                                new PNotify({
                                    text: data['message'],
                                    addclass: 'bg-success'
                                });
                                window.location = "dcclerks/dcretailers.php";
//                                window.location = "orders.html";
                            } else {

                            }
                            $("#status-login").hide();
                        } else {
                            new PNotify({
                                text: data['message'],
                                addclass: 'bg-warning'
                            });
                            $("#status-login").hide();

                        }

                    });
                    return returned;
                });

                $('#pnotify-solid-primary2').on('click', function () {
                    new PNotify({
                        title: 'Primary notice',
                        text: 'Check me out! I\'m a notice.',
                        addclass: 'bg-primary'
                    });
                });

            });

            function lstorage(data) {
                for (var key in data) {
                    // console.log(key);
                    if(key=="retailer_name")
                    {
                      console.log(data['retailer_name']);  
                      
                    }
                    
                    localStorage.setItem(key, data[key]);

                }
//      alert("key is code and value is "+localStorage.getItem('code'));
            }
        </script>

    </head>

    <body class="login-container">

        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html"></a>

                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                </ul>
            </div>


        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content">

                        <!-- Simple login form -->
                        <form id="login-form" method="post">
                            <div class="panel panel-body col-lg-6 col-lg-offset-3">
                                <div class="text-center">
                                    <div class="icon-object border-slate-300 "><img style="width:70px; height: 50px;" src="assets/images/1.jpg"/></div>
                                    <h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
                                </div>
                                <div class="row">
                                    <div class="form-group has-feedback has-feedback-left col-lg-6">
                                        <select required id="selectcountry" name="category_id" class="select">
                                            <optgroup label="Select country code">
                                                <option value="+254">Kenya</option>
                                            </optgroup>

                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input type="number" required class="form-control" id="phone" name="phonehidden" placeholder="07.....">

                                    </div>

                                </div>





                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password" required class="form-control" id="password" name="password" placeholder="Password">
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <img id="status-login" class="text-center center-block "  src="assets/loader/loader.gif"/>
                                    <input type="submit" value="Sign In" class="btn text-center center-block" style="background:#FF3A1D;">
                                </div>
                                <div class="text-center">
                                     <a href="reset.php">Reset Password</a>
                                </div>
                            </div>
                        </form>
                        <!-- /simple login form -->

                        <!-- Footer -->
                        <div class="footer text-muted text-center">
                            &copy; <?php date('Y'); ?>. <a href="#">Mikan Associates</a>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>


</html>
