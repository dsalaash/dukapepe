<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Order
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Order extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->library('mpesa');
    }
    public function linuxtestsms()
    {
      
      echo $this->Order_Model->linuxtestsms();
    }
public function returnitem()
    {
        $data=array();
       $access_response=$this->Management_Model->validate_request($this->input->post('access_token_return'));    
      if($access_response['code']==1)
      {
         $data['stock_id']=$this->input->post('return_product');
         $data['quantity']=$this->input->post('return_quantity');
         $data['order_id']=$this->input->post('return_order');
         $response= $this->Order_Model->returnitem($data);
      }
      else{
       $response= json_encode($access_response);   
      }
        // echo json_encode($data);
        echo $response; 
    }
    public function capturepayment()
    {
      $data=array();
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token_payer'));    
      if($access_response['code']==1)
      {
          // $sub_cat_id= $this->input->post('sub_cat_id');
         $data['paid_by']=$this->input->post('paid_by');
         $data['order_id']=$this->input->post('payment_order');
         $data['amount_paid']=$this->input->post('amount_paid');
         $response= $this->Order_Model->capturepayment($data);
      }
      else{
       // $response= json_encode($access_response);  
       $response=array();
            $response['message']="Error Uploading Image, Product not added";
            $response['code']=0; 
      }
        echo $response; 
    }
    public function get_order_count_by_status(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
  
        $response= $this->Order_Model->get_order_count_by_status();
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response; 
    }
    public function  place_order(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $received_json= $this->input->post('order');
        $data= json_decode($received_json,true);      
        $order_id= $this->input->post('order_id');      
        $response = $this->Order_Model->place_order($data,$order_id);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    
    public function make_payment(){
            $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
            if($access_response['code']==1){
          
            $decoded_res= array();
            $response= json_encode(array());
            
            $cost= $this->input->post('cost');
            $payment_method= $this->input->post('payment_method');
            $order_id= $this->input->post('order_id');
            $received_json= $this->input->post('order');
            
            if($payment_method==1){
            $phone=str_replace("+","", $this->input->post('phone'));
            $response = $this->initiate_payments($phone, $cost, $order_id);
            $decoded_res= json_decode($response,true);
            }
            else{
                $decoded_res['code']=0;
            }
            
           if($this->input->post('order')!=null && (($decoded_res['code']==1 && $payment_method==1) || $payment_method!=1)){
            $data= json_decode($received_json,true);
            $response = $this->Order_Model->place_order($data,$order_id,$payment_method);
            }
            }
            else{
             $response= json_encode($access_response);    
            }
            
           echo $response;
    }
    public function update_order_status(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $data=array();
        $data['order_id']= $this->input->post('order_id');
        $data['status_id']= $this->input->post('status_id');
        
        $response= $this->Order_Model->update_order_status($data);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }

    public function delivery(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $data=array();
        $data['order_id']= $this->input->post('order_id');   
        $data['driver_id']= $this->input->post('driver_id'); 
        $response= $this->Order_Model->delivery($data);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    
    public function myorders(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
      $data=array();
      $retailer= $this->input->post('retailer_id');
      $data['retailer_id'] = $retailer;
      $response = $this->Order_Model->myorders($data);
      }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
       
    
    public function order_status(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $data=array();
        $order_id=$this->input->post('order_id');
        $data['order_id']=$order_id;
        
        $response=$this->Order_Model->order_status($data);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    
    
    
    public function order_details(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){

        $data=array();
        $order_id= $this->input->post('order_id');
        $data['order_id']=$order_id;
        
        $response= $this->Order_Model->order_details($data);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;
    }
    
    public function get_order_by_status(){
      //  $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      // if($access_response['code']==1){

        $status_id= $this->input->post('status_id');
        
        $response= $this->Order_Model->get_order_by_status($status_id);
      //   }
      // else{
      //  $response= json_encode($access_response);   
      // }
        echo $response; 
    }
     public function get_order_ordered(){
       

        
        
        $response= $this->Order_Model->get_order_ordered(1);
      
        echo $response; 
    }
    
       
       public function update_supplier_order(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
           if($access_response['code']==1){
        $so_id= $this->input->post('supplier_order_id');
        $status_id= $this->input->post('status_id');
        
        $data['so_id']=$so_id;
        $data['status_id'] = $status_id;
        
        $response= $this->Order_Model->update_supplier_order_status($data);
       }
      else{
       $response= json_encode($access_response);   
      }
        echo $response; 
       }
       
       public function get_supplier_orders(){
           $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
           if($access_response['code']==1){
                $data=array();
                if($this->input->post('supplier_id')!=null){
                $supplier_id=$this->input->post('supplier_id');
                $data['supplier_id'] = $supplier_id;
                }
                if($this->input->post('status_id')!=null){
                $status_id = $this->input->post('status_id');
                $data['status_id'] = $status_id;
                }
                if($this->input->post('order_id')!=null){
                $status_id = $this->input->post('order_id');
                $data['order_id'] = $status_id;
                }
                if($this->input->post('supplier_order_id')!=null){
                $supplier_order_id = $this->input->post('supplier_order_id');
                $data['supplier_order_id'] = $supplier_order_id;
                }
                
                $response = $this->Order_Model->get_supplier_orders($data);
                }
            else{
                $response= json_encode($access_response);   
            }
        echo $response; 
       }
       
       
       public function get_delivery_options(){
            $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
            if($access_response['code']==1){
                $data['distance'] = $this->input->post('distance');
                $data['weight'] = $this->input->post('weight');
                if($this->input->post('supplier_id')!=null){
                $data['supplier_id'] = $this->input->post('supplier_id');
                }
                else{
                 $data['supplier_id'] =0;   
                }
                
                $response= $this->Order_Model->get_delivery_options($data);
            }
            else{
                $response= json_encode($access_response);   
            }
        echo $response; 
       }
       
       public function cancel_order(){
         $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
            if($access_response['code']==1){
                $data['order_id'] = $this->input->post('order_id');
                $data['retailer_id'] = $this->input->post('retailer_id');
                $response= $this->Order_Model->cancel_order($data);
            }
            else{
                $response= json_encode($access_response);   
            }
        echo $response;     
           
       }
       
       public function check_payment(){
           $data=array();
           if($this->input->post('order_id')){
           $data['order_id'] = $this->input->post('order_id');
            }
           
           if($this->input->post('paid_status')!=null){
           $data['paid_status'] = $this->input->post('paid_status');
           }
           
           $response= $this->Order_Model->check_payment($data);
           
           echo $response;
       }
       
       public function fetch_mpesa_payment(){
           $data=array();
           if($this->input->post('order_id')){
           $data['order_id'] = $this->input->post('order_id');
            }
           
            $response=$this->Order_Model->fetch_mpesa_payment($data);
            echo $response;
       }
       
       
           public function initiate_payments($phone,$amount,$order_id){
           $message="";$code=0;
        $merchant_transaction_id= $this->mpesa->generateRandomString();
        $password=$this->mpesa->getPasword();
        $response_data = $this->mpesa->processCheckOutRequest($password,MERCHANT_ID, $merchant_transaction_id,$order_id,$amount,$phone);
        $response_message = $response_data['message'];
        if($response_message!=""){
            $this->SendSMS->action_send($phone,$response_message);
        }
        else{
            $trx_id=$response_data['trx_id'];
            $counter=0;
            while(1==1){
                $response = $this->check_payment_status($password,$trx_id,$merchant_transaction_id);
                sleep(3);
                $counter++;
                if($response['trx_status']=="Success" OR $response['trx_status']=="Failed" ){
                    //print_r($transaction_details);
                    $response['order_id'] = $order_id;
                    $this->Payment_Model->save_mpesa_transaction($response);

                    if($response['trx_status']=="Success"){
                       $this->Payment_Model->update_order($order_id);
                       $message="Payment for Order Number : ".$order_id." Made successfully.";   
                       $code=1;
                    }
                    else{
                   $message="Error while making payments: Description: ".$response['description'];        
                    }
                $this->SendSMS->action_send($phone,$message);
                break;
                }
                else{
                    
                }
            }
        }
        $answer['message'] = $message;
        $answer['code'] = $code;
        
        return json_encode($answer);
    }
    
     public function check_payment_status($pass,$trx_id_num,$mti){
        $response = $this->mpesa->statusRequest($pass,MERCHANT_ID,$trx_id_num, $mti); 
        return $response;
    }
    public function get_details(){
     $pass = $this->input->post('pass');
     $trx_id_num = $this->input->post('trx_id');
     $mti = $this->input->post('mti'); 
    $response = $this->check_payment_status($pass,$trx_id_num,$mti);
    echo json_encode($response); 
    }
    
    public function approve_payment(){
         $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
         if($access_response['code']==1){
        $data['order_id'] = $this->input->post('order_id');
        $data['paid'] = 1;
        
        $response= $this->Order_Model->approve_payment($data);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
    public function deleter(){
        
        $message = $this->Order_Model->gettodeleteOrders();
        
        echo 'complete: '.$message;
    }
    
    
    public function delete_order(){
         $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
         if($access_response['code']==1){
        $order_id = $this->input->post('order_id');
        
        $response= $this->Order_Model->delete1($order_id);
        }
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    public function update_ordertype()
    {
      $agent_id = $this->input->post('agent_id');
      $initial_date = $this->input->post('initial_date');
      $order_type = $this->input->post('order_type');
        
        $response= $this->Order_Model->update_ordertype($agent_id,$initial_date,$order_type);
        echo $response;
    }
    public function update_agentid()
    {
      $initial_date = $this->input->post('initial_date');
      $response= $this->Order_Model->update_agentid($initial_date);
      echo $response;
    }
}