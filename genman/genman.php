
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include("../links.php") ?>
    <!-- /theme JS files -->

</head>

<body>
   <?php include("../topbar.php") ?>


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main sidebar -->
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">

                        <!-- User menu -->

                        <!-- /user menu -->

                        <?php include("../sidebargen.php") ?>
                    </div>
                </div>
                <!-- /main sidebar -->
                <script type="text/javascript">
                     if(localStorage.getItem('access_token')=="")
                          {
                           window.location = "http://localhost/dukapepe_portal/dukapepe/";
                          }
                      $(document).ready(function ()
                       {
                        //fech initial analytics year
                          
                           var formData = {'access_token': localStorage.getItem('access_token')};
                           var url = "http://192.168.191.2/dukapepeapi/index.php/Analytics/genman_dashboard_index";
                           $.ajax({
                           type:'POST',
                           url: url,
                           data: formData,
                           success:function(data)
                           {
                            var parse = JSON.parse(data);
                            console.log(parse);
                            //
                            var jan = parseFloat(parse['jan_total']);
                            if(parse['jan_total']==null)
                            {
                              jan=0.0;  
                            }
                             var feb = parseFloat(parse['feb_total']);
                            if(parse['feb_total']==null)
                            {
                              feb=0.0;  
                            }
                             var march = parseFloat(parse['march_total']);
                            if(parse['march_total']==null)
                            {
                              march=0.0;  
                            }
                             var april = parseFloat(parse['april_total']);
                            if(parse['april_total']==null)
                            {
                              april=0.0;  
                            }
                             var may = parseFloat(parse['may_total']);
                            if(parse['may_total']==null)
                            {
                              may=0.0;  
                            }
                             var june = parseFloat(parse['june_total']);
                            if(parse['june_total']==null)
                            {
                              june=0.0;  
                            }
                             var july = parseFloat(parse['july_total']);
                            if(parse['july_total']==null)
                            {
                              july=0.0;  
                            }
                             var august = parseFloat(parse['august_total']);
                            if(parse['august_total']==null)
                            {
                              august=0.0;  
                            }
                             var september = parseFloat(parse['september_total']);
                            if(parse['september_total']==null)
                            {
                              september=0.0;  
                            }
                             var octomber = parseFloat(parse['octomber_total']);
                            if(parse['octomber_total']==null)
                            {
                              jan=0.0;  
                            }
                              var november = parseFloat(parse['november_total']);
                            if(parse['november_total']==null)
                            {
                              november=0.0;  
                            }
                              var december = parseFloat(parse['december_total']);
                            if(parse['december_total']==null)
                            {
                              december=0.0;  
                            }
                            //
                            $("#total_orders").append(parse['total_orders_this_year']);
                            $("#total_sales").append(parse['total_sales_this_year']);
                            $("#total_customers").append(parse['total_customers_this_year']);
                               
                              $(function () 
                                {
                                    Highcharts.chart('highchartcontainer', 
                                    {
                                                chart: {
                                                    type: 'bar'
                                                },
                                                title: {
                                                    text: 'Stacked bar chart'
                                                },
                                                xAxis: {
                                                    categories: parse['dcs']
                                                },
                                                yAxis: {
                                                    min: 0,
                                                    title: {
                                                        text: 'Total fruit consumption'
                                                    }
                                                },
                                                legend: {
                                                    reversed: true
                                                },
                                                plotOptions: {
                                                    series: {
                                                        stacking: 'normal',
                                                        cursor: 'pointer',
                                                        point:
                                                        {
                                                            events:
                                                            {
                                                                click: function ()
                                                                {
                                                                    alert('Category: ' + this.category + ',      value: ' + this.y);
                                                                }
                                                            }
                                                        }
                                                    }
                                                },
                                                series: [{
                                                    name: 'Retailers',
                                                    data: parse['retailers_no']
                                                }, {
                                                    name: 'Orders',
                                                    data: parse['orders_no']
                                                },{
                                                    name: 'Sales',
                                                    data: parse['sales_amount']
                                                }]
                                            });

                                    }); 
    
                            },
                           error:function(data)
                           {
                            console.log(data);
                           }        

                       });
                        ///fech intial analytics year

                         //fech initial analytics year
                          
                           var formData = {'access_token': localStorage.getItem('access_token')};
                           var url = "http://192.168.191.2/dukapepeapi/index.php/Analytics/genman_dashboard_index_this_month";
                           $.ajax({
                           type:'POST',
                           url: url,
                           data: formData,
                           success:function(data)
                           {
                            var parse= JSON.parse(data);
                            $("#new_customers_this_month").append(parse['total_retailers_this_month']);
                            $("#orders_this_month").append(parse['total_orders_this_month']);
                            $("#total_sales_this_month").append(parse['total_sales_this_month']);
                            // console.log(parse['sales_amount']); console.log(parse['retailers_no']) console.log(parse['orders_no']);
                            // $(function () 
                            //     {
                            //         Highcharts.chart('thismonthpiechart',
                            //          {
                                                        
                            //                     chart: {
                            //                         type: 'bar'
                            //                     },
                            //                     title: {
                            //                         text: 'Stacked bar chart'
                            //                     },
                            //                     xAxis: {
                            //                         categories: parse['dcs']
                            //                     },
                            //                     yAxis: {
                            //                         min: 0,
                            //                         title: {
                            //                             text: 'Total fruit consumption'
                            //                         }
                            //                     },
                            //                     legend: {
                            //                         reversed: true
                            //                     },
                            //                     plotOptions: {
                            //                         series: {
                            //                             stacking: 'normal',
                            //                             cursor: 'pointer',
                            //                             point:
                            //                             {
                            //                                 events:
                            //                                 {
                            //                                     click: function ()
                            //                                     {
                            //                                         alert('Category: ' + this.category + ',      value: ' + this.y);
                            //                                     }
                            //                                 }
                            //                             }
                            //                         }
                            //                     },
                            //                     series: [{
                            //                         name: 'sales',
                            //                         data: parse['sales_amount']
                            //                     }, {
                            //                         name: 'Retailers',
                            //                         data: parse['retailers_no']
                            //                     }, {
                            //                         name: 'Orders',
                            //                         data: parse['orders_no']
                            //                     }]
                            //                 });
                            //                 });


                            },
                           error:function(data)
                           {
                            console.log(data);
                           }        

                       });
                        ///fech intial analytics year

$( "#datepicker" ).datepicker({
  altFormat: "yy-mm-dd"
});
// Getter
var altFormat = $( ".selector" ).datepicker( "option", "altFormat" );
// Setter
$( "#datepicker" ).datepicker( "option", "altFormat", "yy-mm-dd" );
  });          
 </script>


<!-- Modal  retailer distribution-->
<div class="modal fade" id="dcretailersdistribution" tabindex="-1" role="dialog" aria-labelledby="DC Retailer " aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DC -Retailer Distribution</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div id="containerdcretailersdistribution" style="width:100%; height:400px;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal  retailer distribution-->
<!-- Modal  order distribution-->
<div class="modal fade" id="dcordersdistribution" tabindex="-1" role="dialog" aria-labelledby="DC order Distribution" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DC - Order Distribution</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

         <div id="containerdcordersdistribution" style="width:100%; height:400px;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal  order distribution-->
<!-- Modal  sales distribution-->
<div class="modal fade" id="dcsalesdistribution" tabindex="-1" role="dialog" aria-labelledby="DC order Distribution" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DC -Sales Dstribution</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        
         <div id="containerdcsalesdistribution" style="width:100%; height:400px;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal  sales distribution-->

            <!-- Main content -->
            <div class="content-wrapper">
                <!-- //customerdistribution modal-->

               <!--  ///customerdistribution modal -->

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard</h4>
                        </div>

                        <div class="heading-elements">
                            <div class="heading-btn-group">
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
                                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
                            </div>
                        </div>
                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-gear position-left"></i>
                                    More Analytics
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="montlyanalytics.php"><i class="icon-user-lock"></i>Monthly</a></li>
                                    <li><a href="annual.php"><i class="icon-statistics"></i>Annual</a></li>
                                    <li><a href="topitems.php"><i class="icon-statistics"></i>topitems</a></li>
                                    <!-- <li><a href="#"><i class="icon-statistics"></i>DC Monthly Comparisons</a></li>
                                    <li><a href="#"><i class="icon-statistics"></i>5 best Selling Items Pe DC</a></li> -->
                                    <!-- <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><i class="icon-gear"></i> All settings</a></li> -->
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->
                    <div class="row">
                        <div class="col-lg-6">

                            <!-- Sales stats -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">This Year</h6>
                                    <div class="heading-elements">
                                        <!-- <form class="heading-form" action="#">
                                          <div class="form-group">
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input type='text' class="form-control" id="datepicker" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </form> -->
                                    </div>
                                </div>

                                <div class="container-fluid">
                                    <div class="row text-center">
                                        <div class="col-md-4">
                                            <div class="content-group">
                                                <h5 class="text-semibold no-margin"  id="logged_in_users"><i class="icon-calendar5 position-left text-slate"></i><a href="#"  id="total_customers"  data-toggle="modal" data-target="#dcretailersdistribution"></a></h5>
                                                <span class="text-muted text-size-small">New Retailers</span>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="content-group">
                                                <h5 class="text-semibold no-margin" ><i class="icon-calendar52 position-left text-slate" ></i>
                                                    <a href="#"  id="total_orders" data-toggle="modal" data-target="#dcordersdistribution"></a></h5>
                                                <span class="text-muted text-size-small">Total orders</span>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="content-group">
                                                <h5 class="text-semibold no-margin"><i class="icon-cash3 position-left text-slate"></i><a href="#"  id="total_sales" data-toggle="modal" data-target="#dcsalesdistribution"></a></h5>
                                                <span class="text-muted text-size-small">Total Sales</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="content-group-sm" id="app_sales"></div>
                               <div id="highchartcontainer" style="width:100%; height:400px;">
                                   
                               </div>
                            </div>
                            <!-- /sales stats -->

                        </div>
                        <div class="col-lg-6">

                            <!-- Traffic sources -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">This Month</h6>
                                    <div class="heading-elements">
                                        <form class="heading-form" action="#">
                                            <div class="form-group">
                                                <!-- <label class="checkbox-inline checkbox-switchery checkbox-right switchery-xs">
                                                    <input type="checkbox" class="switch" checked="checked">
                                                    Live update:
                                                </label> -->
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <ul class="list-inline text-center">
                                                <li>
                                                    <a href="#" class="btn border-teal text-teal btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-user"></i></a>
                                                </li>
                                                <li class="text-left">
                                                    <div class="text-semibold">New Customers</div>
                                                    <div class="text-muted" id="new_customers_this_month"></div>
                                                </li>
                                            </ul>

                                            <div class="col-lg-10 col-lg-offset-1">
                                                <div class="content-group" id="new-visitors"></div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <ul class="list-inline text-center">
                                                <li>
                                                    <a href="#" class="btn border-warning-400 text-warning-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-cart"></i></a>
                                                </li>
                                                <li class="text-left">
                                                    <div class="text-semibold">Total Orders</div>
                                                    <div class="text-muted" id="orders_this_month"></div>
                                                </li>
                                            </ul>

                                            <div class="col-lg-10 col-lg-offset-1">
                                                <div class="content-group" id="new-sessions"></div>
                                            </div>
                                        </div>

                                        <div class="col-lg-4">
                                            <ul class="list-inline text-center">
                                                <li>
                                                    <a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-checkmark3 text-size-max"></i></a>
                                                </li>
                                                <li class="text-left">
                                                    <div class="text-semibold">Total Sales</div>
                                                    <div class="text-muted" id="total_sales_this_month"></div>
                                                </li>
                                            </ul>

                                            <div class="col-lg-10 col-lg-offset-1">
                                                <div class="content-group" id="total-online"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="position-relative" id="traffic-sources">
                                    
                                     <div id="thismonthpiechart" style="width:100%; height:400px;"></div>
                                </div>
                            </div>
                            <!-- /traffic sources -->

                        </div>

                        
                    </div>
                    <!-- /main charts -->


                    <!-- Dashboard content -->
                   
                    <!-- /dashboard content -->


                    <!-- Footer -->
               <div class="footer text-muted">
                            <?php include("../footer.php") ?>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->

    </body>
</html>
