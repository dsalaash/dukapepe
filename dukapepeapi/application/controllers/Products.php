<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Products
 *
 * @author mwamb
 */
header('Access-Control-Allow-Origin: *');
class Products extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
         $this->load->library('image');
    }
    public function add(){
      $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $data=array();
        
        $image_url= $this->image->upload_image();
        
        if($image_url!="" && $image_url!=null){
        $sub_cat_id= $this->input->post('sub_cat_id');
        $product_name= $this->input->post('product_name');
        $product_description= $this->input->post('product_description');
        $data['sub_cat_id']=$sub_cat_id;
        $data['product_name']=$product_name;
        $data['product_description']=$product_description;
        $data['image_url']=$image_url;
        $response= $this->Products_Model->add($data);
        }
        else{
         $response=array();
            $response['message']="Error Uploading Image, Product not added";
            $response['code']=0;   
        }
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
    
    public function update(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){

        $data=array();
        $sub_cat_id= $this->input->post('sub_cat_id');
        $product_id= $this->input->post('product_id');
        $product_name= $this->input->post('product_name');
        $product_description= $this->input->post('product_description');
        $data['sub_cat_id']=$sub_cat_id;
        $data['product_id']=$product_id;
        $data['product_name']=$product_name;
        $data['product_description']=$product_description;
        
        $response= $this->Products_Model->update($data);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
    
    public function getProductDetails(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $product_id=$this->input->post('product_id');
        $response= $this->Products_Model->getProductDetails($product_id);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
    
    public function fetch(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $sub_cat_id=$this->input->post('sub_cat_id');
        $response= $this->Products_Model->fetch($sub_cat_id);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
    public function fetch_all(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $response= $this->Products_Model->fetch_all();
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
    public function save_images(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $product_id= $this->input->post('product_id');
        $data=array();
        $images=array();
        $i=1;
        while($i<5){
            //save_image and get url
         $images[$i]=$product_id.'_image_'.$i.'.jpg';   
         $i++;
        }
        $data['product_id']=$product_id;
        $data['image_urls']=$images;
        $response= $this->Products_Model->save_images($data);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
    public  function save_image(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $image_data['product_id']= $this->input->post('product_id');
        $image_data['image_url']= $this->image->upload_image();
        $this->Products_Model->save_image($image_data);
        
        $response=array();
        $response['message']="Image Saved successfully.";
        $response['code']=1;
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
    public function delete_image(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
      if($access_response['code']==1){
        $image_id= $this->input->post('pi_id');
        $response= $this->Products_Model->delete_image($image_id);
        }
      else{
       $response= json_encode($access_response);   
      }
        echo $response;  
    }
   
    public function get_product_images(){
       $product_id= $this->input->post('product_id');
       $response= $this->Products_Model->get_product_images($product_id);
       
       echo $response;
        
    }
    
    public function delete(){
        $access_response=$this->Management_Model->validate_request($this->input->post('access_token'));    
        if($access_response['code']==1){
        $product_id= $this->input->post('product_id');
        $response= $this->Products_Model->delete($product_id);
        }
        
        else{
            $response= json_encode($access_response);   
        }
        echo $response;
    }
    
}
