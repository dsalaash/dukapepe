<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Duka pepe | DC Stock</title>

    <?php include("../links.php") ?>

    <!-- /theme JS files -->
    <script>
      if(localStorage.getItem('access_token')=="")
          {
           window.location = "http://localhost/dukapepe_portal/dukapepe/";
          }
       
        $(document).ready(function () {
            $(".status-progress").hide();
              var formData = {'access_token': localStorage.getItem('access_token')};
                var url = base_url + "user/get_all_system_users";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                        console.log(data);
                           var user_row="";   

                         
                          
                               
                        $.each(JSON.parse(data), function(k, v) {
                         
                       var type="";
                          if(v.user_type==1)
                          {
                            type="retailer";
                          }
                          if(v.user_type==2)
                          {
                            type="Customer care";
                          }
                           if(v.user_type==4)
                          {
                            type="Director";
                          }
                          
                          if(v.user_type==6)
                          {
                            type="Super admin";
                          }
                          if(v.user_type==7)
                          {
                            type="Agent";
                          }
                           if(v.user_type==8)
                          {
                            type="Clerk";
                          }
                          user_row+="<tr><td>"+v.retailer_name+"</td><td>"+v.phone+"</td><td>"+v.email+"</td><td>"+type+"</td><td>"+v.dc+"</td><td>  <div class='dropdown'><button class='btn btn-flat no-border dropdown-toggle dropleft' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><span class='glyphicon glyphicon-list'></span></button><div class='dropdown-menu dropdown-menu-right' aria-labelledby='dropdownMenuButton'><ul class='list-group '><li class='list-group-item'><a class='dropdown-item text-muted' href='javascript: assignrolegenman("+v.retailer_id+")'>Super Admin</a></li><li class='list-group-item'><a class='dropdown-item text-muted' href='javascript: assignrolecc("+v.retailer_id+")'>Customer Care</a></li><li class='list-group-item'><a class='dropdown-item text-muted'  href='javascript: assignroledirector("+v.retailer_id+")'>Director</a></li><li class='list-group-item'><a class='dropdown-item text-muted'  href='javascript: assignroleAgent("+v.retailer_id+")'>Sales Agent</a></li><li class='list-group-item'><a class='dropdown-item text-muted'  href='javascript: assignroledcclerk("+v.retailer_id+")'>DC clerk</a></li><li class='list-group-item'><a class='dropdown-item text-muted'  href='javascript: deactivateaccount("+v.phone+")'>Deactivate Account</a></li></ul></div></div></td></tr>";
                               
                            });  
                            $("#userstablebody").append(user_row);
                            $("#example").DataTable();

                                        
                                },
                       error:function(data){

                                  console.log(data);
                       }        

        });
           ////////////////////
           $("#edit_cat_form").on("submit", function(e) 
           {
             e.preventDefault();
             var resend_activation = $("#reset_code_phone").val();
             var formData = {'access_token': localStorage.getItem('access_token'),'phone':resend_activation};

             var url = "http://192.168.191.2/dukapepeapi/index.php/retailer/resend_activation";

                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         new PNotify({
                                    text: data,
                                    addclass: 'bg-success'
                                });
                                        
                                },
                       error:function(data){

                                  console.log(data);
                       }        

                   });
         
  
           });  
             
            //////////////////    
           
            });
                function deactivateaccount(id)
                {
                    var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':4};
                    console.log(formData);
                    var url = base_url + "user/deactivateaccount";
                    $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                                 var msg=   JSON.parse(data);
                                 new PNotify({
                                    text: msg['message'],
                                    addclass: 'bg-success'
                                });
                                        
                                },
                       error:function(data){

                                 new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                       }        

                   });   
                }
                function assignroledirector(id)
                {
                var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':4};
                console.log(formData);
                var url = base_url + "user/assign_user_type";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var msg=   JSON.parse(data);
                         new PNotify({
                                    text: msg['message'],
                                    addclass: 'bg-success'
                                });
                           window.location.reload();
                                        
                                },
                       error:function(data){

                                 new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                                   window.location.reload();
                       }        

                   });
                }
                function assignroleAgent(id)
                {
                    var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':7};
                     var url = base_url + "user/assign_user_type";
                     $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         new PNotify({
                                    text: data,
                                    addclass: 'bg-success'
                                });
                           window.location.reload();
                                        
                                },
                       error:function(data){
 new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                                   window.location.reload();
                       }        

                        });
                }
                 function assignrolegenman(id)
                {
                  var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':6};
                var url = base_url + "user/assign_user_type";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var msg=   JSON.parse(data);
                         new PNotify({
                                    text: msg['message'],
                                    addclass: 'bg-success'
                                });
                           window.location.reload();
                                        
                                },
                       error:function(data){
                               new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                                 window.location.reload();
                       }        

        });
                }
                 function assignrolecc(id)
                {
                var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':2};
                var url = base_url + "user/assign_user_type";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                      var msg=   JSON.parse(data);
                         new PNotify({
                                    text: msg['message'],
                                    addclass: 'bg-success'
                                });
                         window.location.reload();
                                        
                                },
                       error:function(data){

                                   new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                                   window.location.reload();
                       }        

        });
                }

                 function assignroledcclerk(id)
                {
                   var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':8};
                var url = base_url + "user/assign_user_type";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         var msg=   JSON.parse(data);
                         new PNotify({
                                    text: msg['message'],
                                    addclass: 'bg-success'
                                });
                         window.location.reload();
                                        
                                },
                       error:function(data){
                                                   new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                                   window.location.reload();
                                  console.log(data);
                       }        

        });
                }
                 function assignroledcsales(id)
                {
                    var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':8};
                var url = base_url + "user/assign_user_type";
                $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                          var msg=   JSON.parse(data);
                         new PNotify({
                                    text: msg['message'],
                                    addclass: 'bg-success'
                                });  
                         window.location.reload();
                                },
                       error:function(data){

                                  console.log(data);
                                   new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                                   window.location.reload();
                       }        

        });
                }
                 
                 function assignroledir(id)
                {
                    var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':2};
                     var url = base_url + "user/assign_user_type";
                     $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         new PNotify({
                                    text: data,
                                    addclass: 'bg-success'
                                });
                         window.location.reload();
                                        
                                },
                       error:function(data){
                              new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                                   window.location.reload();
                       }        

                        });
                }
                 function revokerole(id)
                {
                    var formData = {'access_token': localStorage.getItem('access_token'),'user_id':id,'user_type_id':4};
                    var url = base_url + "user/assign_user_type";
                    $.ajax({
                       type:'POST',
                       url: url,
                       data: formData,
                       success:function(data){
                         new PNotify({
                                    text: data,
                                    addclass: 'bg-success'
                                });
                           window.location.reload();
                                        
                                },
                       error:function(data){
 new PNotify({
                                    text: "Record already Exsist",
                                    addclass: 'bg-success'
                                });
                                   window.location.reload();
                       }        

                    });
                }
       

    </script>

</head>

<body>

<?php include("../topbar.php") ?>


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->

                <!-- /user menu -->

                <?php include("../sidebargen.php") ?>
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">All Users In the system</span>
                        </h4>
                    </div>

                </div>


            </div>
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">
                <div class="content clearfix">

                        <!-- Basic datatable -->
                        <div class="panel panel-flat">
                            <!-- <div class="panel-heading">
                                <h5 class="panel-title">All Users In the system</h5>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div> -->

                            <div class="panel-body table-responsive">
                                <!--<a href="javascript: ajaxmodaladd()" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-pencil"></i>Add Orders</a>-->
                                <img class="text-center center-block status-progress"  src="../assets/loader/loader.gif"/>
                            


                          
                      <table class="table datatable-basic" id="example" style="margin-bottom: 120px;">
                        <thead>
                        <tr>

                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>

                      <th>Role</th>
                      <th>DC</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="userstablebody">
                            
                        </tbody>

                    </table>
                    </div>
                        </div>
                    </div>
                </div>
                    

                <!-- Basic datatable -->
                <!-- <div class="panel ">
                   


                    <table class="table datatable-basic" id="example">
                        <thead>
                        <tr>

                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Approved</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="userstablebody">
                            
                        </tbody>

                    </table>
                </div> -->
                <!-- /basic datatable -->

              


               


               
               


               


                <!-- Footer -->
                <div class="footer text-muted">
                    <?php include("../footer.php") ?>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
