<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Retailer_Model
 *
 * @author mwamb
 */
class Retailer_Model extends CI_Model{
    //put your code here
    public $retailers='retailers';
    public $login='login_details';
    public $validation='account_validation';
 
    public function __construct() {
        parent::__construct();
        
        $this->load->library('encryption');
        $this->load->library('encrypt');
    }
    
    public function register($retailer_data){
      $response=array();
      $phone=$retailer_data['phone'];
      $counter= $this->checkNewUser($phone);
      if($counter==0){
        if($this->checkCC($retailer_data['referred_by'])>0){
          $password=$retailer_data['password'];
          $user_type=$retailer_data['user_type'];
          unset($retailer_data['password']);
          unset($retailer_data['user_type']);
          // get location details
         $location_name=$retailer_data['location_name'];
         $latitude=$retailer_data['latitude'];
         $longitude=$retailer_data['longitude'];
         
          unset($retailer_data['location_name']);
          unset($retailer_data['latitude']);
          unset($retailer_data['longitude']);
       // add to retailers
       $this->db->insert($this->retailers,$retailer_data);
       
       $login_array=array();
       $login_array['phone']=$phone;
       $login_array['password']=$password;
       $login_array['user_type']=$user_type;
       
       $this->db->insert($this->login,$login_array);
       
       //array user activation
       $validation_array = array();
       $validation_array['phone']=$phone;
       $raw_code= $this->getCode();
       $encrypted_code = $this->encryption->encrypt($raw_code);
       $validation_array['activation_code']=$encrypted_code;
       $this->db->insert($this->validation,$validation_array);
       
       // notify user through sms and email
       $message="Your Dukapepe activation code is ".$raw_code;
      $this->SendSMS->action_send($phone,$message);
//      $this->SendMail->action_send($phone,$message);
      
       $location_data=array();
       $location_data['retailer_id']=$this->get_id($phone);
       $location_data['location_name']=$location_name;
       $location_data['latitude']=$latitude;
       $location_data['longitude']=$longitude;
       // add that location
       $this->Delivery_Point_Model->add($location_data);
      
      $response['message']="Retailer added successfully. Enter code to activate your account";
      $response['code']=1;
      $response['phone']=$phone;
      }
      else{
//          no such reference
      $response['message']="Wrong reference code entered.";
      $response['code']=0;   
      }
      }
      else{
       $response['message']="Failed to add retailer, Similar records exist";
      $response['code']=0;   
      }
      return json_encode($response);   
    }
     public function clerkregistercustomer($retailer_data,$supplier)
    {
      $response=array();
      $phone=$retailer_data['phone'];
      $counter= $this->checkNewUser($phone);
      if($counter==0){
        if($this->checkCC($retailer_data['referred_by'])>0){
          $password=$retailer_data['password'];
          $user_type=$retailer_data['user_type'];
          unset($retailer_data['password']);
          unset($retailer_data['user_type']);
          // get location details
         $location_name=$retailer_data['location_name'];
         $latitude=$retailer_data['latitude'];
         $longitude=$retailer_data['longitude'];
         
          unset($retailer_data['location_name']);
          unset($retailer_data['latitude']);
          unset($retailer_data['longitude']);
       // add to retailers
       $this->db->insert($this->retailers,$retailer_data);
       
       $login_array=array();
       $login_array['phone']=$phone;
       $login_array['password']=$password;
       $login_array['user_type']=$user_type;
       $login_array['is_active']=1;
       
       $this->db->insert($this->login,$login_array);
       
       //array user activation
       $validation_array = array();
       $validation_array['phone']=$phone;
       $raw_code= $this->getCode();
       $encrypted_code = $this->encryption->encrypt($raw_code);
       $validation_array['activation_code']=$encrypted_code;
       $validation_array['is_active']=1;
       $this->db->insert($this->validation,$validation_array);
       
       // notify user through sms and email
       $message="Thank you for doing shopping with dukapepe. Please we look forward to having you again and again. Karibu tena";
      $this->SendSMS->action_send($phone,$message);
//      $this->SendMail->action_send($phone,$message);
      
       $location_data=array();
       $location_data['retailer_id']=$this->get_id($phone);
       $location_data['location_name']=$location_name;
       $location_data['latitude']=$latitude;
       $location_data['longitude']=$longitude;
       // add that location
       $this->Delivery_Point_Model->add($location_data);
       $linkingdata['retailer_id']=$this->get_id($phone);
       $linkingdata['supplier_id']=$supplier;
       $this->link_retailer($linkingdata);
      
      $response['message']="Retailer added successfully. Enter code to activate your account";
      $response['code']=1;
      $response['phone']=$phone;
      }
      else{
//          no such reference
      $response['message']="Wrong reference code entered.";
      $response['code']=0;   
      }
      }
      else{
       $response['message']="Failed to add retailer, Similar records exist";
      $response['code']=0;   
      }
      return json_encode($response); 
    }
    public function checkNewUser($phone){
     $this->db->select('retailer_id');
        $this->db->or_where('phone',$phone);
        $this->db->from($this->retailers);
        $count=$this->db->count_all_results();
        
        return $count;   
    }
    
     public function activate_new_account($data){
        $response=array();
       $message="";
       $code=0;
       $this->db->select('activation_code');
       $this->db->where('phone',$data['phone']);
       $this->db->where('is_active',1);
       $this->db->from($this->validation);
       $returned_data= $this->db->get();
       $new_data=$returned_data->result_array();
       if(count($new_data)==1){
       $reset_code = $this->encryption->decrypt($new_data[0]['activation_code']);
       if($reset_code==$data['activation_code']){
         $message="Success: Code verified successfully."; 
         $code=1; 
         $this->activate_retailer($data['phone'],1);
         $this->deactivate_code($data['phone'],0);
         
         //alert cc team.
         $retailer_information= $this->getretailerinfo($data['phone']);
         $this->alertCCTeam($retailer_information);
       }
       else{
       $message="Error: Wrong activation code entered."; 
       $code=0;   
       }}
       else{
       $message="Error: Unidentified user."; 
       $code=0;
       }
       $response['message']=$message;
       $response['code']=$code;
      
       return json_encode($response);    
    } 
   public function activate_retailer($phone,$is_active){
     //update retailer table 
       $data=array();
       $data['is_active']=$is_active;
       
     $this->db->where('phone',$phone);
     $this->db->update($this->retailers,$data);
     // update login details
     $this->db->where('phone',$phone);
     $this->db->update($this->login,$data);
   } 
   
    public function deactivate_code($phone,$is_active){
      $data=array();
      $data['is_active']=$is_active;
      $this->db->where('phone',$phone);
      $this->db->update($this->validation,$data);
    }
    
   public function resend_activation($data){
       $phone=$data['phone'];
       $raw_code= $this->getCode();
       $encrypted_code = $this->encryption->encrypt($raw_code);
       $validation_array['activation_code']=$encrypted_code;
       $validation_array['phone']=$phone;
       $validation_array['is_active']=1;
       
      $message="Your Dukapepe activation code is ".$raw_code;
      $this->SendSMS->action_send($phone,$message);
//      $this->SendMail->action_send($phone,$message);
      
       $this->db->replace($this->validation,$validation_array);
       
       //response here
      $response=array();
      $response['message']="Activation code resent successfully.";
      $response['code']=1;
      
      return json_encode($response);
   }
    public function login_retailer($login_data){
     $response=array();
     $phone=$login_data['phone'];
     $password=$login_data['password'];
     
     $this->db->select('retailer_id,retailer_name,'.$this->retailers.'.phone AS phone_no,mpesa_number,email,location_name,latitude,longitude,password');
     $this->db->where($this->retailers.'.phone',$phone);
     $this->db->where($this->login.'.is_active',1);
     $this->db->from($this->retailers);
     $this->db->join($this->login,$this->retailers.'.phone='.$this->login.'.phone');
     $returned_data= $this->db->get();
     $data=$returned_data->result_array();
     if(count($data)==0){
      // no such user or user is de-activated   
       $response['message']="Unknown phone number";
       $response['code']=0;
     }
     else{   
     $dec_pass = $this->encryption->decrypt($data[0]['password']);
     // compare passwords
     if($password==$dec_pass){ // login success
       $response['message']="Login successful.";
       $response['code']=1; 
       $response['retailer_id']=$data[0]['retailer_id'];
       $response['retailer_name']=$data[0]['retailer_name'];
       $response['phone']=$data[0]['phone_no'];
       $response['mpesa_number']=$data[0]['mpesa_number'];
       $response['email']=$data[0]['email'];
       $response['location_name']=$data[0]['location_name'];
       $response['latitude']=$data[0]['latitude'];
       $response['longitude']=$data[0]['longitude'];
     }
     else{ // wrong username and password combination
      $response['message']="Wrong phone and password combination.";
       $response['code']=0;   
     }
     }
    
     return json_encode($response);
    }
   
   public function new_password($data){
       $phone= $data['phone'];
       unset($data['phone']);
       
       $this->db->where('phone',$phone);
       $this->db->update($this->login,$data);
       
       $response=array();
       $response['message']="Password updated successfully.";
       $response['code']=1;
       
       return json_encode($response);
   }
   
    public function getCode(){
    return ((rand(10000,99999))*2);  
    }
    
    public function update_profile($data){
       $base_url= base_url()."images/";
       $retailer_id=$data['retailer_id'];
       unset($data['retailer_id']);
       $this->db->where('retailer_id',$retailer_id);
       $this->db->update($this->retailers,$data);
       $response=array();
       $response['message']="Profile updated successfully.";
       $response['code']=1;
       $response['email']=$data['email'];
       if(isset($data['image_url'])){
       $response['image_url']=$base_url.$data['image_url'];
       }
       return json_encode($response);
        
    }
    
    public function get_id($phone){
        $this->db->select('retailer_id');
        $this->db->where('phone',$phone);
        $this->db->from($this->retailers);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return $data[0]['retailer_id'];
    }
    
    public function fetch_all($where){
        $this->db->select('retailers.retailer_id,retailers.retailer_name,retailers.phone AS phone,retailers.email AS email,retailers.is_active AS is_active,retailers.
is_approved, CONCAT(first_name," ",last_name) AS referred_by, retailers.timestamp AS time_registered');
        if(isset($where['is_approved'])){
        $this->db->where('is_approved',$where['is_approved']);
        }
        if(isset($where['referred_by'])){
        $this->db->where('referred_by',$where['referred_by']);
        }
        if(isset($where['start_date']) && isset($where['end_date'])){
        $this->db->where($this->retailers.'.timestamp >=',$where['start_date'].' 00:00:00');
        $this->db->where($this->retailers.'.timestamp <=',$where['end_date'].' 24:59:00');
        }
        $this->db->from($this->retailers);
        $this->db->join('customer_care', $this->retailers.'.referred_by=customer_care.cc_id','left');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        
        if(!isset($where['is_summary'])){
        $i=0;
        while($i<count($data)){
           $suppliers= $this->linked_suppliers($data[$i]['retailer_id']);
           $data[$i]['suppliers']=$suppliers;
            $i++;
        }
        }
        
        return json_encode($data);
    }
    public function fetch_all_by_dc($data)
    {
      $this->db->select('*');
      $this->db->from('clerk_dc');
      $this->db->where('clerk_id',$data['clerk']);
      $returned_qty= $this->db->get();
      $supp=$returned_qty->result_array();
  


        $this->db->select('retailers.retailer_id,retailers.retailer_name,retailers.phone AS phone,retailers.email AS email,retailers.is_active AS is_active,retailers.is_approved, retailers.timestamp AS time_registered');
        $this->db->from('retailers');
        $this->db->join('retailer_suppliers','retailers.retailer_id=retailer_suppliers.retailer_id');
        $this->db->where('retailer_suppliers.supplier_id',$supp[0]['supplier_id']);
        $returned_data= $this->db->get();
        $dataa['retailers']=$returned_data->result_array();
        $dataa['supplier']=$supp[0]['supplier_id'];
        return $dataa; 
    }
    
    public function linked_suppliers($retailer_id){
     $this->db->select('retailer_suppliers.id AS id, suppliers.supplier_id AS supplier_id,CONCAT(first_name," ",last_name) AS supplier_name,business_name,location_name,supplier_code,phone,email,availability,is_active');
            $this->db->where('retailer_suppliers.retailer_id',$retailer_id);
            $this->db->from('suppliers');
            $this->db->join('retailer_suppliers','retailer_suppliers.supplier_id=suppliers.supplier_id');
            $s_data= $this->db->get();
            $linked_suppliers=$s_data->result_array();
            return $linked_suppliers;
    }
    
    public function link_retailer($data){
        $counter=$this->linkchecker($data);
        if($counter==0){
          $this->db->insert('retailer_suppliers',$data);
          $num= $this->db->affected_rows();
          if($num>0){
              $response['message'] = "Retailer linked successfully to supplier";
              $response['code'] = 1;
          }
          else{
              $response['message'] = "No change detected.";
              $response['code'] = 0;
          }
      }
      else{
          $response['message'] = "Error, Retailer already linked to this supplier";
          $response['code'] = 0;
      }
       
      return json_encode($response);
    }
    
    public function linkchecker($data){
        $this->db->select('id');
        $this->db->where('retailer_id',$data['retailer_id']) ;
        $this->db->where('supplier_id',$data['supplier_id']) ;
        $this->db->from('retailer_suppliers');
        $count=$this->db->count_all_results();
        return $count;
    }
    public function unlink_retailer($data){
        $this->db->where('id',$data['id']);
        $this->db->delete('retailer_suppliers');
        $num = $this->db->affected_rows();
        if($num>0){
            $response['message'] = "Supplier un linked successfully.";
            $response['code'] = 1;
        }
        else{
           $response['message'] = "No action performed";
           $response['code'] = 0; 
        }
        
        return json_encode($response);
    }
    
    public function getretailerinfo($phone){
        $this->db->select('retailer_name,phone');
        $this->db->where('phone',$phone);
        $this->db->from($this->retailers);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return $data[0];
    }
    
    public function alertCCTeam($retailer_information){
        $message="Dear customer care, a new client, ".$retailer_information['retailer_name']." of phone number ".$retailer_information['phone'].", has registered to the system.";
        //get retailers
        $this->db->select('first_name,last_name,phone,email');
        $this->db->where('is_notification_number',1);
        $this->db->from('customer_care');
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        
        $total=count($data);
        $i=0;
        while($i<$total){
        $name=$data[$i]['first_name']." ".$data[$i]['last_name'];    
        $phone=$data[$i]['phone'] ;
//        $email=$data[$i]['email'];
        
          // send notification
//        $sms="Hi ".$name.", ".$message;
        $this->SendSMS->action_send($phone,$message);
//        $this->SendMail->action_send($email,$message);
        $i++;
        }
        $retailersms="Dear ".$retailer_information['retailer_name'].",\n Thank you for registering on Dukapepe. Our customer care agent will approve your account shortly.";
        //send message to the retailer
        $this->SendSMS->action_send($retailer_information['phone'],$retailersms);
    }
    
    public function checkCC($cc_id){
        if($cc_id==""){
            $count=1;       
        }
        else{
            $this->db->select('cc_id');
            $this->db->where('cc_id',$cc_id);
            $this->db->from('customer_care');
            $count=$this->db->count_all_results();
        }
        return $count; 
    }
    
    public function approve($data){
        $retailer_id=$data['retailer_id'];
        unset($data['retailer_id']);
        $this->db->where('retailer_id',$retailer_id);
        $this->db->update($this->retailers,$data);
        $num = $this->db->affected_rows();
        if($num>0){
            $retailer_data= $this->retailerinfoID($retailer_id);
            
            if($data['is_approved']==1){
            $response['message'] = "Retailer approved successfully."; 
            $message="Hi ".$retailer_data['retailer_name'].",\nYour Dukapepe account has been approved. Kindly log in and start shopping.";
            }
            else{
            $response['message'] = "Approval status updated successfully, now retailer needs approval to access account";
            $message="Hi ".$retailer_data['retailer_name'].",\nYour Dukapepe account has been suspended. Kindly contact administrator on.".CONTACT_PHONE." for help";
            }
            $response['code'] = 1;
           
           $this->SendSMS->action_send($retailer_data['phone'],$message);
            
        }
        else{
         $response['message'] = "No change was detected in the system"; 
         $response['code'] = 0;
        }
        
        return json_encode($response);
    }
    
    public function retailerinfoID($retailer_id){
        $this->db->select('retailer_name,phone');
        $this->db->where('retailer_id',$retailer_id);
        $this->db->from($this->retailers);
        $returned_data= $this->db->get();
        $data=$returned_data->result_array();
        return $data[0];
    }
    
    
}
